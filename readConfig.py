import ConfigParser
#from configparser import ConfigParser

config = ConfigParser.ConfigParser()

config.read('.env')

DATABASE_PORT=config.get('database','DATABASE_PORT')
DATABASE_USER=config.get('database','USER')
DATABASE_PASSWORD=config.get('database','PASSWORD')
DATABASE_FOLDER=config.get('database','DATABASE_FOLDER')

WEBMIN_PORT=config.get('webmin','WEBMIN_PORT')
WEBMIN_USER=config.get('webmin','USER')
webmin_PASSWORD=config.get('webmin','PASSWORD')

PHPMYADMIN_PORT=config.get('phpmyadmin','PHPMYADMIN_PORT')
PHPMYADMIN_USER=config.get('phpmyadmin','USER')
PHPMYADMIN_PASSWORD=config.get('phpmyadmin','password')

WEB_PORT=config.get('web','WEB_PORT')
WEB_SRC_FOLDER=config.get('web','WEB_SRC_FOLDER')

#CRAWLER_SRC_FOLDER=config.get('crawler','CRAWLER_SRC_FOLDER')
#IMG_THUMBNAILS_FOLDER=config.get('crawler','IMG_THUMBNAILS_FOLDER')
