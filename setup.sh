############################################################################################
###### Execute with prod parameter for a Production environment: sh setup.sh prod ##########
############################################################################################


#Install docker and docker-compose if not installed
sudo apt-get install docker.io docker-compose -y
#checkout aneris crawler
git clone git@bitbucket.org:momentfactory/aneris_crawler.git src/crawler
#checkout aneris web code
git clone git@bitbucket.org:momentfactory/aneris_ui.git src/laravel
#set the right permission on laravel app
chmod -R 755 src/laravel/app
chown -R www-data src/laravel/app
#create aneris user to connect to Mofa stock
sudo groupadd aneris
sudo useradd -g aneris aneris
#Create folder where Mos=fa stock will be mounted
mkdir -p /mnt/lab /mnt/inspiration /mnt/external_stock /mnt/_inbox /mnt/librairies /mnt/life /mnt/projets /mnt/mofastock

#copy Mofa credentials 
cp infra/crawler/configs/cred-file /etc/samba/cred-file
if sudo cat /etc/fstab |
grep -qFe "//mtl-collections.ad.mofa.studio/lab"
then
  echo fstab contains mtl-collections config, skipping .......
else
  cat infra/crawler/configs/fstab >> /etc/fstab
fi
mount -a  

#execute below code only in dev environment
if [ "$1" != "prod" ]; then
#Install cifs and samba packages 
sudo apt-get install cifs-utils -y
apt-get install samba -y 
#check samba config
if sudo cat /etc/samba/smb.conf |
  grep -qFe "[aneris_ws]"
then
  echo 'samba contains aneris_ws config, skipping .......'
else
  sed -i '/\[global\]/a \ \ \ guest account = root' /etc/samba/smb.conf
  cat samba.conf >> /etc/samba/smb.conf
  sudo /etc/init.d/smbd restart
fi

fi
# end ------

	#copy mysql conf and change permission
	#mkdir -p /opt/aneris
	#mkdir -p /opt/aneris/database/configs
	#cp -r infra/database/mysql /opt/aneris/database/configs
	#chmod -R 644 /opt/aneris/database/configs
	#chown root:root /opt/aneris/database/configs
