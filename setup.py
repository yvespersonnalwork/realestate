import readConfig
import subprocess
from argparse import ArgumentParser

parser = ArgumentParser()

# Add more options if you like
parser.add_argument("-e", "--env", dest="ENV_TYPE",default="dev",
                    help="define the type of environment, specify Dev for Development or Prod for Production", 
choices=['dev', 'prod'], metavar="Environment_Type")

args = parser.parse_args()
ENV_TYPE=args.ENV_TYPE

cmd="sudo apt-get install docker.io docker-compose -y"
subprocess.check_output(['bash','-c', cmd])

#create aneris user to connect to Mofa stock
cmd="sudo groupadd aneris"
subprocess.check_output(['bash','-c', cmd])
cmd="sudo useradd -g aneris aneris"
subprocess.check_output(['bash','-c', cmd])

#Create folder where Mos=fa stock will be mounted
cmd="mkdir -p /mnt/lab /mnt/inspiration /mnt/external_stock /mnt/_inbox /mnt/librairies /mnt/life /mnt/projets /mnt/mofastock /opt/aneris"
subprocess.check_output(['bash','-c', cmd])

#copy Mofa credentials
cmd="cp infra/crawler/configs/cred-file /etc/samba/cred-file"
subprocess.check_output(['bash','-c', cmd])

if "//mtl-collections.ad.mofa.studio/lab" in open("/etc/fstab").read():
    print "fstab contains mtl-collections config, skipping ......."
else:
  cmd="cat infra/crawler/configs/fstab-prod >> /etc/fstab"
  if ENV_TYPE=="dev":
     cmd="cat infra/crawler/configs/fstab-dev >> /etc/fstab"
  subprocess.check_output(['bash','-c', cmd])

subprocess.check_output(['bash','-c', "mount -a"])


if ENV_TYPE=="dev":
  #Install cifs and samba packages
  cmd="sudo apt-get install cifs-utils -y"
  subprocess.check_output(['bash','-c', cmd])
  cmd="apt-get install samba -y"
  subprocess.check_output(['bash','-c', cmd])
  #checkout aneris crawler
  cmd = "git clone git@bitbucket.org:momentfactory/aneris_crawler.git" + " " + readConfig.CRAWLER_SRC_FOLDER
  subprocess.check_output(['bash','-c', cmd])
  #checkout aneris web code
  cmd="git clone git@bitbucket.org:momentfactory/aneris_ui.git" + " " + readConfig.WEB_SRC_FOLDER
  subprocess.check_output(['bash','-c', cmd])

  if "[aneris_ws]" in open("/etc/samba/smb.conf").read():
     print "samba contains aneris_ws config, skipping ......."
  else:
     subprocess.check_output(['bash','-c', "sed -i '/\[global\]/a \ \ \ guest account = root' /etc/samba/smb.conf"])
     subprocess.check_output(['bash','-c', "cat samba.conf >> /etc/samba/smb.conf"])
     subprocess.check_output(['bash','-c', "sudo /etc/init.d/smbd restart"])

#set the right permission on laravel app
cmd="chmod -R 755 " +  readConfig.WEB_SRC_FOLDER
subprocess.check_output(['bash','-c', cmd])
cmd="chown -R www-data " +  readConfig.WEB_SRC_FOLDER
subprocess.check_output(['bash','-c', cmd])









