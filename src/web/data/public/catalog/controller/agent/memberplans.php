<?php
class ControllerAgentMemberplans extends Controller {
	public function index() {
		$this->load->language('agent/agentsignup');
		
		///$this->agent->isLogged();
		
		$agent_id=$this->agent->getId();
		
		$data['agent_id'] = $this->agent->getId();
		 

		$data['text_memberplanlist']   			=$this->language->get('text_memberplanlist');
		$data['text_propertylist']   			=$this->language->get('text_propertylist');
		$data['text_validity']   				=$this->language->get('text_validity');
		$data['text_close']   					=$this->language->get('text_close');
		$data['text_top']   					=$this->language->get('text_top');
		$data['text_feature']   				=$this->language->get('text_feature');
		$data['text_latest']   					=$this->language->get('text_latest');
		$data['text_bottom']   					=$this->language->get('text_bottom');
		$data['text_buynow']   					=$this->language->get('text_buynow');
        $data['login']=$this->url->link('agent/login', '', true);
		$this->load->model('membership/plans');
		$data['memberships']=$this->model_membership_plans->getPlansies($data);
		  $data['membershipsall']=array();
		  $memberships_results =$this->model_membership_plans->getPlansies($data);

		  foreach($memberships_results as $memberships_result){
			if(!empty($memberships_result['price'])){
				$price =$this->currency->format($memberships_result['price'], $this->config->get('config_currency'));
			}else{
				$price='';
			}
			if(!empty($memberships_result['type'])){
				$ttypename =strtoupper($memberships_result['type'][0]);
			}else{
				$ttypename='';
			}


			  $data['membershipsall'][]=array(
			   'price'          => $this->currency->format($memberships_result['price'], $this->config->get('config_currency')),
			   'typename'       => $ttypename,
			   'type'           => $memberships_result['type'],
			   'name'           => $memberships_result['name'],
			   'property_limit' => $memberships_result['property_limit'],
			   'plans_id' 		=> $memberships_result['plans_id'],
			   'top'      		=> $memberships_result['top'],
			   'bottom'      	=> $memberships_result['bottom'],
			   'no_of_day'      	=> $memberships_result['no_of_day'],
			   'feature'      	=> $memberships_result['feature'],
			   'latest'      	=> $memberships_result['latest'],
			   'countinue'     	=> $this->url->link('agent/pp_standard&property_agent_id='.$this->agent->getId())
			 );
		}

		$data['property_agent_id'] = $this->agent->getId();

		$data['column_left'] 					= $this->load->controller('common/column_left');
		$data['column_right'] 					= $this->load->controller('common/column_right');
		$data['content_top']	 				= $this->load->controller('common/content_top');
		$data['content_bottom'] 				= $this->load->controller('common/content_bottom');
		$data['footer'] 						= $this->load->controller('common/footer');
		$data['header'] 						= $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('agent/memberplans', $data));
	}

	public function renewplans() {
		$this->load->model('agent/agent');
		$this->load->language('agent/agentsignup');
		$json = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {	
			$this->model_agent_agent->updaterenewplans($this->request->post);
			$json['success'] = $this->language->get('text_renew');	
			$json['redirec'] =$this->url->link('agent/pp_standard&property_agent_id='.$this->agent->getId());
			
		}	
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}

