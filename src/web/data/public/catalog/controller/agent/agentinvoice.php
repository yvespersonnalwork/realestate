<?php  
class ControlleragentAgentinvoice extends Controller {

 	private $error = array();
 	public function index() {
	 	if (!$this->agent->isLogged()) {
			$this->response->redirect($this->url->link('agent/login', '', true));
		}

		$this->load->language('agent/agentinvoice');
		$this->load->model('agent/agent');
		$this->load->model('tool/image');	

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('agent/viewagent')
		);

		$data['title'] = $this->language->get('text_invoice');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] 			= $this->language->get('heading_title');

		$data['entry_invoice']				= $this->language->get('entry_invoice');
		$data['entry_agent']				= $this->language->get('entry_agent');
		$data['entry_agentname'] 		= $this->language->get('entry_agentname');
	 	$data['entry_plans'] 			= $this->language->get('entry_plans');
		$data['entry_email'] 			= $this->language->get('entry_email');
		$data['entry_positions'] 		= $this->language->get('entry_positions');
		$data['entry_address'] 		= $this->language->get('entry_address');
		$data['entry_propertylimit'] 		= $this->language->get('entry_propertylimit');
		$data['entry_price'] 		= $this->language->get('entry_price');
		$data['entry_pstatus'] 		= $this->language->get('entry_pstatus');
		$data['entry_planstart'] 		= $this->language->get('entry_planstart');
		$data['entry_orderstatus'] 		= $this->language->get('entry_orderstatus');
		$data['entry_planend'] 		= $this->language->get('entry_planend');
		$data['entry_top'] 		= $this->language->get('entry_top');
		$data['entry_bottom'] 		= $this->language->get('entry_bottom');
		$data['entry_feature'] 		= $this->language->get('entry_feature');
		$data['entry_latest'] 		= $this->language->get('entry_latest');
		
		$data['text_descriptions'] 	    = $this->language->get('text_descriptions');
		$data['text_country'] 			= $this->language->get('text_country');
		$data['text_city'] 				= $this->language->get('text_city');
		$data['text_image'] 			= $this->language->get('text_image');
		$data['text_address'] 			= $this->language->get('text_address');
		$data['text_contact'] 			= $this->language->get('text_contact');
		$data['text_country']			= $this->language->get('text_country');
		$data['text_zone'] 				= $this->language->get('text_zone');
		$data['text_print'] 				= $this->language->get('text_print');
		$data['text_postcode']			= $this->language->get('text_postcode');
		$data['entry_qty'] 				= $this->language->get('entry_qty');

		$this->load->model('tool/image');

      //	$agents_info=$this->model_agent_agent->getShowAgent($this->agent->getId());

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }


		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['image'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['image'] = '';
        }
		
		
		if(isset($agents_billInfo['property_agent_id'])){
			$property_agent_id = $agents_billInfo['property_agent_id'];
		} else {
			$property_agent_id = '';
		} 
		
		$agents_billInfo=$this->model_agent_agent->getShowPlans($this->agent->getId());
		$agents_info=$this->model_agent_agent->getagentplaninfo($property_agent_id);

		if(!empty($agents_info['top'])){
			$data['top'] = $agents_info['top'];
		} else {
			$data['top'] = '';
		}

		if(!empty($agents_info['feature'])){
			$data['feature'] = $agents_info['feature'];
		} else {
			$data['feature'] = '';
		}

		if(!empty($agents_info['latest'])){
			$data['latest'] = $agents_info['latest'];
		} else {
			$data['latest'] = '';
		}

		if(!empty($agents_info['bottom'])){
			$data['bottom'] = $agents_info['bottom'];
		} else {
			$data['bottom'] = '';
		}

		if(!empty($agents_info['property_limit'])){
			$data['property_limit'] = $agents_info['property_limit'];
		} else {
			$data['property_limit'] = '';
		}

		if(!empty($agents_info['expiry_date'])){
			$data['expirydate'] = $agents_info['expiry_date'];
		} else {
			$data['expirydate'] = '';
		}

		if(isset($agents_billInfo['order_id'])){
			$data['order_id'] = $agents_billInfo['order_id'];
		} else {
			$data['order_id'] = '';
		}

		if(isset($agents_billInfo['order_status_id'])){
			$order_status_id = $agents_billInfo['order_status_id'];
		} else {
			$order_status_id = '';
		} 
		
		$order_statuses = $this->model_agent_agent->getOrderStatuse($order_status_id);
	
		if(isset($order_statuses['name'])){
			$data['nameorder'] = $order_statuses['name'];
		} else {
			$data['nameorder'] = '';
		}

		$this->load->model('membership/plans');
		$planinfo = $this->model_membership_plans->getPlan($agents_info['plan_id']);
		if(isset($planinfo['name'])){
			$data['planname'] = $planinfo['name'];
		} else {
			$data['planname'] = '';
		}

		if(!empty($planinfo['price'])){
			$data['price'] = $this->currency->format($planinfo['price'],$this->config->get('config_currency'));
		} else {
			$data['price'] = '';
		}
	
		if(!empty($agents_info['date_added'])){
			$data['plandate'] = date('Y-m-d', strtotime($agents_info['date_added']));
		} else {
			$data['plandate'] = '';
		}
		
		if (isset($planinfo['status'])) {
			$data['status'] = $this->language->get('text_enable');
		}else{
			$data['status'] = $this->language->get('text_disable');
		}

		$data['agent_id']      =$this->agent->getId();
		$data['agentname']	   =$agents_info['agentname'];
		$data['email']		   =$agents_info['email'];
		$data['address']	   =$agents_info['address'];

		$this->load->model('localisation/country');
		$this->load->model('localisation/zone');
	 	$this->load->model('membership/plans');

	 	$getZone     				= $this->model_localisation_zone->getZone($agents_info['zone_id']);
		$getCountry  				= $this->model_localisation_country->getCountry($agents_info['country_id']);

		$data['country']	    	=$getCountry['name'];
		$data['zone']	    		=$getZone['name'];
		
		$data['location']			= $getCountry['name'].' '. $getZone['name'].' '. $agents_info['city'].' '.$agents_info['pincode'];
		$this->response->setOutput($this->load->view('agent/agentinvoice',$data));
	}


 }