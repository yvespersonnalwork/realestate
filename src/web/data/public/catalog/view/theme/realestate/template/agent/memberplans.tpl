<?php echo $header; ?>
<div class="realestate_plan">
	<div class="container">
      <div class="row m-auto text-center">
        <div class="tab-first"></div>
        <?php foreach ($membershipsall as $membership) { ?>
        <div class="col-sm-4 princing-item green">
          <div class="pricing-divider">
              <h3 class="text-light"><?php echo $membership['name']?></h3>
            <h4 class="my-0 display-2 text-light font-weight-normal mb-3"><span class="h3"></span> <?php echo $membership['price']; ?> </h4>
          </div>
          <div class="card-body bg-white mt-0 shadow">
            <ul class="list-unstyled mb-5 position-relative">
              <li><strong> <?php echo $text_top?></strong><?php if($membership['top'] == 0) { ?> <i class="fa fa-close" style="font-size:20px;color:red"></i><?php } else { ?> <b><?php echo $membership['top']?></b> <?php } ?></li>
              <li><strong> <?php echo $text_feature?></strong><?php if($membership['feature'] == 0) { ?> <i class="fa fa-close" style="font-size:20px;color:red"></i><?php } else { ?> <b><?php echo $membership['feature']?></b> <?php } ?></li>
              <li><strong> <?php echo $text_bottom?></strong><?php if($membership['bottom'] == 0) { ?> <i class="fa fa-close" style="font-size:20px;color:red"></i><?php } else { ?> <b><?php echo $membership['bottom']?></b> <?php } ?></li>
              <li><b><?php echo $membership['property_limit']?></b><?php echo $text_propertylist?> </li>
              <li><b><?php echo $membership['no_of_day']?></b> <b><?php echo $membership['type']?></b> <?php echo $text_validity?> </li>
            </ul>
              <div class="renewplan<?php echo $membership['plans_id']?>">
              <?php if(!empty($agent_id)){ ?>
			     <a type="button" class="btn btn-lg btn-block  btn-custom addplans" rel="<?php echo $membership['plans_id']?>">
			     <input type="hidden" name="property_agent_id" value="<?php echo $property_agent_id; ?>"/><?php echo $text_buynow; ?>
                 <input type="hidden" name="plans_id" value="<?php echo $membership['plans_id']?>"/></a>
			  <?php } else{  ?>
			     <a href="<?php echo $login; ?>" class="btn btn-lg btn-block  btn-custom">
			       <?php echo $text_buynow; ?></a>
			  <?php } ?>
			
			
                
         
              </div>
          </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
$(document).on('click','.addplans',function() {
var plans_id = $(this).attr('rel');
$.ajax({
url: 'index.php?route=agent/memberplans/renewplans',
type: 'post',
data: $('.renewplan'+plans_id+' input[type=\'hidden\']'),
dataType: 'json',
beforeSend: function(){
},
complete: function(){

},
  success: function(json){
    $('.alert, .text-danger').remove();
    if (json['redirec']) {
      window.location.href = ''+json['redirec']+'';
    }
  },
  error: function(xhr, ajaxOptions, thrownError) {
    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  }
});

});

</script>
