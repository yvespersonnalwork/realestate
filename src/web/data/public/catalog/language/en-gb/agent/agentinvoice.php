<?php

///heading
$_['heading_title']            = 'Agent Detail';

// ///label name
$_['text_edit']                = 'Edit Agent';
$_['text_dashboard']           = 'Dashboard';
$_['text_agent']               = 'Agent List';
$_['text_descriptions']        = 'Description';
$_['text_positions']           = 'Positions';
$_['text_country']             = 'Country';
$_['text_city']                = 'City';
$_['text_image']           	   = 'Image';
$_['text_country']             = 'Country';
$_['text_zone']                = 'Zone';
$_['text_postcode']            = 'Postcode';
$_['text_enable']              = 'Enabled';
$_['text_disable']             = 'Disable';
$_['text_print']              = 'Print';

$_['entry_invoice']            = 'Invoice';
$_['entry_agent']              = 'Agent';
$_['entry_agent_detail']       = 'Agent Details';
$_['entry_contact']            = 'Contact';
$_['entry_agentname']          = 'Agent Name';
$_['entry_plans']              = 'Plans';
$_['entry_email']              = 'Email ID';
$_['entry_address']            = 'Adderss';
$_['entry_propertylimit']      = 'Property Limit';
$_['entry_pstatus']            = 'Plan Status';
$_['entry_planstart']          = 'Plan Start Date';
$_['entry_planend']            = 'Plan End Date';
$_['entry_top']                = 'Top';
$_['entry_feature']            = 'Feature';
$_['entry_latest']            = 'Latest';
$_['entry_bottom']              = 'Bottom';
$_['entry_price']              = 'Price';
$_['entry_orderstatus']        = 'Order Status';

// Entry
$_['entry_bad']                = 'Bad';
$_['entry_bad']                = 'Bad';