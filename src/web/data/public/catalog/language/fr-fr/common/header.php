<?php
// Text
$_['text_home']          = 'Accueil';
$_['text_testimonial']   = 'Témoignage';
$_['text_login']         = "S'identifier";
$_['text_logout']        = 'Se déconnecter';
$_['text_search']        = 'Chercher';
$_['text_logged']         = '<a href="%s">%s</a>';
$_['text_call']          = 'Appelez nous  :';

$_['text_all']           = 'Montre tout';
$_['text_wishlist']      = 'Liste de souhaits (%s)';
$_['text_shopping_cart'] = 'Panier';
$_['text_category']      = 'Les catégories';
$_['text_account']       = 'Mon compte';
$_['text_register']      = "S'inscrire";
$_['text_order']         = "Historique des commandes";
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Téléchargements';
$_['text_checkout']      = 'Check-out';

