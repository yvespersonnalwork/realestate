<?php
// Heading
$_['heading_title']    = 'Entretien';

// Text
$_['text_maintenance'] = 'Entretien';
$_['text_message']     = '<h1 style = "text-align: center;"> Nous effectuons actuellement des travaux de maintenance planifiés. <br/> Nous reviendrons dans les meilleurs délais. S\'il vous plaît revenez bientôt.</h1>';