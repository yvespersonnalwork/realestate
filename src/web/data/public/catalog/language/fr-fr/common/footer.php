<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Service Clients';
$_['text_extra']        = 'Suppléments';
$_['text_contact']      = 'Contactez-nous';
$_['text_return']       = 'Résultats';
$_['text_sitemap']      = 'Plan du site ';
$_['text_manufacturer'] = 'Marques';
$_['text_voucher']      = 'Certificats Cadeaux';
$_['text_affiliate']    = 'Les affiliés';
$_['text_special']      = 'Promotions';
$_['text_account']      = 'Mon compte';
$_['text_order']        = 'Mon compte';
$_['text_wishlist']     = 'Historique des commandes';
$_['text_newsletter']   = 'Bulletin';
$_['button_submit']     = 'Envoyer le message';
$_['error_name']        = 'Le nom du formulaire doit comporter entre 3 et 64 caractères.!';
$_['error_email']       = 'Adresse électronique ne semble pas être valide!';
$_['error_description'] = 'La description doit comporter plus de 50 caractères.!';

//xml
$_['text_links']        = 'Liens';
$_['text_aboutus']      = 'À propos de nous';
$_['text_faq']          = 'FAQ';
$_['text_read']         = 'Lire la suite';
$_['text_tags']         = 'Mots clés';
$_['text_latestnews']   = 'Dernières nouvelles';
$_['text_fgallery']     = 'Galerie';
$_['text_home']     	= 'Accueil';
$_['text_properties']   = 'Propriétés';
$_['text_need']         = "Besoin d'aide pour?";
//xml update
//$_['text_powered']      = 'Propulsé par <a href="http://www.opencart.com"> OpenCart </a> <br />% s & copie; %s';
$_['text_powered']      = 'Propulsé par <a href=""> Realestate </a>% s & copy; %s';
//xml update
