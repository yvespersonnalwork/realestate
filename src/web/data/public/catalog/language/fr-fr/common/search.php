<?php
// Text
$_['text_search']           = 'Chercher';
$_['text_findproperty'] 	= 'TROUVER UNE PROPRIÉTÉ ';
$_['text_bathroom']   		= '--Sélectionnez une salle de bain--';
$_['text_bedroom']     	    = '--Sélectionnez une chambre--';
$_['text_propertyselect']  	= 'Type de propriété ';
$_['text_advancesearch']    = 'Recherche Avancée';
$_['text_bedrooms']   	    = 'Chambres';
$_['text_bathrooms']   	    = 'Salle de bain';
$_['text_nearestplace']     = 'Endroit le plus proche';
$_['text_amenities']   	    = 'Équipements';
$_['text_propertycatagory'] = 'Catégorie de propriété';
$_['text_location'] 		= 'Emplacement';
$_['text_city'] 			= 'Ville';
$_['text_minprice'] 		= 'Prix min';
$_['text_maxprice'] 		= 'Prix max';
$_['button_search']   	 = 'Chercher';
