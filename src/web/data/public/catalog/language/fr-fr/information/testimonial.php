<?php
//Heading
$_['heading_title']    = 'Témoignage';

//Entry
$_['entry_captcha'] = 'Captcha';

// Text
$_['text_name']    = 'votre nom';
$_['text_image']    = 'Votre image';
$_['text_country'] = 'Nom de votre pays ';
$_['text_message2'] = 'Votre message';
$_['text_message'] = 'Votre message a été envoyé';
$_['text_error'] 	= 'Témoignages Page non trouvée!';


//Button
$_['button_send'] = 'Envoyer';

//Error
$_['error_name'] 	= 'Erreur! Champ vide.';
$_['error_country'] = 'Erreur! Champ vide.';
$_['error_enquiry'] = 'Erreur! Champ vide.';
$_['error_captcha'] = 'Erreur! le code ne correspond pas à l'image.';
?>