<?php
// Text
$_['text_subject']	= '%s - Évaluation du produit';
$_['text_waiting']	= 'Vous avez une nouvelle évaluation de produit en attente.';
$_['text_product']	= 'Produit: %s';
$_['text_reviewer']	= 'Critique: %s';
$_['text_rating']	= 'Évaluation: %s';
$_['text_review']	= 'Texte de révision:';