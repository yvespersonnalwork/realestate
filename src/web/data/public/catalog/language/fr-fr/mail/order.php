<?php
// Text
$_['text_new_subject']          = '%s - Ordre %s';
$_['text_new_greeting']         = 'Merci de votre intérêt pour les produits% s. Votre commande a été reçue et sera traitée une fois le paiement confirmé..';
$_['text_new_received']         = 'Vous avez reçu une commande.';
$_['text_new_link']             = 'Pour voir votre commande, cliquez sur le lien ci-dessous:';
$_['text_new_order_detail']     = 'détails de la commande';
$_['text_new_instruction']      = 'Instructions';
$_['text_new_order_id']         = 'numéro de commande:';
$_['text_new_date_added']       = 'date ajoutée:';
$_['text_new_order_status']     = 'Statut de la commande:';
$_['text_new_payment_method']   = 'Mode de paiement:';
$_['text_new_shipping_method']  = 'Mode de livraison:';
$_['text_new_email']            = 'Email:';
$_['text_new_telephone']        = 'Téléphone:';
$_['text_new_ip']               = 'Adresse IP:';
$_['text_new_payment_address']  = 'adresse de facturation';
$_['text_new_shipping_address'] = 'Adresse de livraison';
$_['text_new_products']         = 'Des produits';
$_['text_new_product']          = 'Des produits';
$_['text_new_model']            = 'Modèle';
$_['text_new_quantity']         = 'Quantité';
$_['text_new_price']            = 'Prix';
$_['text_new_order_total']      = 'Total des commandes';
$_['text_new_total']            = 'Totale';
$_['text_new_download']         = 'Une fois votre paiement validé, vous pouvez cliquer sur le lien ci-dessous pour accéder à vos produits téléchargeables.:';
$_['text_new_comment']          = 'Les commentaires pour votre commande sont:';
$_['text_new_footer']           = 'S\'il vous plaît répondre à cet e-mail si vous avez des questions.';
$_['text_update_subject']       = '%s - Mise à jour de la commande %s';
$_['text_update_order']         = 'numéro de commande:';
$_['text_update_date_added']    = 'Date de commande:';
$_['text_update_order_status']  = 'Votre commande a été mise à jour au statut suivant:';
$_['text_update_comment']       = 'Les commentaires pour votre commande sont:';
$_['text_update_link']          = 'Pour voir votre commande, cliquez sur le lien ci-dessous:';
$_['text_update_footer']        = 'S\'il vous plaît répondre à cet email si vous avez des questions.';
