<?php
// Text
$_['text_subject']  = '%s - Demande de réinitialisation du mot de passe';
$_['text_greeting'] = 'A new password was requested for %s customer account.';
$_['text_change']   = 'Pour réinitialiser votre mot de passe, cliquez sur le lien ci-dessous.:';
$_['text_ip']       = 'L\'IP utilisé pour faire cette requête était:% s';
