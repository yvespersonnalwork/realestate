<?php
// Text
$_['text_subject']		        = '%s - Programme d'affiliation';
$_['text_welcome']		        = "Merci d’avoir rejoint le programme d’affiliation% s!';
$_['text_login']                = 'Votre compte a maintenant été créé et vous pouvez vous connecter en utilisant votre adresse e-mail et votre mot de passe en visitant notre site Web ou à l\'adresse suivante:';
$_['text_approval']		        = 'Votre compte doit être approuvé avant de pouvoir vous connecter. Une fois approuvé, vous pouvez vous connecter en utilisant votre adresse e-mail et votre mot de passe en visitant notre site Web ou à l\'adresse suivante:';
$_['text_services']		        = 'Une fois connecté, vous serez en mesure de générer des codes de suivi, de suivre les paiements de commission et de modifier les informations de votre compte.';
$_['text_thanks']		        = 'Merci,';
$_['text_new_affiliate']        = 'Nouvel Affilié';
$_['text_signup']		        = 'Un nouvel affilié s\'est inscrit:';
$_['text_store']		        = 'le magasin:';
$_['text_firstname']	        = 'Prénom:';
$_['text_lastname']		        = 'Nom de famille:';
$_['text_company']		        = 'Compagnie:';
$_['text_email']		        = 'Email:';
$_['text_telephone']	        = 'Téléphone:';
$_['text_website']		        = 'Site Internet:';
$_['text_order_id']             = 'numéro de commande:';
$_['text_transaction_subject']  = '%s - Commission d\'Affiliation';
$_['text_transaction_received'] = 'Vous avez reçu% s commission!';
$_['text_transaction_total']    = 'Votre montant total de commission est maintenant %s.';
