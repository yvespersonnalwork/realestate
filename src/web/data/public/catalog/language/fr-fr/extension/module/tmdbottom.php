<?php
// Heading
$_['heading_title']     = 'TMD Bottom';
// Text
$_['featured_tax']          = 'Taxe ex:';
$_['featured_details']      = 'Détails';
$_['featured_price']         = 'Prix:';
$_['featured_sqft']               = 'En vedette_pi2:';
$_['featured_amenities']         = 'En vedette_équipements:';
$_['featured_NearestPlace']     ='En vedette_Lieu le plus proche:';
$_['featured_rent']              ='En vedette_location:';
$_['featured_apartment'] 	= 'Appartement';
$_['featured_4Daysago'] 	= 'il y a 4 jours';
$_['featured_bedrooms'] 	= 'chambres';
$_['featured_bathrooms'] 	= 'salle de bain';
$_['featured_garge'] 		= 'Équipements';
$_['featured_nearest'] 		= 'La plus proche';

