<?php
// Heading
$_['heading_title']    = 'Filiale';

// Text
$_['text_register']    = 'S'inscrire';
$_['text_login']       = 'S'identifier';
$_['text_logout']      = 'Se déconnecter';
$_['text_forgotten']   = 'mot de passe oublié';
$_['text_account']     = 'Mon compte';
$_['text_edit']        = 'Modifier le compte';
$_['text_password']    = 'Mot de passe';
$_['text_payment']     = 'Options de paiement';
$_['text_tracking']    = 'Suivi des affiliés';
$_['text_transaction'] = 'Transactions';
