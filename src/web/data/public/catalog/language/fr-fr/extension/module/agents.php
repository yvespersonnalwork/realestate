<?php

// Heading

$_['heading_title']    = 'Agente';


// left site Dashboard
$_['text_register']    = "S'inscrire";
$_['text_login']       = "S'identifier";
$_['text_logout']      = 'Se déconnecter';
$_['text_membership']  = 'Adhésion';
$_['text_dashboard']   = 'Tableau de bord';
$_['text_editagent']   = "Agent d'édition";
$_['text_forgot']      = 'Mot de passe oublié';
$_['text_chagepass']   = 'Changer le mot de passe';
$_['text_viewagent']   = 'Agent de vue';
$_['text_addproperty'] = 'Agent de vue...';
$_['text_manage']      = 'Ajouter une propriété...';
$_['text_invoice']     = 'Gérer la propriété';
$_['text_renew']       = 'Plan de renouvellement';
$_['text_wishlist']    = 'Liste de souhaits';



