<?php
// Heading 
$_['heading_title'] 	 = "Lettre d'information";

//Fields
$_['text_newsletter']    = 'Bulletin';
$_['entry_email'] 		 = 'Email';
$_['entry_name'] 		 = 'Nom';

//Buttons
$_['entry_unbutton'] 	 = 'Se désabonner';

//text
$_['text_subscribe'] 	 = 'Abonnez-vous ici';
$_['text_nemail'] 	     = 'Entrer votre Email';

$_['mail_subject']   	 = "Lettre d'information s'abonner";

//Error
$_['error_invalid'] 	 = '<i class=\"fa fa-exclamation-circle\"></i> Avertissement: Email invalide Ajouter un email valide';

$_['subscribe']	    	 = "Votre courriel s'est abonné avec succès";
$_['unsubscribe'] 	     = 'Désabonné avec succès';
$_['alreadyexist'] 	     = 'Email existe déjà';
$_['notexist'] 	    	 = "L'identifiant email n'existe pas";
//xml update
$_['text_signup'] 		 = "S'inscrire";
$_['text_tosee'] 		 = "pour voir ce que vos amis aiment.";
$_['button_signup'] 	 = "S'inscrire";
$_['entry_button'] 		 = "Souscrire";
$_['text_sub']           = "Inscription à la newsletter";
$_['text_connect']       = 'Relier';
$_['text_receive']       = 'Recevez notre newsletter par e-mail ';
//xml
?>
