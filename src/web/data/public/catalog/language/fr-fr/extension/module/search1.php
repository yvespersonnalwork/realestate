<?php

// Heading

$_['heading_title']    		 = 'Trouver une propriété';



///search find property  

$_['findproperty_details']      	 = 'Détails';
$_['findproperty_findproperty']   	 = 'Trouver une propriété';
$_['findproperty_search']            = 'Chercher';
$_['findproperty_bathroom']   		 = '--Sélectionnez une salle de bain--';
$_['findproperty_bedroom']     	     = '--Sélectionnez une chambre--';
$_['findproperty_propertyselect']  	 = 'Type de propriété ';
$_['findproperty_advancesearch']     = 'Recherche Avancée';
$_['findproperty_bedrooms']   	     = 'Chambres';
$_['findproperty_bathrooms']   	     = 'Salle de bain';
$_['findproperty_nearestplace']      = 'Endroit le plus proche';
$_['findproperty_amenities']   	     = 'Équipements';
$_['findproperty_propertycatagory']  = 'Catégorie de propriété';
$_['findproperty_price']        	 = 'Prix:';
$_['findproperty_properties']      	 = 'Propriétés';
$_['findproperty_country']      	 = 'Pays';
$_['findproperty_area']      		 = 'Zone';
$_['findproperty_SqFt']      		 = 'Pieds carrés';
$_['findproperty_select']      		 = '----Sélectionnez les options----';

/// value input serch 

$_['entry_city']            = 'Ville';
$_['entry_address']      	= 'Adresse';
$_['entry_Neighborhood']    = 'quartier';
$_['entry_Zipcode']      	= 'Code postal';
$_['entry_State']     		= 'Etat';
$_['entry_bedrooms']      	= 'Chambres';
$_['entry_bathrooms']      	= 'Salle de bain';
$_['button_search']       	= 'Chercher';



//xml

