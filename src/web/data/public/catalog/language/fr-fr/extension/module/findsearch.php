<?php

// Heading

$_['heading_title']    		    = 'Propriétés';
$_['heading_findproperty']   	= 'Trouver une propriété';

/// findproperty text
$_['findproperty_bathrooms']      	 = 'Salle de bain';
$_['findproperty_details']      	 = 'Détails';
$_['findproperty_findproperty']   	 = 'Trouver une propriété';
$_['findproperty_country']      	 = 'Pays';
$_['findproperty_Price']      		 = 'Prix';
$_['findproperty_area']      		 = 'Zone';
$_['findproperty_SqFt']      		 = 'Pieds carrés';
$_['findproperty_bedrooms']      	 = 'Chambres';
$_['findproperty_bathrooms']      	 = 'Salle de bain';
$_['findproperty_properties']      	 = 'Propriétés';
$_['findproperty_amenities']      	 = 'Équipements';
$_['findproperty_nearestplace']      = 'Lieu le plus proche';
$_['findproperty_advancesearch']     = 'Recherche Avancée';
$_['findproperty_price']        	 = 'Prix:';
$_['findproperty_sqft']          	 = 'trouver une propriété_pi2:';
$_['findproperty_rent']   			 ='trouver une propriété_location:';
$_['findproperty_bed']   			 ='--Sélectionnez une chambre--';
$_['findproperty_bath']   			 ='--Sélectionnez une salle de bain--';

///  find property input text

$_['entry_city']                        = 'Ville';
$_['entry_bedrooms']      	            = 'Chambres';
$_['entry_bathrooms']      	            = 'Salle de bain';
$_['entry_address']      	         	= 'Adresse';
$_['entry_Neighborhood']                = 'quartier';
$_['entry_Zipcode']      	            = 'Code postal';
$_['entry_State']     		            = 'Etat';

/// btn
$_['button_search']      	           = 'Chercher';
