<?php
// Heading
$_['heading_title'] = ' Ma liste de favoris';

// Text
$_['text_account']  			= 'Compte';
$_['text_instock']  		 	= 'En stock';
$_['text_Favourtielist']	 	= 'Liste Favourtie (%s)';
$_['text_login']   			 	= 'Vous devez <a href="%s"> vous connecter </a> ou <a href="%s"> créer un compte </a> pour enregistrer <a href="%s">% s </a>. dans votre liste <a href="%s"> Favourtie</a>!';
$_['text_success']  			= 'Succès: vous avez ajouté <a href="%s">% s </a> à votre liste <a href="%s"> Favourtie.</a>!';
$_['text_remove']   			= 'Succès: vous avez modifié votre liste Favourtie!';
$_['text_empty']    			= 'Votre liste Favourtie est vide.';
$_['column_image']  		  	= 'Image';
$_['column_name']   			= 'Nom';
$_['column_model']  			= 'Modèle';
$_['column_stock']  			= 'Stock';
$_['column_price']  			= 'Prix';
$_['column_view'] 	    		= 'Vue';
$_['column_remove'] 			= 'Retirer';
$_['column_propertyname'] 		= 'Nom de la propriété';