<?php

// Heading

$_['heading_title']     = "Déconnexion de l'agent";

// Text

$_['text_message']      = "<p>Vous avez été déconnecté de votre agent. Vous pouvez maintenant quitter votre ordinateur en toute sécurité. </ P> <p> Votre panier a été enregistré. Les éléments qu'il contient sont restaurés chaque fois que vous vous reconnectez à votre agent..</p>";

$_['text_register']     = "S'inscrire";

$_['text_login']        = "S'identifier";