<?php

// Heading

$_['heading_title']        = 'Agent Modifier';
$_['heading_register']        = 'Modifier';

// ///label name
$_['text_agent']           = "Nom d'agent";
$_['text_image']      	   = 'Image';
$_['text_descriptions']    = 'Descriptions';
$_['text_positions']       = 'Positions';
$_['text_email']           = 'Email';
$_['text_address']         = 'Adresse ';
$_['text_contact']         = 'Contact';
$_['text_city']            = 'Ville';
$_['text_postcode']        = 'Code postal';
$_['text_country']         = 'Pays';
$_['text_zone']            = 'Région / Etat';
$_['text_plans']           = 'Des plans';
$_['text_password']        = 'Mot de passe';
$_['text_confirm']         = 'Confirmer le mot de passe';
$_['text_social']          = ' Lien social';
$_['text_facebook']        = 'Facebook';
$_['text_twitter']         = 'Twitter';
$_['text_validate']        = 'valider';
$_['text_number']          = 'nombre';
$_['text_instagram']       = 'Instagram';
$_['text_googleplus']      = 'Google +';
$_['text_pinterest']       = 'Pinterest';
$_['text_newsletter']      = 'Souscrire';


/// input  varibale
$_['entry_agent']           = "Nom d'agent";
$_['entry_image']      	    = 'Image';
$_['entry_descriptions']    = 'Descriptions';
$_['entry_positions']       = 'Positions';
$_['entry_email']           = 'Email';
$_['entry_address']         = 'Adresse ';
$_['entry_contact']         = 'Contact';
$_['entry_city']            = 'Ville';
$_['entry_postcode']        = 'Code postal';
$_['entry_country']         = 'Pays';
$_['entry_zone']            = 'Région / Etat';
$_['entry_plans']           = 'Des plans';
$_['entry_password']        = 'Mot de passe';
$_['entry_confirm']         = 'Confirmer le mot de passe';
$_['entry_social']          = ' Lien social';
$_['entry_facebook']        = 'Facebook';
$_['entry_twitter']         = 'Twitter';
$_['entry_validate']        = 'valider';
$_['enter_number']          = 'nombre';
$_['entry_instagram']       = 'Instagram';
$_['entry_googleplus']      = 'Google +';
$_['entry_pinterest']       = 'Pinterest';
$_['entry_newsletter']      = 'Souscrire';
$_['entry_firstname']      = 'Prénom';
$_['entry_lastname']       = 'Nom de famille';
$_['entry_telephone']      = 'Téléphone';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Compagnie';
$_['entry_address_2']      = 'Adresse 2';
$_['entry_select']         = '---Sélectionnez une option-----';
$_['entry_selectplans']    = '---Plans choisis-----';


/// btn
$_['button_submit']        = 'Soumettre';
$_['button-upload']        = 'Télécharger';


// Error
$_['error_exists']         = "Avertissement: l'adresse e-mail est déjà enregistrée!";
$_['error_agentname']      = "Le nom de l'agent doit comporter entre 1 et 32 caractères.!";
$_['error_positions']      = 'Les positions doivent comporter entre 1 et 32 caractères!';
$_['error_description']    = 'la description doit comporter entre 4 et 400 caractères!';
$_['error_contact']        = 'Le contact doit comporter entre 10 caractères.!';
$_['error_image']          = 'Besoin de télécharger une image!';
$_['error_lastname']       = 'Le nom de famille doit comporter entre 1 et 32 caractères!';
$_['error_email']          = 'Adresse électronique ne semble pas être valide!';
$_['error_telephone']      = 'Le téléphone doit comporter entre 3 et 32 caractères.!';
$_['error_address']        = "L'adresse doit comporter entre 3 et 128 caractères!";
$_['error_city']           = 'La ville doit comporter entre 2 et 128 caractères.!';
$_['error_postcode']       = 'Le code postal doit comporter entre 10 et 10 caractères.!';
$_['error_country']        = "S'il vous plaît sélectionner un pays!";
$_['error_plans']          = 'Veuillez sélectionner un forfait!';
$_['error_zone']           = 'Veuillez sélectionner une région / un état!';
$_['error_password']       = 'Le mot de passe doit comporter entre 4 et 20 caractères.!';
$_['error_confirm']        = 'La confirmation du mot de passe ne correspond pas au mot de passe!';
$_['error_agree']          = 'Attention: vous devez accepter le% s!';
