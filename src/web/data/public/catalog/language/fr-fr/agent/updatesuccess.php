<?php
// Heading
$_['heading_title'] = 'Votre compte a été mis à jour!';
// Text
$_['text_message']  = "<p>Toutes nos félicitations! Votre nouveau compte a été correctement créé avec UpdateSuccess! </ P> <p> Vous pouvez désormais profiter des privilèges des membres pour améliorer votre expérience de magasinage en ligne avec nous. </ P> <p> Si vous avez des questions sur le fonctionnement de ce site. boutique en ligne, veuillez envoyer un e-mail au propriétaire du magasin. </ p> <p> Une confirmation a été envoyée à l'adresse e-mail fournie. Si vous ne l'avez pas reçu dans l'heure, veuillez <a href='%s'> nous contacter.</a>.</p>";
$_['text_approval'] = '<p>Merci de vous être inscrit à% s! </ P> <p> Vous serez averti par e-mail une fois votre compte activé par le propriétaire du magasin. </ P> <p> Si vous avez des questions sur le fonctionnement de Pour cette boutique en ligne, veuillez <a href="%s"> contacter le propriétaire du magasin.</a>.</p>';
$_['text_account']  = 'Compte';
$_['text_success']  = 'Mettre à jour le succès';
