<?php

///heading
$_['heading_title']            = "Détail de l'agent";

// ///label name
$_['text_edit']                = "Agent d'édition";
$_['text_dashboard']           = 'Tableau de bord';
$_['text_agent']               = "Liste d'agents";
$_['text_descriptions']        = 'La description';
$_['text_positions']           = 'Positions';
$_['text_country']             = 'Pays';
$_['text_city']                = 'Ville';
$_['text_image']           	   = 'Image';
$_['text_country']             = 'Pays';
$_['text_zone']                = 'Zone';
$_['text_postcode']            = 'Code postal';
$_['text_enable']              = 'Activée';
$_['text_disable']             = 'Désactiver';
$_['text_print']              = 'Impression';

$_['entry_invoice']            = "Facture d'achat";
$_['entry_agent']              = 'Agente';
$_['entry_agent_detail']       = "Détails de l'agent";
$_['entry_contact']            = 'Contact';
$_['entry_agentname']          = "Nom d'agent";
$_['entry_plans']              = 'Des plans';
$_['entry_email']              = 'Identifiant Email';
$_['entry_address']            = 'Adresse';
$_['entry_propertylimit']      = 'Limite de propriété';
$_['entry_pstatus']            = 'Statut du plan';
$_['entry_planstart']          = 'Date de début du plan';
$_['entry_planend']            = 'Date de fin du plan';
$_['entry_top']                = 'Haut';
$_['entry_feature']            = 'Fonctionnalité';
$_['entry_latest']            = 'Dernière';
$_['entry_bottom']              = 'Bas';
$_['entry_price']              = 'Prix';
$_['entry_orderstatus']        = 'Statut de la commande';

// Entry
$_['entry_bad']                = 'Mauvais';
$_['entry_bad']                = 'Mauvais';