<?php

// Heading

$_['heading_title']                = 'Connexion de l'agent';

// Text

$_['text_register']                = "S'inscrire";
$_['text_login']                   = "S'identifier";
$_['text_new_agent']               = 'Nouvel agent';
$_['text_agentsignup']             = 'Compte d'agent';
$_['text_register_agent']          = "En créant un agent, vous pourrez faire vos achats plus rapidement, être au courant du statut d’une commande et garder une trace des commandes que vous avez déjà passées..";
$_['text_returning_agent']         = 'Agent de retour';
$_['text_i_am_returning_agent']    = 'Je suis un agent de scrutin';
$_['text_forgotten']               = 'mot de passe oublié';

// Entry
$_['entry_email']                  = 'Adresse e-mail';
$_['entry_password']               = 'Mot de passe';

// Error
$_['error_login']                  = "Avertissement: Aucune correspondance pour l'adresse électronique et / ou le mot de passe.";
$_['error_attempts']               = "Avertissement: Aucune correspondance pour l'adresse électronique et / ou le mot de passe.";
$_['error_approved']               = 'Avertissement: votre compte nécessite une approbation avant de pouvoir vous connecter..';
$_['error_approved_pay']           = "Attention: votre paiement n'est pas encore confirmé.";