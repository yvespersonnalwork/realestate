<?php

// Heading

$_['heading_title'] = 'Votre compte a été créé!';

// Text

$_['text_message']  = "<p>Toutes nos félicitations! Votre nouveau compte a été créé avec succès! </ P> <p> Vous pouvez maintenant profiter des privilèges des membres pour améliorer votre expérience de magasinage en ligne avec nous..</p>";
$_['text_approval'] = "<p>Merci de vous être inscrit à% s! </P> <p> Vous serez averti par e-mail une fois votre compte activé par l'administrateur..</p>";
$_['text_account']  = 'Compte';
$_['text_success']  = 'Succès';

