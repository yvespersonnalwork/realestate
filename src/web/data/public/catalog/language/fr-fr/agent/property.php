<?php

// Heading 

$_['heading_title']                 = 'Propriété';
$_['heading_list']                  = 'Liste de propriété';
$_['heading_add']                   = 'Ajouter une propriété';
$_['heading_agentsignup']           = "Inscription de l'agent";


//label name

$_['text_meta_title']         = 'Titre méta';
$_['text_meta_descriptions']  = 'Meta Description';
$_['text_meta_keyword'] 	  = 'Mots-clés méta';
$_['text_name']               = 'Nom';
$_['text_description']        ='La description';
$_['text_tag']                = 'Tag';
$_['text_SEO_URL']            = 'URL de référencement';
$_['text_sort_order']         = 'Ordre de tri';
$_['text_images']             = 'Image';
$_['text_image']             = 'Image';
$_['text_status']             = 'Statut';
$_['text_video']             = 'Vidéo';
$_['text_property_status']   = 'État de la propriété';
$_['text_Price']             = 'Prix';
$_['text_pricelabel']        = 'Étiquette de prix';
$_['text_category']          = 'Catégorie';
$_['text_country']           ='Pays';
$_['text_Zone_region']       = 'Zone / région';
$_['text_city']              = 'Ville';
$_['text_local_area']        = 'Zone locale';
$_['text_pincode']           = 'Code PIN';
$_['text_map']               = 'Obtenir la carte';
$_['text_latitude']          = 'Latitude';
$_['text_longitude']         = 'Longitude';
$_['text_images']            = 'Image';
$_['text_builtin']           = 'Construit en ';
$_['text_Parkingspaces']     = 'Places de parking';
$_['text_roomcount']         = 'Nombre de chambre';
$_['text_bathrooms']         = 'Salle de bain';
$_['text_area']    		     = 'Zone';
$_['text_neighborhood']      = 'quartier';
$_['text_bedrooms']          = 'chambres';
$_['text_upload']          	 = 'Votre téléchargement de fichier avec succès';
$_['text_enable']          	 = 'Activer';
$_['text_disable']          	= 'Désactiver';
$_['text_pricefrom']          	= 'De';
$_['text_pricerange']          	= 'Échelle des prix';
$_['text_top']          	= 'Haut';
$_['text_feature']          	= 'Fonctionnalité';
$_['text_latest']          	= 'Dernière ';
$_['text_bottom']          	= 'Bas';



////input value name

$_['entry_meta_title']         = 'Titre méta';
$_['entry_meta_descriptions']  = 'Meta Description';
$_['entry_meta_keyword'] 	   = 'Meta Tag Keywords';
$_['entry_name']               = 'Nom';
$_['entry_description']        ='La description';
$_['entry_tag']                = 'Étiquette';
$_['entry_SEO_URL']            = 'URL de référencement';
$_['entry_sort_order']         = 'Ordre de tri';
$_['entry_images']             = 'Image';
$_['entry_image']             = 'Image';
$_['entry_status']             = 'Statut';
$_['entry_video']              = 'Vidéo';
$_['entry_property_status']    = 'État de la propriété';
$_['entry_Price']              = 'Prix';
$_['entry_pricelabel']         = 'Étiquette de prix';
$_['entry_category']           = 'Catégorie';
$_['entry_country']            ='Pays';
$_['entry_Zone_region']        = 'Zone / région';
$_['entry_city']               = 'Ville';
$_['entry_local_area']         = 'Zone locale';
$_['entry_pincode']            = 'Code PIN';
$_['entry_map']                = 'Obtenir la carte';
$_['entry_latitude']           = 'Latitude';
$_['entry_longitude']          = 'Longitude';
$_['entry_builtin']            = 'Construit en ';
$_['entry_Parkingspaces']      = 'Construit en';
$_['entry_roomcount']          = 'Nombre de chambre';
$_['entry_bathrooms']          = 'Salle de bain';
$_['entry_area']    		   = 'Zone';
$_['entry_neighborhood']       = 'quartier';
$_['entry_kanal']              = 'Kanal';
$_['entry_enable']             = 'Activer';
$_['entry_disable']            = 'Désactiver';
$_['entry_arealenght']         = 'Longueur';
$_['entry_bedrooms']           = 'Chambres';
$_['entry_destinies']          = 'Destinies';
$_['entry_enable']             = 'Activer';
$_['entry_disable']            = 'Désactiver';
$_['entry_select']             = '----Sélectionnez les options----';
$_['text_successedit']           = 'Mise à jour avec succès de la propriété';
$_['text_successdelete']           = 'Supprimer avec succès la propriété';

$_['text_yards']           = 'cours';
$_['text_kanal']           = 'kanal';
$_['text_sqfit']           = 'sqfit';

$_['text_expirymsg']           = 'La date du plan que vous choisissez a expiré, vous ne pouvez donc ajouter aucune propriété.';
$_['text_crosslimit']          = 'Vous franchissez la limite pour ajouter une propriété.';



///btn

 $_['button_add']        = 'Ajouter';
$_['button_cancel']      = 'Annuler';
$_['button_save']        = 'sauver';
$_['button_upload']      = 'Télécharger';


///message add and delete  update

$_['message_confirm']    = 'Êtes-vous sûr ?';


// tab name


$_['tab_general']           = 'Général';
$_['tab_data']              = 'Les données';
$_['tab_Address_Map']       = 'Adresse et carte';
$_['tab_image']             = 'Image';
$_['tab_feature']           = 'Fonctionnalité';
$_['tab_neareast_place']    = 'Lieu le plus proche';
$_['tab_customfield']       = 'Champ personnalisé';

//breadcrumbs name
$_['breadcrumbs_property']          = 'Propriété';
$_['breadcrumbs_propertylist']      = 'Formulaire de propriété';

// agent Property listing 
$_['column_description']     = 'Nom';
$_['column_property_status'] = 'État de la propriété';
$_['column_image']           = 'Image';
$_['column_name']            = 'Nom';
$_['column_price']           = 'Prix';
$_['column_status']          = 'Statut';
$_['column_action']          = 'action';
$_['column_position']        = 'Position';


///filter search 
$_['filter_price_range']        = 'De';
$_['filter_price_from']         = 'Échelle des prix';
$_['filter_enable']             = 'Activer';
$_['filter_disable']            = 'Désactiver';
$_['filter_name']                ='Nom' ; 
$_['filter_status']               = 'Statut';    
$_['filter_select']               = '----Sélectionnez les options----';    


// Error
$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_name']             = ' Le nom doit comporter entre 2 et 255 caractères.!';
$_['error_status']         = 'Sélectionnez le statut de la propriété!';

