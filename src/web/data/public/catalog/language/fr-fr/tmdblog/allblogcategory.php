<?php
// Text


$_['heading_title']            = 'Tmd Tous les BlogCatégorie';

// Entry
$_['entry_qty']                = 'Qté';
$_['entry_name']               = 'votre nom';
$_['entry_review']             = 'Votre avis';
$_['entry_rating']             = 'Évaluation';
$_['entry_good']               = 'Bien';
$_['entry_bad']                = 'Mauvais';

// Tabs
$_['tab_description']          = 'La description';
$_['tab_attribute']            = 'spécification';
$_['tab_review']               = 'Commentaires (%s)';
$_['text_blogs']               = 'BlogCatégorie ';
$_['text_views']               = 'Les vues';

//new code start here
$_['text_posted']               = 'Posté par:';
$_['text_readmore']               = 'Lire la suite';
$_['text_tweet']               = 'Tweet';
//new code end here

// Error
$_['error_name']               = 'Avertissement: le nom de la révision doit comporter entre 3 et 25 caractères!';
$_['error_text']               = 'Avertissement: le texte de révision doit comporter entre 25 et 1000 caractères.!';
$_['error_rating']             = 'Attention: Veuillez sélectionner un avis!';
$_['error_captcha']            = 'Avertissement: le code de vérification ne correspond pas à l\'image!';