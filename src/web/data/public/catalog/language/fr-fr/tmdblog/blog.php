<?php
// Text


$_['heading_title']            = 'Dernier message';

// Entry
$_['entry_qty']                = 'Qté';
$_['entry_name']               = 'votre nom';
$_['entry_review']             = 'Votre avis ';
$_['entry_rating']             = 'Évaluation';
$_['entry_good']               = 'Bien';
$_['entry_bad']                = 'Mauvais';

// Tabs
$_['tab_description']          = 'La description';
$_['tab_attribute']            = 'spécification';
$_['tab_review']               = 'Commentaires (%s)';
$_['text_blogs']               = 'Les blogs';
$_['text_views']               = 'Views';
$_['text_error']               = 'Message non trouvé!';

//new code start here
$_['text_coments']             = 'commentaires';
$_['text_tweet']               = 'Tweet';
$_['text_forcomment']          = 'Pour commenter, vous devez';
$_['text_login']               = 'S\'identifier';
$_['text_belogin']             = 'Doit être connecté pour commenter';
$_['text_register']            = 'Sinon compte Cliquez sur <a href="index.php?route=agent/agentsignup"> Enregistrer</a>';
$_['text_forgot']              = 'Mot de passe oublié';
$_['text_password']              = 'Mot de passe';
$_['button_signin']              = 'Se connecter';
$_['text_email']              = 'Email';
$_['text_tages']              = 'Mots clés:';
//new code end here

// Error
$_['error_name']               = 'Avertissement: le nom de la révision doit comporter entre 3 et 25 caractères!';
$_['error_text']               = 'Attention: le texte de révision doit comporter entre 25 et 1000 caractères!';
$_['error_rating']             = 'Attention: Veuillez sélectionner un avis!';
$_['error_captcha']            = 'Avertissement: le code de vérification ne correspond pas à l\'image!';
