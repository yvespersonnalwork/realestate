<?php
// Text
$_['text_upload']    = 'Votre fichier a été chargé avec succès!';
// Error
$_['error_filename'] = 'Le nom de fichier doit comporter entre 3 et 64 caractères.!';
$_['error_filetype'] = 'type de fichier invalide!';
$_['error_upload']   = 'Téléchargement requis!';
