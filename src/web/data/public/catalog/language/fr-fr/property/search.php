<?php
// Heading
$_['heading_title']     = 'Chercher';
$_['heading_tag']		= 'Étiquette - ';
// Text
$_['text_search']       = 'Produits répondant aux critères de recherche';
$_['text_keyword']      = 'Mots clés';
$_['text_category']     = 'toutes catégories';
$_['text_sub_category'] = 'Rechercher dans les sous-catégories';
$_['text_empty']        = 'Il ya aucun produit correspondant aux critères de recherche.';
$_['text_quantity']     = 'Qté:';
$_['text_manufacturer'] = 'Marque:';
$_['text_model']        = 'Code produit:';
$_['text_points']       = 'Points de récompense:';
$_['text_price']        = 'Prix:';
$_['text_tax']          = 'Taxe ex:';
$_['text_reviews']      = 'Basé sur les commentaires de% s.';
$_['text_compare']      = 'Comparaison de produit (%s)';
$_['text_sort']         = 'Trier par:';
$_['text_default']      = 'Défaut';
$_['text_name_asc']     = 'Nom (A - Z)';
$_['text_name_desc']    = 'Nom (Z - A)';
$_['text_price_asc']    = 'Prix (Faible &gt; Haute)';
$_['text_price_desc']   = 'Prix (Haute &gt; Faible)';
$_['text_rating_asc']   = 'Évaluation (Le plus bas)';
$_['text_rating_desc']  = 'Évaluation (Plus haut)';
$_['text_model_asc']    = 'Modèle (A - Z)';
$_['text_model_desc']   = 'Modèle (Z - A)';
$_['text_limit']        = 'Spectacle:';

// Entry
$_['entry_search']      = 'Critères de recherche';
$_['entry_description'] = 'Rechercher dans les descriptions de produits';