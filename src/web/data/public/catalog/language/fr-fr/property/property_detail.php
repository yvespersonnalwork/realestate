<?php

// heading

$_['heading_title']                          = 'Détail de la propriété';


/// right side contact text

$_['propertydetails_contact']               = 'Agent de contact';
$_['propertydetails_contactnow']            = 'Contacter maintenant';
$_['propertydetails_property']              = 'Propriété';

//  popup text 
$_['popupmessage_sendmsg']                  = "Envoyer un message à l'agent";
$_['entry_name']                            = 'Nom';
$_['entry_email']                           = 'Email';
$_['entry_msg']                             = 'Message';

///successAgent
$_['agent_success']                        = 'Succès: envoyez votre agent de messagerie';
/// btn 
$_['button_submit']                        = 'Soumettre';
$_['button_upload']                        = 'Télécharger';
/// 
$_['propertydetails_price']                = 'Prix:';
$_['propertydetails_sqft']                 = 'Zone';
$_['propertydetails_amenities']            = 'Équipements';
$_['propertydetails_nearestplace']         = 'Lieu le plus proche';
$_['propertydetails_description']          = 'Description de la propriété';
$_['propertydetails_contactagent']         = 'Agent de contact';
$_['propertydetails_customfield']         = 'Description Personnalisé';
$_['propertydetails_video']                = 'Vidéo de propriété';
$_['entry_maplocation']                = 'Localisation de la carte';
$_['entry_address']                = 'Adresse';
$_['entry_zip']                = 'Zip *: français';
$_['entry_city']                = 'Ville';
$_['entry_neighborhood']                = 'Neighborhood';
$_['entry_state_county']                = 'Etat / Pays';
$_['entry_relatedproperty']                = 'Propriété associée';
$_['propertydetails_address']                = 'Adresse de la propriété';
$_['propertydetails_details']                = 'Détail';

//xml 2/10/2018
$_['text_type']                   = 'Type:';
$_['text_bedroom']                = 'Chambre';
$_['text_bathroom']               = 'Salle de bains';
$_['text_kitchen']                = 'Cuisine';
$_['text_garage']                 = 'Garage';
$_['text_hall']                   = 'Salle à manger';
$_['text_feet']                   = 'Pieds carrés';
$_['text_parking']            = 'Place de parking:';
$_['text_built']            = 'Construit en:';
$_['text_propertystatus']            = 'Property Status:';
$_['text_areatype']            = "Aire d'atterrissage";
