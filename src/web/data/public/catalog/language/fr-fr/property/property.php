<?php

// Heading 
$_['heading_title']          = 'Propriété ';
$_['heading_list']           = 'Liste de propriété ';

// Text
$_['text_success']           = 'Succès: vous avez modifié l\'information!';
$_['text_list']              = 'Liste de propriété';
$_['text_add']               = 'Ajouter une propriété';
$_['text_edit']              = 'Editer la propriété';
$_['text_form']              = 'Ajouter une propriété';
$_['text_enable']            = 'Activer';
$_['text_disable']           = 'Désactiver';
$_['text_default']           = 'Défaut';
$_['outdoor_pool']           = 'Piscine extérieure';
$_['text_propertylist']      = 'Formulaire de propriété';
$_['text_property']          = 'Propriété';
$_['text_confirm']           = 'Êtes-vous sûr ?';
$_['button_add']             = 'Ajouter';
$_['tab_customfield']        = 'Douane';
$_['text_map']               = 'Obtenir la carte';

$_['text_top']               = 'Succès: Cette propriété est ajouter en haut !';
$_['text_untop']             = 'Succès: cette propriété est supprimée en haut!';
$_['text_feature']           = 'Succès: cette propriété est ajoutée dans la fonctionnalité!';
$_['text_unfeature']         = 'Succès: cette propriété est supprimée dans la fonctionnalité!';
$_['text_latest']            = 'Succès: Cette propriété est ajoutée à la dernière!';
$_['text_unlatest']          = 'Succès: Cette propriété est supprimée en dernier!';
$_['text_bottom']            = 'Succès: Cette propriété est ajouter en bas!';
$_['text_unbottom']          = 'Succès: cette propriété est supprimer en bas!';

// Column
$_['column_description']     = 'Nom';
$_['column_property_status'] = 'État de la propriété';
$_['column_image']           = 'Image';
$_['column_name']            = 'Nom';
$_['column_price']           = 'Prix';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_price_range']     = 'Échelle des prix';
$_['column_price_from']      = 'De';
$_['column_agent']           = 'Agente';
$_['column_category']        = 'Category';
$_['column_approved']        = 'Approuvée';
$_['column_status']          = 'Statut';
$_['column_action']          = 'action';
$_['button_cancel']          = 'Annuler';
$_['button_save']            = 'sauver';
$_['text_select']            = '----Sélectionnez les options----';

// Entry
$_['entry_title']           = 'Titre';
$_['entry_approved']        = 'Approuvée';
$_['entry_video']           = 'Vidéo';
$_['entry_neighborhood']    = 'quartier';
$_['entry_area']    = 'Zone';
$_['entry_bedrooms']    = 'Chambres';
$_['entry_bathrooms']    = 'Salle de bain';
$_['entry_roomcount']    = 'Nombre de chambre';
$_['entry_Parkingspaces'] = 'Places de parking';
$_['entry_builtin']      = 'Construit en ';
$_['entry_amenities']    = 'Équipements';
$_['entry_arealenght']    = 'Longueur';
$_['entry_pricelabel']    = 'Étiquette de prix';
$_['entry_Price']           = 'Prix';
$_['entry_agent']           = 'Agente';
$_['entry_meta_title']      = 'Titre méta';
$_['entry_property_status'] = 'État de la propriété';
$_['entry_country']         = 'Pays';
$_['entry_Zone_region']     = 'Zone / région';
$_['entry_city']            = 'Ville';
$_['entry_destinies']            = 'Destinies';
$_['entry_meta_keyword']            = 'meta_keyword';
$_['entry_local_area']      = 'Zone locale';
$_['entry_pincode']         = 'Code PIN';
$_['entry_latitude']         = 'Latitude';
$_['entry_longitude']         = 'Longitude';
$_['entry_images']            = 'Image';
$_['entry_titles']            = 'Titre';
$_['entry_alt']               = 'Alt';
$_['entry_category']           = 'Catégorie';
$_['text_successedit']           = 'Éditer avec succès la mise à jour';
$_['entry_title']         = 'Longitude';
$_['entry_meta_descriptions']= ' Meta Description';
$_['entry_tag']             = 'Étiquette';
$_['entry_image']           = 'Image';
$_['entry_SEO_URL']         = 'URL de référencement';
$_['entry_name']            = 'Nom';
$_['entry_description']     ='La description';
$_['entry_cetogarey']       ='Magasins';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'URL de référencement';
$_['entry_bottom']           = 'Bas';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_layout']           = 'Remplacement de mise en page';

// Help
$_['help_keyword']           = "N'utilisez pas d'espaces, remplacez-les par des espaces et assurez-vous que l'URL de référencement est unique au monde..";
$_['help_bottom']            = 'Afficher en bas de page.';

// Error

$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des informations!';
$_['error_title']            = 'Le titre du formulaire doit comporter entre 3 et 64 caractères.!';
$_['error_name']            = ' Le nom doit comporter entre 2 et 255 caractères.!';
$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'URL de référencement déjà utilisée!';
$_['error_account']          = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de magasin.!';
$_['error_checkout']         = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée en tant que conditions de caisse.!';
$_['error_affiliate']        = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes affiliés du magasin.!';
$_['error_return']           = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour du magasin.!';
$_['error_store']            = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';
