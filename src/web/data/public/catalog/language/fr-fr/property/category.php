<?php

// heading

$_['heading_title1']     = 'Nos propriétés';

////  product info text_limit
$_['text_price']                 = 'Prix:';
$_['text_amenities']             = 'Équipements';
$_['text_nearestplace']          = 'Lieu le plus proche';
$_['text_details']               = 'Détails';



//filter

$_['text_limit']                 = 'Spectacle:';
$_['text_sort']                  = 'Trier par:';

$_['text_sort']                  = 'Trier par:';
$_['text_default']               = 'Défaut';
$_['text_name_asc']              = 'Nom (A - Z)';
$_['text_name_desc']             = 'Nom (Z - A)';
$_['text_price_asc']             = 'Prix (Faible &gt; Haute)';
$_['text_price_desc']            = 'Prix (Haute &gt; Faible)';
$_['text_rating_asc']            = 'Évaluation (Le plus bas)';
$_['text_rating_desc']           = 'Évaluation (Plus haut)';
$_['text_model_asc']             = 'Modèle (A - Z)';
$_['text_model_desc']            = 'Modèle (Z - A)';

$_['text_sqft']               = 'pi2:';
$_['text_amenities']         = 'Équipements:';
$_['text_NearestPlace']     ='Lieu le plus proche:';
$_['text_rent']              ='Location:';
$_['text_apartment'] 	= 'Appartement';
$_['text_4Daysago'] 	= 'il y a 4 jours';
$_['text_bedrooms'] 	= 'chambres';
$_['text_bathrooms'] 	= 'salle de bain';
$_['text_garge'] 		= 'Garge';
$_['text_nearest'] 		= 'La plus proche';

$_['text_price'] 		= 'Prix';






$_['text_empty']         		 = 'Il n\'y a aucun produit à lister dans cette catégorie.';

//category  not found
$_['text_error']         		 = 'Catégorie non trouvée!';


///

