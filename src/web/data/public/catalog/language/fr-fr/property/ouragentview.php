<?php
// Text
$_['heading_title']            = 'Nos agents';
$_['text_property']            = 'Propriété';

$_['text_refine']       = 'Affiner votre recherche';
$_['text_product']      = 'Des produits';
$_['text_error']        = 'Catégorie non trouvée!';
$_['text_empty']        = 'Il n\'y a aucun produit à lister dans cette catégorie.';
$_['text_quantity']     = 'Qté:';
$_['text_manufacturer'] = 'Marque:';
$_['text_noproperty'] 	= 'Aucune propriété:';
$_['text_model']        = 'Code produit:';
$_['text_points']       = 'Points de récompense:';
$_['text_price']        = 'Prix:';
$_['text_tax']          = 'Taxe ex:';
$_['text_compare']      = 'Comparaison de produit (%s)';
$_['text_sort']         = 'Trier par:';
$_['text_default']      = 'Défaut';
$_['text_name_asc']     = 'Nom (A - Z)';
$_['text_name_desc']    = 'Nom (Z - A)';
$_['text_price_asc']    = 'Prix (Faible &gt; Haute)';
$_['text_price_desc']   = 'Prix (Haute &gt; Faible)';
$_['text_rating_asc']   = 'Évaluation (Le plus bas)';
$_['text_rating_desc']  = 'Évaluation (Plus haut)';
$_['text_model_asc']    = 'Modèle (A - Z)';
$_['text_model_desc']   = 'Modèle (Z - A)';
$_['text_limit']        = 'Spectacle:';
$_['text_details']      = 'Détails';
$_['text_rent']         = 'Location';
$_['text_price']        = 'Prix';
$_['text_sqft']         = 'Prix';
$_['text_amenities']    = 'Prix';
$_['text_NearestPlace'] = 'Prix';
$_['text_apartment'] 	= 'Appartement';
$_['text_4Daysago'] 	= 'il y a 4 jours';
$_['text_bedrooms'] 	= 'chambres';
$_['text_bathrooms'] 	= 'salle de bain';
$_['text_garge'] 		= 'Équipements';
$_['text_nearest'] 		= 'La plus proche';
