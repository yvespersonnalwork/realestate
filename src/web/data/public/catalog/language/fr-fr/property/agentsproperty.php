<?php

// Text

$_['heading_title']            = 'Détail de l\'agent';

$_['text_edit']                = 'Agent d\'édition';

$_['text_dashboard']           = 'Tableau de bord';

$_['text_agent']               = 'Liste d\'agents';

$_['text_agentname']           = 'Nom d\'agent';

$_['text_descriptions']        = 'La description';

$_['text_positions']           = 'Positions';

$_['text_country']             = 'Pays';

$_['text_city']                = 'Ville';

$_['text_image']           		 = 'Image';

$_['text_address']             = 'Adresse';

$_['text_contact']             = 'Contact';

$_['text_email']               = 'Identifiant Email';

$_['text_country']             = 'Pays';

$_['text_zone']                = 'Zone';

$_['text_postcode']            = 'Code postal';
$_['text_agentproperty']            = 'Propriétés de l\'agent';

// Entry

$_['entry_bad']                = 'Mauvais';

$_['entry_bad']                = 'Mauvais';





