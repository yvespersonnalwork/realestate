<?php
class ModelAgentcron extends Model {
		
	public  function Expiryplanmail($property_agent_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "property_agent WHERE property_agent_id='".$this->agent->getId()."'");
		
		$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "plans p LEFT JOIN " .DB_PREFIX. "plans_description pd ON (p.plans_id = pd.plans_id) WHERE p.plans_id ='".$query->row['plans_id']."' and pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		$dayofreturn = $query1->row['no_of_day'];
		$daytype = $query1->row['type'];

		$orderdate   = date('Y-m-d', strtotime($query->row['date_added']));
		$datesss     = strtotime("+".$dayofreturn." $daytype ", strtotime($orderdate));
		$expirydate  = date("Y-m-d", $datesss);
		$currentdate = date("Y-m-d");
	
		if($currentdate > $expirydate){
			$this->load->model('agent/mail');
			$this->load->model('agent/agent');
			$type = 'agent_expiry_plan';
			
			$mailinfo = $this->model_agent_mail->getMailInfo($type);
			$agent_info = $this->model_agent_agent->getAgent($property_agent_id);
			
			if(!empty($mailinfo['message'])){
				if(isset($mailinfo['type'])){
				$find = array(
					'{agentname}',
					'{email}',
					'{contact}',
					'{country_id}',
					'{zone_id}',
					'{planname}',
					'{planenddate}',
					'{propertylimit}',
					
				);
				
				if(isset($agent_info['agentname'])) {
					$agentname = $agent_info['agentname'];
				} else {
					$agentname='';
				}
				
				if(isset($agent_info['email'])) {
					$emails = $agent_info['email'];
				} else {
					$emails='';
				}

				if(isset($agent_info['contact'])) {
					$contact = $agent_info['contact'];
				} else {
					$contact='';
				}

				$this->load->model('localisation/country');
				$country_info = $this->model_localisation_country->getCountry($agent_info['country_id']);
				if(isset($country_info['name'])) {
					$countryname = $country_info['name'];
				} else {
					$countryname='';
				}

				$this->load->model('localisation/zone');
				$zone_info = $this->model_localisation_zone->getZone($agent_info['zone_id']);
				if(isset($zone_info['name'])) {
					$zonename = $zone_info['name'];
				} else {
					$zonename='';
				}

				if(isset($query1->row['name'])) {
					$planname = $query1->row['name'];
				} else {
					$planname='';
				}

				if(isset($query1->row['property_limit'])) {
					$propertylimit = $query1->row['property_limit'];
				} else {
					$propertylimit='';
				}


				$replace = array(
					'agentname'	=> $agentname,
					'emails'	=> $emails,
					'contact'	=> $contact,
					'country_id'=> $countryname,
					'zone_id'	=> $zonename,
					'planname'	=> $planname,
					'planenddate'=> $expirydate,
					'propertylimit'=> $propertylimit,
				);

				$subject = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $mailinfo['subject']))));

				$message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $mailinfo['message']))));
				
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($emails);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject($subject);
				$mail->setHtml(html_entity_decode($message));
				$mail->send();
						
			}
		}

		return $query->row;
		}
	}	
}