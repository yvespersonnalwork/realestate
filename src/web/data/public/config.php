<?php
// HTTP
define('HTTP_SERVER', 'http://realestate.localhost:8080/');

// HTTPS
define('HTTPS_SERVER', 'http://realestate.localhost:8080/');

// DIR
define('DIR_APPLICATION', '/var/www/app/public/catalog/');
define('DIR_SYSTEM', '/var/www/app/public/system/');
define('DIR_IMAGE', '/var/www/app/public/image/');
define('DIR_LANGUAGE', '/var/www/app/public/catalog/language/');
define('DIR_TEMPLATE', '/var/www/app/public/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/app/public/system/config/');
define('DIR_CACHE', '/var/www/app/public/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/app/public/system/storage/download/');
define('DIR_LOGS', '/var/www/app/public/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/app/public/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/app/public/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'realestate_db');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'realestate');
define('DB_DATABASE', 'realestate');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
