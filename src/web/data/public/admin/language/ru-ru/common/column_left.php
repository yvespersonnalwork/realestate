<?php

// Text

$_['text_blogdashboard']               = 'Панель инструментов блога';
///left side menu
$_['text_setting']                   = 'настройки';
$_['text_themecontrol'] 		 	 = 'Контроль темы';
$_['text_backup']                    = 'Восстановления резервной копии';
$_['text_seo_url']                     = 'SEO URL';
$_['text_property']                  = 'Имущество'; 
$_['text_category']                  = 'Тип объявления / Категория';
$_['text_order_status']              = 'Статусы недвижимости';
$_['text_feature'] 		 			 = 'Характеристики';
$_['text_nearest'] 		 			 = 'Ближайшее место';
$_['text_units'] 		 			 = 'Единицы';
$_['text_enquiry'] 		 			 = 'Запрос недвижимости';
$_['text_agent']                     = 'агент';
$_['text_custom_field']              = 'Пользовательское поле';
$_['text_contact']                   = 'почта';
$_['text_catalog']                   = 'Каталог';
$_['text_country']                   = 'страны';
$_['text_coupon']                    = 'купоны';
$_['text_currency']                  = 'валюты';
$_['text_customer_group']            = 'Группы клиентов';
$_['text_dashboard']                 = 'Приборная доска';
$_['text_design']                    = 'дизайн';
$_['text_download']                  = 'Загрузки';
$_['text_log']                       = 'Журналы ошибок';
$_['text_event']                     = 'События';
$_['text_extension']                 = 'расширения';
$_['text_filter']                    = 'фильтры';
$_['text_geo_zone']                  = 'Гео зоны';
$_['text_information']               = 'Информационные страницы';
$_['text_installer']                 = 'Установщик расширений';
$_['text_language']                  = 'Языки';
$_['text_localisation']              = 'локализация';
$_['text_location']                  = 'Расположение сайта';
$_['text_contact']                   = 'почта';
$_['text_marketing']                 = 'маркетинг';
$_['text_menu']                      = 'Менеджер по маркетингу';
$_['text_modification']              = 'изменения';
$_['text_manufacturer']              = 'Производители';
$_['text_option']                    = 'Опции';
$_['text_order']                     = 'заказы';
$_['text_seo_url']                     = 'Seo url';

$_['text_product']                   = 'Товары';

$_['text_reports']                   = 'Отчеты';

$_['text_report_sale']               = 'Продажи';

$_['text_report_sale_order']         = 'заказы';

$_['text_report_sale_tax']           = 'налог';

$_['text_report_sale_shipping']      = 'Перевозка';

$_['text_report_sale_return']        = 'Возвращает';

$_['text_report_sale_coupon']        = 'купоны';

$_['text_report_product']            = 'Товары';

$_['text_report_product_viewed']     = 'Рассматриваемый';

$_['text_report_product_purchased']  = 'купленный';

$_['text_report_customer']           = 'Клиенты';

$_['text_report_customer_activity']  = 'Активность клиентов';

$_['text_report_customer_search']    = 'Поиск клиентов';

$_['text_report_customer_online']    = 'Клиенты онлайн';

$_['text_report_customer_order']     = 'заказы';

$_['text_report_customer_reward']    = 'Бонусные очки';

$_['text_report_customer_credit']    = 'кредит';

$_['text_report_marketing']          = 'маркетинг';

$_['text_report_affiliate']          = 'Филиалы';

$_['text_report_affiliate_activity'] = 'Партнерская деятельность';

$_['text_review']                    = 'Отзывы';

$_['text_return']                    = 'Возвращает';

$_['text_return_action']             = 'Вернуться Действия';

$_['text_return_reason']             = 'Причины возврата';

$_['text_return_status']             = 'Статусы возврата';

$_['text_sale']                      = 'Продажи';

$_['text_setting']                   = 'настройки';

$_['text_store']                     = 'Расширение веб-сайта';

$_['text_stock_status']              = 'Статусы акций';



$_['text_tax']                       = 'налоги';

$_['text_tax_class']                 = 'Налоговые классы';

$_['text_tax_rate']                  = 'Налоговые ставки';

$_['text_translation']               = 'Редактор языков';

$_['text_theme']                     = 'Редактор тем';

$_['text_tools']                     = 'инструменты';

$_['text_upload']                    = 'Загрузки';

$_['text_user']                      = 'пользователей';

$_['text_voucher']                   = 'Подарочные сертификаты';

$_['text_voucher_theme']             = 'Ваучер Темы';

$_['text_weight_class']              = 'Весовые классы';

$_['text_length_class']              = 'Классы длины';

$_['text_zone']                      = 'зон';

$_['text_recurring']                 = 'Повторяющиеся профили';

$_['text_order_recurring']           = 'Повторяющиеся заказы';

$_['text_openbay_extension']         = 'OpenBay Pro';

$_['text_openbay_dashboard']         = 'Приборная доска';

$_['text_openbay_orders']            = 'Массовое обновление заказа';

$_['text_openbay_items']             = 'Массовое обновление заказа...';

$_['text_openbay_ebay']              = 'eBay';

$_['text_openbay_amazon']            = 'Амазонка (EU)';

$_['text_openbay_amazonus']          = 'Амазонка (US)';

$_['text_openbay_etsy']            	 = 'Etsy';

$_['text_openbay_settings']          = 'настройки';

$_['text_openbay_links']             = 'Ссылки на товары';

$_['text_openbay_report_price']      = 'Ценовой отчет';

$_['text_openbay_order_import']      = 'Заказать импорт';

$_['text_paypal']                    = 'PayPal';

$_['text_paypal_search']             = 'Поиск';

$_['text_complete_status']           = 'Заказы выполнены'; 

$_['text_processing_status']         = 'Обработка заказов'; 

$_['text_other_status']              = 'Другие статусы'; 





$_['text_property']                  = 'Имущество'; 

$_['text_order_status']              = 'Статусы недвижимости';

$_['text_category']                  = 'Список категорий / Категория';

$_['text_customer']                  = 'Заказчик недвижимости';

$_['text_custom_field']              = 'Пользовательское поле';

$_['text_agent']                     = 'агент';
$_['text_mail']                      = 'Шаблон почты агента';

$_['text_users']                     = 'пользователь';

$_['text_user_group']                = 'Группы пользователей';

$_['text_pagebuilder']              = 'Построитель страниц';

$_['text_layout']                    = 'Настройка макета страницы';

$_['text_banner']                    = 'Баннер';

$_['text_module']                    = 'Настройка модуля';

$_['text_system']                    = 'настройка';

$_['text_backup']                    = 'Восстановления резервной копии';

$_['text_pages']                     = 'страницы';

$_['text_membership']                = 'членство';

$_['text_plans']                     = 'планы';

$_['text_variation']                 = 'пакет';

$_['text_megaheader1']               = 'Мега заголовок';

$_['text_megaheader']                = 'Мега Меню Заголовка';

$_['text_newssubscribe']             = 'Подписчики на рассылку';

$_['text_faqcat']   				 = 'Faq Категория';		

$_['text_faq']     					 = 'Часто задаваемые вопросы';

$_['text_testimonial']               = 'свидетельство';

$_['text_photo']          			 = 'Фото';

$_['text_gallerydashboard'] 		 = 'Панель инструментов галереи';

$_['text_feature'] 		 			 = 'Характеристики';

$_['text_nearest'] 		 			 = 'Ближайшее место';

$_['text_enquiry'] 		 			 = 'Запрос недвижимости';

$_['text_agents'] 		 			 = 'Агенты по недвижимости';

$_['text_themecontrol'] 		 			 = 'Контроль темы';


/* lanuge add */
$_['text_analytics'] 		 			 = 'Google и FB аналитика';



