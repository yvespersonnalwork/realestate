<?php

// Heading
$_['heading_title']    = ' запрос';

// Text
$_['text_success']     = 'Успех: Вы изменили шаблон рассылки!';
$_['text_export']     = 'экспорт';
$_['text_list']      = 'Список недвижимости Запрос';
$_['text_add']      = 'Добавить запрос';
$_['text_edit']     = 'Редактировать запрос';
$_['text_form']      = 'Форма запроса';
$_['text_success']           = 'успешно: Добавить свой запрос!';
$_['text_successedit']       = 'успешно: вы изменили запрос';
$_['text_successdelete']     = 'успешно: Ваш запрос Удалить';
$_['text_enable']   = 'включить';
$_['text_disable']   = 'запрещать';

// Column
$_['column_property']   = 'Имущество';
$_['column_name']      	= 'название';
$_['column_email']     	= 'Эл. адрес';
$_['column_agentname']  = 'Имя агента';
$_['column_description']= 'Описание';
$_['column_action']    	= 'действие';

// Lable
$_['lable_property']   	= 'Имущество';
$_['lable_name']        = 'название';
$_['lable_email']       = 'Эл. адрес';
$_['lable_agent']   	= 'агент';
$_['lable_description'] = 'Описание';

// Entry
$_['entry_property']   	= 'Имущество';
$_['entry_name']        = 'название';
$_['entry_email']       = 'Эл. адрес';
$_['entry_agent']   	= 'агент';
$_['entry_description'] = 'Описание';

//button
$_['button_shortcut']       = 'кратчайший путь';

// Error
$_['error_permission'] 	= 'Предупреждение: у вас нет прав на изменение шаблона рассылки!';
$_['error_email_exist']	= 'Идентификатор электронной почты уже существует';
$_['error_email_id']    = 'Неверный формат электронной почты';
$_['error_email']      	= 'Неверный формат электронной почты';
$_['error_name']        = ' Имя должно быть от 2 до 255 символов!';



?>