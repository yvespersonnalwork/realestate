<?php

// Heading 

$_['heading_title']          = 'Имущество ';

// Text
$_['text_success']           = 'успешно: добавить вашу собственность!';
$_['text_successedit']       = 'успешно: вы изменили свойство';
$_['text_successdelete']     = 'успешно: вы удалили имущество';
$_['text_list']              = 'Список недвижимости';
$_['text_add']               = 'Добавить недвижимость';
$_['text_edit']              = 'Изменить свойство';
$_['text_form']              = 'Добавить недвижимость';
$_['text_enable']            = 'включить';
$_['text_disable']           = 'запрещать';
$_['text_default']           = 'По умолчанию';
$_['text_upload']            = 'Ваш текст успешно загружен';
$_['outdoor_pool']           = 'Открытый бассейн';
$_['tab_customfield']        = 'изготовленный на заказ';
$_['button_desapprove']     = 'Desapprove';

/// Filter
// Column
$_['filterlablel_name']            = 'название';
$_['filterlablel_property_status'] = 'Статус недвижимости';
$_['filterlablel_price']           = 'Цена';
$_['filterlablel_status']          = 'Статус';
$_['filterlablel_price_range']     = 'Ценовой диапазон';
$_['filterlablel_price_from']      = 'От';
$_['filterlablel_agent']           = 'агент';

// Column
$_['column_image']           = 'Образ';
$_['column_name']            = 'название';
$_['column_property_status'] = 'Статус недвижимости';
$_['column_price']            = 'Цена';
$_['column_status']          = 'Статус';
$_['column_action']          = 'действие';
$_['column_price_range']      = 'Ценовой диапазон';
$_['column_price_from']      = 'От';
$_['column_agent']            = 'Имя агента';
$_['column_category']         = 'категория';
$_['column_approved']         = 'Одобренный';

// Lable
$_['lable_title']           = 'заглавие';
$_['lable_approved']           = 'Одобренный';
$_['lable_video']           = 'видео';
$_['lable_neighborhood']    = 'окрестности';
$_['lable_area']    = 'Площадь';
$_['lable_bedrooms']    = 'спальни';
$_['lable_bathrooms']    = 'В ванных комнатах';
$_['lable_roomcount']    = 'Количество комнат';
$_['lable_Parkingspaces'] = 'Парковочные места';
$_['lable_builtin']      = 'Встроенный ';
$_['lable_amenities']    = 'Удобства';
$_['lable_arealenght']    = 'длина';
$_['lable_pricelabel']    = 'Ценник';
$_['lable_Price']           = 'Цена';
$_['lable_agent']           = 'агент';
$_['lable_meta_title']      = 'Мета заголовок';
$_['lable_property_status'] = 'Статус недвижимости';
$_['lable_country']         = 'Страна';
$_['lable_Zone_region']     = 'Зона / область';
$_['lable_city']            = 'город';
$_['lable_destinies']            = 'Судьбы';
$_['lable_meta_keyword']            = 'meta_keyword';
$_['lable_local_area']      = 'Окрестности';
$_['lable_pincode']         = 'Pincode / Zipcode';
$_['lable_latitude']         = 'широта';
$_['lable_longitude']         = 'Долгота';
$_['lable_images']            = 'Образ';
$_['lable_titles']            = 'заглавие';
$_['lable_alt']               = 'Alt';
$_['lable_category']           = 'категория';
$_['text_map']           	= 'Получить карту';
$_['lable_title']         = 'Долгота';
$_['lable_meta_descriptions']= ' Мета-описание';
$_['lable_tag']             = 'Тег';
$_['lable_image']           = 'Образ';
$_['lable_SEO_URL']         = 'SEO URL';
$_['lable_name']            = 'название';
$_['lable_description']     ='Описание';
$_['lable_cetogarey']       ='магазины';
$_['lable_meta_title'] 	     = 'Название метатега';
$_['lable_meta_keyword'] 	 = 'Ключевые слова метатега';
$_['lable_meta_description'] = 'Описание метатега';
$_['lable_keyword']          = 'SEO URL';
$_['lable_bottom']           = 'Низ';
$_['lable_status']           = 'Статус';
$_['lable_sort_order']       = 'Порядок сортировки';
$_['lable_layout']           = 'Переопределение макета';

// Entry
$_['entry_title']           = 'заглавие';
$_['entry_approved']           = 'Одобренный';
$_['entry_video']           = 'видео';
$_['entry_neighborhood']    = 'окрестности';
$_['entry_area']    = 'Площадь';
$_['entry_bedrooms']    = 'спальни';
$_['entry_bathrooms']    = 'В ванных комнатах';
$_['entry_roomcount']    = 'Количество комнат';
$_['entry_Parkingspaces'] = 'Парковочные места';
$_['entry_builtin']      = 'Встроенный ';
$_['entry_amenities']    = 'Удобства';
$_['entry_arealenght']    = 'длина';
$_['entry_pricelabel']    = 'Ценник';
$_['entry_Price']           = 'Цена';
$_['entry_agent']           = 'агент';
$_['entry_meta_title']      = 'Мета заголовок';
$_['entry_property_status'] = 'Статус недвижимости';
$_['entry_country']         = 'Страна';
$_['entry_Zone_region']     = 'Зона / область';
$_['entry_city']            = 'город';
$_['entry_destinies']            = 'Судьбы';
$_['entry_meta_keyword']            = 'meta_keyword';
$_['entry_local_area']      = 'Окрестности';
$_['entry_pincode']         = 'Pincode / Zipcode';
$_['entry_latitude']         = 'широта';
$_['entry_longitude']         = 'Долгота';
$_['entry_images']            = 'Образ';
$_['entry_titles']            = 'заглавие';
$_['entry_alt']               = 'Alt';
$_['entry_category']           = 'категория';
$_['text_map']           	= 'Получить карту';
$_['entry_title']         = 'Долгота';
$_['entry_meta_descriptions']= ' Мета-описание';
$_['entry_tag']             = 'Тег';
$_['entry_image']           = 'Образ';
$_['entry_SEO_URL']         = 'SEO URL';
$_['entry_name']            = 'название';
$_['entry_description']     ='Описание';
$_['entry_cetogarey']       ='магазины';
$_['entry_meta_title'] 	     = 'Название метатега';
$_['entry_meta_keyword'] 	 = 'Ключевые слова метатега';
$_['entry_meta_description'] = 'Описание метатега';
$_['entry_keyword']          = 'SEO URL';
$_['entry_bottom']           = 'Низ';
$_['entry_status']           = 'Статус';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_layout']           = 'Переопределение макета';


$_['text_yards']           = 'ярды';
$_['text_kanal']           = 'Kanal';
$_['text_sqfit']           = 'кв. фут';


// Help
$_['help_keyword']           = 'Не используйте пробелы, вместо этого заменяйте пробелы на - и убедитесь, что URL-адрес SEO является глобально уникальным.';
$_['help_bottom']            = 'Дисплей в нижнем колонтитуле.';

// Error
$_['error_warning']          = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission']       = 'Предупреждение: у вас нет разрешения на изменение информации!';
$_['error_title']            = 'Заголовок формы должен быть от 3 до 64 символов!';
$_['error_name']             = ' Имя должно быть от 2 до 255 символов!';
$_['error_description']      = 'Описание должно содержать более 3 символов!';
$_['error_meta_title']       = 'Название мета должно быть больше 3 и меньше 255 символов!';
$_['error_keyword']          = 'SEO URL уже используется!';
$_['error_account']          = 'Предупреждение: эта информационная страница не может быть удалена, так как она в настоящее время назначена в качестве условия учетной записи магазина!';
$_['error_checkout']         = 'Предупреждение: эта информационная страница не может быть удалена, так как она в настоящее время назначена в качестве условий оформления покупки!';
$_['error_affiliate']        = 'Предупреждение: эта информационная страница не может быть удалена, так как она в настоящее время назначена в качестве условий аффилированного лица магазина!';
$_['error_return']           = 'Предупреждение: эта информационная страница не может быть удалена, так как она в настоящее время назначена в качестве условий возврата магазина!';
$_['error_store']            = 'Предупреждение: эта информационная страница не может быть удалена, так как она в настоящее время используется% s магазинами!';
$_['error_country_id']       = 'Пожалуйста, заполните поле Страна';
$_['error_zone_id']          = 'Пожалуйста, заполните поле зоны ';
$_['error_sort_order']       = 'Пожалуйста, заполните порядок сортировки ';
$_['error_status']         = 'Выберите Статус недвижимости!';