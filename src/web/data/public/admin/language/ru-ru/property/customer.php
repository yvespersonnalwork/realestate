<?php

// Heading

$_['heading_title']         = 'Покупатель';



// Text

$_['text_success']          = 'Успех: вы изменили клиентов!';

$_['text_list']             = 'Список клиентов';

$_['text_add']              = 'Добавить клиента';

$_['text_edit']             = 'Изменить клиента';

$_['text_default']          = 'По умолчанию';

$_['text_balance']          = 'Остаток средств';



// Column

$_['column_name']           = 'имя покупателя';

$_['column_plans']           = 'план';

$_['column_email']          = 'Эл. почта';

$_['column_customer_group'] = 'Группа клиентов';

$_['column_status']         = 'Статус';

$_['column_date_added']     = 'Дата Добавлена';

$_['column_comment']        = 'Комментарий';

$_['column_description']    = 'Описание';

$_['column_amount']         = 'Количество';

$_['column_points']         = 'Точки';

$_['column_ip']             = 'IP';

$_['column_total']          = 'Всего аккаунтов';

$_['column_action']         = 'действие';



// Entry

$_['entry_customer_group']  = 'Группа клиентов';

$_['entry_firstname']       = 'Имя';

$_['entry_plans']           = 'планы';

$_['entry_lastname']        = 'Фамилия';

$_['entry_email']           = 'Эл. почта';

$_['entry_telephone']       = 'телефон';

$_['entry_fax']             = 'факс';

$_['entry_newsletter']      = 'Новостная рассылка';

$_['entry_status']          = 'Статус';

$_['entry_approved']        = 'Одобренный';

$_['entry_safe']            = 'Безопасный';

$_['entry_password']        = 'пароль';

$_['entry_confirm']         = 'подтвердить';

$_['entry_company']         = 'Компания';

$_['entry_address_1']       = 'Адрес 1';

$_['entry_address_2']       = 'Адрес 2';

$_['entry_city']            = 'город';

$_['entry_postcode']        = 'почтовый индекс';

$_['entry_country']         = 'Страна';

$_['entry_zone']            = 'Регион / Штат';

$_['entry_default']         = 'Адрес по умолчанию';

$_['entry_comment']         = 'Комментарий';

$_['entry_description']     = 'Описание';

$_['entry_amount']          = 'Количество';

$_['entry_points']          = 'Точки';

$_['entry_name']            = 'имя покупателя';

$_['entry_ip']              = 'IP';

$_['entry_date_added']      = 'Дата Добавлена';



// Help

$_['help_safe']             = 'Установите значение true, чтобы этот клиент не попал в систему защиты от мошенничества.';

$_['help_points']           = 'Используйте минус, чтобы убрать очки';



// Error

$_['error_warning']         = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';

$_['error_permission']      = 'Предупреждение: у вас нет разрешения на изменение клиентов!';

$_['error_exists']          = 'Внимание: адрес электронной почты уже зарегистрирован!';

$_['error_firstname']       = 'Имя должно быть от 1 до 32 символов!';

$_['error_lastname']        = 'Фамилия должна быть от 1 до 32 символов!';

$_['error_email']           = 'Адрес электронной почты не является действительным!';

$_['error_telephone']       = 'Телефон должен быть от 3 до 32 символов!';

$_['error_password']        = 'Пароль должен быть от 4 до 20 символов!';

$_['error_confirm']         = 'Пароль и подтверждение пароля не совпадают!';

$_['error_address_1']       = 'Адрес 1 должен быть от 3 до 128 символов!';

$_['error_city']            = 'Город должен содержать от 2 до 128 символов!';

$_['error_postcode']        = 'Почтовый индекс должен быть от 2 до 10 символов для этой страны!';

$_['error_country']         = 'Пожалуйста, выберите страну!';

$_['error_zone']            = '%s требуется!';