<?php

// Heading

$_['heading_title']        = 'Пользовательское поле';

// Text
$_['text_success']         = 'Успех: вы изменили пользовательские поля!';
$_['text_list']            = 'Список настраиваемых полей';
$_['text_add']             = 'Добавить настраиваемое поле';
$_['text_edit']            = 'Изменить настраиваемое поле';
$_['text_choose']          = 'выберите';
$_['text_select']          = 'Выбрать';
$_['text_radio']           = 'Радио';
$_['text_checkbox']        = 'флажок';
$_['text_input']           = 'вход';
$_['text_text']            = 'Текст';
$_['text_textarea']        = 'Textarea';
$_['text_file']            = 'файл';
$_['text_date']            = 'Дата';
$_['text_datetime']        = 'Дата и Время';
$_['text_time']            = 'Время';
$_['text_account']         = 'учетная запись';
$_['text_address']         = 'Адрес';
$_['text_regex']           = 'Regex';
$_['text_success']           = 'успешно: добавьте свое настраиваемое поле';
$_['text_successedit']       = 'успешно: вы изменили пользовательское поле';
$_['text_successdelete']     = 'успешно: пользовательское поле удалено';

// Column
$_['column_name']          = 'Имя настраиваемого поля';
$_['column_type']          = 'Тип';
$_['column_sort_order']    = 'Порядок сортировки';
$_['column_action']        = 'действие';

// Lable
$_['lable_name']           = 'Имя настраиваемого поля';
$_['lable_type']           = 'Тип';
$_['lable_status']         = 'Статус';
$_['lable_sort_order']     = 'Порядок сортировки';
$_['lable_value']          = 'Значение';
$_['lable_validation']     = 'Проверка';
$_['lable_location']       = 'Место нахождения';
$_['lable_custom_value']   = 'Имя значения настраиваемого поля';
$_['lable_customer_group'] = 'Группа клиентов';
$_['entry_required']       = 'необходимые';

// Entry
$_['entry_name']           = 'Имя настраиваемого поля';
$_['entry_type']           = 'Тип';
$_['entry_status']         = 'Статус';
$_['entry_sort_order']     = 'Порядок сортировки';
$_['entry_value']          = 'Значение';
$_['entry_validation']     = 'Проверка';
$_['entry_location']       = 'Место нахождения';
$_['entry_custom_value']   = 'Имя значения настраиваемого поля';
$_['entry_customer_group'] = 'Группа клиентов';
$_['entry_required']       = 'необходимые';

// Help
$_['help_regex']           = 'Используйте регулярное выражение. E.g: /[a-zA-Z0-9_-]/';
$_['help_sort_order']      = 'Используйте минус для обратного отсчета от последнего поля в наборе.';

// Error
$_['error_permission']     = 'Предупреждение: у вас нет прав на изменение пользовательских полей!';
$_['error_name']           = 'Имя настраиваемого поля должно быть от 1 до 128 символов!';
$_['error_type']           = 'Предупреждение: требуются значения пользовательских полей!';
$_['error_custom_value']   = 'Требуемые значения полей!';
