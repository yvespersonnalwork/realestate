<?php

// Heading 

$_['heading_title']          = 'Ближайшее место';

// Text

$_['text_success']           = 'Успех: вы изменили информацию!';
$_['text_list']              = 'Ближайший список';
$_['text_add']               = 'Добавить форму';
$_['text_edit']              = 'Редактировать форму';
$_['text_form']              = 'Добавить ближайший';
$_['text_enable']            = 'включить';
$_['text_disable']           = 'запрещать';
$_['text_default']           = 'По умолчанию';
$_['text_success']           = 'успешно: Добавить ближайшее место!';
$_['text_successedit']       = 'успешно: вы изменили Ближайшее место';
$_['text_successdelete']     = 'успешно: Ближайшее место удалено';

// Column

$_['column_image']        		= 'Образ';
$_['column_name']            	= 'название';
$_['column_sort_order']      	= 'Порядок сортировки';
$_['column_status']           	= 'Статус';
$_['column_action']           	= 'действие';

// Lable
$_['lable_name']            	= 'название';
$_['lable_image']           	= 'Образ';
$_['lable_sort_order']       	= 'Порядок сортировки';
$_['lable_status']           	= 'Статус';

// Entry
$_['entry_name']            	= 'название';
$_['entry_image']           	= 'imОбразage';
$_['entry_sort_order']       	= 'Порядок сортировки';
$_['entry_status']           	= 'Статус';

// Error

$_['error_warning']          = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission']       = 'Предупреждение: у вас нет разрешения на изменение информации!';
$_['error_name']             = 'Название Название должно быть от 3 до 64 символов!';

