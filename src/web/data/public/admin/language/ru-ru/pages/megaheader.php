<?php
// Heading
$_['heading_title']          = 'Мега Меню Заголовка';

//Text
$_['text_list']         	 = 'Список меню';
$_['text_form']              = 'Мега заголовок';
$_['text_category']          = 'категория';
$_['text_info']              = 'Информация';
$_['text_manufact']          = 'производитель';
$_['text_custom']            = 'изготовленный на заказ';
$_['text_select']            = 'Выбрать';
$_['text_editor']            = 'редактор';
$_['text_product']           = 'Товар';
$_['text_enable']            = 'Включено';
$_['text_disable']           = 'инвалид';
$_['text_no_results']        = 'безрезультатно';
$_['text_category_image']    = 'Изображение категории';
$_['text_category_description'] = 'Описание категории';
$_['text_manufatureimage']   = 'Изображение производителя';
$_['text_patternimage']   	= 'Шаблон изображения';
$_['entry_patternimage']     = 'Фоновый узор';
$_['entry_selectimagetype']  = 'Выберите тип фона';
//// new changes  27-10-2016///
$_['entry_store']            = 'Веб-сайт';
//// new changes  27-10-2016///


$_['button_edit']            = 'редактировать';
$_['button_add']             = 'Добавить новое';
$_['button_delete']          = 'удалять';
$_['button_save']            = 'Сохранить';
$_['button_setting']         = 'Настройка мега заголовка';
$_['button_cancel']          = 'Canel';
$_['text_success']           = 'Успех: вы изменили мега заголовки! ';
$_['button_custom']          = 'Добавить пользовательское меню';

$_['text_product1']          = 'Имущество';
$_['text_subcategory']       = 'Подкатегория';
$_['text_price']         	 = 'Цена';
$_['text_pname']         	 = 'Имя свойства';
$_['text_image']         	 = 'Образ';
$_['text_model']        	 = 'модель';
$_['text_upc']        	     = 'СКП';
$_['text_sku']         		 = 'SKU';
$_['text_description']       = 'Описание';


/// label name 
$_['text_image']          	 = 'Изображение на заднем плане';
$_['text_status']           = 'Статус';
$_['text_sort_order']       = 'Порядок сортировки';
$_['text_title']            = 'Заголовок пользовательского заголовка';
$_['text_row']           	 = 'Строка';
$_['text_col']           	 = 'Cols';
$_['text_enable']           = 'Включить заголовок категории подменю';
$_['text_url']           	 = 'URL';
$_['text_open']           	 = 'Открыть в новой вкладке';
$_['text_type']           	 = 'Тип';
$_['text_custname']         = 'Пользовательское имя';
$_['text_custurl'] 	     = 'Пользовательский URL';
$_['text_parent'] 	         = 'родитель';
$_['text_submenu'] 		 = 'Sub Megamenu';
$_['text_description']      = 'MegaMenu Описание';
$_['text_product']          = 'Товар';
$_['text_category']         = 'категория';
$_['text_row']         	 = 'Строка';
$_['text_customcode']       = 'Пользовательский код';
$_['button_setting']         = 'настройки';
$_['text_information']      = 'Информация';
$_['text_manufacturer']     = 'производитель';
$_['text_editor']            = 'редактор';
$_['text_icontitle']		 = 'Значок';
$_['text_showicon']		 = 'Включить иконку';

$_['text_selectimagetype']  = 'Выберите тип фона';
//// new changes  27-10-2016///
$_['text_store']            = 'Веб-сайт';

// Entry
$_['entry_image']          	 = 'Изображение на заднем плане';
$_['entry_status']           = 'Статус';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_title']            = 'Заголовок пользовательского заголовка';
$_['entry_row']           	 = 'Строка';
$_['entry_col']           	 = 'Cols';
$_['entry_enable']           = 'Включить заголовок категории подменю';
$_['entry_url']           	 = 'URL';
$_['entry_open']           	 = 'Открыть в новой вкладке';
$_['entry_type']           	 = 'Тип';
$_['entry_custname']         = 'Пользовательское имя';
$_['entry_custurl'] 	     = 'Пользовательский URL';
$_['entry_parent'] 	         = 'родитель';
$_['entry_submenu'] 		 = 'Sub Megamenu';
$_['entry_description']      = 'MegaMenu Описание';
$_['entry_product']          = 'Товар';
$_['entry_category']         = 'категория';
$_['entry_row']         	 = 'Строка';
$_['entry_customcode']       = 'Пользовательский код';
$_['button_setting']         = 'настройки';
$_['entry_information']      = 'Информация';
$_['entry_manufacturer']     = 'производитель';
$_['text_editor']            = 'редактор';
$_['entry_icontitle']		 = 'Значок';
$_['entry_showicon']		 = 'Включить иконку';

// column_edit
$_['column_title']           = 'Название меню';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_status']          = 'Статус';
$_['column_edit']            = 'действие';

 // Error
$_['error_title']      		 = 'Название должно быть от 3 до 21 символов'; 
$_['error_image']      		 = 'Пожалуйста, выберите изображение'; 

//filter
$_['button_filter']      	 = 'Фильтр';
$_['entry_title']      		 = 'Заголовок пользовательского заголовка';
$_['entry_status']      	 = 'Статус';
$_['text_enabled']      	 = 'включить';
$_['text_disabled']      	 = 'запрещать';

//help
$_['help_keyword']           = 'Не используйте пробелы, замените пробелы на - и убедитесь, что ключевое слово является глобально уникальным.';
$_['help_bottom']            = 'Дисплей в нижнем колонтитуле.';
$_['help_category']          = 'Автозаполнение';
$_['help_product']           = 'Автозаполнение';
$_['help_showicon']          = 'Включить только значок в меню';
