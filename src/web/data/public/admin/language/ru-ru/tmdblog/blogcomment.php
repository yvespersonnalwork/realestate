<?php
// Heading
$_['heading_title']          = 'Комментарий блога';

// Text
$_['text_success']           = 'Успех: Вы изменили комментарий в блоге!';
$_['text_list']              = 'Список комментариев блога';
$_['text_add']               = 'Добавить комментарий в блоге';
$_['text_edit']              = 'Редактировать комментарий в блоге';
$_['text_default']           = 'По умолчанию';
$_['text_no_comment']        = 'Без комментариев ';
$_['text_dash']              = 'Приборная доска';
$_['text_cate']              = 'категория';
$_['text_comm']              = 'Комментарий';
$_['text_sett']              = 'настройки';
$_['text_addmodule']         = 'Добавить модуль';
$_['text_none']              = '--Никто--';
$_['text_enabled']           = 'Включено';
$_['text_disabled']          = 'инвалид';

// Tab
$_['tab_comment']            = 'Комментарии';

// Column

$_['column_date_added']      = 'Дата Добавлена';
$_['column_status']          = 'Статус';
$_['column_image']           = 'Образ';
$_['column_title']           = 'Название комментария блога';
$_['column_sort_order']	     = 'Порядок сортировки';
$_['column_action']          = 'действие';

$_['column_name']            = 'имя покупателя';
$_['column_view']            = 'Просмотры';
$_['column_customer']        = 'Покупатель';
$_['column_comment']         = 'Комментарий';


// Entry
$_['entry_name']             = 'Название комментария блога';
$_['entry_customername']     = 'Покупатель';
$_['entry_status']           = 'Статус';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_image']            = 'Образ';
$_['entry_tmdblog']          = 'Выберите блог';
$_['entry_date_added']       = 'Дата Добавлена';
$_['entry_comment']          = 'Комментарий';


// Help
$_['help_tmdblog']           = 'Автозаполнение';
$_['help_bottom']            = 'Дисплей в нижнем колонтитуле.';

// Error
$_['error_warning']          = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission']       = 'Предупреждение: у вас нет разрешения на изменение статьи!';
$_['error_title']            = 'Название комментария блога должно быть от 3 до 64 символов!';
$_['error_description']      = 'Описание должно содержать более 3 символов!';
$_['error_meta_title']       = 'Название мета должно быть больше 3 и меньше 255 символов!';
$_['error_keyword']          = 'SEO ключевое слово уже используется!';
$_['error_account']          = 'Предупреждение: эту страницу статьи нельзя удалить, так как она в настоящее время назначена в качестве условия учетной записи магазина!';
$_['error_checkout']         = 'Предупреждение: эту страницу статьи нельзя удалить, так как она в настоящее время назначена в качестве условий оформления заказа в магазине!';
$_['error_affiliate']        = 'Предупреждение: эта страница статьи не может быть удалена, так как в настоящее время она назначена условиями аффилированного лица магазина!';
$_['error_return']           = 'Предупреждение: эту страницу статьи нельзя удалить, так как она в настоящее время назначена условиями возврата магазина!';
$_['error_store']            = 'Предупреждение: эту страницу статьи нельзя удалить, так как в настоящее время она используется% s магазинами!';