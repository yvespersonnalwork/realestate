<?php
// Heading
$_['heading_title']          = 'Блог';

// Text
$_['text_success']           = 'Успех: вы изменили блог!';
$_['text_list']              = 'Список блогов';
$_['text_add']               = 'Добавить блог';
$_['text_edit']              = 'Редактировать блог';
$_['text_default']           = 'По умолчанию';
$_['entry_username']         = 'Имя пользователя';
$_['entry_blogcoment']       = 'Показать комментарии';
$_['tab_comment']            = 'Комментарии';
$_['column_customer']        = 'Покупатель';
$_['column_comment']         = 'Комментарий';
$_['text_no_comment']        = 'Без комментариев ';
$_['text_dash']                     = 'Приборная доска';
$_['text_blog']                    = 'Блоги';
$_['text_cate']                   = 'категория';
$_['text_comm']                    = 'Комментарий';
$_['text_sett']                    = 'настройки';
$_['text_addmodule']                    = 'Добавить модуль';

// Column
$_['column_date_added']      = 'Дата Добавлена';
$_['entry_date_added']       = 'Дата Добавлена';
$_['column_status']          = 'Статус';
$_['column_image']           = 'Образ';
$_['column_title']           = 'Название блога';
$_['column_sort_order']	     = 'Порядок сортировки';
$_['column_action']          = 'действие';
$_['text_enabled']           = 'Включено';
$_['text_disabled']          = 'инвалид';
$_['column_name']            = 'Название блога';
$_['column_view']            = 'Просмотры';


// Entry
$_['entry_name']            = 'Название блога';
$_['entry_description']      = 'Описание';
$_['entry_store']            = 'магазины';
$_['entry_meta_title'] 	     = 'Название метатега';
$_['entry_meta_keyword'] 	 = 'Ключевые слова метатега';
$_['entry_meta_description'] = 'Описание метатега';
$_['entry_keyword']          = 'Ключевое слово SEO';
$_['entry_bottom']           = 'Низ';
$_['entry_status']           = 'Статус';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_layout']           = 'Переопределение макета';
$_['entry_image']            = 'Образ';
$_['entry_tag']              = 'Теги';
$_['text_none']              = '--Никто--';
$_['entry_tmdblogcategory']  = 'Tmd BlogCategory';
$_['help_tmdblogcategory']   = 'Автозаполнение';

// Help
$_['help_keyword']           = 'Не используйте пробелы, замените пробелы на - и убедитесь, что ключевое слово является глобально уникальным.';
$_['help_bottom']            = 'Дисплей в нижнем колонтитуле.';

// Error
$_['error_warning']          = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission']       = 'Предупреждение: у вас нет разрешения на изменение статьи!';
$_['error_title']            = 'Название блога должно быть от 3 до 64 символов!';
$_['error_description']      = 'Описание должно содержать более 3 символов!';
$_['error_meta_title']       = 'Название мета должно быть больше 3 и меньше 255 символов!';
$_['error_keyword']          = 'SEO ключевое слово уже используется!';
$_['error_account']          = 'Предупреждение: эту страницу статьи нельзя удалить, так как она в настоящее время назначена в качестве условия учетной записи магазина!';
$_['error_checkout']         = 'Предупреждение: эту страницу статьи нельзя удалить, так как она в настоящее время назначена в качестве условий оформления заказа в магазине.!';
$_['error_affiliate']        = 'Предупреждение: эта страница статьи не может быть удалена, так как в настоящее время она назначена условиями аффилированного лица магазина!';
$_['error_return']           = 'Предупреждение: эту страницу статьи нельзя удалить, так как она в настоящее время назначена условиями возврата магазина!';
$_['error_store']            = 'Предупреждение: эту страницу статьи нельзя удалить, так как в настоящее время она используется% s магазинами!';