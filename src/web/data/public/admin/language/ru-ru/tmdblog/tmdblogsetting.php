<?php
// Heading
$_['heading_title']          = 'Настройка блога';

// Text
$_['text_success']           = 'Успех: вы изменили категории!';
$_['text_list']              = 'Список настроек блога Tmd';
$_['text_add']               = 'Добавить настройки блога Tmd';
$_['text_edit']              = 'Изменить настройки блога Tmd';
$_['text_default']           = 'По умолчанию';
$_['text_searchpage']        = 'Страница поиска';
$_['text_categorypage']      = 'Категория блога';
$_['text_relatedblog']       = 'Связанная страница';
$_['text_blogpage']          = 'Страница блога';
$_['text_listpage']          = 'Страница листинга';
$_['text_page']              = 'страница';

// Column
$_['column_name']            = 'Название блога';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'действие';

// Entry
$_['entry_commentlimit']     = 'предел';
$_['entry_name']             = 'Название блога';
$_['entry_description']      = 'Описание';
$_['entry_meta_title'] 	     = 'заглавие';
$_['entry_meta_keyword'] 	 = 'Ключевые слова';
$_['entry_meta_description'] = 'Описание';
$_['entry_keyword']          = 'Ключевое слово SEO';
$_['entry_parent']           = 'родитель';
$_['entry_filter']           = 'фильтры';
$_['entry_store']            = 'магазины';
$_['entry_image']            = 'Образ';
$_['entry_top']              = 'верхний';
$_['entry_column']           = 'Колонны';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_titlesize']        = 'Размер заголовка блога';
$_['entry_titlecolor']       = 'Цвет текста заголовка блога';
$_['entry_thumbimg']         = 'Размер баннера комментария';
$_['entry_comntbanner']      = 'Размер баннера комментария';
$_['text_blogcommentsglobal']= 'Globaly Show / Скрыть комментарии в блоге';
$_['entry_blogcomments']     = 'Комментарии в блоге';
$_['help_blogcomment']       = 'Выберите опцию, чтобы скрыть и показать все комментарии блогов';

$_['entry_article']          = 'Блог';
$_['entry_google']           = 'Доля Google';
$_['entry_pinterest']        = 'Pinterest поделиться';
$_['entry_twitter']          = 'Поделиться в Twitter';
$_['entry_facebook']         = 'Поделиться в Фейсбуке';
$_['entry_feedbackrow']      = 'Коробка обратной связи';
$_['entry_descp']            = 'Описание';
$_['entry_articleimg']       = 'Изображение блога';
$_['entry_width']            = 'ширина';
$_['entry_height']           = 'Рост';

$_['entry_descptextcolor']      = 'Цвет текста описания';
$_['entry_articletextcolor']    = 'Цвет текста блога';
$_['entry_postboxbgcolor']      = 'Цвет фона окна блога';
$_['entry_containerbgcolor']    = 'Фоновый цвет контейнера блога';
$_['entry_titlebgcolor']     	= 'Цвет Темы Блога';
$_['entry_themehovercolor']     = 'Тема блога Hover Color';
$_['entry_titletextcolor']      = 'Цвет текста заголовка';
$_['entry_blogtextcolor']       = 'Цвет текста заголовка при наведении';
$_['entry_bloglayout']    	    = 'раскладка';
$_['entry_blogperpage']      	= 'страница';
$_['entry_blogperrow']    	    = 'Строка';
$_['entry_dispalytitle']    	= 'заглавие';
$_['entry_dispalydes']       	= 'Описание';
$_['entry_blogthumbsize']    	= 'Размер большого пальца';
$_['entry_dispalyblogtimg']    	= 'Образ';
$_['entry_keyword']          = 'Ключевое слово SEO';

$_['help_meta_title'] 	    	= 'Название метатега';
$_['help_meta_keyword'] 		= 'Ключевые слова метатега';
$_['help_meta_description'] 	= 'Описание метатега';
$_['help_blogthumbsize']   		= 'Блог Блог Thumb <br/> Размер(W*H)';

$_['help_article']           	= 'Включить или отключить блог';
$_['help_google']           	= 'Включить или отключить Google Share';
$_['help_pinterest']            = 'Включить или отключить общий доступ к Pinterest';
$_['help_twitter']           	= 'Включить или отключить Twitter';
$_['help_facebook']           	= 'Включить или отключить Facebook';
$_['help_feedbackrow']          = 'Включить или отключить окно обратной связи';
$_['help_descp']           		= 'Включить или отключить описание';
$_['help_articleimg']           = 'Включить или отключить изображение блога';
$_['help_keyword']           = 'Не используйте пробелы, замените пробелы на - и убедитесь, что ключевое слово является глобально уникальным.';

$_['tab_setting']           	= 'настройка';
$_['tab_color']             	= 'Настроить цвет';
$_['tab_main']             		= 'генеральный';
$_['tab_blogseting']            = 'Настройка блога';
$_['tab_support']               = 'Служба поддержки';

// Help
$_['help_filter']            	= '(Автозаполнение)';
$_['help_keyword']           	= 'Не используйте пробелы, замените пробелы на - и убедитесь, что ключевое слово является глобально уникальным.';
$_['help_top']               	= 'Отображение в верхней строке меню. Работает только для верхних родительских категорий.';
$_['help_column']            	= 'Количество столбцов для использования в нижних 3 категориях. Работает только для верхних родительских категорий.';

// Error
$_['error_warning']          	= 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission']       	= 'Предупреждение: у вас нет прав на изменение категорий!';
$_['error_name']             	= 'Внимание! У вас нет разрешения на изменение. Заголовок блога должен содержать от 2 до 32 символов!';
$_['error_meta_title']       	= 'Заголовок мета-блога должен содержать не менее 3 и не более 255 символов!';
$_['error_keyword']          = 'SEO ключевое слово уже используется!';