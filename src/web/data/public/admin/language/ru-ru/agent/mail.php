<?php
// Heading
$_['heading_title']     	 = 'Шаблон почты агента';

// Text
$_['text_success']       	 = 'Успех: вы изменили !';
$_['text_list']           	 = 'Список шаблонов электронной почты для отправки агенту';
$_['text_add']          	 = 'Добавить почту';
$_['text_edit']         	 = 'Редактировать почту';
$_['text_default']      	 = 'По умолчанию';
$_['text_enable']      	     = 'включить';
$_['text_disable']      	 = 'запрещать';
$_['text_agentregister']     = 'агент_регистр_почта';
$_['text_agentapproved']     = 'агент_одобренный_почта';
$_['text_agentproperty']     = 'агент_add_имущество_админ_почта';
$_['text_propertyapp']       = 'имущество_одобренный_почта';
$_['text_agentforgotte']     = 'агент_забытый_почта';

// Column
$_['column_name']       	 = 'название';
$_['column_date']        	 = 'Дата Добавлена';
$_['column_action']      	 = 'действие';

// Lable
$_['lable_subject']          = 'Предмет';
$_['lable_message']          = 'Сообщение';
$_['lable_name']             = 'название';
$_['lable_status']           = 'Статус';
$_['lable_type']             = 'Тип';

// Entry
$_['entry_subject']          = 'Предмет';
$_['entry_message']          = 'Сообщение';
$_['entry_name']             = 'название';
$_['entry_status']           = 'Статус';
$_['entry_type']             = 'Тип';

/// Tab
$_['tab_mail']               = 'почта';
$_['tab_info']               = 'Информация';

// Error
$_['error_warning'] 		 = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 		 = 'Предупреждение: у вас нет разрешения на изменение производителей!';
$_['error_name']             = 'Имя должно быть от 2 до 64 символов!';
$_['error_type']             = 'Пожалуйста, выберите тип почты';