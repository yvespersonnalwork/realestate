<?php

// Heading

$_['heading_title']                = 'Настройки сайта';



// Text

$_['text_stores']                  = 'Веб-сайт';

$_['tab_store']                  = 'Веб-сайт';

$_['text_success']                 = 'Успех: вы изменили настройки сайта!';

$_['text_edit']                    = 'Изменить настройки сайта';

$_['text_product']                 = 'список';

$_['text_review']                  = 'Отзывы';

$_['text_voucher']                 = 'Ваучеры';

$_['text_tax']                     = 'налоги';

$_['text_account']                 = 'учетная запись';

$_['text_checkout']                = 'Проверять, выписываться';

$_['text_stock']                   = 'Склад';

$_['text_affiliate']               = 'Филиалы';

$_['text_captcha']                 = 'Защитный код';

$_['text_register']                = 'регистр';

$_['text_guest']                   = 'Гостевой заказ';

$_['text_return']                  = 'Возвращает';

$_['text_contact']                 = 'контакт';

$_['text_mail']                    = 'почта';

$_['text_smtp']                    = 'SMTP';

$_['text_mail_alert']              = 'Почтовые оповещения';

$_['text_mail_account']            = 'регистр';

$_['text_mail_affiliate']          = 'Добавить недвижимость';

$_['text_mail_order']              = 'заказы';

$_['text_mail_review']             = 'Отзывы';

$_['text_general']                 = 'генеральный';

$_['text_security']                = 'Безопасность';

$_['text_upload']                  = 'Загрузки';

$_['text_error']                   = 'Обработка ошибок';

//label name

$_['text_meta_title']             = 'Мета Название';

$_['text_meta_description']       = 'Описание метатега';

$_['text_meta_keyword']           = 'Ключевые слова метатега';

$_['text_layout']                 = 'Макет по умолчанию';

$_['text_theme']                  = 'тема';

$_['text_name']                   = 'Название сайта';

$_['text_owner']                  = 'Владелец сайта';

$_['text_address']                = 'Адрес';

$_['text_email']                   = 'Эл. почта';

$_['text_telephone']               = 'телефон';

$_['text_map']                     = 'Вставить код карты';

$_['text_mapkey']              = 'Ключ Google Map';

$_['text_image']                  = 'Образ';

$_['text_location']               = 'Расположение сайта';

$_['text_country']                = 'Страна';

$_['text_zone']                   = 'Регион / Штат';

$_['text_language']               = 'язык';

$_['text_admin_language']         = 'Административный язык';

$_['text_currency']               = 'валюта';

$_['text_currency_auto']          = 'Автообновление валюты';

$_['text_length_class']           = 'Длина класса';

$_['text_weight_class']           = 'Весовая категория';

$_['text_limit_admin']            = 'Элементы по умолчанию на странице (Admin)';

$_['text_product_count']          = 'Количество категорий';

$_['text_customer_online']        = 'Клиенты онлайн';

$_['text_customer_activity']      = 'Деятельность клиентов';

$_['text_customer_search']        = 'Журнал Поиск клиентов';

$_['text_customer_group']         = 'Группа клиентов';

$_['text_customer_group_display'] = 'Группы клиентов';

$_['text_customer_price']         = 'Вход Отображение цен';

$_['text_login_attempts']         = 'Максимальное количество попыток входа';

$_['text_account']                = 'Условия аккаунта';

$_['text_captcha']                = 'Защитный код';

$_['text_captcha_page']           = 'Captcha Page';

$_['text_logo']                   = 'Логотип сайта';

$_['text_icon']                   = 'Значок';

$_['text_ftp_hostname']           = 'FTP хозяин';

$_['text_ftp_port']               = 'FTP порт';

$_['text_ftp_username']           = 'FTP имя пользователя';

$_['text_ftp_password']           = 'FTP пароль';

$_['text_ftp_root']               = 'FTP корень';

$_['text_ftp_status']             = 'включить FTP';

$_['text_mail_protocol']          = 'Почтовый протокол';

$_['text_mail_parameter']         = 'Параметры почты';

$_['text_mail_smtp_hostname']     = 'Имя хоста SMTP';

$_['text_mail_smtp_username']     = 'Имя пользователя SMTP';

$_['text_mail_smtp_password']     = 'Пароль SMTP';

$_['text_mail_smtp_port']         = 'Порт SMTP';

$_['text_mail_smtp_timeout']      = 'Тайм-аут SMTP';

$_['text_mail_alert']             = 'Alert Mail';

$_['text_mail_alert_email']       = 'Дополнительная электронная почта';

$_['text_secure']                 = 'Использовать SSL';

$_['text_shared']                 = 'Использовать общие сеансы';

$_['text_robots']                 = 'Роботы';

$_['text_seo_url']                = 'Используйте SEO URL';

$_['text_file_max_size']	       = 'Максимальный размер файла';

$_['text_file_ext_allowed']       = 'Разрешенные расширения файлов';

$_['text_file_mime_allowed']      = 'Разрешенные типы файлов MIME';

$_['text_maintenance']            = 'Режим технического обслуживания';

$_['text_password']               = 'Разрешить забытый пароль';

$_['text_encryption']             = 'Ключ шифрования';

$_['text_compression']            = 'Уровень сжатия на выходе';

$_['text_error_display']          = 'Ошибки отображения';

$_['text_error_log']              = 'Ошибки журнала';

$_['text_error_filename']         = 'Имя файла журнала ошибок';

$_['text_status']                 = 'Статус';

$_['tab_socialmedia']              = 'Footer Social Icons';

$_['text_aboutdes']         	   = 'О нас Описание';

$_['text_title']         	       = 'заглавие:';

$_['text_phoneno']         	   = 'Номер телефона:';

$_['text_mobile']         	       = 'Номер мобильного:';

$_['text_phoneno']         	   = 'Номер телефона:';

$_['text_email']         	       = 'Эл. адрес:';

$_['text_address2']         	   = 'Адрес:';

$_['text_fburl']         	       = 'URL-адрес Facebook:';

$_['text_google']         	       = 'URL Google:';

$_['text_twet']         	       = 'URL твиттера:';

$_['text_in']         	           = 'LinkedIn:';

$_['text_instagram']         	   = 'Instagram:';

$_['text_pinterest']         	   = 'Pinterest';

$_['text_youtube']         	   = 'YouTube';

$_['text_blogger']         	   = 'Blogger';

$_['text_footericon']             = 'Логотип нижнего колонтитула';

// Entry  input  name 

$_['entry_meta_title']             = 'Мета Название';

$_['entry_meta_description']       = 'Описание метатега';

$_['entry_meta_keyword']           = 'Ключевые слова метатега';

$_['entry_layout']                 = 'Макет по умолчанию';

$_['entry_theme']                  = 'тема';

$_['entry_name']                   = 'Название сайта';

$_['entry_owner']                  = 'Владелец сайта';

$_['entry_address']                = 'Адрес';

$_['entry_email']                   = 'Эл. почта';

$_['entry_telephone']               = 'телефон';

$_['entry_map']                     = 'Вставить код карты';

$_['entry_mapkey']              = 'Ключ Google Map';

$_['entry_image']                  = 'Образ';

$_['entry_location']               = 'Расположение сайта';

$_['entry_country']                = 'Страна';

$_['entry_zone']                   = 'Регион / Штат';

$_['entry_language']               = 'язык';

$_['entry_admin_language']         = 'Административный язык';

$_['entry_currency']               = 'валюта';

$_['entry_currency_auto']          = 'Автообновление валюты';

$_['entry_length_class']           = 'Длина класса';

$_['entry_weight_class']           = 'Весовая категория';

$_['entry_limit_admin']            = 'Элементы по умолчанию на странице (Admin)';

$_['entry_product_count']          = 'Количество категорий';

$_['entry_customer_online']        = 'Клиенты онлайн';

$_['entry_customer_activity']      = 'Деятельность клиентов';

$_['entry_customer_search']        = 'Журнал Поиск клиентов';

$_['entry_customer_group']         = 'Группа клиентов';

$_['entry_customer_group_display'] = 'Группы клиентов';

$_['entry_customer_price']         = 'Вход Отображение цен';

$_['entry_login_attempts']         = 'Максимальное количество попыток входа';

$_['entry_account']                = 'Условия аккаунта';

$_['entry_captcha']                = 'Защитный код';

$_['entry_captcha_page']           = 'Captcha Page';

$_['entry_logo']                   = 'Логотип сайта';

$_['entry_icon']                   = 'Значок';

$_['entry_ftp_hostname']           = 'FTP хозяин';

$_['entry_ftp_port']               = 'FTP порт';

$_['entry_ftp_username']           = 'FTP имя пользователя';

$_['entry_ftp_password']           = 'FTP пароль';

$_['entry_ftp_root']               = 'FTP корень';

$_['entry_ftp_status']             = 'Включить FTP';

$_['entry_mail_protocol']          = 'Почтовый протокол';

$_['entry_mail_parameter']         = 'Параметры почты';

$_['entry_mail_smtp_hostname']     = 'Имя хоста SMTP';

$_['entry_mail_smtp_username']     = 'Имя пользователя SMTP';

$_['entry_mail_smtp_password']     = 'Пароль SMTP';

$_['entry_mail_smtp_port']         = 'Порт SMTP';

$_['entry_mail_smtp_timeout']      = 'Тайм-аут SMTP';

$_['entry_mail_alert']             = 'Alert Mail';

$_['entry_mail_alert_email']       = 'Дополнительная электронная почта';

$_['entry_secure']                 = 'Использовать SSL';

$_['entry_shared']                 = 'Использовать общие сеансы';

$_['entry_robots']                 = 'Роботы';

$_['entry_seo_url']                = 'Используйте SEO URL';

$_['entry_file_max_size']	       = 'Максимальный размер файла';

$_['entry_file_ext_allowed']       = 'Разрешенные расширения файлов';

$_['entry_file_mime_allowed']      = 'Разрешенные типы файлов MIME';

$_['entry_maintenance']            = 'Режим технического обслуживания';

$_['entry_password']               = 'Разрешить забытый пароль';

$_['entry_encryption']             = 'Ключ шифрования';

$_['entry_compression']            = 'Уровень сжатия на выходе';

$_['entry_error_display']          = 'Ошибки отображения';

$_['entry_error_log']              = 'Ошибки журнала';

$_['entry_error_filename']         = 'Имя файла журнала ошибок';

$_['entry_status']                 = 'Статус';

$_['tab_socialmedia']              = 'Footer Social Icons';

$_['entry_aboutdes']         	   = 'О нас Описание';

$_['entry_title']         	       = 'заглавие:';

$_['entry_phoneno']         	   = 'Номер телефона:';

$_['entry_mobile']         	       = 'Номер мобильного:';

$_['entry_phoneno']         	   = 'Номер телефона:';

$_['entry_email']         	       = 'Эл. адрес:';

$_['entry_address2']         	   = 'Адрес:';

$_['entry_fburl']         	       = 'URL-адрес Facebook:';

$_['entry_google']         	       = 'URL Google:';

$_['entry_twet']         	       = 'URL твиттера:';

$_['entry_in']         	           = 'LinkedIn:';

$_['entry_instagram']         	   = 'Instagram:';

$_['entry_pinterest']         	   = 'Pinterest';

$_['entry_youtube']         	   = 'YouTube';

$_['entry_blogger']         	   = 'Blogger';

$_['entry_footericon']             = 'Логотип нижнего колонтитула';



// Help

$_['help_location']                = 'Различные местоположения вашего сайта, которые вы хотите отобразить в форме обратной связи.';

$_['help_currency']                = 'Изменить валюту по умолчанию. Очистите кеш браузера, чтобы увидеть изменения и сбросить существующие cookie.';

$_['help_currency_auto']           = 'Настройте свой веб-сайт для автоматического обновления валют ежедневно.';

$_['help_limit_admin']   	       = 'Определяет, сколько элементов администратора показано на странице (заказы, клиенты и т. Д.)).';

$_['help_product_count']           = 'Показать количество товаров в подкатегориях в меню категорий заголовка веб-сайта. Имейте в виду, что это приведет к экстремальному снижению производительности в магазинах с большим количеством подкатегорий!';

$_['help_customer_online']         = 'Отслеживание клиентов онлайн через раздел отчетов клиентов.';

$_['help_customer_activity']       = 'Отслеживание активности клиентов через раздел отчетов клиентов.';

$_['help_customer_group']          = 'Группа клиентов по умолчанию.';

$_['help_customer_group_display']  = 'Отображение групп клиентов, которые новые клиенты могут выбрать для использования, например, оптовые и бизнес, при регистрации.';

$_['help_customer_price']          = 'Показывать цены только при входе клиента.';

$_['help_login_attempts']          = 'Максимально допустимое количество попыток входа в систему до блокировки учетной записи в течение 1 часа. Аккаунты клиентов и партнеров могут быть разблокированы на страницах администрирования клиентов или партнеров.';

$_['help_account']                 = 'Заставляет людей соглашаться с условиями до создания учетной записи.';









$_['help_captcha']                 = 'Капчу использовать для регистрации, логина, контактов и отзывов.';

$_['help_icon']                    = 'Значок должен быть в формате PNG размером 16 x 16 пикселей.';

$_['help_ftp_root']                = 'Каталог, в котором хранится ваша установка RealEstate. Обычно \ 'public_html/\'.';

$_['help_mail_protocol']           = 'Выбирайте «Mail» только если ваш хост не отключил функцию php mail.';

$_['help_mail_parameter']          = 'При использовании \ 'Mail \', дополнительные параметры почты могут быть добавлены здесь (e.g. -f email@storeaddress.com).';

$_['help_mail_smtp_hostname']      = 'Add \'tls://\' or \'ssl://\' префикс, если требуется защищенное соединение. (e.g. tls://smtp.gmail.com, ssl://smtp.gmail.com).';

$_['help_mail_smtp_password']      = 'Для Gmail вам может понадобиться установить пароль для конкретного приложения здесь: https://security.google.com/settings/security/apppasswords.';

$_['help_mail_alert']              = 'Выберите, какие функции вы хотите получать по электронной почте, когда клиент использует их.';

$_['help_mail_alert_email']        = 'Любые дополнительные электронные письма, которые вы хотите получать по электронной почте, в дополнение к основной электронной почте магазина. (разделенные запятой).';

$_['help_secure']                  = 'Чтобы использовать SSL, проверьте на своем хосте, установлен ли сертификат SSL, и добавьте URL-адрес SSL в каталог и файлы конфигурации администратора.';

$_['help_shared']                  = 'Попробуйте поделиться сессионным cookie между магазинами, чтобы корзину можно было передавать между разными доменами.';

$_['help_robots']                  = 'Список пользовательских агентов веб-сканера, с которыми не будут использоваться общие сеансы. Используйте отдельные строки для каждого пользовательского агента.';

$_['help_seo_url']                 = 'Чтобы использовать SEO URL, необходимо установить mod-rewrite модуля apache и переименовать htaccess.txt в .htaccess.';

$_['help_file_max_size']		   = 'Максимальный размер файла изображения, который вы можете загрузить в Image Manager. Введите как байт.';

$_['help_file_ext_allowed']        = 'Добавьте, какие расширения файлов разрешено загружать. Используйте новую строку для каждого значения.';

$_['help_file_mime_allowed']       = 'Добавьте, какие типы файлов MIME могут быть загружены. Используйте новую строку для каждого значения.';

$_['help_maintenance']             = 'Запрещает клиентам просматривать ваш магазин. Вместо этого они увидят сообщение об обслуживании. Если вы вошли в систему как администратор, вы увидите магазин как обычно.';

$_['help_password']                = 'Разрешить использование забытого пароля для администратора. Это будет отключено автоматически, если система обнаружит попытку взлома.';

$_['help_encryption']              = 'Пожалуйста, предоставьте секретный ключ, который будет использоваться для шифрования личной информации при обработке заказов.';

$_['help_compression']             = 'GZIP для более эффективной передачи запрашивающим клиентам. Уровень сжатия должен быть между 0 - 9.';



// Error

$_['error_warning']                = 'Внимание: пожалуйста, внимательно проверьте форму на наличие ошибок!';

$_['error_permission']             = 'Предупреждение: у вас нет прав на изменение настроек!';

$_['error_meta_title']             = 'Название должно быть от 3 до 32 символов!';

$_['error_name']                   = 'Название сайта должно быть от 3 до 32 символов!';

$_['error_owner']                  = 'Владелец сайта должен содержать от 3 до 64 символов!';

$_['error_address']                = 'Адрес сайта должен быть от 10 до 256 символов!';

$_['error_mapkey']                 = 'Пожалуйста, введите ключ карты';

$_['error_map']                    = 'Пожалуйста, введите карту Google Map код для вставки';

$_['error_email']                  = 'Адрес электронной почты не является действительным!';

$_['error_telephone']              = 'Телефон должен быть от 3 до 32 символов!';

$_['error_limit']       	       = 'Требуется лимит!';

$_['error_login_attempts']         = 'Количество попыток входа должно быть больше 0!';

$_['error_customer_group_display'] = 'Вы должны включить группу клиентов по умолчанию, если вы собираетесь использовать эту функцию!';



$_['error_processing_status']      = 'Вы должны выбрать как минимум 1 статус заказа';



$_['error_ftp_hostname']           = 'Требуется FTP-хост!';

$_['error_ftp_port']               = 'Требуется порт FTP!';

$_['error_ftp_username']           = 'Требуется имя пользователя FTP!';

$_['error_ftp_password']           = 'Требуется пароль FTP!';

$_['error_error_filename']         = 'Требуется имя файла журнала ошибок!';

$_['error_malformed_filename']	   = 'Ошибка неверного имени файла журнала!';

$_['error_encryption']             = 'Ключ шифрования должен содержать от 32 до 1024 символов!';

