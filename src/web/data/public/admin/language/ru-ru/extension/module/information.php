<?php

// Heading

$_['heading_title']    = 'Информация';
// Text

$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успешно: Вы изменили информационный модуль!';
$_['text_edit']        = 'Редактировать информационный модуль';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение информационного модуля!';