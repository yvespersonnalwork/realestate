<?php
// Heading
$_['heading_title']    		= 'TMD Блог Фильтр Продукт<b>(TMD)</b>';

// Text
$_['text_extension']        = 'расширения';
$_['text_success']     		= 'Успех: Вы изменили модуль fliterproduct!';
$_['text_edit']        		= 'Редактировать модуль fliterproduct';
$_['text_tab']         		= 'табуляция';
$_['text_vertcal']     		= 'Вертикальный / Normal';
$_['text_normal']      		= 'Нормальный';
$_['text_crousal']     		= 'Вертикальная + Carousel';
$_['text_tabcrousal']  		= 'Tab + Carousel';

// Entry
$_['entry_name']       		= 'Имя модуля';
$_['entry_layouttype'] 		= 'Тип макета';
$_['entry_crousaltype']		= 'Тип карусели';
$_['entry_limit']      		= 'предел';
$_['entry_width']      		= 'ширина';
$_['entry_height']     		= 'Рост';
$_['entry_status']     		= 'Статус';
$_['entry_moduletitle']     = 'Заголовок модуля';
$_['entry_title']       	= 'Название модуля';
$_['entry_product']     	= 'Товар';

// Help


$_['tab_recentpost']     = 'Недавний пост Модуль';
$_['tab_popular']     = 'Популярный модуль';
$_['tab_comment']     = 'Комментарий';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля fliterproduct!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина требуется!';
$_['error_height']     = 'Требуемая высота!';