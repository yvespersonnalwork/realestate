<?php

// Heading

$_['heading_title']    = 'Баннер';



// lable

$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успех: Вы изменили модуль баннера!';
$_['text_edit']        = 'Редактировать модуль баннера';
$_['text_name']        = 'Имя модуля';
$_['text_banner']      = 'Баннер';
$_['text_dimension']   = 'Размер (Ш х В) и тип изменения размера';
$_['text_width']       = 'ширина';
$_['text_height']      = 'Рост';
$_['text_status']      = 'Статус';


// Entry

$_['entry_name']       = 'Имя модуля';

$_['entry_banner']     = 'Баннер';

$_['entry_dimension']  = 'Баннер';

$_['entry_width']      = 'ширина';

$_['entry_height']     = 'Рост';

$_['entry_status']     = 'Статус';



// Error

$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля баннера!';

$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';

$_['error_width']      = 'Ширина требуется!';

$_['error_height']     = 'Требуемая высота!';