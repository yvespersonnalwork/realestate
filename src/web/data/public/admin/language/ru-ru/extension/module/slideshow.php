<?php

// Heading

$_['heading_title']    = 'Слайд-шоу';


// Text

$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успешно: Вы изменили модуль слайд-шоу!';
$_['text_edit']        = 'Редактировать модуль слайд-шоу';

/// Lable text
$_['text_name']       = 'Имя модуля';
$_['text_banner']     = 'Баннер';
$_['text_width']      = 'ширина';
$_['text_height']     = 'Рост';
$_['text_status']     = 'Status';


// Entry
$_['entry_named']      = 'Имя модуля';
$_['entry_banner']     = 'Баннер';
$_['entry_width']      = 'ширина';
$_['entry_height']     = 'Рост';
$_['entry_status']     = 'Status';

// Error

$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля слайд-шоу!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина требуется!';
$_['error_height']     = 'Требуемая высота!';