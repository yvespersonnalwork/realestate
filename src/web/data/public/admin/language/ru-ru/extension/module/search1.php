<?php

// Heading

$_['heading_title']    = 'Поиск';



// Text

$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успешно: Вы изменили модуль поиска!';
$_['text_edit']        = 'Редактировать модуль поиска';
// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'Предупреждение. У вас нет прав на изменение модуля поиска!';