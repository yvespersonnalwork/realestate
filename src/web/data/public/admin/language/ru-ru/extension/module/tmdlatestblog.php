<?php
// Heading
$_['heading_title']    = 'Tmd Последний блог';

// Text
$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успешно: Вы изменили модуль tmdlatestblog!';
$_['text_edit']        = 'Редактировать Tmd Последний модуль блога';

// Entry
$_['entry_name']       = 'Имя модуля';
$_['entry_limit']      = 'предел';
$_['entry_width']      = 'ширина';
$_['entry_height']     = 'Рост';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля tmdlatestblog!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина требуется!';
$_['error_height']     = 'Требуемая высота!';