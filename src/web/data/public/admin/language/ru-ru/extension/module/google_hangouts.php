<?php

// Heading

$_['heading_title']    = 'Google Hangouts';


// Text

$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успешно: Вы изменили модуль Google Hangouts!';
$_['text_edit']        = 'Изменить модуль Google Hangouts';

// Entry

$_['entry_code']       = 'Google Talk Code';
$_['entry_status']     = 'Статус';

// Help

$_['help_code']        = 'Идти к <a href="https://developers.google.com/+/hangouts/button" target="_blank">Создать значок чата Google Hangout</a> and copy &amp; вставьте сгенерированный код в текстовое поле.';

// Error

$_['error_permission'] = 'Внимание! У вас нет прав на изменение модуля Google Hangouts!';

$_['error_code']       = 'Код требуется';