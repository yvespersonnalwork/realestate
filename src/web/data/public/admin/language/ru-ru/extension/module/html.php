<?php

// Heading

$_['heading_title']     = 'HTML-контент';



// Text

$_['text_extension']    = 'расширения';
$_['text_success']      = 'Успешно: Вы изменили модуль HTML Content!';
$_['text_edit']         = 'Модуль редактирования содержимого HTML';
// lable
$_['text_name']        = 'Имя модуля';
$_['text_title']       = 'Заголовок заголовка';
$_['text_description'] = 'Описание';
$_['text_status']      = 'Статус';
// Entry
$_['entry_name']        = 'Имя модуля';
$_['entry_title']       = 'Заголовок заголовка';
$_['entry_description'] = 'Описание';
$_['entry_status']      = 'Статус';

// Error
$_['error_permission']  = 'Предупреждение: у вас нет прав на изменение модуля содержимого HTML!';
$_['error_name']        = 'Имя модуля должно быть от 3 до 64 символов!';