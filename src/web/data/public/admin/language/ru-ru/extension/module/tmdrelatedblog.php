<?php
// Heading
$_['heading_title']    = 'Связанный Блог Tmd';

// Text
$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успех: Вы изменили модуль Tmd Related Blog!';
$_['text_edit']        = 'Редактировать модуль блога Tmd';

// Entry
$_['entry_name']       = 'Имя модуля';
$_['entry_product']    = 'Товары';
$_['entry_limit']      = 'предел';
$_['entry_width']      = 'ширина';
$_['entry_height']     = 'Рост';
$_['entry_status']     = 'Статус';

// Help
$_['help_product']     = '(Автозаполнение)';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля Tmd Related Blog!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина требуется!';
$_['error_height']     = 'Требуемая высота!';