<?php

// Heading
$_['heading_title']    = 'Агент Правая сторона';

// Text

$_['text_extension']   = 'расширения';
$_['text_success']     = 'Успех: вы изменили модуль агента!';
$_['text_edit']        = 'Редактировать модуль агента';

// Lable
$_['lable_name']       = 'Имя модуля';
$_['lable_limit']      = 'предел';
$_['lable_width']      = 'ширина';
$_['lable_height']     = 'Рост';
$_['lable_status']     = 'Статус';

// Entry
$_['entry_name']       = 'Имя модуля';
$_['entry_limit']      = 'предел';
$_['entry_width']      = 'ширина';
$_['entry_height']     = 'Рост';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'Предупреждение: у вас нет прав на изменение модуля агента!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_height']     = 'Требуемая высота!';