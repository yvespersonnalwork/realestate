<?php

// Heading

$_['heading_title']       = 'Фотогалерея (TMD)';



// Lable

$_['text_extension']      = 'расширения';
$_['text_success']        = 'Успех: Вы модифицировали модуль TMD Photo Gallery!';
$_['text_content_top']    = 'Содержание Top';
$_['text_content_bottom'] = 'Содержание снизу';
$_['text_column_left']    = 'Колонка слева';
$_['text_column_right']   = 'Правая колонка';
$_['text_edit']           = 'Редактировать фотогалерею (TMD)';

$_['text_name']           = 'Имя модуля';
$_['text_limit']          = 'предел'; 
$_['text_image']          = 'Изображение (Ш х В) и тип изменения размера';
$_['text_layout']         = 'раскладка';
$_['text_position']       = 'Позиция';
$_['text_status']         = 'Статус';
$_['text_sort_order']     = 'Порядок сортировки:';
$_['text_album']    	  = 'Альбом';
$_['text_width']          = 'ширина';
$_['text_height']         = 'Рост';

// input value Entry
$_['entry_name']          = 'Имя модуля';
$_['entry_limit']         = 'предел'; 
$_['entry_image']         = 'Изображение (Ш х В) и тип изменения размера';
$_['entry_layout']        = 'раскладка';
$_['entry_position']      = 'Позиция';
$_['entry_status']        = 'Статус';
$_['entry_sort_order']    = 'Порядок сортировки:';
$_['entry_album']    	  = 'Альбом';
$_['entry_width']         = 'ширина';
$_['entry_height']        = 'Рост';

// Error

$_['error_permission']    = 'Внимание: у вас нет прав на изменение модуля TMD Photo Gallery!';

$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';

$_['error_width']      = 'Ширина требуется!';

$_['error_height']     = 'Требуемая высота!';

?>