<?php

// Heading

$_['heading_title']     = 'Баннеры';



// Text

$_['text_success']      = 'Успех: вы изменили баннеры!';

$_['text_list']         = 'Список баннеров';

$_['text_add']          = 'Добавить баннер';

$_['text_edit']         = 'Редактировать баннер';

$_['text_default']      = 'По умолчанию';




// Column

$_['column_name']       = 'Название баннера';
$_['column_status']     = 'Статус';
$_['column_action']     = 'действие';
//label

$_['text_name']        = 'Название баннера';
$_['text_title']       = 'заглавие';
$_['text_link']        = 'Ссылка на сайт';
$_['text_image']       = 'Образ';
$_['text_status']      = 'Статус';
$_['text_sort_order']  = 'Порядок сортировки';
// Entry
$_['entry_name']        = 'Название баннера';
$_['entry_title']       = 'заглавие';
$_['entry_link']        = 'Ссылка на сайт';
$_['entry_image']       = 'Образ';
$_['entry_status']      = 'Статус';
$_['entry_sort_order']  = 'Порядок сортировки';

// Error

$_['error_permission'] = 'Предупреждение: у вас нет разрешения на изменение баннеров!';

$_['error_name']       = 'Название баннера должно быть от 3 до 64 символов!';

$_['error_title']      = 'Название баннера должно быть от 2 до 64 символов!';