<?php
// Heading
$_['heading_title']          = 'Blog Kategorisi';

// Text
$_['text_success']           = 'Başarı: tmdblogcategory'i değiştirdiniz!';
$_['text_list']              = 'TMD Blog Kategori Listesi';
$_['text_add']               = 'TMD Blog Kategorisi Ekle';
$_['text_edit']              = 'TMD Blog Kategorisini Düzenle';
$_['text_default']           = 'Varsayılan';

// Column
$_['column_date_added']      = 'Ekleme Tarihi';
$_['entry_date_added']       = 'Ekleme Tarihi';
$_['column_status']          = 'durum';
$_['column_image']           = 'görüntü';
$_['column_title']           = 'TMD Blog Kategori Başlığı';
$_['column_sort_order']	     = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';
$_['text_enabled']           = 'Etkin';
$_['text_disabled']          = 'engelli';
$_['column_name']            = 'DisabledBlog Kategori Adı';
$_['column_count']           = 'saymak';


// Entry
$_['entry_name']            = 'Blog Kategorisi Adı';
$_['entry_description']      = 'Açıklama';
$_['entry_store']            = 'Mağazalar';
$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['entry_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';
$_['entry_meta_description'] = 'Meta Tag Açıklama';
$_['entry_keyword']          = 'SEO Anahtar Kelime';
$_['entry_bottom']           = 'Alt';
$_['entry_status']           = 'durum';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';
$_['entry_image']            = 'Imagegörüntü';
$_['entry_tag']              = 'Etiketler';
$_['text_none']              = '--Yok--';
$_['entry_parent']           = 'ebeveyn';

// Help
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin genel olarak benzersiz olduğundan emin olun.';
$_['help_bottom']            = 'Alt altbilgide göster.';

// Error
$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: tmdblogcategory'i değiştirme izniniz yok!';
$_['error_title']            = 'TMD Blog Kategori Başlığı 3 ile 64 karakter arasında olmalıdır!';
$_['error_description']      = 'Açıklama 3 karakterden fazla olmalı!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO anahtar kelimesi zaten kullanılıyor!';
$_['error_account']          = 'Uyarı: Bu tmdblogcategory sayfa şu anda mağaza hesabı şartları olarak atandığından silinemiyor!';
$_['error_checkout']         = 'Uyarı: Bu tmdblogcategory sayfa şu anda mağaza ödeme koşulları olarak atandığından silinemiyor!';
$_['error_affiliate']        = 'Uyarı: Bu tmdblogcategory sayfa şu anda mağazaya bağlı kuruluş terimleri olarak atandığından silinemiyor!';
$_['error_return']           = 'Uyarı: Bu tmdblogcategory sayfa şu anda mağaza iade şartları olarak atandığından silinemiyor!';
$_['error_store']            = 'Uyarı: Bu tmdblogcategory sayfası şu anda% s depoları tarafından kullanıldığından silinemez!';