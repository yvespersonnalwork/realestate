<?php
// Heading
$_['heading_title']          = 'Blog';

// Text
$_['text_success']           = 'Başarı: Blog’u değiştirdiniz!';
$_['text_list']              = 'Blog Listesi';
$_['text_add']               = 'Blog ekle';
$_['text_edit']              = 'Blogu Düzenle';
$_['text_default']           = 'Varsayılan';
$_['entry_username']         = 'Kullanıcı adı';
$_['entry_blogcoment']       = 'Yorumları Göster';
$_['tab_comment']            = 'Yorumlar';
$_['column_customer']        = 'Müşteri';
$_['column_comment']         = 'Yorum Yap';
$_['text_no_comment']        = 'Yorum yok ';
$_['text_dash']                     = 'gösterge paneli';
$_['text_blog']                    = 'Bloglar';
$_['text_cate']                   = 'Kategori';
$_['text_comm']                    = 'Yorum Yap';
$_['text_sett']                    = 'Ayarlar';
$_['text_addmodule']                    = 'Modül ekle';

// Column
$_['column_date_added']      = 'Ekleme Tarihi';
$_['entry_date_added']       = 'Ekleme Tarihi';
$_['column_status']          = 'durum';
$_['column_image']           = 'görüntü';
$_['column_title']           = 'Blog Adı';
$_['column_sort_order']	     = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';
$_['text_enabled']           = 'Etkin';
$_['text_disabled']          = 'engelli';
$_['column_name']            = 'Blog Adı';
$_['column_view']            = 'Görünümler';


// Entry
$_['entry_name']            = 'Blog Adı';
$_['entry_description']      = 'Açıklama';
$_['entry_store']            = 'Mağazalar';
$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['entry_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';
$_['entry_meta_description'] = 'Meta Tag Açıklama';
$_['entry_keyword']          = 'SEO Anahtar Kelime';
$_['entry_bottom']           = 'Alt';
$_['entry_status']           = 'durum';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';
$_['entry_image']            = 'görüntü';
$_['entry_tag']              = 'Etiketler';
$_['text_none']              = '--Yok--';
$_['entry_tmdblogcategory']  = 'Tmd BlogCategory';
$_['help_tmdblogcategory']   = 'Otomatik tamamlama';

// Help
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin genel olarak benzersiz olduğundan emin olun.';
$_['help_bottom']            = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin altbilgide global olarak benzersiz bir ekran olduğundan emin olun.';

// Error
$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Makaleyi değiştirme izniniz yok!';
$_['error_title']            = 'Blog adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_description']      = 'Açıklama 3 karakterden fazla olmalı!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO anahtar kelimesi zaten kullanılıyor!';
$_['error_account']          = 'Uyarı: Bu makale sayfası şu anda mağaza hesabı şartları olarak atandığından silinemiyor!';
$_['error_checkout']         = 'Uyarı: Bu makale sayfası şu anda mağaza ödeme koşulları olarak atandığından silinemiyor!';
$_['error_affiliate']        = 'Uyarı: Bu makale sayfası şu anda mağazaya bağlı kuruluş koşulları olarak atandığından silinemiyor!';
$_['error_return']           = 'Uyarı: Bu makale sayfası şu anda mağaza iade koşulları olarak atandığından silinemiyor!';
$_['error_store']            = 'Uyarı: Bu makale sayfası şu anda% s mağazaları tarafından kullanıldığından silinemiyor!';