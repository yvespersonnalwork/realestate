<?php
// Heading
$_['heading_title']          = 'Blog Ayarı';

// Text
$_['text_success']           = 'Başarı: Kategorileri değiştirdiniz!';
$_['text_list']              = 'Tmd Blog Ayar Listesi';
$_['text_add']               = 'Tmd Blog Ayarı Ekleyin';
$_['text_edit']              = 'Tmd Blog Ayarını Düzenle';
$_['text_default']           = 'Varsayılan';
$_['text_searchpage']        = 'Arama Sayfası';
$_['text_categorypage']      = 'Blog Kategorisi';
$_['text_relatedblog']       = 'İlgili Sayfa';
$_['text_blogpage']          = 'Blog Sayfası';
$_['text_listpage']          = 'İlan Sayfası';
$_['text_page']              = 'Sayfa';

// Column
$_['column_name']            = 'blog başlığı';
$_['column_sort_order']      = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';

// Entry
$_['entry_commentlimit']     = 'Limit';
$_['entry_name']             = 'blog başlığı';
$_['entry_description']      = 'Açıklama';
$_['entry_meta_title'] 	     = 'Başlık';
$_['entry_meta_keyword'] 	 = 'Anahtar kelimeler';
$_['entry_meta_description'] = 'Açıklama';
$_['entry_keyword']          = 'SEO Kelimeler';
$_['entry_parent']           = 'ebeveyn';
$_['entry_filter']           = 'Filtreler';
$_['entry_store']            = 'Mağazalar';
$_['entry_image']            = 'görüntü';
$_['entry_top']              = 'Üst';
$_['entry_column']           = 'Sütunlar';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_titlesize']        = 'Blog Başlığı Boyutu';
$_['entry_titlecolor']       = 'Blog Başlığı Metin Rengi';
$_['entry_thumbimg']         = 'Yorum Afiş Başlığı Boyutu';
$_['entry_comntbanner']      = 'Yorum Afiş Boyutu';
$_['text_blogcommentsglobal']= 'Globaly Show/Hide Blog Comments';
$_['entry_blogcomments']     = 'Blog Yorumları';
$_['help_blogcomment']       = 'Tüm blogların yorumlarını gizle ve göster seçeneği';

$_['entry_article']          = 'Blog';
$_['entry_google']           = 'Google paylaşımı';
$_['entry_pinterest']        = 'Pinterest paylaş';
$_['entry_twitter']          = 'Twitter paylaşımı';
$_['entry_facebook']         = 'Facebook paylaşımı';
$_['entry_feedbackrow']      = 'Geri bildirim kutusu';
$_['entry_descp']            = 'Açıklama';
$_['entry_articleimg']       = 'Blog Resmi';
$_['entry_width']            = 'Genişlik';
$_['entry_height']           = 'Yükseklik';

$_['entry_descptextcolor']      = 'Açıklama Metin Rengi';
$_['entry_articletextcolor']    = 'Blog Metin Rengi';
$_['entry_postboxbgcolor']      = 'Blog Kutusu Arka Plan Rengi';
$_['entry_containerbgcolor']    = 'Blog Box Background Color';
$_['entry_titlebgcolor']     	= 'Blog Teması Rengi';
$_['entry_themehovercolor']     = 'Blog Teması Vurgulu Rengi';
$_['entry_titletextcolor']      = 'Başlık Rengi';
$_['entry_blogtextcolor']       = 'Başlık Vurgulu Metin Rengi';
$_['entry_bloglayout']    	    = 'düzen';
$_['entry_blogperpage']      	= 'Sayfa';
$_['entry_blogperrow']    	    = 'Kürek çekmek';
$_['entry_dispalytitle']    	= 'Başlık';
$_['entry_dispalydes']       	= 'Açıklama';
$_['entry_blogthumbsize']    	= 'Başparmak boyutu';
$_['entry_dispalyblogtimg']    	= 'Imagegörüntü';
$_['entry_keyword']          = 'SEO Kelimeler';

$_['help_meta_title'] 	    	= 'Meta Etiket Başlığı';
$_['help_meta_keyword'] 		= 'Meta Tag Anahtar Kelimeler';
$_['help_meta_description'] 	= 'Meta Tag Açıklama';
$_['help_blogthumbsize']   		= 'Blog Blog Thumb <br/> Boyut(W*H)';

$_['help_article']           	= 'Blogu Etkinleştir veya Devre Dışı Bırak';
$_['help_google']           	= 'Google paylaşımını etkinleştirin veya devre dışı bırakın';
$_['help_pinterest']            = 'Pinterest paylaşımını etkinleştirin veya devre dışı bırakın';
$_['help_twitter']           	= 'Twitter'ı Etkinleştirme veya Devre Dışı Bırakma';
$_['help_facebook']           	= 'Facebook'u Etkinleştir veya Devre Dışı Bırak';
$_['help_feedbackrow']          = 'Geri Bildirim Kutusunu Etkinleştir veya Devre Dışı Bırak';
$_['help_descp']           		= 'Açıklamayı Etkinleştir veya Devre Dışı Bırak';
$_['help_articleimg']           = 'Blog Resmini Etkinleştirme veya Devre Dışı Bırakma';
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin genel olarak benzersiz olduğundan emin olun.';

$_['tab_setting']           	= 'Ayar';
$_['tab_color']             	= 'Renk Ayarla';
$_['tab_main']             		= 'Genel';
$_['tab_blogseting']            = 'Blog Ayarı';
$_['tab_support']               = 'Destek';

// Help
$_['help_filter']            	= '(Otomatik tamamlama)';
$_['help_keyword']           	= 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin genel olarak benzersiz olduğundan emin olun.';
$_['help_top']               	= 'Üst menü çubuğunda görüntüleyin. Yalnızca üst ana kategoriler için çalışır.';
$_['help_column']            	= 'En alttaki 3 kategori için kullanılacak sütun sayısı. Yalnızca üst ana kategoriler için çalışır.';

// Error
$_['error_warning']          	= 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       	= 'Uyarı: Kategorileri değiştirme izniniz yok!';
$_['error_name']             	= 'Blog Başlığı 2 ile 32 karakter arasında olmalıdır!';
$_['error_meta_title']       	= 'Blog Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO anahtar kelimesi zaten kullanılıyor!';