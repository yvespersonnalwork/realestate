<?php
// Heading
$_['heading_title']          = 'Blog Yorum';

// Text
$_['text_success']           = 'Başarı: Blog Yorumunu değiştirdiniz!';
$_['text_list']              = 'Blog Yorum Listesi';
$_['text_add']               = 'Blog Yorum Ekle';
$_['text_edit']              = 'Blog Yorumunu Düzenle';
$_['text_default']           = 'Varsayılan';
$_['text_no_comment']        = 'Yorum yok ';
$_['text_dash']              = 'gösterge paneli';
$_['text_cate']              = 'Kategori';
$_['text_comm']              = 'Yorum Yap';
$_['text_sett']              = 'Ayarlar';
$_['text_addmodule']         = 'Modül ekle';
$_['text_none']              = '--Yok--';
$_['text_enabled']           = 'Etkin';
$_['text_disabled']          = 'engelli';

// Tab
$_['tab_comment']            = 'Yorumlar';

// Column

$_['column_date_added']      = 'Ekleme Tarihi';
$_['column_status']          = 'durum';
$_['column_image']           = 'görüntü';
$_['column_title']           = 'Blog Yorum Adı';
$_['column_sort_order']	     = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';

$_['column_name']            = 'müşteri adı';
$_['column_view']            = 'Görünümler';
$_['column_customer']        = 'Müşteri';
$_['column_comment']         = 'Yorum Yap';


// Entry
$_['entry_name']             = 'Blog Yorum Adı';
$_['entry_customername']     = 'Müşteri';
$_['entry_status']           = 'durum';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_image']            = 'görüntü';
$_['entry_tmdblog']          = 'Blog Seç';
$_['entry_date_added']       = 'Ekleme Tarihi';
$_['entry_comment']          = 'Yorum Yap';


// Help
$_['help_tmdblog']           = 'Otomatik tamamlama';
$_['help_bottom']            = 'Alt altbilgide göster.';

// Error
$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Makaleyi değiştirme izniniz yok!';
$_['error_title']            = 'Blog Yorum adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_description']      = 'Açıklama 3 karakterden fazla olmalı!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO anahtar kelimesi zaten kullanılıyor!';
$_['error_account']          = 'Uyarı: Bu makale sayfası şu anda mağaza hesabı şartları olarak atandığından silinemiyor!';
$_['error_checkout']         = 'Uyarı: Bu makale sayfası şu anda mağaza ödeme koşulları olarak atandığından silinemiyor!';
$_['error_affiliate']        = 'Uyarı: Bu makale sayfası şu anda mağazaya bağlı kuruluş koşulları olarak atandığından silinemiyor!';
$_['error_return']           = 'Uyarı: Bu makale sayfası şu anda mağaza iade koşulları olarak atandığından silinemiyor!';
$_['error_store']            = 'Uyarı: Bu makale sayfası şu anda% s mağazaları tarafından kullanıldığından silinemiyor!';