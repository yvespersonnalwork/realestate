<?php

// header

$_['heading_title']   = 'Parolanızı mı unuttunuz?';



// Text

$_['text_forgotten']  = 'Unutulan Şifre';

$_['text_your_email'] = 'E-posta adresiniz Adres';

$_['text_email']      = 'Hesabınızla ilişkili e-posta adresini girin. Size e-posta ile gönderilen bir şifre sıfırlama bağlantısının gönderilmesi için gönder düğmesine tıklayın.';

$_['text_success']    = 'Yönetici e-posta adresinizi onay bağlantısını içeren bir e-posta gönderildi.';



// Entry

$_['entry_email']     = 'E-Posta Adres';

$_['entry_password']  = 'Yeni Şifre';

$_['entry_confirm']   = 'Onaylamak';



// Error

$_['error_email']     = 'Uyarı: E-Posta Adresimiz kayıtlarımızda bulunamadı, lütfen tekrar deneyin!';

$_['error_password']  = 'Şifre 4 ila 20 karakter arasında olmalıdır!';

$_['error_confirm']   = 'Şifre 4 ila 20 karakter arasında olmalıdır!';