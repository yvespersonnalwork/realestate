<?php

// Text

$_['text_blogdashboard']               = 'Blog Panosu';
///left side menu
$_['text_setting']                   = 'Ayarlar';
$_['text_themecontrol'] 		 	 = 'Tema Kontrolü';
$_['text_backup']                    = 'Yedekle / Geri yükle';
$_['text_seo_url']                     = 'Seo url';
$_['text_property']                  = 'özellik'; 
$_['text_category']                  = 'İlan Türü / Kategorisi';
$_['text_order_status']              = 'Gayrimenkul Durumları';
$_['text_feature'] 		 			 = 'Özellikler';
$_['text_nearest'] 		 			 = 'En yakın yer';
$_['text_units'] 		 			 = 'Birimler';
$_['text_enquiry'] 		 			 = 'Gayrimenkul Sorgulama';
$_['text_agent']                     = 'ajan';
$_['text_custom_field']              = 'Özel alan';
$_['text_contact']                   = 'Posta';
$_['text_catalog']                   = 'Katalog';
$_['text_country']                   = 'Ülkeler';
$_['text_coupon']                    = 'Kuponlar';
$_['text_currency']                  = 'Kurlar';
$_['text_customer_group']            = 'Müşteri grupları';
$_['text_dashboard']                 = 'gösterge paneli';
$_['text_design']                    = 'dizayn';
$_['text_download']                  = 'İndirilenler';
$_['text_log']                       = 'Hata Günlüğüs';
$_['text_event']                     = 'Olaylar';
$_['text_extension']                 = 'Uzantıları';
$_['text_filter']                    = 'Filtreler';
$_['text_geo_zone']                  = 'Coğrafi Bölgeler';
$_['text_information']               = 'Bilgi Sayfaları';
$_['text_installer']                 = 'Uzantı Yükleyicisi';
$_['text_language']                  = 'duujjil';
$_['text_localisation']              = 'yerelleştirme';
$_['text_location']                  = 'Web sitesi konumu';
$_['text_contact']                   = 'Posta';
$_['text_marketing']                 = 'Pazarlama';
$_['text_menu']                      = 'Menü Yöneticisi';
$_['text_modification']              = 'Değişiklikler';
$_['text_manufacturer']              = 'Üreticiler';
$_['text_option']                    = 'Seçenekler';
$_['text_order']                     = 'Emirler';
$_['text_seo_url']                     = 'Seo url';

$_['text_product']                   = 'Ürünler';

$_['text_reports']                   = 'Raporlar';

$_['text_report_sale']               = 'Satış';

$_['text_report_sale_order']         = 'Emirler';

$_['text_report_sale_tax']           = 'Vergi';

$_['text_report_sale_shipping']      = 'Nakliye';

$_['text_report_sale_return']        = 'İade';

$_['text_report_sale_coupon']        = 'Kuponlar';

$_['text_report_product']            = 'Ürünler';

$_['text_report_product_viewed']     = 'Görüntülenen';

$_['text_report_product_purchased']  = 'satın alındı';

$_['text_report_customer']           = 'Müşteriler';

$_['text_report_customer_activity']  = 'Müşteri etkinliği';

$_['text_report_customer_search']    = 'Müşteri aramaları';

$_['text_report_customer_online']    = 'Müşteriler Çevrimiçi';

$_['text_report_customer_order']     = 'Emirler';

$_['text_report_customer_reward']    = 'Ödül Puanları';

$_['text_report_customer_credit']    = 'Kredi';

$_['text_report_marketing']          = 'Pazarlama';

$_['text_report_affiliate']          = 'Bağlı';

$_['text_report_affiliate_activity'] = 'Ortaklık Etkinliği';

$_['text_review']                    = 'yorumlar';

$_['text_return']                    = 'İade';

$_['text_return_action']             = 'İade İşlemleri';

$_['text_return_reason']             = 'İade Nedenleri';

$_['text_return_status']             = 'Dönüş durumları';

$_['text_sale']                      = 'Satış';

$_['text_setting']                   = 'Ayarlar';

$_['text_store']                     = 'Uzantı Web sitesi';

$_['text_stock_status']              = 'Stok Durumları';



$_['text_tax']                       = 'Vergiler';

$_['text_tax_class']                 = 'Vergi Sınıfları';

$_['text_tax_rate']                  = 'Vergi oranları';

$_['text_translation']               = 'Dil editörü';

$_['text_theme']                     = 'Tema editörü';

$_['text_tools']                     = 'Araçlar';

$_['text_upload']                    = 'Yüklemeler';

$_['text_user']                      = 'Kullanıcılar';

$_['text_voucher']                   = 'hediye çekleri';

$_['text_voucher_theme']             = 'Kupon Temaları';

$_['text_weight_class']              = 'Ağırlık Sınıfları';

$_['text_length_class']              = 'Uzunluk Sınıfları';

$_['text_zone']                      = 'bölgeler';

$_['text_recurring']                 = 'Tekrarlanan Profiller';

$_['text_order_recurring']           = 'Düzenli Siparişler';

$_['text_openbay_extension']         = 'OpenBay Pro';

$_['text_openbay_dashboard']         = 'gösterge paneli';

$_['text_openbay_orders']            = 'Toplu sipariş güncellemesi';

$_['text_openbay_items']             = 'Öğeleri yönet';

$_['text_openbay_ebay']              = 'eBay';

$_['text_openbay_amazon']            = 'Amazon (EU)';

$_['text_openbay_amazonus']          = 'Amazon (US)';

$_['text_openbay_etsy']            	 = 'Etsy';

$_['text_openbay_settings']          = 'Ayarlar';

$_['text_openbay_links']             = 'Öğe bağlantıları';

$_['text_openbay_report_price']      = 'Fiyat raporu';

$_['text_openbay_order_import']      = 'Sipariş içe aktarma';

$_['text_paypal']                    = 'PayPal';

$_['text_paypal_search']             = 'Arama';

$_['text_complete_status']           = 'Tamamlanan Siparişler'; 

$_['text_processing_status']         = 'Sipariş işleme'; 

$_['text_other_status']              = 'Diğer durumlar'; 





$_['text_property']                  = 'özellik'; 

$_['text_order_status']              = 'Gayrimenkul Durumları';

$_['text_category']                  = 'Kategori Listesi / Kategori';

$_['text_customer']                  = 'Mülkleri Listele Müşteri';

$_['text_custom_field']              = 'Özel alan';

$_['text_agent']                     = 'ajan';
$_['text_mail']                      = 'Aracı Posta Şablonu';

$_['text_users']                     = 'kullanıcı';

$_['text_user_group']                = 'Kullanıcı Grupları';

$_['text_pagebuilder']              = 'Sayfa Oluşturucu';

$_['text_layout']                    = 'Sayfa Düzeni Ayarı';

$_['text_banner']                    = 'afiş';

$_['text_module']                    = 'Modül Ayarı';

$_['text_system']                    = 'Ayar';

$_['text_backup']                    = 'Yedekle / Geri yükle';

$_['text_pages']                     = 'Sayfalar';

$_['text_membership']                = 'Üyelik';

$_['text_plans']                     = 'Planlar';

$_['text_variation']                 = 'paket';

$_['text_megaheader1']               = 'Mega Başlık';

$_['text_megaheader']                = 'Mega Başlık Menüsü';

$_['text_newssubscribe']             = 'Bülten Aboneleri';

$_['text_faqcat']   				 = 'SSS Kategorisi';		

$_['text_faq']     					 = 'SSS';

$_['text_testimonial']               = 'bonservis';

$_['text_photo']          			 = 'Fotoğraflar';

$_['text_gallerydashboard'] 		 = 'Galeri Gösterge Tablosu';

$_['text_feature'] 		 			 = 'Özellikler';

$_['text_nearest'] 		 			 = 'En yakın yer';

$_['text_enquiry'] 		 			 = 'Gayrimenkul Sorgulama';

$_['text_agents'] 		 			 = 'Gayrimenkul Acenteleri';

$_['text_themecontrol'] 		 			 = 'Tema Kontrolü';


/* lanuge add */
$_['text_analytics'] 		 			 = 'google ve fb analizleri';



