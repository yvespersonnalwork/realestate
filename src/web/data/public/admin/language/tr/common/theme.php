<?php

// Heading 
$_['heading_title']          = 'Tema';

// Text
$_['text_success']           = 'Başarı: Bilgileri değiştirdiniz!';
$_['text_list']              = 'Tema Listesi';
$_['text_form']              = 'Tema denetleyicisi';
$_['text_edit']              = 'Temayı Düzenle';
$_['text_default']           = 'Varsayılan';
$_['text_header1']           = 'Başlık1';
$_['text_header2']           = 'Başlık2';
$_['text_header3']           = 'Başlık3';
$_['text_header4']           = 'Başlık4';
$_['text_header5']           = 'Başlık5';
$_['text_footer1']           = 'Footer1';
$_['text_footer2']           = 'Footer2';
$_['text_footer3']           = 'Footer3';
$_['text_footer4']           = 'Footer4';
$_['text_agent1']            = 'Kurumu1';
$_['text_agent2']            = 'Kurumu2';
$_['text_agent3']            = 'Kurumu3';
$_['text_proprties1']        = 'Proprties1';
$_['text_proprties2']        = 'Proprties2';
$_['text_proprties3']        = 'Proprties3';
$_['text_search1']        	 = 'Search1';
$_['text_search2']        	 = 'Search2';
$_['text_search3']        	 = 'Search3';

$_['tab_setting']            = 'Ayar';
$_['tab_header']             = 'Başlık';
$_['tab_footer']             = 'Alt Bilgi';
$_['tab_prolayout']          = 'Ana Sayfa Düzenleri';
$_['tab_agent']              = 'Acentemiz Anasayfa';
$_['tab_properites']         = 'Özellikler Düzeni';
$_['tab_search']             = 'Arama Düzeni';
$_['tab_socialmedia']        = 'Altbilgi Sosyal Simgeleri';

// Lable
$_['lable_twittercode']            = 'Twitter Kodu';
$_['lable_fburl']         	       = 'Facebook URL:';
$_['lable_google']         	       = 'Google URL'si:';
$_['lable_twet']         	       = 'Twitter URL'si:';
$_['lable_in']         	           = 'LinkedIn:';
$_['lable_instagram']         	   = 'LinkedIn:';
$_['lable_pinterest']         	   = 'pinterest';
$_['lable_youtube']         	   = 'Youtube';
$_['lable_blogger']         	   = 'Blogger';
$_['lable_footericon']             = 'Altbilgi Logosu';
$_['lable_aboutdes']         	   = 'Hakkımızda Açıklama';
$_['lable_status']                 = 'durum';
$_['lable_aboutdes']         	   = 'Hakkımızda Açıklama';
$_['lable_title']         	       = 'Başlık:';
$_['lable_phoneno']         	   = 'Telefon yok:';
$_['lable_mobile']         	       = 'Telefon numarası:';
$_['lable_phoneno']         	   = 'Telefon yok:';
$_['lable_email']         	       = 'E-posta:';
$_['lable_address2']         	   = 'Adres:';

// Entry
$_['entry_twittercode']            = 'Twitter Kodu';
$_['entry_fburl']         	       = 'Facebook URL:';
$_['entry_google']         	       = 'Google Url:';
$_['entry_twet']         	       = 'Twitter Url:';
$_['entry_in']         	           = 'LinkedIn:';
$_['entry_instagram']         	   = 'Instagram:';
$_['entry_pinterest']         	   = 'pinterest';
$_['entry_youtube']         	   = 'Youtube';
$_['entry_blogger']         	   = 'Blogger';
$_['entry_footericon']             = 'Altbilgi Logosu';
$_['entry_aboutdes']         	   = 'Hakkımızda Açıklama';
$_['entry_title']         	       = 'Başlık:';
$_['entry_mobile']         	       = 'Telefon numarası:';
$_['entry_phoneno']         	   = 'Telefon yok:';
$_['entry_email']         	       = 'E-posta:';
$_['entry_address2']         	   = 'Adres:';

// Error
$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Bilgileri değiştirme izniniz yok!';
$_['error_title']            = 'form Başlığı 3 ile 64 karakter arasında olmalıdır!';
$_['error_description']      = 'Açıklama 400'den fazla karakter olmalı!';
$_['error_name']             = 'Ad, 3'ten büyük ve 64 karakterden küçük olmalıdır!';											   

