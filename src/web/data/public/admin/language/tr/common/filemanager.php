<?php

// Heading

$_['heading_title']    = 'Image Manager';



// Text

$_['text_uploaded']    = 'Success: Your file has been uploaded!';

$_['text_directory']   = 'Success: Directory created!';

$_['text_delete']      = 'Success: Your file or directory has been deleted!';



// Entry

$_['entry_search']     = 'Arama..';

$_['entry_folder']     = 'Klasör adı';



// Error

$_['error_permission'] = 'Uyarı: İzin Reddedildi!';

$_['error_filename']   = 'Uyarı: Dosya adı 3 ile 255 arasında olmalıdır!';

$_['error_folder']     = 'Uyarı: Klasör adı 3 ile 255 arasında olmalıdır!';

$_['error_exists']     = 'Uyarı: Aynı ada sahip bir dosya veya dizin zaten var!';

$_['error_directory']  = 'Uyarı: Dizin mevcut değil!';

$_['error_filesize']   = 'Uyarı: Yanlış dosya boyutu!';

$_['error_filetype']   = 'Uyarı: Yanlış dosya türü!';

$_['error_upload']     = 'Uyarı: Dosya bilinmeyen bir nedenden dolayı yüklenemedi!';

$_['error_delete']     = 'Uyarı: Bu dizini silemezsiniz.!';

