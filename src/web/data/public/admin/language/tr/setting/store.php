<?php

// Heading

$_['heading_title']                = 'Mağazalar';



// Text

$_['text_settings']                = 'Ayarlar';

$_['text_success']                 = 'Başarı: Mağazaları değiştirdiniz!';

$_['text_list']                    = 'Mağaza Listesi';

$_['text_add']                     = 'Mağaza ekle';

$_['text_edit']                    = 'Mağazayı Düzenle';

$_['text_items']                   = 'Öğeler';

$_['text_tax']                     = 'Vergiler';

$_['text_account']                 = 'hesap';

$_['text_checkout']                = 'Çıkış yapmak';

$_['text_stock']                   = 'Stok';

$_['text_shipping']                = 'teslimat adresi';

$_['text_payment']                 = 'fatura adresi';



// Column

$_['column_name']                  = 'Dükkan adı';

$_['column_url']	               = 'Mağaza URL'si';

$_['column_action']                = 'Aksiyon';



// Entry

$_['entry_url']                    = 'Mağaza URL'si';

$_['entry_ssl']                    = 'SSL URL';

$_['entry_meta_title']             = 'Meta Başlığı';

$_['entry_meta_description']       = 'Meta Tag Açıklama';

$_['entry_meta_keyword']           = 'Meta Tag Anahtar Kelimeler';

$_['entry_layout']                 = 'Varsayılan Düzen';

$_['entry_theme']                  = 'Tema';

$_['entry_name']                   = 'Dükkan adı';

$_['entry_owner']                  = 'Dükkan sahibi';

$_['entry_address']                = 'Adres';

$_['entry_email']                  = 'E-Posta';

$_['entry_telephone']              = 'Telefon';

$_['entry_image']                  = 'görüntü';

$_['entry_location']               = 'Mağaza Konumu';

$_['entry_country']                = 'ülke';

$_['entry_zone']                   = 'Bölge / Eyalet';

$_['entry_language']               = 'Dil';

$_['entry_currency']               = 'Para birimi';

$_['entry_tax']                    = 'Vergileri Gösteren Fiyatları Göster';

$_['entry_tax_default']            = 'Mağaza Vergi Adresini Kullan';

$_['entry_tax_customer']           = 'Müşteri Vergi Adresini Kullan';

$_['entry_customer_group']         = 'müşteri grubu';

$_['entry_customer_group_display'] = 'Müşteri grupları';

$_['entry_customer_price']         = 'Giriş Ekran Fiyatları';

$_['entry_account']                = 'Hesap Koşulları';

$_['entry_cart_weight']            = 'Sepet Sayfasındaki Ağırlığı Göster';

$_['entry_checkout_guest']         = 'Konuk Ödeme';

$_['entry_checkout']               = 'Ödeme Koşulları';

$_['entry_order_status']           = 'Sipariş durumu';

$_['entry_stock_display']          = 'Stok Gösterimi';

$_['entry_stock_checkout']         = 'Stok Ödeme';

$_['entry_logo']                   = 'Mağaza Logosu';

$_['entry_icon']                   = 'ikon';

$_['entry_secure']                 = 'SSL kullan';



// Help

$_['help_url']                     = 'Mağazaya tam URL’yi ekleyin. Eklediğinizden emin olun \'/\' sonunda. Example: http://www.yourdomain.com/path/<br /><br />Don\'t yeni bir mağaza oluşturmak için dizinleri kullan. Her zaman başka bir etki alanı veya alt etki alanı barındırma işaret etmelidir.';

$_['help_ssl']                     = 'Mağazaya SSL URL'si. Eklediğinizden emin olun \'/\' sonunda. Example: http://www.yourdomain.com/path/<br /><br />Don\'t Yeni bir mağaza oluşturmak için dizinleri kullanın. Her zaman başka bir etki alanı veya alt etki alanı barındırma işaret etmelidir.';

$_['help_location']                = 'İstediğiniz farklı mağaza konumları bize ulaşın formunda bize ulaşın.';

$_['help_currency']                = 'Varsayılan para birimini değiştirin. Değişikliklerini görmek ve mevcut çerezinizi sıfırlamak için tarayıcı önbelleğinizi temizleyin.';

$_['help_tax_default']             = 'Müşteri giriş yapmamışsa vergileri hesaplamak için mağaza adresini kullanın. Müşterinin gönderim veya ödeme adresi için mağaza adresini kullanmayı seçebilirsiniz.';

$_['help_tax_customer']            = 'Vergileri hesaplamak için giriş yaparken müşterilerin varsayılan adresini kullanın. Müşterinin sevkıyat veya ödeme adresi için varsayılan adresi kullanmayı seçebilirsiniz.';

$_['help_customer_group']          = 'Varsayılan müşteri grubu.';

$_['help_customer_group_display']  = 'Yeni müşterilerin kaydolurken toptan satış ve iş gibi seçebilecekleri müşteri gruplarını görüntüleyin.';

$_['help_customer_price']          = 'Sadece bir müşteri giriş yaptığında fiyatları göster.';

$_['help_account']                 = 'Hesap oluşturmadan önce kişileri şartları kabul etmeye zorlar.';

$_['help_checkout_guest']          = 'Müşterilerin hesap oluşturmadan ödeme yapmasına izin ver. İndirilebilir bir ürün alışveriş sepetindeyken bu kullanılamaz.';

$_['help_checkout']                = 'Bir müşteriyi ödeme yapmadan önce kişileri şartları kabul etmeye zorlar.';

$_['help_order_status']            = 'Bir sipariş işlendiğinde varsayılan sipariş durumunu ayarlayın.';

$_['help_stock_display']           = 'Bir sipariş işlendiğinde varsayılan sipariş durumunu ayarlayın.';

$_['help_stock_checkout']          = 'Sipariş ettikleri ürünler stokta bulunmuyorsa müşterilerin hala ödeme yapmasına izin ver.';

$_['help_icon']                    = 'Simge, 16 piksel x 16 piksel olan bir PNG olmalıdır.';

$_['help_secure']                  = 'Bir SSL sertifikası kuruluysa, sunucunuzu SSL kontrolü ile kullanmak için.';



// Error

$_['error_warning']                = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_permission']             = 'Uyarı: Mağazalarda değişiklik yapma izniniz yok!';

$_['error_url']                    = 'Mağaza URL’si gerekli!';

$_['error_meta_title']             = 'Başlık 3 ile 32 karakter arasında olmalıdır!';

$_['error_name']                   = 'Mağaza Adı 3 ile 32 karakter arasında olmalıdır!';

$_['error_owner']                  = 'Mağaza Sahibinin 3 ile 64 karakter arasında olması gerekir!';

$_['error_address']                = 'Mağaza Adresi 10 ile 256 karakter arasında olmalıdır!';

$_['error_email']                  = 'E-posta adresi geçerli görünmüyor!';

$_['error_telephone']              = 'Telefon 3 - 32 karakter arasında olmalıdır!';

$_['error_customer_group_display'] = 'Bu özelliği kullanacaksanız varsayılan müşteri grubunu eklemelisiniz!';

$_['error_default']                = 'Uyarı: Varsayılan mağazanızı silemezsiniz!';

$_['error_store']                  = 'Uyarı: Bu Mağaza, şu anda% s siparişlerine atandığı için silinemiyor!';

