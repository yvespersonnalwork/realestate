<?php

// Heading

$_['heading_title']                = 'Web sitesi ayarları';



// Text

$_['text_stores']                  = 'Web sitesi';

$_['tab_store']                  = 'Web sitesi';

$_['text_success']                 = 'Başarı: Web sitesi ayarlarını değiştirdiniz!';

$_['text_edit']                    = 'Web Sitesi Ayarını Düzenle';

$_['text_product']                 = 'listeleme';

$_['text_review']                  = 'yorumlar';

$_['text_voucher']                 = 'kuponları';

$_['text_tax']                     = 'Vergiler';

$_['text_account']                 = 'hesap';

$_['text_checkout']                = 'Çıkış yapmak';

$_['text_stock']                   = 'Stok';

$_['text_affiliate']               = 'Bağlı';

$_['text_captcha']                 = 'Captcha';

$_['text_register']                = 'Kayıt olmak';

$_['text_guest']                   = 'Konuk Ödeme';

$_['text_return']                  = 'İade';

$_['text_contact']                 = 'Temas';

$_['text_mail']                    = 'Posta';

$_['text_smtp']                    = 'SMTP';

$_['text_mail_alert']              = 'Posta Uyarıları';

$_['text_mail_account']            = 'Kayıt olmak';

$_['text_mail_affiliate']          = 'Mülk ekle';

$_['text_mail_order']              = 'Emirler';

$_['text_mail_review']             = 'yorumlar';

$_['text_general']                 = 'Genel';

$_['text_security']                = 'Güvenlik';

$_['text_upload']                  = 'Yüklemeler';

$_['text_error']                   = 'Hata işleme';

//label name

$_['text_meta_title']             = 'Meta Başlığı';

$_['text_meta_description']       = 'Meta Tag Açıklama';

$_['text_meta_keyword']           = 'Meta Tag Anahtar Kelimeler';

$_['text_layout']                 = 'Varsayılan Düzen';

$_['text_theme']                  = 'Tema';

$_['text_name']                   = 'Web sitesi adı';

$_['text_owner']                  = 'Web Sitesi Sahibi';

$_['text_address']                = 'Adres';

$_['text_email']                   = 'E-Posta';

$_['text_telephone']               = 'Telefon';

$_['text_map']                     = 'Harita Kodunu Göm';

$_['text_mapkey']              = 'Google Harita anahtarı';

$_['text_image']                  = 'görüntü';

$_['text_location']               = 'Web sitesi konumu';

$_['text_country']                = 'ülke';

$_['text_zone']                   = 'Bölge / Eyalet';

$_['text_language']               = 'Dil';

$_['text_admin_language']         = 'Yönetim dili';

$_['text_currency']               = 'Para birimi';

$_['text_currency_auto']          = 'Otomatik Güncelleme Para Birimi';

$_['text_length_class']           = 'Uzunluk sınıfı';

$_['text_weight_class']           = 'Ağırlık sınıfı';

$_['text_limit_admin']            = 'Sayfa Başına Varsayılan Öğeler (Admin)';

$_['text_product_count']          = 'Kategori İlan Sayısı';

$_['text_customer_online']        = 'Müşteriler Çevrimiçi';

$_['text_customer_activity']      = 'Müşteri etkinliği';

$_['text_customer_search']        = 'Müşteri Aramalarını Günlüğe Kaydet';

$_['text_customer_group']         = 'müşteri grubu';

$_['text_customer_group_display'] = 'müşteri grubu';

$_['text_customer_price']         = 'Giriş Ekran Fiyatları';

$_['text_login_attempts']         = 'Maksimum Giriş Denemesi';

$_['text_account']                = 'Hesap Koşulları';

$_['text_captcha']                = 'Captcha';

$_['text_captcha_page']           = 'Captcha Sayfası';

$_['text_logo']                   = 'Web Sitesi Logosu';

$_['text_icon']                   = 'ikon';

$_['text_ftp_hostname']           = 'FTP evsahibi';

$_['text_ftp_port']               = 'FTP Liman';

$_['text_ftp_username']           = 'FTP Kullanıcı adı';

$_['text_ftp_password']           = 'FTP Parola';

$_['text_ftp_root']               = 'FTP Kök';

$_['text_ftp_status']             = 'etkinleştirme FTP';

$_['text_mail_protocol']          = 'Posta Protokolü';

$_['text_mail_parameter']         = 'Mail Parametreleri';

$_['text_mail_smtp_hostname']     = 'SMTP Hostadı';

$_['text_mail_smtp_username']     = 'SMTP Kullanıcı adı';

$_['text_mail_smtp_password']     = 'SMTP Parola';

$_['text_mail_smtp_port']         = 'SMTP Liman';

$_['text_mail_smtp_timeout']      = 'SMTP Zaman aşımı';

$_['text_mail_alert']             = 'Uyarı postası';

$_['text_mail_alert_email']       = 'Ek Uyarı Postası';

$_['text_secure']                 = 'SSL kullan';

$_['text_shared']                 = 'Paylaşılan Oturumları Kullan';

$_['text_robots']                 = 'Robotlar';

$_['text_seo_url']                = 'SEO URL'lerini kullan';

$_['text_file_max_size']	       = 'Maksimum Dosya Boyutu';

$_['text_file_ext_allowed']       = 'İzin Verilen Dosya Uzantıları';

$_['text_file_mime_allowed']      = 'İzin Verilen Dosya MIME Türleri';

$_['text_maintenance']            = 'Bakım Modu';

$_['text_password']               = 'Unutulan Şifreye İzin Ver';

$_['text_encryption']             = 'Şifreleme anahtarı';

$_['text_compression']            = 'Çıktı Sıkıştırma Seviyesi';

$_['text_error_display']          = 'Hataları görüntüleme';

$_['text_error_log']              = 'Günlük Hataları';

$_['text_error_filename']         = 'Hata Günlüğü Dosya Adı';

$_['text_status']                 = 'durum';

$_['tab_socialmedia']              = 'Altbilgi Sosyal Simgeleri';

$_['text_aboutdes']         	   = 'Hakkımızda Açıklama';

$_['text_title']         	       = 'Başlık:';

$_['text_phoneno']         	   = 'Telefon yok:';

$_['text_mobile']         	       = 'Telefon numarası:';

$_['text_phoneno']         	   = 'Telefon yok:';

$_['text_email']         	       = 'E-posta:';

$_['text_address2']         	   = 'Adres:';

$_['text_fburl']         	       = 'Facebook Url:';

$_['text_google']         	       = 'Google Url:';

$_['text_twet']         	       = 'Twitter Url:';

$_['text_in']         	           = 'LinkedIn:';

$_['text_instagram']         	   = 'Instagram:';

$_['text_pinterest']         	   = 'Pinterest';

$_['text_youtube']         	   = 'Youtube';

$_['text_blogger']         	   = 'Blogger';

$_['text_footericon']             = 'Altbilgi Logosu';

// Entry  input  name 

$_['entry_meta_title']             = 'Meta Başlığı';

$_['entry_meta_description']       = 'Meta Tag Açıklama';

$_['entry_meta_keyword']           = 'Meta Tag Anahtar Kelimeler';

$_['entry_layout']                 = 'Varsayılan Düzen';

$_['entry_theme']                  = 'Tema';

$_['entry_name']                   = 'E-Posta';

$_['entry_owner']                  = 'Web Sitesi Sahibi';

$_['entry_address']                = 'Web Sitesi Sahibi';

$_['entry_email']                   = 'E-Posta';

$_['entry_telephone']               = 'Telefon';

$_['entry_map']                     = 'Harita Kodunu Göm';

$_['entry_mapkey']              = 'Google Harita anahtarı';

$_['entry_image']                  = 'görüntü';

$_['entry_location']               = 'Web sitesi konumu';

$_['entry_country']                = 'ülke';

$_['entry_zone']                   = 'Bölge / Eyalet';

$_['entry_language']               = 'Dil';

$_['entry_admin_language']         = 'Yönetim dili';

$_['entry_currency']               = 'Para birimi';

$_['entry_currency_auto']          = 'Otomatik Güncelleme Para Birimi';

$_['entry_length_class']           = 'Uzunluk sınıfı';

$_['entry_weight_class']           = 'Ağırlık sınıfı';

$_['entry_limit_admin']            = 'Default Items Per Page (Admin)';

$_['entry_product_count']          = 'Kategori İlan Sayısı';

$_['entry_customer_online']        = 'Müşteriler Çevrimiçi';

$_['entry_customer_activity']      = 'Müşteri etkinliği';

$_['entry_customer_search']        = 'Müşteri Aramalarını Günlüğe Kaydet';

$_['entry_customer_group']         = 'müşteri grubu';

$_['entry_customer_group_display'] = 'Müşteri grupları';

$_['entry_customer_price']         = 'Giriş Ekran Fiyatları';

$_['entry_login_attempts']         = 'Maksimum Giriş Denemesi';

$_['entry_account']                = 'Hesap Koşulları';

$_['entry_captcha']                = 'Captcha';

$_['entry_captcha_page']           = 'Captcha Sayfası';

$_['entry_logo']                   = 'Web Sitesi Logosu';

$_['entry_icon']                   = 'ikon';

$_['entry_ftp_hostname']           = 'FTP evsahibi';

$_['entry_ftp_port']               = 'FTP Liman';

$_['entry_ftp_username']           = 'FTP Kullanıcı adı';

$_['entry_ftp_password']           = 'FTP Parola';

$_['entry_ftp_root']               = 'FTP Kök';

$_['entry_ftp_status']             = 'etkinleştirme FTP';

$_['entry_mail_protocol']          = 'Posta Protokolü';

$_['entry_mail_parameter']         = 'Mail Parametreleri';

$_['entry_mail_smtp_hostname']     = 'SMTP Hostadı';

$_['entry_mail_smtp_username']     = 'SMTP Kullanıcı adı';

$_['entry_mail_smtp_password']     = 'SMTP Parola';

$_['entry_mail_smtp_port']         = 'SMTP Liman';

$_['entry_mail_smtp_timeout']      = 'SMTP Zaman aşımı';

$_['entry_mail_alert']             = 'Uyarı postası';

$_['entry_mail_alert_email']       = 'Ek Uyarı Postası';

$_['entry_secure']                 = 'SSL kullan';

$_['entry_shared']                 = 'Paylaşılan Oturumları Kullan';

$_['entry_robots']                 = 'Robotlar';

$_['entry_seo_url']                = 'SEO URL'lerini kullan';

$_['entry_file_max_size']	       = 'Maksimum Dosya Boyutu';

$_['entry_file_ext_allowed']       = 'İzin Verilen Dosya Uzantıları';

$_['entry_file_mime_allowed']      = 'İzin Verilen Dosya MIME Türleri';

$_['entry_maintenance']            = 'Bakım Modu';

$_['entry_password']               = 'Unutulan Şifreye İzin Ver';

$_['entry_encryption']             = 'Şifreleme anahtarı';

$_['entry_compression']            = 'Çıktı Sıkıştırma Seviyesi';

$_['entry_error_display']          = 'Hataları görüntüleme';

$_['entry_error_log']              = 'Günlük Hataları';

$_['entry_error_filename']         = 'Hata Günlüğü Dosya Adı';

$_['entry_status']                 = 'durum';

$_['tab_socialmedia']              = 'Altbilgi Sosyal Simgeleri';

$_['entry_aboutdes']         	   = 'Hakkımızda Açıklama';

$_['entry_title']         	       = 'Başlık:';

$_['entry_phoneno']         	   = 'Telefon yok:';

$_['entry_mobile']         	       = 'Telefon numarası:';

$_['entry_phoneno']         	   = 'Telefon yok:';

$_['entry_email']         	       = 'E-posta:';

$_['entry_address2']         	   = 'Adres:';

$_['entry_fburl']         	       = 'Facebook URL:';

$_['entry_google']         	       = 'Google Url:';

$_['entry_twet']         	       = 'Twitter Url:';

$_['entry_in']         	           = 'LinkedIn:';

$_['entry_instagram']         	   = 'Instagram:';

$_['entry_pinterest']         	   = 'Pinterest';

$_['entry_youtube']         	   = 'Youtube';

$_['entry_blogger']         	   = 'Blogger';

$_['entry_footericon']             = 'Altbilgi Logosu';



// Help

$_['help_location']                = 'İstediğiniz farklı Web sitesi konumları bize ulaşın formunda bize ulaşın.';

$_['help_currency']                = 'Varsayılan para birimini değiştirin. Değişikliklerini görmek ve mevcut çerezinizi sıfırlamak için tarayıcı önbelleğinizi temizleyin.';

$_['help_currency_auto']           = 'Web sitenizi günlük para birimlerini otomatik olarak güncelleyecek şekilde ayarlayın.';

$_['help_limit_admin']   	       = 'Sayfa başına kaç yönetici öğesinin gösterileceğini belirler (siparişler, müşteriler vb.).';

$_['help_product_count']           = 'Web sitesinin ön başlık kategori menüsünde alt kategorilerin içindeki ürünlerin sayısını gösterin. Dikkatli olun, bu çok fazla alt kategoriye sahip mağazalar için aşırı performansa neden olur!';

$_['help_customer_online']         = 'Müşteri raporları bölümünden müşterileri çevrimiçi takip edin.';

$_['help_customer_activity']       = 'Müşteri faaliyetlerini müşteri raporları bölümünden takip edin.';

$_['help_customer_group']          = 'Varsayılan müşteri grubu.';

$_['help_customer_group_display']  = 'Varsayılan müşteri grubu.';

$_['help_customer_price']          = 'Sadece bir müşteri giriş yaptığında fiyatları göster.';

$_['help_login_attempts']          = 'Hesap 1 saat boyunca kilitlenmeden önce izin verilen maksimum giriş denemesi. Müşteri ve satış ortağı hesapları, müşteri veya satış ortağı yönetici sayfalarında açılabilir.';

$_['help_account']                 = 'Hesap oluşturmadan önce kişileri şartları kabul etmeye zorlar.';









$_['help_captcha']                 = 'Captcha kayıt, giriş, iletişim ve yorumlar için kullanmak.';

$_['help_icon']                    = 'Simge, 16 piksel x 16 piksel olan bir PNG olmalıdır.';

$_['help_ftp_root']                = 'RealEstate kurulumunuzun bulunduğu dizin saklanır. Normalde \'public_html/\'.';

$_['help_mail_protocol']           = 'Ana makineniz php posta işlevini devre dışı bırakmadığı sürece sadece \ 'Mail \' seçeneğini seçin.';

$_['help_mail_parameter']          = '\ 'Posta \' kullanırken, buraya ek posta parametreleri eklenebilir (örneğin, -f email@storeaddress.com).';

$_['help_mail_smtp_hostname']      = 'Add \'tls://\' or \'ssl://\' güvenlik bağlantısı gerekiyorsa önek. (e.g. tls://smtp.gmail.com, ssl://smtp.gmail.com).';

$_['help_mail_smtp_password']      = 'Gmail için, burada uygulamaya özel bir şifre oluşturmanız gerekebilir: https://security.google.com/settings/security/apppasswords.';

$_['help_mail_alert']              = 'Bir müşteri bunları kullandığında hangi özelliklerle ilgili bir uyarı e-postası almak istediğinizi seçin.';

$_['help_mail_alert_email']        = 'Ana mağaza e-postasına ek olarak, uyarı e-postasını almak istediğiniz diğer e-postalar. (comma separated).';

$_['help_secure']                  = 'Bir SSL sertifikası kuruluysa, sunucunuzu SSL kontrolünden geçirmek için SSL URL'sini katalog ve admin config dosyalarına ekleyin.';

$_['help_shared']                  = 'Oturum tanımlama bilgisini mağazalar arasında paylaşmaya çalışın, böylece sepet farklı alanlar arasında geçirilebilir.';

$_['help_robots']                  = 'Paylaşılan oturumların kullanmayacağı web tarayıcısı kullanıcı aracıları listesi. Her kullanıcı aracısı için ayrı satırlar kullanın.';

$_['help_seo_url']                 = 'SEO URL'lerini kullanmak için, apache mod mod-rewrite kurulu olmalı ve htaccess.txt dosyasını .htaccess olarak yeniden adlandırmalısınız.';

$_['help_file_max_size']		   = 'Image Manager'da yükleyebileceğiniz maksimum resim dosyası boyutu. Bayt olarak gir.';

$_['help_file_ext_allowed']        = 'Hangi dosya uzantılarının yüklenmesine izin verildiğini ekleyin. Her değer için yeni bir satır kullanın.';

$_['help_file_mime_allowed']       = 'Hangi dosya mime türlerinin yüklenmesine izin verildiğini ekleyin. Her değer için yeni bir satır kullanın.';

$_['help_maintenance']             = 'Müşterilerin mağazanıza göz atmasını önler. Bunun yerine bir bakım mesajı görecekler. Yönetici olarak giriş yaptıysanız, mağazayı normal olarak göreceksiniz.';

$_['help_password']                = 'Yönetici için unutulan parolanın kullanılmasına izin verin. Sistem bir saldırı girişimi tespit ederse bu otomatik olarak devre dışı bırakılır.';

$_['help_encryption']              = 'Lütfen siparişleri işlerken özel bilgileri şifrelemek için kullanılacak bir gizli anahtar girin.';

$_['help_compression']             = 'Talep eden müşterilere daha verimli transfer için GZIP. Sıkıştırma seviyesi 0 - 9 arasında olmalıdır.';



// Error

$_['error_warning']                = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_permission']             = 'Uyarı: Ayarları değiştirme izniniz yok!';

$_['error_meta_title']             = 'Başlık 3 ile 32 karakter arasında olmalıdır!';

$_['error_name']                   = 'Başlık 3 ile 32 karakter arasında olmalıdır!';

$_['error_owner']                  = 'Web Sitesi Sahibi, 3 ile 64 karakter arasında olmalıdır!';

$_['error_address']                = 'Web sitesi adresi 10 ile 256 karakter arasında olmalıdır!';

$_['error_mapkey']                 = 'Lütfen harita anahtarını girin';

$_['error_map']                    = 'Lütfen haritayı google map yerleştirme kodunu giriniz';

$_['error_email']                  = 'E-posta adresi geçerli görünmüyor!';

$_['error_telephone']              = 'Telefon 3 - 32 karakter arasında olmalıdır!';

$_['error_limit']       	       = 'Telefon 3 - 32 karakter arasında olmalıdır!';

$_['error_login_attempts']         = 'Giriş Girişimi den büyük olmalıdır 0!';

$_['error_customer_group_display'] = 'Bu özelliği kullanacaksanız varsayılan müşteri grubunu eklemelisiniz!';



$_['error_processing_status']      = 'En az 1 sipariş süreci seçmelisiniz';



$_['error_ftp_hostname']           = 'FTP Ana bilgisayar gerekli!';

$_['error_ftp_port']               = 'FTP Liman gerekli!';

$_['error_ftp_username']           = 'FTP Kullanıcı adı gerekli!';

$_['error_ftp_password']           = 'FTP şifre gerekli!';

$_['error_error_filename']         = 'Error Günlük Dosya adı gerekli!';

$_['error_malformed_filename']	   = 'Error Kötü Biçimli Günlük Dosya Adı!';

$_['error_encryption']             = 'Şifreleme Anahtarı 32 ila 1024 karakter arasında olmalıdır!';

