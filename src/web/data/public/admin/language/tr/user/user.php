<?php

// Heading

$_['heading_title']         = 'Kullanıcılar';



// Text

$_['text_success']          = 'Başarı: Kullanıcıları değiştirdiniz!';

$_['text_list']             = 'Kullanıcı listesi';

$_['text_add']              = 'Kullanıcı Ekle';

$_['text_edit']             = 'kullanıcıyı düzenle';



// Column

$_['column_username']       = 'Kullanıcı adı';

$_['column_status']         = 'durum';

$_['column_date_added']     = 'Ekleme Tarihi';

$_['column_action']         = 'Aksiyon';

/// label name 

$_['text_username']        = 'Kullanıcı adı';

$_['text_user_group']      = 'Kullanıcı grubu';

$_['text_password']        = 'Parola';

$_['text_confirm']         = 'Onaylamak';

$_['text_firstname']       = 'İsim';

$_['text_lastname']        = 'Soyadı';

$_['text_email']           = 'E-Posta';

$_['text_image']           = 'görüntü';

$_['text_status']          = 'durum';




// Entry

$_['entry_username']        = 'Kullanıcı adı';
$_['entry_user_group']      = 'Kullanıcı grubu';
$_['entry_password']        = 'Parola';
$_['entry_confirm']         = 'Onaylamak';
$_['entry_firstname']       = 'İsim';
$_['entry_lastname']        = 'Soyadı';
$_['entry_email']           = 'E-Posta';
$_['entry_image']           = 'görüntü';
$_['entry_status']          = 'durum';
$_['text_enabled']          = 'Etkin';
$_['text_disabled']         = 'engelli';


///btn 
$_['button_save']           = 'Kayıt etmek';
$_['button_cancel']         = 'İptal etmek';

// Error

$_['error_permission']      = 'Uyarı: Kullanıcıları değiştirme izniniz yok!';

$_['error_account']         = 'Uyarı: Kendi hesabınızı silemezsiniz.!';

$_['error_exists_username'] = 'Uyarı: Kullanıcı adı zaten kullanılıyor!';

$_['error_username']        = 'Kullanıcı adı 3 ile 20 karakter arasında olmalıdır!';

$_['error_password']        = 'Şifre 4 ila 20 karakter arasında olmalıdır!';

$_['error_confirm']         = 'Şifre ve şifre onayı eşleşmiyor!';

$_['error_firstname']       = 'Ad, 1 - 32 karakter arasında olmalıdır!';

$_['error_lastname']        = 'Soyadı 1 ile 32 karakter arasında olmalıdır!';

$_['error_email']           = 'E-posta adresi geçerli görünmüyor!';

$_['error_exists_email']    = 'Uyarı: E-Posta Adresi zaten kayıtlı!';