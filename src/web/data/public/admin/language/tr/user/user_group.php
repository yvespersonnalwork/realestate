<?php

// Heading

$_['heading_title']    = 'Kullanıcı Grupları';



// Text

$_['text_success']     = 'Başarı: Kullanıcı gruplarını değiştirdiniz!';

$_['text_list']        = 'Kullanıcı grubu';

$_['text_add']         = 'Kullanıcı Grubu Ekle';

$_['text_edit']        = 'Kullanıcı Grubunu Düzenle';



// Column

$_['column_name']      = 'Kullanıcı Grubu Adı';

$_['column_action']    = 'Kullanıcı Grubu Adı';



///label name 

$_['text_name']       = 'User Group Name';

$_['text_access']     = 'Access Permission';

$_['text_modify']     = 'Modify Permission';


// Entry

$_['entry_name']       = 'User Group Name';

$_['entry_access']     = 'Access Permission';

$_['entry_modify']     = 'Modify Permission';


///btn 
$_['button_save']           = 'Kayıt etmek';
$_['button_cancel']         = 'İptal etmek';


// Error

$_['error_permission'] = 'Uyarı: Kullanıcı gruplarını değiştirme izniniz yok.!';

$_['error_name']       = 'Kullanıcı Grubu Adı, 3 ile 64 karakter arasında olmalıdır!';

$_['error_user']       = 'Uyarı: Bu kullanıcı grubu,% s kullanıcısına atanmış olduğu için silinemiyor.!';