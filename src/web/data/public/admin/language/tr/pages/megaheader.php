<?php
// Heading
$_['heading_title']          = 'Mega Başlık Menüsü';

//Text
$_['text_list']         	 = 'Menülerin Listesi';
$_['text_form']              = 'Mega başlık';
$_['text_category']          = 'Kategori';
$_['text_info']              = 'Bilgi';
$_['text_manufact']          = 'Üretici firma';
$_['text_custom']            = 'görenek';
$_['text_select']            = 'seçmek';
$_['text_editor']            = 'Editör';
$_['text_product']           = 'Ürün';
$_['text_enable']            = 'Etkin';
$_['text_disable']           = 'engelli';
$_['text_no_results']        = 'sonuç yok';
$_['text_category_image']    = 'Kategori Resmi';
$_['text_category_description'] = 'Kategori tanımı';
$_['text_manufatureimage']   = 'Üretici resmi';
$_['text_patternimage']   	= 'Görüntü deseni';
$_['entry_patternimage']     = 'Desen arka plan';
$_['entry_selectimagetype']  = 'Arkaplan türünü seçin';
//// new changes  27-10-2016///
$_['entry_store']            = 'Web sitesi';
//// new changes  27-10-2016///


$_['button_edit']            = 'Düzenle';
$_['button_add']             = 'Yeni ekle';
$_['button_delete']          = 'silmek';
$_['button_save']            = 'Kayıt etmek';
$_['button_setting']         = 'Mega başlık ayarı';
$_['button_cancel']          = 'Canel';
$_['text_success']           = 'Başarı: Mega başlıklarını değiştirdiniz! ';
$_['button_custom']          = 'Özel Menü Ekle';

$_['text_product1']          = 'özellik';
$_['text_subcategory']       = 'Alt kategori';
$_['text_price']         	 = 'Fiyat';
$_['text_pname']         	 = 'Mülkiyet adı';
$_['text_image']         	 = 'görüntü';
$_['text_model']        	 = 'model';
$_['text_upc']        	     = 'UPC';
$_['text_sku']         		 = 'SKU';
$_['text_description']       = 'Açıklama';


/// label name 
$_['text_image']          	 = 'Arka plan görüntüsü';
$_['text_status']           = 'durum';
$_['text_sort_order']       = 'Sıralama düzeni';
$_['text_title']            = 'Özel Başlık Başlığı';
$_['text_row']           	 = 'Kürek çekmek';
$_['text_col']           	 = 'Cols';
$_['text_enable']           = 'Alt Menü Kategori Başlığını Etkinleştir';
$_['text_url']           	 = 'URL';
$_['text_open']           	 = 'Yeni sekmede aç';
$_['text_type']           	 = 'tip';
$_['text_custname']         = 'Özel ad';
$_['text_custurl'] 	     = 'Özel URL';
$_['text_parent'] 	         = 'ebeveyn';
$_['text_submenu'] 		 = 'Sub Megamenu';
$_['text_description']      = 'MegaMenu Tanım';
$_['text_product']          = 'Ürün';
$_['text_category']         = 'Kategori';
$_['text_row']         	 = 'Kürek çekmek';
$_['text_customcode']       = 'Özel kod';
$_['button_setting']         = 'Ayarlar';
$_['text_information']      = 'Bilgi';
$_['text_manufacturer']     = 'Üretici firma';
$_['text_editor']            = 'Editör';
$_['text_icontitle']		 = 'ikon';
$_['text_showicon']		 = 'Simgeyi Etkinleştir';

$_['text_selectimagetype']  = 'Arkaplan türünü seçin';
//// new changes  27-10-2016///
$_['text_store']            = 'Web sitesi';

// Entry
$_['entry_image']          	 = 'Arka plan görüntüsü';
$_['entry_status']           = 'durum';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_title']            = 'Özel Başlık Başlığı';
$_['entry_row']           	 = 'Kürek çekmek';
$_['entry_col']           	 = 'Cols';
$_['entry_enable']           = 'Enable Sub Menu Category Title';
$_['entry_url']           	 = 'URL';
$_['entry_open']           	 = 'Yeni sekmede aç';
$_['entry_type']           	 = 'tip';
$_['entry_custname']         = 'Özel ad';
$_['entry_custurl'] 	     = 'Özel URL';
$_['entry_parent'] 	         = 'ebeveyn';
$_['entry_submenu'] 		 = 'Sub Megamenu';
$_['entry_description']      = 'MegaMenu Tanım';
$_['entry_product']          = 'Ürün';
$_['entry_category']         = 'Kategori';
$_['entry_row']         	 = 'Kürek çekmek';
$_['entry_customcode']       = 'Özel kod';
$_['button_setting']         = 'Ayarlar';
$_['entry_information']      = 'Bilgi';
$_['entry_manufacturer']     = 'Üretici firma';
$_['text_editor']            = 'Editör';
$_['entry_icontitle']		 = 'ikon';
$_['entry_showicon']		 = 'Simgeyi Etkinleştir';

// column_edit
$_['column_title']           = 'Menü Başlığı';
$_['column_sort_order']      = 'Sıralama düzeni';
$_['column_status']          = 'durum';
$_['column_edit']            = 'Aksiyon';

 // Error
$_['error_title']      		 = 'Başlık 3 ile 21 karakter arasında olmalıdır'; 
$_['error_image']      		 = 'Lütfen resim seç'; 

//filter
$_['button_filter']      	 = 'filtre';
$_['entry_title']      		 = 'Özel Başlık Başlığı';
$_['entry_status']      	 = 'durum';
$_['text_enabled']      	 = 'etkinleştirme';
$_['text_disabled']      	 = 'Devre dışı';

//help
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin genel olarak benzersiz olduğundan emin olun.';
$_['help_bottom']            = 'Alt altbilgide göster.';
$_['help_category']          = 'Otomatik tamamlama';
$_['help_product']           = 'Otomatik tamamlama';
$_['help_showicon']          = 'Menüde Yalnızca Simgeyi Etkinleştir';
