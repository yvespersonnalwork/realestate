<?php

// Heading

$_['heading_title']       = 'Görüşler';



// Text

$_['text_success']      = 'Başarı: Referansları değiştirdiniz!';

$_['text_list']      = 'Görüşler Listesi';

$_['text_add']      = 'Referans ekle';

$_['text_edit']      = 'Referansları Düzenle';



// Column

$_['column_author']     = 'Yazar';

$_['column_rating']     = 'ülke';

$_['column_status']     = 'durum';

$_['column_date_added'] = 'Ekleme Tarihi';

$_['column_action']     = 'Aksiyon';

$_['entry_image']     = 'görüntü';

$_['button_insert']     = 'Eklemek';




///label

$_['text_product']     = 'ülke';
$_['text_author']      = 'Yazar';
$_['text_country']     = 'ülke';
$_['text_rating']      = 'Değerlendirme:';
$_['text_status']      = 'durum';
$_['text_text']        = 'Metin';
$_['text_good']        = 'İyi';
$_['text_bad']         = 'Kötü';
$_['text_image']         = 'görüntü';


// Entry

$_['entry_product']     = 'ülke';
$_['entry_author']      = 'Yazar';
$_['entry_country']     = 'ülke';
$_['entry_rating']      = 'Değerlendirme:';
$_['entry_status']      = 'durum';
$_['entry_text']        = 'Metin';
$_['entry_good']        = 'İyi';
$_['entry_bad']         = 'Kötü';


/// btn

$_['button_save']          = 'kayıt etmek';
$_['button_cancel']          = 'İptal etmek';

// Error

$_['error_permission']  = 'Uyarı: Referansları değiştirme izniniz yok.!';

$_['error_author']      = 'Yazar 3 ile 64 karakter arasında olmalıdır!';

$_['error_text']        = 'Referans metin 1 karakterden büyük olmalıdır!';

?>