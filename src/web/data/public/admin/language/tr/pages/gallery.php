<?php

// Heading

$_['heading_title']          = 'galeri';



// Text

$_['text_success']           = 'Başarı: Galeri’yi değiştirdiniz!';

$_['text_default']           = 'Varsayılan';

$_['text_image_manager']     = 'Görüntü Yöneticisi';

$_['text_browse']            = 'Araştır';

$_['text_clear']             = 'Açık';

$_['text_list']              = 'Galeri Listesi';

$_['text_add']               = 'Galeri Listesi Ekle';

$_['text_edit']              = 'Galeri Listesi Ekle';

$_['entry_url']          	 = 'SEO URL'si';



// Column

$_['column_name']            = 'Albüm Adı';

$_['column_sort_order']      = 'Sıralama düzeni';

$_['column_image']           = 'Sort Order';

$_['column_status']          = 'durum';

$_['column_action']          = 'Aksiyon';



// Entry

$_['entry_name']             = 'Albüm Adı:';

$_['entry_description']      = 'Açıklama:';

$_['entry_image']            = 'Albüm Çerçevesi:';

$_['entry_sort_order']       = 'Sıralama düzeni:';

$_['entry_status']           = 'durum:';



// Error 

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_permission']       = 'Uyarı: Galeri’yi değiştirme izniniz yok.!';

$_['error_name']             = 'Galeri Adı 2 ile 32 karakter arasında olmalıdır!';

?>