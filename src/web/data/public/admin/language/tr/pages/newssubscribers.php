<?php

// Heading

$_['heading_title']    = 'Bülten Aboneleri';



// Text

$_['text_success']     = 'Başarı: Bülten abonelerini değiştirdiniz!';

$_['text_export']     = 'ihracat';

$_['text_list']     = 'Bülten Abonelerinin Listesi';

$_['text_add']     = 'Bülten Aboneleri Ekleme';

$_['text_edit']     = 'Bülten Abonelerini Düzenle';



// Column

$_['column_name']      = 'isim';

$_['column_email']     = 'E-posta';

$_['column_action']    = 'Aksiyon';



// Entry

$_['entry_name']       = 'isim';

$_['entry_code']       = 'E-posta';







// Error

$_['error_email_exist']= 'E-posta Kimliği zaten var';

$_['error_email_id']      = 'Geçersiz e-posta formatı';

$_['error_permission'] = 'Uyarı: Bülten abonelerini değiştirme izniniz yok!';

?>