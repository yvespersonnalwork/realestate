<?php

// Heading

$_['heading_title']    		= 'Mega başlık';



$_['text_megaheader']      	= 'Mega başlık';

$_['text_success']     		= 'Başarı: Mega başlık ayar modülünü değiştirdiniz!';

$_['text_add']        		= 'Mega başlık ayarı ekle';



// Entry

$_['entry_bgtitle']     	= 'Başlık Bg Rengi';

$_['entry_bghovtitle']     	= 'Başlık Bg Vurgulu Rengi';

$_['entry_bg_color']     	= 'Başlık Bg Rengi';

$_['entry_title']     	 	= 'Başlık Başlığı Rengi';

$_['entry_hovtitle']     	= 'Üstbilgi Başlığı Hover Color';

$_['entry_link']         	= 'Başlık Bağlantısı Rengi';

$_['entry_link_hover']   	= 'Başlık Bağlantısı Rengi (vurgulu)';

$_['entry_powered']      	= 'Üstbilgi Powered Metin Rengi';

$_['entry_title_font']   	= 'Üstbilgi Başlığı Yazı Tipi Boyutu';

$_['entry_sub_link']     	= 'Alt Bağlantı Yazı Tipi Boyutu';

$_['entry_product_limit']	= 'Ürün Sınırı';

$_['entry_category_limit'] 	= 'Kategori Sınırı';

$_['entry_product_image']  	= 'Ürün resmi';

$_['entry_height']         	= 'Yükseklik';

$_['entry_width']          	= 'Genişlik';

$_['entry_category_image']  = 'Kategori Resmi';

$_['entry_manufacture_image'] = 'Görüntü imalatı';

$_['entry_drpmebg']			  = 'Açılan Menü bg Renk';

$_['help_category_image']			  = 'Kategori görüntüsünün boyutunu ayarla';

/* update code */

$_['entry_menuexpend']	      = 'Menüyü Genişlet';

$_['text_no']	              = 'Yok hayır';

$_['text_yes']	              = 'Evet';

/* update code */





// Error

$_['error_permission'] 		  = 'Uyarı: Mega başlık ayar modülünü değiştirme izniniz yok.!';



// help

$_['help_bg_color'] 		  = 'Başlığın arka plan rengi';

$_['help_title'] 			  = 'Başlık başlığının metin rengi';

$_['help_link'] 		      = 'Başlık bağlantılarının metin rengi';

$_['help_link_hover'] 		  = 'Başlık bağlantılarının metin rengi (vurgulu)';

$_['help_powered'] 			  = 'Üstbilginin altındaki metnin metni';

$_['help_title_font'] 		  = 'Başlık başlığının yazı tipi boyutu';

$_['help_sub_link'] 		  = 'Üstbilgi bağlantısının yazı tipi boyutu';

$_['help_product_limit'] 	  = 'Ürün Sınırı Sadece Kategori ve İmalat içindir';

$_['help_category_limit'] 	  = 'Kategori Sınırı';

$_['help_product_image'] 	  = 'Ürün Resmi Yükseklik, genişlik';

$_['help_category_image'] 	  = 'Kategori Resim Yükseklik, genişlik';

$_['help_manufacture_image']  = 'Imalat Görüntü Yüksekliği, genişlik';

$_['help_hovtitle'] 		  = 'Metin Vurgulu Başlığın rengi';

$_['help_bgtitle'] 		      = 'Menü Başlığı Bg Color';

$_['help_bghovetitle'] 		  = 'Menü Başlığı Vurgulu Rengi';

$_['help_drpmebg'] 		      = 'Açılır Menüyü Seçin bg Renk';