<?php
// Heading
$_['heading_title']          = 'SSS';

// Text
$_['text_success']           = 'Başarı: SSS’de değişiklik yaptınız!';
$_['text_list']              = 'SSS Listesi';
$_['text_add']               = 'SSS ekle';
$_['text_edit']              = 'SSS'yi düzenle';
$_['text_default']           = 'Varsayılan';

// Column
$_['column_name']            = 'SSS Adı';
$_['column_sort_order']      = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';


///

$_['text_category']         = 'Kategori';
$_['text_name']             = 'Soru';
$_['text_description']      = 'Cevap';
$_['text_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['text_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';
$_['text_meta_description'] = 'Meta Tag Açıklama';
$_['text_keyword']          = 'SEO Anahtar Kelime';
$_['text_parent']           = 'ebeveyn';
$_['text_filter']           = 'Filtreler';
$_['text_store']            = 'Web sitesi';
$_['text_image']            = 'görüntü';
$_['text_top']              = 'Üst';
$_['text_column']           = 'Sütunlar';
$_['text_sort_order']       = 'Sıralama düzeni';
$_['text_status']           = 'durum';
$_['text_layout']           = 'Mizanpaj Geçersiz Kılma';



// Entry
$_['entry_category']         = 'Kategori';
$_['entry_name']             = 'Soru';
$_['entry_description']      = 'Cevap';
$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['entry_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';
$_['entry_meta_description'] = 'Meta Tag Açıklama';
$_['entry_keyword']          = 'SEO Anahtar Kelime';
$_['entry_parent']           = 'ebeveyn';
$_['entry_filter']           = 'Filtreler';
$_['entry_store']            = 'Web sitesi';
$_['entry_image']            = 'görüntü';
$_['entry_top']              = 'Üst';
$_['entry_column']           = 'Sütunlar';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_status']           = 'durum';
$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';

/// btn
$_['button_save']             = 'Kayıt etmek';
$_['button_cancel']           = 'İptal etmek';


// Help
$_['help_filter']            = '(Otomatik tamamlama)';
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve anahtar kelimenin genel olarak benzersiz olduğundan emin olun.';
$_['help_top']               = 'Üst menü çubuğunda görüntüleyin. Yalnızca üst ana SSS için çalışır.';
$_['help_column']            = 'Dipteki 3 SSS için kullanılacak sütun sayısı. Yalnızca üst ana SSS için çalışır.';

// Error
$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: SSS’de değişiklik izniniz yok!';
$_['error_name']             = 'SSS Adı 2 ile 32 karakter arasında olmalıdır!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO anahtar kelimesi zaten kullanılıyor!';
$_['error_category']          = 'Kategoriler yoksa, lütfen önce kategori ekleyin, sonra kategori ekleyin!	';