<?php
// Heading
$_['heading_title']          = 'Galeri Ayarı';

// Text
$_['text_success']           = 'Başarı: Kategorileri değiştirdiniz!';
$_['text_list']              = 'Tmd GalleryAyar Listesi';
$_['text_add']               = 'Tmd Gallery Ekle';
$_['text_edit']              = 'Tmd Galerisini Düzenle';
$_['text_default']           = 'Varsayılan';

// Column
$_['column_name']            = 'Galeri Başlığı';
$_['column_sort_order']      = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';

// Entry
$_['entry_width']       	 = 'Genişlik';
$_['entry_height']       	 = 'Yükseklik';
$_['entry_image_popup']  	 = 'Popup Size';
$_['entry_thumb_popup']  	 = 'Başparmak boyutu';
$_['entry_limtofgallery']  	 = 'Fotoğraf Sınırını Ayarla';
$_['entry_namecolor']  		 = 'Fotoğraf Adı Rengi';
$_['entry_desccolor'] 	     = 'Açıklama renk';
$_['entry_totalphotocolor']  = 'Toplam Fotoğraf Rengi';

$_['tab_color']              = 'Renk Ayarla';
$_['tab_setting']            = 'Galeri Ayarı';

// Error
$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Kategorileri değiştirme izniniz yok!';
$_['error_name']             = 'Uyarı: Kategorileri değiştirme izniniz yok. Balo Ünvanı 2 ile 32 karakter arasında olmalıdır!';
$_['error_meta_title']       = 'Galeri Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';