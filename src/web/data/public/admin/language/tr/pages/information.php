<?php

// Heading

$_['heading_title']          = 'Bilgi Sayfaları';



// Text

$_['text_success']           = 'Başarı: Bilgileri değiştirdiniz!';

$_['text_list']              = 'Bilgi sayfalarının listesi';

$_['text_add']               = 'Bilgi ekle';

$_['text_edit']              = 'Bilgi Düzenle';

$_['text_default']           = 'Varsayılan';



// Column

$_['column_title']           = 'Bilgi Sayfası Başlığı';

$_['column_sort_order']	     = 'Sıralama düzeni';

$_['column_action']          = 'Aksiyon';



//label name
$_['text_title']            = 'Bilgi Başlığı';

$_['text_description']      = 'Açıklama';

$_['text_store']            = 'Mağazalar';

$_['text_meta_title'] 	     = 'Meta Etiket Başlığı';

$_['text_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';

$_['text_meta_description'] = 'Meta Tag Açıklama';

$_['text_keyword']          = 'SEO URL'si';

$_['text_bottom']           = 'Alt';

$_['text_status']           = 'durum';

$_['text_sort_order']       = 'Sıralama düzeni';

$_['text_layout']           = 'Mizanpaj Geçersiz Kılma';





// Entry

$_['entry_title']            = 'Bilgi Başlığı';

$_['entry_description']      = 'Bilgi BaşlığıAçıklama';

$_['entry_store']            = 'Mağazalar';

$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';

$_['entry_meta_keyword'] 	 = 'Meta Tag Title';

$_['entry_meta_description'] = 'Meta Tag Açıklama';

$_['entry_keyword']          = 'SEO URL'si';

$_['entry_bottom']           = 'Alt';

$_['entry_status']           = 'durum';

$_['entry_sort_order']       = 'Sıralama düzeni';

$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';






// Help

$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve SEO URL’nin global olarak benzersiz olduğundan emin olun.';

$_['help_bottom']            = 'Alt altbilgide göster.';



// Error

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_permission']       = 'Uyarı: Bilgileri değiştirme izniniz yok!';

$_['error_title']            = 'Bilgi Başlığı 3 ile 64 karakter arasında olmalıdır!';

$_['error_description']      = 'Açıklama 3 karakterden fazla olmalı!';

$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';

$_['error_keyword']          = 'SEO URL'si zaten kullanılıyor!';

$_['error_account']          = 'Uyarı: Bu bilgi sayfası şu anda Web sitesi hesap koşulları olarak atandığından silinemez.!';

$_['error_checkout']         = 'Uyarı: Bu bilgi sayfası şu anda Web sitesi ödeme koşulları olarak atandığından silinemez!';

$_['error_affiliate']        = 'Uyarı: Bu bilgi sayfası şu anda Web sitesi bağlı şartları olarak atandığından silinemez!';

$_['error_return']           = 'Uyarı: Bu bilgi sayfası şu anda Web Sitesi iade şartları olarak atandığından silinemez!';

$_['error_store']            = 'Uyarı: Bu bilgi sayfası şu anda% s mağazası tarafından kullanıldığından silinemiyor!';

