<?php

// Heading

$_['heading_title']           = 'Ülkeler';

// Text

$_['text_success']            = 'Başarı: Ülkeleri değiştirdiniz!';
$_['text_list']               = 'Ülke Listesi';
$_['text_add']                = 'Ülke ekle';
$_['text_edit']               = 'Ülkeyi Düzenle';

// label name

$_['text_name']              = 'Ülke adı';
$_['text_iso_code_2']        = 'ISO kod (2)';
$_['text_iso_code_3']        = 'ISO kod (3)';
$_['text_address_format']    = 'Adres biçimi';
$_['text_postcode_required'] = 'Posta Kodu Gerekli';
$_['text_status']            = 'durum';


// Column
$_['column_name']             = 'Ülke adı';
$_['column_iso_code_2']       = 'ISO kod (2)';
$_['column_iso_code_3']       = 'ISO kod (3)';
$_['column_action']           = 'Aksiyon';

/// btn 
$_['button_save']               = 'Kayıt etmek';
$_['button_cancel']             = 'İptal etmek';



// Entry
$_['entry_name']              = 'Ülke adı';
$_['entry_iso_code_2']        = 'ISO kodu (2)';
$_['entry_iso_code_3']        = 'ISO kodu (3)';
$_['entry_address_format']    = 'Adres biçimi';
$_['entry_postcode_required'] = 'Posta Kodu Gerekli';
$_['entry_status']            = 'durum';

$_['text_disabled']           = 'engelli';
$_['text_enabled']           = 'Etkin';
/// btn 



// Help

$_['help_address_format']     = 'İsim = {firstname}<br />Last isim = {lastname}<br />şirket = {company}<br />Adres 1 = {address_1}<br />Adres 2 = {address_2}<br />Kent = {city}<br />posta kodu = {postcode}<br />bölge = {zone}<br />Bölge kodu = {zone_code}<br />ülke = {country}';

// Error
$_['error_permission']        = 'Uyarı: Ülkeleri değiştirme izniniz yok!';
$_['error_name']              = 'Ülke Adı 3 ila 128 karakter arasında olmalıdır!';
$_['error_default']           = 'Uyarı: Bu ülke şu anda varsayılan mağaza ülkesi olarak atandığından silinemez!';
$_['error_store']             = 'Uyarı: Bu ülke şu anda% s mağazalarına atandığından silinemiyor!';
$_['error_address']           = 'Uyarı: Bu ülke şu anda% s adres defteri girişine atandığı için silinemiyor!';
$_['error_affiliate']         = 'Uyarı: Bu ülke şu anda% s üyesine atandığından silinemez!';
$_['error_zone']              = 'Uyarı: Bu ülke şu anda% s bölgelerine atandığından silinemiyor!';
$_['error_zone_to_geo_zone']  = 'Uyarı: Bu ülke şu anda% s bölgelerine coğrafi bölgelere atandığından silinemiyor!';