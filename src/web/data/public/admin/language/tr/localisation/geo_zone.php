<?php

// Heading

$_['heading_title']      = 'Geo Bölgeleri';

// Text

$_['text_success']       = 'Başarı: Coğrafi bölgeleri değiştirdiniz!';
$_['text_list']          = 'Coğrafi Bölge Listesi';
$_['text_add']           = 'Coğrafi Bölge Ekle';
$_['text_edit']          = 'Coğrafi Bölgeyi Düzenle';

//label name
$_['text_name']         = 'Coğrafi Bölge Adı';
$_['text_description']  = 'Açıklama';
$_['text_country']      = 'ülke';
$_['text_zone']         = 'bölge';

// Column

$_['column_name']        = 'Coğrafi Bölge Adı';
$_['column_description'] = 'Açıklama';
$_['column_action']      = 'Aksiyon';
// Entry

$_['entry_name']         = 'Coğrafi Bölge Adı';
$_['entry_description']  = 'Açıklama';
$_['entry_country']      = 'ülke';
$_['entry_zone']         = 'bölge';
$_['text_disabled']           = 'engelli';
$_['text_enabled']           = 'Etkin';

/// btn 
$_['button_save']               = 'Kayıt etmek';
$_['button_cancel']             = 'İptal etmek';
$_['button_geo_zone_add']       = 'geo_zone_add';
$_['button_remove']             = 'Kaldır';



// Error

$_['error_permission']   = 'Uyarı: Coğrafi bölgeleri değiştirme izniniz yok!';
$_['error_name']         = 'Coğrafi Bölge Adı 3 ile 32 karakter arasında olmalıdır!';
$_['error_description']  = 'Açıklama Adı 3 ile 255 karakter arasında olmalıdır!';
$_['error_tax_rate']     = 'Uyarı: Bu coğrafi bölge şu anda bir veya daha fazla vergi oranına atandığından silinemez!';