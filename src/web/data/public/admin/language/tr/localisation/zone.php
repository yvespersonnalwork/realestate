<?php

// Heading

$_['heading_title']          = 'bölgeler';


// Text

$_['text_success']           = 'Başarı: Değiştirilmiş bölgeleriniz var!';
$_['text_list']              = 'Bölge Listesi';
$_['text_add']               = 'Bölge ekle';
$_['text_edit']              = 'Alanı Düzenle';

///label name

$_['text_name']             = 'Bölge Adı';
$_['text_code']             = 'Bölge kodu';
$_['text_country']          = 'ülke';
$_['text_status']           = 'durum';

// Column

$_['column_name']            = 'Bölge Adı';
$_['column_code']            = 'Bölge Kodu';
$_['column_country']         = 'ülke';
$_['column_action']          = 'Aksiyon';

// Entry input value name 

$_['entry_name']             = 'Bölge Adı';
$_['entry_code']             = 'Bölge kodu';
$_['entry_country']          = 'ülke';
$_['entry_status']           = 'durum';
$_['text_disabled']           = 'engelli';
$_['text_enabled']           = 'Etkin';

/// btn 
$_['button_save']               = 'Kayıt etmek';
$_['button_cancel']             = 'İptal etmek';
$_['button_geo_zone_add']       = 'geo_zone_add';
$_['button_remove']             = 'Kaldır';


// Error


$_['error_permission']       = 'Uyarı: Bölgeleri değiştirmek için izniniz yok!';
$_['error_name']             = 'Bölge Adı 3 ile 128 karakter arasında olmalıdır!';
$_['error_default']          = 'Uyarı: Bu bölge, şu anda varsayılan mağaza bölgesi olarak atandığından silinemez!';
$_['error_store']            = 'Uyarı: Bu bölge şu anda% s mağazalarına atandığından silinemez!';
$_['error_address']          = 'Uyarı: Bu bölge şu anda% s adres defteri girdilerine atandığından silinemez!';
$_['error_affiliate']        = 'Uyarı: Bu bölge şu anda% s bağlı şirketlerine atandığından silinemez!';
$_['error_zone_to_geo_zone'] = 'Uyarı: Bu bölge, şu anda coğrafi bölgelere% s bölgelerine atandığından silinemez!';