<?php

// Heading 

$_['heading_title']          = 'Emlak Tipi ';

// Text

$_['text_success']           = 'Başarı: Bilgileri değiştirdiniz!';

$_['text_list']              = 'Emlak Listesi';

$_['text_add']               = 'Mülk ekle';

$_['text_edit']              = 'Formu düzenle';

$_['text_form']              = 'Mülkiyet Türü Ekle';

$_['text_enable']              = 'etkinleştirme';

$_['text_disable']            = 'Devre dışı';

$_['text_default']           = 'Varsayılan';



//13/2/2018/

$_['text_success']           = 'Başarıyla: Size Mülkiyet türü ekleyin ';

$_['text_successedit']       = 'başarıyla değiştirildiniz: Özellik türü ';

$_['text_successdelete']     = 'başarıyla gönderildi: Mülkiyet türü Sil';

//13/2/2018/





// Column

$_['column_description']     = 'isim';

$_['column_name']            = 'isim';

$_['column_sort_order']      = 'Sıralama düzeni';

$_['column_status']           = 'durum';

$_['column_action']           = 'Aksiyon';

// Entry

$_['entry_title']           = 'Başlık';

$_['entry_image']           = 'görüntü';

$_['entry_SEO_URL']         = 'SEO URL'si';

$_['entry_name']            = 'isim';

$_['entry_description']     ='Açıklama';

$_['entry_store']            = 'Mağazalar';

$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';

$_['entry_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';

$_['entry_meta_description'] = 'Meta Tag Açıklama';

$_['entry_keyword']          = 'SEO URL'si';

$_['entry_bottom']           = 'Alt';

$_['entry_status']           = 'durum';

$_['entry_sort_order']       = 'Sıralama düzeni';

$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';



// Help

$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve SEO URL’nin global olarak benzersiz olduğundan emin olun.';

$_['help_bottom']            = 'Alt altbilgide göster.';

// Error

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_permission']       = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_name']            = 'form Başlığı 3 ile 64 karakter arasında olmalıdır!';

$_['error_desription']      = 'Açıklama 3 karakterden fazla olmalı!';



$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';

$_['error_keyword']          = 'SEO URL'si zaten kullanılıyor!';

$_['error_account']          = 'Uyarı: Bu bilgi sayfası, şu anda mağaza hesabı şartları olarak atandığından silinemiyor!';

$_['error_checkout']         = 'Uyarı: Bu bilgi sayfası, şu anda mağaza ödeme koşulları olarak atandığından silinemiyor!';

$_['error_affiliate']        = 'Uyarı: Bu bilgi sayfası, şu anda mağazaya bağlı kuruluş koşulları olarak atandığından silinemiyor!';

$_['error_return']           = 'Uyarı: Bu bilgi sayfası, şu anda mağaza iade koşulları olarak atandığından silinemez!';

$_['error_store']            = 'Uyarı: Bu bilgi sayfası şu anda% s mağazası tarafından kullanıldığından silinemiyor!';