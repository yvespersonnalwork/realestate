<?php

// Heading
$_['heading_title']          = 'Kategori Listesi';

// Text

$_['text_success']           = 'Başarı: Kategorileri değiştirdiniz!';
$_['text_list']              = 'Emlak Kategorisi Listesi';
$_['text_add']               = 'Kategori ekle';
$_['text_edit']              = 'Kategoriyi Düzenle';
$_['text_default']           = 'Varsayılan';

// Column
$_['column_name']            = 'Kategori adı';
$_['column_sort_order']      = 'Sıralama düzeni';
$_['column_action']          = 'Aksiyon';

/// Category Lable ////

$_['lable_name']             = 'Kategori adı';
$_['lable_description']      = 'Açıklama';
$_['lable_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['lable_meta_description'] = 'Meta Tag Açıklama';
$_['lable_meta_keyword']     = 'Meta Tag Anahtar Kelimeler';
$_['lable_parent']           = 'ebeveyn';
$_['lable_filter']           = 'Filtreler';
$_['lable_store']            = 'Web sitesi';
$_['lable_keyword']          = 'SEO URL'si';
$_['lable_image']            = 'görüntü';
$_['lable_top']              = 'Üst';
$_['lable_column']           = 'Sütunlar';
$_['lable_sort_order']       = 'Sıralama düzeni';
$_['lable_status']           = 'durum';
$_['lable_layout']           = 'Mizanpaj Geçersiz Kılma';

// Entry

$_['entry_name']             = 'Kategori adı';
$_['entry_description']      = 'Açıklama';
$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['entry_meta_keyword']     = 'Meta Tag Anahtar Kelimeler';
$_['entry_meta_description'] = 'Meta Tag Açıklama';
$_['entry_keyword']          = 'SEO URL'si';
$_['entry_parent']           = 'ebeveyn';
$_['entry_filter']           = 'Filtreler';
$_['entry_store']            = 'Web sitesi';
$_['entry_image']            = 'görüntü';
$_['entry_top']              = 'Üst';
$_['entry_column']           = 'Sütunlar';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_status']           = 'durum';
$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';

// Help

$_['help_filter']            = '(Otomatik tamamlama)';
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve SEO URL’nin global olarak benzersiz olduğundan emin olun.';
$_['help_top']               = 'Üst menü çubuğunda görüntüleyin. Yalnızca üst ana kategoriler için çalışır.';
$_['help_column']            = 'En alttaki 3 kategori için kullanılacak sütun sayısı. Yalnızca üst ana kategoriler için çalışır.';

// Error

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Kategorileri değiştirme izniniz yok!';
$_['error_name']             = 'Kategori Adı 2 ile 255 karakter arasında olmalıdır!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO URL'si zaten kullanılıyor!';
$_['error_parent']           = 'Seçtiğiniz üst kategori geçerli olanın çocuğu!';

