<?php

// Heading

$_['heading_title']    = 'Gayrimenkul Birimleri';

// Text

$_['text_success']     = 'Başarı: Mülkiyet birimlerini değiştirdiniz!';
$_['text_list']        = 'Emlak birimleri listesi';
$_['text_add']         = 'Emlak Birimi Ekle';
$_['text_edit']        = 'Mülkiyet Birimlerini Düzenle';
$_['text_success']     = 'Başarıyla: Mülkiyet biriminizi ekleyin!';
$_['text_successedit'] = 'başarıyla: değiştirilmiş mülk biriminiz';
$_['text_successdelete']= 'başarıyla: mülk biriminiz Sil';

// Column
$_['column_name']      = 'Mülkiyet birimi adı';
$_['column_action']    = 'Aksiyon';

// Lable 
$_['lable_name']       = 'Mülkiyet birimi adı';

// Entry
$_['entry_name']       = 'Mülkiyet birimi adı';



// Error

$_['error_permission'] = 'Uyarı: Mülkiyet birimlerini değiştirme izniniz yok!';
$_['error_name']       = 'Özellik Durumu Adı 3 ile 32 karakter arasında olmalıdır!';
$_['error_default']    = 'Uyarı: Bu özellik durumu, şu anda varsayılan mağaza özelliği durumu olarak atandığından silinemez!';
