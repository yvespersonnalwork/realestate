<?php

// Heading 

$_['heading_title']          = 'En yakın yer';

// Text

$_['text_success']           = 'Başarı: Bilgileri değiştirdiniz!';
$_['text_list']              = 'En yakın liste';
$_['text_add']               = 'Form ekle';
$_['text_edit']              = 'Formu düzenle';
$_['text_form']              = 'En Yakın Ekle';
$_['text_enable']            = 'etkinleştirme';
$_['text_disable']           = 'Devre dışı';
$_['text_default']           = 'Varsayılan';
$_['text_success']           = 'başarıyla ekle: En yakın yeri ekle!';
$_['text_successedit']       = 'başarıyla: en yakın yeri değiştirdiniz';
$_['text_successdelete']     = 'başarıyla: en yakın yer';

// Column

$_['column_image']        		= 'görüntü';
$_['column_name']            	= 'isim';
$_['column_sort_order']      	= 'Sıralama düzeni';
$_['column_status']           	= 'durum';
$_['column_action']           	= 'Aksiyon';

// Lable
$_['lable_name']            	= 'isim';
$_['lable_image']           	= 'image';
$_['lable_sort_order']       	= 'Sıralama düzeni';
$_['lable_status']           	= 'durum';

// Entry
$_['entry_name']            	= 'isim';
$_['entry_image']           	= 'image';
$_['entry_sort_order']       	= 'Sıralama düzeni';
$_['entry_status']           	= 'durum';

// Error

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Bilgileri değiştirme izniniz yok!';
$_['error_name']             = 'Ad Başlık 3 ile 64 karakter arasında olmalıdır!';

