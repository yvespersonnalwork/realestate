<?php

// Heading
$_['heading_title']    = ' Soruşturma';

// Text
$_['text_success']     = 'Başarı: Bülten Şablonunu değiştirdiniz!';
$_['text_export']     = 'ihracat';
$_['text_list']      = 'Emlak Sorgu Listesi';
$_['text_add']      = 'Talep ekle';
$_['text_edit']     = 'Sorguyu Düzenle';
$_['text_form']      = 'Talep Formu';
$_['text_success']           = 'başarıyla: Sorgu ekle!';
$_['text_successedit']       = 'başarıyla: Soruşturma değiştirdiniz';
$_['text_successdelete']     = 'başarıyla: sen Sorgulama Sil';
$_['text_enable']   = 'etkinleştirme';
$_['text_disable']   = 'devre dışı';

// Column
$_['column_property']   = 'özellik';
$_['column_name']      	= 'isim';
$_['column_email']     	= 'E-posta';
$_['column_agentname']  = 'Ajan adı';
$_['column_description']= 'Açıklama';
$_['column_action']    	= 'Aksiyon';

// Lable
$_['lable_property']   	= 'özellik';
$_['lable_name']        = 'isim';
$_['lable_email']       = 'E-posta';
$_['lable_agent']   	= 'ajan';
$_['lable_description'] = 'Açıklama';

// Entry
$_['entry_property']   	= 'özellik';
$_['entry_name']        = 'isim';
$_['entry_email']       = 'E-posta';
$_['entry_agent']   	= 'ajan';
$_['entry_description'] = 'Açıklama';

//button
$_['button_shortcut']       = 'Kısayol';

// Error
$_['error_permission'] 	= 'Uyarı: Haber bültenini değiştirme izniniz yok Şablon!';
$_['error_email_exist']	= 'E-posta Kimliği zaten var';
$_['error_email_id']    = 'Geçersiz e-posta formatı';
$_['error_email']      	= 'Geçersiz e-posta formatı';
$_['error_name']        = ' İsim 2 ile 255 karakter arasında olmalıdır!';



?>