<?php

// Heading

$_['heading_title']    = 'Gayrimenkul Durumları';

// Text

$_['text_success']     = 'Başarı: Özellik durumlarını değiştirdiniz!';
$_['text_list']        = 'Gayrimenkul Durum Listesi';
$_['text_add']         = 'Mülkiyet Durumu Ekle';
$_['text_edit']        = 'Mülkiyet Durumunu Düzenle';
$_['text_success']     = 'başarıyla: Mülkiyet durumunu ekle!';
$_['text_successedit'] = 'başarıyla: mülk durumunu değiştirdiniz';
$_['text_successdelete']= 'başarıyla: mülkiyet durumu sil';

// Column
$_['column_name']      = 'Mülk Durumu Adı';
$_['column_action']    = 'Aksiyon';

// Lable 
$_['lable_name']       = 'Mülk Durumu Adı';

// Entry
$_['entry_name']       = 'Mülk Durumu Adı';



// Error

$_['error_permission'] = 'Uyarı: Mülkiyet durumlarını değiştirme izniniz yok!';
$_['error_name']       = 'Özellik Durumu Adı 3 ile 32 karakter arasında olmalıdır!';
$_['error_default']    = 'Uyarı: Bu özellik durumu, şu anda varsayılan mağaza özelliği durumu olarak atandığından silinemez!';
