<?php

// Heading 

$_['heading_title']          = 'özellik ';

// Text

$_['text_success']           = 'Başarı: Bilgileri değiştirdiniz!';
$_['text_list']              = 'Özellik listesi';
$_['text_add']               = 'Form ekle';
$_['text_edit']              = 'Formu düzenle';
$_['text_enable']            = 'etkinleştirme';
$_['text_form']           	 = 'Özellik ekle';
$_['text_disable']           = 'Devre dışı';
$_['text_default']           = 'Varsayılan';
$_['text_success']           = 'Başarıyla: Size Özellik Ekleme';
$_['text_successedit']       = 'başarıyla: Özelliği değiştirdiniz';
$_['text_successdelete']     = 'başarıyla: sen Özellik Sil';

// Column

$_['column_image']           = 'görüntü';
$_['column_name']            = 'isim';
$_['column_sort_order']    	 = 'Sıralama düzeni';
$_['column_status']          = 'durum';
$_['column_action']          = 'Aksiyon';

// Lable

$_['lable_name']            = 'isim';
$_['lable_image']           = 'görüntü';
$_['lable_sort_order']       = 'Sıralama düzeni';
$_['lable_status']           = 'durum';

// Entry

$_['entry_name']            = 'isim';
$_['entry_image']           = 'görüntü';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_status']           = 'durum';

// Help

$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve SEO URL’nin global olarak benzersiz olduğundan emin olun.';
$_['help_bottom']            = 'Alt altbilgide göster.';

// Error

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission']       = 'Uyarı: Bilgileri değiştirme izniniz yok!';
$_['error_title']            = 'Uyarı: Bilgileri değiştirme izniniz yok!';
$_['error_name']            = ' İsim 2 ile 255 karakter arasında olmalıdır!';
$_['error_description']      = 'Açıklama 3 karakterden fazla olmalı!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO URL'si zaten kullanılıyor!';
$_['error_account']          = 'Uyarı: Bu bilgi sayfası şu anda Web sitesi hesap koşulları olarak atandığından silinemez!';
$_['error_checkout']         = 'Uyarı: Bu bilgi sayfası şu anda Web sitesi ödeme koşulları olarak atandığından silinemez!';
$_['error_affiliate']        = 'Uyarı: Bu bilgi sayfası şu anda Web sitesi bağlı şartları olarak atandığından silinemez!';
$_['error_return']           = 'Uyarı: Bu bilgi sayfası şu anda Web Sitesi iade şartları olarak atandığından silinemez!';
$_['error_store']            = 'Uyarı: Bu bilgi sayfası şu anda% s mağazası tarafından kullanıldığından silinemiyor!';

