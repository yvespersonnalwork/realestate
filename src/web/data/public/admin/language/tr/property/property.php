<?php

// Heading 

$_['heading_title']          = 'özellik ';

// Text
$_['text_success']           = 'Başarıyla: Mülkiyet ekle!';
$_['text_successedit']       = 'başarıyla: Mülkiyet değiştirildi';
$_['text_successdelete']     = 'başarıyla: Mülkiyet Sil';
$_['text_list']              = 'Emlak Listesi';
$_['text_add']               = 'Mülk ekle';
$_['text_edit']              = 'Mülk ekle';
$_['text_form']              = 'Mülk ekle';
$_['text_enable']            = 'etkinleştirme';
$_['text_disable']           = 'Devre dışı';
$_['text_default']           = 'Varsayılan';
$_['text_upload']            = 'Metniniz başarıyla yüklendi';
$_['outdoor_pool']           = 'Açık havuz';
$_['tab_customfield']        = 'görenek';
$_['button_desapprove']     = 'Desapprove';

/// Filter
// Column
$_['filterlablel_name']            = 'isim';
$_['filterlablel_property_status'] = 'Mülkiyet durumu';
$_['filterlablel_price']           = 'Fiyat';
$_['filterlablel_status']          = 'durum';
$_['filterlablel_price_range']     = 'Fiyat aralığı';
$_['filterlablel_price_from']      = 'itibaren';
$_['filterlablel_agent']           = 'ajan';

// Column
$_['column_image']           = 'görüntü';
$_['column_name']            = 'isim';
$_['column_property_status'] = 'Mülkiyet durumu';
$_['column_price']            = 'Fiyat';
$_['column_status']          = 'durum';
$_['column_action']          = 'Aksiyon';
$_['column_price_range']      = 'Fiyat aralığı';
$_['column_price_from']      = 'itibaren';
$_['column_agent']            = 'Ajan adı';
$_['column_category']         = 'Kategori';
$_['column_approved']         = 'onaylı';

// Lable
$_['lable_title']           = 'Başlık';
$_['lable_approved']           = 'onaylı';
$_['lable_video']           = 'Video';
$_['lable_neighborhood']    = 'Komşuluk';
$_['lable_area']    = 'alan';
$_['lable_bedrooms']    = 'Yatak odaları';
$_['lable_bathrooms']    = 'Banyolar';
$_['lable_roomcount']    = 'Oda sayısı';
$_['lable_Parkingspaces'] = 'Park alanları';
$_['lable_builtin']      = 'Yerleşik ';
$_['lable_amenities']    = 'Kolaylıklar';
$_['lable_arealenght']    = 'uzunluk';
$_['lable_pricelabel']    = 'Fiyat etiketi';
$_['lable_Price']           = 'Fiyat';
$_['lable_agent']           = 'ajan';
$_['lable_meta_title']      = 'Meta başlığı';
$_['lable_property_status'] = 'Mülkiyet durumu';
$_['lable_country']         = 'ülke';
$_['lable_Zone_region']     = 'Bölge / bölge';
$_['lable_city']            = 'Kent';
$_['lable_destinies']            = 'Kent';
$_['lable_meta_keyword']            = 'meta_keyword';
$_['lable_local_area']      = 'Yerel alan';
$_['lable_pincode']         = 'Posta kodu bölge / Posta Kodu';
$_['lable_latitude']         = 'Enlem';
$_['lable_longitude']         = 'Boylam';
$_['lable_images']            = 'görüntü';
$_['lable_titles']            = 'Başlık';
$_['lable_alt']               = 'Alt';
$_['lable_category']           = 'Alt';
$_['text_map']           	= 'Harita Alın';
$_['lable_title']         = 'Boylam';
$_['lable_meta_descriptions']= ' Meta Açıklaması';
$_['lable_tag']             = 'Etiket';
$_['lable_image']           = 'görüntü';
$_['lable_SEO_URL']         = 'SEO URL'si';
$_['lable_name']            = 'isim';
$_['lable_description']     ='Açıklama';
$_['lable_cetogarey']       ='Mağazalar';
$_['lable_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['lable_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';
$_['lable_meta_description'] = 'Meta Tag Açıklama';
$_['lable_keyword']          = 'SEO URL';
$_['lable_bottom']           = 'Alt';
$_['lable_status']           = 'durum';
$_['lable_sort_order']       = 'Sıralama düzeni';
$_['lable_layout']           = 'Mizanpaj Geçersiz Kılma';

// Entry
$_['entry_title']           = 'Başlık';
$_['entry_approved']           = 'onaylı';
$_['entry_video']           = 'Video';
$_['entry_neighborhood']    = 'Komşuluk';
$_['entry_area']    = 'alan';
$_['entry_bedrooms']    = 'Yatak odaları';
$_['entry_bathrooms']    = 'Banyolar';
$_['entry_roomcount']    = 'Oda sayısı';
$_['entry_Parkingspaces'] = 'Park alanları';
$_['entry_builtin']      = 'Yerleşik ';
$_['entry_amenities']    = 'Amenities';
$_['entry_arealenght']    = 'uzunluk';
$_['entry_pricelabel']    = 'Fiyat etiketi';
$_['entry_Price']           = 'Fiyat';
$_['entry_agent']           = 'ajan';
$_['entry_meta_title']      = 'Meta başlığı';
$_['entry_property_status'] = 'Mülkiyet durumu';
$_['entry_country']         = 'ülke';
$_['entry_Zone_region']     = 'Bölge / bölge';
$_['entry_city']            = 'Kent';
$_['entry_destinies']            = 'kader tanrıçaları';
$_['entry_meta_keyword']            = 'meta_keyword';
$_['entry_local_area']      = 'Yerel alan';
$_['entry_pincode']         = 'Posta kodu bölge / Posta Kodu';
$_['entry_latitude']         = 'Enlem';
$_['entry_longitude']         = 'Boylam';
$_['entry_images']            = 'görüntü';
$_['entry_titles']            = 'Başlık';
$_['entry_alt']               = 'Alt';
$_['entry_category']           = 'Kategori';
$_['text_map']           	= 'Harita Alın';
$_['entry_title']         = 'Boylam';
$_['entry_meta_descriptions']= ' Meta Açıklaması';
$_['entry_tag']             = 'Etiket';
$_['entry_image']           = 'görüntü';
$_['entry_SEO_URL']         = 'SEO URL';
$_['entry_name']            = 'isim';
$_['entry_description']     ='Açıklama';
$_['entry_cetogarey']       ='Mağazalar';
$_['entry_meta_title'] 	     = 'Meta Etiket Başlığı';
$_['entry_meta_keyword'] 	 = 'Meta Tag Anahtar Kelimeler';
$_['entry_meta_description'] = 'Meta Tag Açıklama';
$_['entry_keyword']          = 'SEO URL';
$_['entry_bottom']           = 'Alt';
$_['entry_status']           = 'durum';
$_['entry_sort_order']       = 'Sıralama düzeni';
$_['entry_layout']           = 'Mizanpaj Geçersiz Kılma';


$_['text_yards']           = 'kilometre';
$_['text_kanal']           = 'kanal';
$_['text_sqfit']           = 'sq ft';


// Help
$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve SEO URL’nin global olarak benzersiz olduğundan emin olun.';
$_['help_bottom']            = 'Alt altbilgide göster.';

// Error
$_['error_warning']          = 'Alt altbilgide göster!';
$_['error_permission']       = 'Uyarı: Bilgileri değiştirme izniniz yok!';
$_['error_title']            = 'form Başlığı 3 ile 64 karakter arasında olmalıdır!';
$_['error_name']             = ' İsim 2 ile 255 karakter arasında olmalıdır!';
$_['error_description']      = 'Açıklama 3 karakterden fazla olmalı!';
$_['error_meta_title']       = 'Meta Başlığı 3'ten büyük ve 255 karakterden kısa olmalıdır!';
$_['error_keyword']          = 'SEO URL'si zaten kullanılıyor!';
$_['error_account']          = 'Uyarı: Bu bilgi sayfası, şu anda mağaza hesabı şartları olarak atandığından silinemiyor!';
$_['error_checkout']         = 'Uyarı: Bu bilgi sayfası, şu anda mağaza ödeme koşulları olarak atandığından silinemiyor!';
$_['error_affiliate']        = 'Uyarı: Bu bilgi sayfası, şu anda mağazaya bağlı kuruluş koşulları olarak atandığından silinemiyor!';
$_['error_return']           = 'Uyarı: Bu bilgi sayfası, şu anda mağaza iade koşulları olarak atandığından silinemez!';
$_['error_store']            = 'Uyarı: Bu bilgi sayfası şu anda% s mağazası tarafından kullanıldığından silinemiyor!';
$_['error_country_id']       = 'Lütfen Ülke Alanını doldurun';
$_['error_zone_id']          = 'Lütfen Bölge Alanını doldurun ';
$_['error_sort_order']       = 'Lütfen Sıralama Düzenini doldurun ';
$_['error_status']         = 'Mülkiyet Durumu Seçin!';