<?php

// Heading

$_['heading_title']        = 'Özel alan';

// Text
$_['text_success']         = 'Başarı: Özel alanları değiştirdiniz!';
$_['text_list']            = 'Özel Alan Listesi';
$_['text_add']             = 'Add Custom Field';
$_['text_edit']            = 'Özel Alan Düzenle';
$_['text_choose']          = 'Seçmek';
$_['text_select']          = 'seçmek';
$_['text_radio']           = 'Radyo';
$_['text_checkbox']        = 'Onay Kutusu';
$_['text_input']           = 'Giriş';
$_['text_text']            = 'Metin';
$_['text_textarea']        = 'textarea';
$_['text_file']            = 'Dosya';
$_['text_date']            = 'tarih';
$_['text_datetime']        = 'Tarih ve amp; zaman';
$_['text_time']            = 'zaman';
$_['text_account']         = 'hesap';
$_['text_address']         = 'Adres';
$_['text_regex']           = 'regex';
$_['text_success']           = 'Başarıyla: Size Özel Alan Ekleyin';
$_['text_successedit']       = 'başarıyla: Özel Alanı değiştirdiniz';
$_['text_successdelete']     = 'başarıyla: Özel Alan Sil';

// Column
$_['column_name']          = 'Özel Alan Adı';
$_['column_type']          = 'tip';
$_['column_sort_order']    = 'Sıralama düzeni';
$_['column_action']        = 'Aksiyon';

// Lable
$_['lable_name']           = 'Özel Alan Adı';
$_['lable_type']           = 'tip';
$_['lable_status']         = 'durum';
$_['lable_sort_order']     = 'Sıralama düzeni';
$_['lable_value']          = 'değer';
$_['lable_validation']     = 'onaylama';
$_['lable_location']       = 'yer';
$_['lable_custom_value']   = 'Özel Alan Değeri Adı';
$_['lable_customer_group'] = 'müşteri grubu';
$_['entry_required']       = 'gereklidir';

// Entry
$_['entry_name']           = 'Özel Alan Adı';
$_['entry_type']           = 'tip';
$_['entry_status']         = 'durum';
$_['entry_sort_order']     = 'Sıralama düzeni';
$_['entry_value']          = 'değer';
$_['entry_validation']     = 'onaylama';
$_['entry_location']       = 'yer';
$_['entry_custom_value']   = 'Özel Alan Değeri Adı';
$_['entry_customer_group'] = 'müşteri grubu';
$_['entry_required']       = 'gereklidir';

// Help
$_['help_regex']           = 'Regex kullan. E.g: /[a-zA-Z0-9_-]/';
$_['help_sort_order']      = 'Setteki son alandan geriye doğru saymak için eksi kullanın.';

// Error
$_['error_permission']     = 'Uyarı: Özel alanları değiştirme izniniz yok!';
$_['error_name']           = 'Özel Alan Adı 1 ila 128 karakter arasında olmalıdır!';
$_['error_type']           = 'Uyarı: Özel Alan Değerleri gerekli!';
$_['error_custom_value']   = 'Özel Alan Değerleri gerekli!';
