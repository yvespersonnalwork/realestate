<?php

// Heading

$_['heading_title']        = 'API'ler';



// Text

$_['text_success']         = 'Başarı: API’leri değiştirdiniz!';

$_['text_list']            = 'API Listesi';

$_['text_add']             = 'API ekle';

$_['text_edit']            = 'API'yi düzenle';

$_['text_ip']              = 'Aşağıda, API’ye erişmesine izin verilen IP’lerin bir listesini oluşturabilirsiniz. Şu anki IP’niz %s';



// Column

$_['column_name']          = 'API Adı';

$_['column_status']        = 'durum';

$_['column_date_added']    = 'Ekleme Tarihi';

$_['column_date_modified'] = 'Değiştirilme tarihi';

$_['column_token']         = 'Jeton';

$_['column_ip']            = 'IP';

$_['column_action']        = 'Aksiyon';



// Entry

$_['entry_name']           = 'API Adı';

$_['entry_key']            = 'API Anahtarı';

$_['entry_status']         = 'durum';

$_['entry_ip']             = 'IP';



// Error

$_['error_permission']     = 'Uyarı: API’leri değiştirme izniniz yok.!';

$_['error_name']           = 'API Adı 3 ila 20 karakter arasında olmalıdır!';

$_['error_key']            = 'API Anahtarı 64 ila 256 karakter arasında olmalıdır!';

