<?php
// Heading
$_['heading_title']     	 = 'Ajan Posta Şablonu';

// Text
$_['text_success']       	 = 'Başarı: Sen değiştirdin !';
$_['text_list']           	 = 'Ajan göndermek için e-posta Şablon Listesi';
$_['text_add']          	 = 'Posta ekle';
$_['text_edit']         	 = 'Posta Düzenle';
$_['text_default']      	 = 'Varsayılan';
$_['text_enable']      	     = 'etkinleştirme';
$_['text_disable']      	 = 'Devre dışı';
$_['text_agentregister']     = 'ajan_kayıt olmak_posta';
$_['text_agentapproved']     = 'ajan_onaylı_posta';
$_['text_agentproperty']     = 'ajan_eklemek_özellik_yönetim_posta';
$_['text_propertyapp']       = 'özellik_onaylı_posta';
$_['text_agentforgotte']     = 'ajan_unutulmuş_posta';

// Column
$_['column_name']       	 = 'isim';
$_['column_date']        	 = 'Ekleme Tarihi';
$_['column_action']      	 = 'Aksiyon';

// Lable
$_['lable_subject']          = 'konu';
$_['lable_message']          = 'Mesaj';
$_['lable_name']             = 'isim';
$_['lable_status']           = 'durum';
$_['lable_type']             = 'tip';

// Entry
$_['entry_subject']          = 'konu';
$_['entry_message']          = 'Mesaj';
$_['entry_name']             = 'isim';
$_['entry_status']           = 'durum';
$_['entry_type']             = 'tip';

/// Tab
$_['tab_mail']               = 'Posta';
$_['tab_info']               = 'Bilgi';

// Error
$_['error_warning'] 		 = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';
$_['error_permission'] 		 = 'Uyarı: Üreticileri değiştirme izniniz yok!';
$_['error_name']             = 'İsim 2 ila 64 karakter arasında olmalıdır!';
$_['error_type']             = 'Lütfen Mail Tipini Seçiniz';