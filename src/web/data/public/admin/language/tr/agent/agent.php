<?php
// Heading
$_['heading_title']    		= 'ajan';

// Text
$_['text_success']     		= 'Başarı: Ajanı değiştirdiniz!';
$_['text_export']     		= 'ihracat';
$_['text_list']      		= 'Ajan Listesi';
$_['text_add']      		= 'Ajan ekle';
$_['text_edit']     		= 'Aracı Düzenle';
$_['text_enable']   		= 'etkinleştirme';
$_['text_disable']   		= 'Devre dışı';

// Filter
$_['filterlable_name']    	= 'isim';
$_['filterlable_status']    = 'durum';

// Column
$_['column_images']   		= 'Görüntüler';
$_['column_name']      		= 'isim';
$_['column_email']     		= 'E-posta';
$_['column_sort_order']  	= 'Sıralama düzeni';
$_['column_status']     	= 'durum';
$_['column_action']   	 	= 'Aksiyon';
$_['column_approve']    	= 'onaylamak';
$_['button_approve']     	= 'onaylamak';
$_['button_desapprove']     = 'Desapprove';

// Lable
$_['lable_Agent']        	= 'Ajan adı';
$_['lable_positions']   	= 'Pozisyonlar';
$_['lable_image'] 			= 'görüntü';
$_['lable_email']         	= 'E-posta';
$_['lable_address']        	= 'Adres';
$_['lable_password']        = 'Parola';
$_['lable_city']        	= 'Kent';
$_['lable_contact']        	= 'Temas';
$_['lable_pincode']        	= 'pincode';
$_['lable_sort_order']   	= 'Sıralama düzeni';
$_['lable_descriptions']   	= 'Açıklama';
$_['lable_country']      	= 'ülke';
$_['lable_status']      	= 'durum';

// Entry
$_['entry_Agent']        	= 'Ajan adı';
$_['entry_positions']   	= 'Pozisyonlar';
$_['entry_image'] 			= 'görüntü';
$_['entry_email']         	= 'E-posta';
$_['entry_address']        	= 'Adres';
$_['entry_password']        = 'Parola';
$_['entry_city']        	= 'Kent';
$_['entry_contact']        	= 'Temas';
$_['entry_pincode']        	= 'pincode';
$_['entry_sort_order']   	= 'Sıralama düzeni';
$_['entry_descriptions']   	= 'Açıklama';
$_['entry_country']      	= 'ülke';
$_['entry_status']      	= 'durum';

//button
$_['button_shortcut']       = 'Kısayol';
// Error

$_['error_email_exist']		= 'E-posta Kimliği zaten var';
$_['error_email']      		= 'Geçersiz e-posta formatı';
$_['error_agentname']            = ' İsim 2 ile 255 karakter arasında olmalıdır!';
$_['error_permission'] = 'Uyarı: Haber bültenini değiştirme izniniz yok Şablon!';

