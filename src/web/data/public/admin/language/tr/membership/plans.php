<?php

// Heading

$_['heading_title']          = 'Planlar';



// Text

$_['text_success']           = 'Başarı: Planları değiştirdiniz!';

$_['text_list']              = 'Planlar Listesi';

$_['text_add']               = 'Planlar Listesi';

$_['text_edit']              = 'Planları Düzenle';

$_['text_default']           = 'Varsayılan';

$_['text_day']          	 = 'Gün';

$_['text_month']          	 = 'Ay';

$_['text_years']           	 = 'Yıl';



$_['text_select']           = '---Seçeneği Seç-----';





$_['tab_variation']          = 'varyasyon';





$_['tab_extraoptions']       = 'Ekstra Seçenekler';

$_['button_remove']          = 'Kaldır';

$_['button_variation_add']   = 'Varyasyon ekle';

$_['button_extraoption_add'] = 'Seçenek ekle';



// Column

$_['column_name']            = 'Planlar Adı';

$_['column_image']           = 'görüntü';

$_['column_price']           = 'Fiyat';

$_['column_sort_order']      = 'Sıralama düzeni';

$_['column_action']          = 'Aksiyon';



// Entry

$_['entry_name']             = 'Planlar Adı';

$_['entry_description']      = 'Açıklama';

$_['entry_keyword']          = 'SEO URL'si';

$_['entry_image']            = 'görüntü';

$_['entry_price']            = 'Fiyat';

$_['entry_sort_order']       = 'Sıralama düzeni';

$_['entry_status']           = 'durum';

$_['entry_variation']        = 'varyasyon';





$_['enter_validate']        = 'varyasyon';

$_['enter_number']        = 'Numara';

$_['entry_title']            = 'Başlık';



// Help

$_['help_keyword']           = 'Boşluk kullanmayın, bunun yerine boşlukları değiştirin - ve SEO URL’nin global olarak benzersiz olduğundan emin olun.';



// Error

$_['error_warning']          = 'Uyarı: Lütfen hataları dikkatlice kontrol edin!';

$_['error_permission']       = 'Uyarı: Planları değiştirme izniniz yok!';

$_['error_name']             = 'Planlar Adı 2 ile 255 karakter arasında olmalıdır!';

$_['error_keyword']          = 'SEO URL'si zaten kullanılıyor!';

