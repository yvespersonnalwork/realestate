<?php

// Heading

$_['heading_title']       		 = 'düzenleri';



// Text

$_['text_success']        		 = 'Başarı: Düzenleri değiştirdiniz!';

$_['text_list']           		 = 'Page Layout Setting';

$_['text_add']            		 = 'Düzen ekle';

$_['text_edit']           		 = 'Düzeni Düzenle';

$_['text_remove']         		 = 'Kaldır';

$_['text_route']          		 = 'Bu düzende kullanılacak mağazayı ve rotaları seçin';

$_['text_module']         		 = 'Modüllerin pozisyonunu seçin';

$_['text_default']        		 = 'Varsayılan';

//xml

$_['text_column_slider']         = 'Sütun kaymak';

$_['text_column_testimonial']    = 'Sütun Övgüleri';

$_['text_content_propty']        = 'İçerik Mülkiyet';

$_['text_column_footer']         = 'Sütun Altbilgisi';

$_['text_column_footer4']         = 'Sütun Altbilgisi Galerisi';

$_['text_column_footer3']         = 'Sütun Altbilgi Bülteni';

$_['text_column_footer5']         = 'Sütun Altbilgisi5';

$_['text_content_top']    		 = 'İçerik Başı';

$_['text_content_bottom'] 		 = 'İçerik Alt';

$_['text_column_left']    		 = 'Sola Sütun';

$_['text_column_right']   		 = 'Sağ Sütun';



// Column

$_['column_name']         		 = 'Düzen Adı';

$_['column_action']       		 = 'Aksiyon';


// label 

$_['text_name']         		 = 'Düzen Adı';
$_['text_store']         		 = 'mağaza';
$_['text_route']        		 = 'Rota';
$_['text_module']       		 = 'modül';


// Entry

$_['entry_name']         		 = 'Düzen Adı';

$_['entry_store']         		 = 'mağaza';

$_['entry_route']        		 = 'Rota';

$_['entry_module']       		 = 'modül';



// Error

$_['error_permission']   		 = 'Uyarı: Düzenleri değiştirme izniniz yok.!';

$_['error_name']         		 = 'Mizanpaj Adı 3 ile 64 karakter arasında olmalıdır!';

$_['error_default']     		 = 'Uyarı: Bu düzen şu anda varsayılan mağaza düzeni olarak atandığından silinemez!';

$_['error_store']        		 = 'Uyarı: Bu düzen şu anda% s mağazalarına atandığından silinemiyor!';

$_['error_product']      		 = 'Uyarı: Bu düzen şu anda% s ürünlerine atandığı için silinemiyor!';

$_['error_category']     		 = 'Uyarı: Bu düzen şu anda% s kategorilerine atandığından silinemiyor!';

$_['error_information']  		 = 'Uyarı: Bu sayfa şu anda% s bilgi sayfasına atandığından silinemiyor!';

