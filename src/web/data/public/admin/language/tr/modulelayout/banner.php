<?php

// Heading

$_['heading_title']     = 'Afişler';



// Text

$_['text_success']      = 'Başarı: Afişleri değiştirdiniz!';

$_['text_list']         = 'Afiş Listesi';

$_['text_add']          = 'Afiş ekle';

$_['text_edit']         = 'Afişi Düzenle';

$_['text_default']      = 'Varsayılan';




// Column

$_['column_name']       = 'Banner Adı';
$_['column_status']     = 'durum';
$_['column_action']     = 'Aksiyon';
//label

$_['text_name']        = 'Banner Adı';
$_['text_title']       = 'Başlık';
$_['text_link']        = 'bağlantı';
$_['text_image']       = 'bağlantı';
$_['text_status']      = 'durum';
$_['text_sort_order']  = 'Sıralama düzeni';
// Entry
$_['entry_name']        = 'Banner Name';
$_['entry_title']       = 'Title';
$_['entry_link']        = 'bağlantı';
$_['entry_image']       = 'görüntü';
$_['entry_status']      = 'durum';
$_['entry_sort_order']  = 'Sıralama düzeni';

// Error

$_['error_permission'] = 'Uyarı: Afiş değiştirme izniniz yok!';

$_['error_name']       = 'Banner Adı 3 ile 64 karakter arasında olmalıdır!';

$_['error_title']      = 'Başlık Başlığı 2 ila 64 karakter arasında olmalıdır!';