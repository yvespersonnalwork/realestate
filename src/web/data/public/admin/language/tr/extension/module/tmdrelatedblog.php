<?php
// Heading
$_['heading_title']    = 'Tmd İlgili Blog';

// Text
$_['text_extension']   = 'Uzantıları';
$_['text_success']     = 'Success: You have modified Tmd Related Blog module!';
$_['text_edit']        = 'Tmd ile İlgili Blog Modülünü Düzenleyin';

// Entry
$_['entry_name']       = 'Modül Adı';
$_['entry_product']    = 'Ürünler';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Genişlik';
$_['entry_height']     = 'Yükseklik';
$_['entry_status']     = 'durum';

// Help
$_['help_product']     = '(Otomatik tamamlama)';

// Error
$_['error_permission'] = 'Uyarı: Tmd Related Blog modülünü değiştirme izniniz yok!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_width']      = 'Genişlik gerekli!';
$_['error_height']     = 'Gerekli yükseklik!';