<?php
// Heading
$_['heading_title']    = 'Tmd en son Blog';

// Text
$_['text_extension']   = 'Uzantıları';
$_['text_success']     = 'Başarı: tmdlatestblog modülünü değiştirdiniz!';
$_['text_edit']        = 'Tmd'nin En Son Blog Modülünü Düzenle';

// Entry
$_['entry_name']       = 'Modül Adı';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Genişlik';
$_['entry_height']     = 'Yükseklik';
$_['entry_status']     = 'durum';

// Error
$_['error_permission'] = 'Uyarı: tmdlatestblog modülünü değiştirme izniniz yok!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_width']      = 'Genişlik gerekli!';
$_['error_height']     = 'Gerekli yükseklik!';