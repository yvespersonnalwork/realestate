<?php

// Heading

$_['heading_title']    = 'Ara toplam';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Toplam toplamı değiştirdiniz!';

$_['text_edit']        = 'Alt Toplam Toplamı Düzenle';



// Entry

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Error

$_['error_permission'] = 'Uyarı: Toplam toplamı değiştirme izniniz yok.!';