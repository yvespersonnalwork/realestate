<?php

// Heading

$_['heading_title']       = 'Fotoğraf Galerisi (TMD)';



// Lable

$_['text_extension']      = 'Uzantıları';
$_['text_success']        = 'Başarı: Modül TMD Fotoğraf Galerisi'ni değiştirdiniz!';
$_['text_content_top']    = 'İçerik Başı';
$_['text_content_bottom'] = 'İçerik Alt';
$_['text_column_left']    = 'Sol Sütun';
$_['text_column_right']   = 'Sağ Sütun';
$_['text_edit']           = 'Fotoğraf Galerisini Düzenle (TMD)';

$_['text_name']           = 'Modül Adı';
$_['text_limit']          = 'limit'; 
$_['text_image']          = 'Resim (G x Y) ve Yeniden Boyutlandırma Türü';
$_['text_layout']         = 'düzen';
$_['text_position']       = 'pozisyon';
$_['text_status']         = 'durum';
$_['text_sort_order']     = 'Sıralama düzeni:';
$_['text_album']    	  = 'Albüm';
$_['text_width']          = 'Genişlik';
$_['text_height']         = 'Yükseklik';

// input value Entry
$_['entry_name']          = 'Modül Adı';
$_['entry_limit']         = 'limit'; 
$_['entry_image']         = 'Resim (G x Y) ve Yeniden Boyutlandırma Türü';
$_['entry_layout']        = 'düzen';
$_['entry_position']      = 'pozisyon';
$_['entry_status']        = 'durum';
$_['entry_sort_order']    = 'Sıralama düzeni:';
$_['entry_album']    	  = 'Albüm';
$_['entry_width']         = 'Genişlik';
$_['entry_height']        = 'Yükseklik';

// Error

$_['error_permission']    = 'Uyarı: TMD Photo Gallery modülünü değiştirme izniniz yok!';

$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';

$_['error_width']      = 'Genişlik gerekli!';

$_['error_height']     = 'Gerekli yükseklik!';

?>