<?php

// Heading

$_['heading_title']    = 'afiş';



// lable

$_['text_extension']   = 'Uzantıları';
$_['text_success']     = 'Başarı: Banner modülünü değiştirdiniz!';
$_['text_edit']        = 'Banner Modülünü Düzenle';
$_['text_name']        = 'Modül Adı';
$_['text_banner']      = 'afiş';
$_['text_dimension']   = 'Boyut (G x Y) ve Yeniden Boyutlandırma Türü';
$_['text_width']       = 'Genişlik';
$_['text_height']      = 'Yükseklik';
$_['text_status']      = 'durum';


// Entry

$_['entry_name']       = 'Modül Adı';

$_['entry_banner']     = 'afiş';

$_['entry_dimension']  = 'Boyut (G x Y) ve Yeniden Boyutlandırma Türü';

$_['entry_width']      = 'Genişlik';

$_['entry_height']     = 'Yükseklik';

$_['entry_status']     = 'durum';



// Error

$_['error_permission'] = 'Uyarı: Afiş modülünü değiştirme izniniz yok!';

$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';

$_['error_width']      = 'Genişlik gerekli!';

$_['error_height']     = 'Gerekli yükseklik!';