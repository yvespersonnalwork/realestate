<?php

// Heading

$_['heading_title']    = 'Ödül Puanları';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Toplam ödül puanını değiştirdiniz!';

$_['text_edit']        = 'Ödül Puanlarını Düzenle Toplam';



// Entry

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Error

$_['error_permission'] = 'Uyarı: Toplam ödül puanını değiştirme izniniz yok.!';