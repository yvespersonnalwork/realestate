<?php

// Heading

$_['heading_title']                    = 'Varsayılan Web Sitesi Teması';



// Text

$_['text_theme']                       = 'Temalar';

$_['text_success']                     = 'Başarı: Varsayılan Web sitesi temasını değiştirdiniz!';

$_['text_edit']                        = 'Varsayılan Web Sitesi Temasını Düzenle';

$_['text_general']                     = 'Genel';

$_['text_product']                     = 'Ürünler';

$_['text_image']                       = 'Görüntüler';



// Entry

$_['entry_directory']                  = 'Tema Dizini';

$_['entry_status']                     = 'durum';

$_['entry_product_limit']              = 'Sayfa Başına Varsayılan Öğeler';

$_['entry_product_description_length'] = 'Liste Açıklama Sınırı';

$_['entry_image_category']             = 'Kategori Resim Boyutu (W x H)';

$_['entry_image_thumb']                = 'Ürün Resmi Thumb Size (W x H)';

$_['entry_image_popup']                = 'Ürün Resmi Popup Size (W x H)';

$_['entry_image_product']              = 'Ürün Resim Listesi Boyutu (W x H)';

$_['entry_image_additional']           = 'Ek Ürün Resim Boyutu (W x H)';

$_['entry_image_related']              = 'İlgili Ürün Resim Boyutu (W x H)';

$_['entry_image_compare']              = 'Görüntü Boyutunu Karşılaştır (W x H)';

$_['entry_image_wishlist']             = 'İstek Listesi Resim Boyutu (W x H)';

$_['entry_image_cart']                 = 'Sepet resim boyutu (W x H)';

$_['entry_image_location']             = 'Web Sitesi Resim Boyutu (W x H)';

$_['entry_width']                      = 'Genişlik';

$_['entry_height']                     = 'Yükseklik';



// Help

$_['help_directory'] 	               = 'Bu alan yalnızca eski temaların yeni tema sistemiyle uyumlu olmasını sağlamak içindir. Burada belirtilen resim boyutu ayarlarında kullanmak için tema dizinini ayarlayabilirsiniz.';

$_['help_product_limit'] 	           = 'Sayfa başına kaç katalog öğesi gösterileceğini belirler (ürünler, kategoriler, vb.))';

$_['help_product_description_length']  = 'Liste görünümünde, kısa açıklama karakter sınırı (kategoriler, özel vb.)';



// Error

$_['error_permission']                 = 'Uyarı: Varsayılan mağaza temasını değiştirme izniniz yok!';

$_['error_limit']       	           = 'Ürün sınırı gerekli!';

$_['error_image_thumb']                = 'Ürün Resmi Thumb Boyut ölçüleri gerekli!';

$_['error_image_popup']                = 'Ürün Resmi Popup Boyut boyutları gerekli!';

$_['error_image_product']              = 'Ürün Listesi Boyut ölçüleri gerekli!';

$_['error_image_category']             = 'Kategori Listesi Boyut boyutları gerekli!';

$_['error_image_additional']           = 'Ek Ürün Resmi Boyut boyutları gerekli!';

$_['error_image_related']              = 'İlgili Ürün Resmi Boyut boyutları gerekli!';

$_['error_image_compare']              = 'Gerekli Resim Boyutu boyutlarını karşılaştırın!';

$_['error_image_wishlist']             = 'Dilek Listesi Resim Boyut ölçüleri gerekli!';

$_['error_image_cart']                 = 'Sepet Resmi Boyut boyutları gerekli!';

$_['error_image_location']             = 'Web Sitesi Resmi Boyut boyutları gerekli!';