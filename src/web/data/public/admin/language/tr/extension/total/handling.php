<?php

// Heading

$_['heading_title']    = 'Hizmet bedeli';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Toplam taşıma ücretini değiştirdiniz!';

$_['text_edit']        = 'Kullanım Ücreti Toplamını Düzenle';



// Entry

$_['entry_total']      = 'sipariş toplamı';

$_['entry_fee']        = 'ücret';

$_['entry_tax_class']  = 'Vergi Sınıfı';

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Help

$_['help_total']       = 'Bu sipariş toplamı aktif hale gelmeden önce siparişin ulaşması gereken toplam ödeme.';



// Error

$_['error_permission'] = 'Uyarı: Toplam taşıma ücreti değiştirme izniniz yok!';