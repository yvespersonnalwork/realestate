<?php

// Heading

$_['heading_title']    = 'Kupon';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Kupon toplamını değiştirdiniz!';

$_['text_edit']        = 'Kuponu Düzenle';



// Entry

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Error

$_['error_permission'] = 'Uyarı: Kupon toplamını değiştirme izniniz yok.!';