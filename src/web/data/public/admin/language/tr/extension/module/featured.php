<?php

// Heading

$_['heading_title']    = 'Öne çıkan';



// Text

$_['text_extension']   = 'Uzantıları';

$_['text_success']     = 'Başarı: Özellikli modülü değiştirdiniz!';

$_['text_edit']        = 'Öne Çıkan Modülü Düzenle';



// Entry

$_['entry_name']       = 'Modül Adı';

$_['entry_property']    = 'Özellikleri';

$_['entry_limit']      = 'limit';

$_['entry_width']      = 'Genişlik';

$_['entry_height']     = 'Yükseklik';

$_['entry_status']     = 'durum';



// Help

$_['help_product']     = '(Otomatik tamamlama)';



// Error

$_['error_permission'] = 'Uyarı: Öne çıkan modülü değiştirme izniniz yok!';

$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';

$_['error_width']      = 'Genişlik gerekli!';

$_['error_height']     = 'Gerekli yükseklik!';