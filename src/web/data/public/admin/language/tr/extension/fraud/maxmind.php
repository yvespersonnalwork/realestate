<?php

// Heading

$_['heading_title']                           = 'MaxMind Dolandırıcılık Karşıtı';



// Text

$_['text_extension']                          = 'Uzantıları';

$_['text_success']                            = 'Başarı: MaxMind sahtekarlığı önleme konusunda değişiklik yaptınız!';

$_['text_edit']                               = 'MaxMind Sahtekarlığı Önleme';

$_['text_signup']                             = 'MaxMind bir sahtekarlık algılama hizmetidir. Eğer yapmazsan\'t sahip olmak için bir lisans anahtarına sahip olmak <a href="http://www.maxmind.com/?rId=opencart" target="_blank"><u>kaydol here</u></a>.';

$_['text_country_match']                      = 'Ülke Eşleşmesi:';

$_['text_country_code']                       = 'Ülke kodu:';

$_['text_high_risk_country']                  = 'Yüksek Riskli Ülke:';

$_['text_distance']                           = 'Mesafe:';

$_['text_ip_region']                          = 'IP bölge:';

$_['text_ip_city']                            = 'IP Kent:';

$_['text_ip_latitude']                        = 'IP Kent:';

$_['text_ip_longitude']                       = 'IP Boylam:';

$_['text_ip_isp']                             = 'ISP:';

$_['text_ip_org']                             = 'IP organizasyon:';

$_['text_ip_asnum']                           = 'ASNUM:';

$_['text_ip_user_type']                       = 'IP Kullanıcı tipi:';

$_['text_ip_country_confidence']              = 'IP Ülke Güven:';

$_['text_ip_region_confidence']               = 'IP Bölge Güven:';

$_['text_ip_city_confidence']                 = 'IP Şehir Güven:';

$_['text_ip_postal_confidence']               = 'IP Posta Güven:';

$_['text_ip_postal_code']                     = 'IP posta kodu:';

$_['text_ip_accuracy_radius']                 = 'IP Doğruluk yarıçapı:';

$_['text_ip_net_speed_cell']                  = 'IP Net Hız Hücresi';

$_['text_ip_metro_code']                      = 'IP Metro Kodu:';

$_['text_ip_area_code']                       = 'IP Alan kodu:';

$_['text_ip_time_zone']                       = 'IP Saat dilimi:';

$_['text_ip_region_name']                     = 'IP Bölge Adı:';

$_['text_ip_domain']                          = 'IP domain:';

$_['text_ip_country_name']                    = 'IP Ülke adı:';

$_['text_ip_continent_code']                  = 'IP Kıta Kodu:';

$_['text_ip_corporate_proxy']                 = 'IP Kurumsal Proxy:';

$_['text_anonymous_proxy']                    = 'Anonim Proxy:';

$_['text_proxy_score']                        = 'Proxy Puanı:';

$_['text_is_trans_proxy']                     = 'Şeffaf Proxy mi:';

$_['text_free_mail']                          = 'Ücretsiz posta:';

$_['text_carder_email']                       = 'Carder Email:';

$_['text_high_risk_username']                 = 'Yüksek Riskli Kullanıcı Adı:';

$_['text_high_risk_password']                 = 'Yüksek Riskli Şifre:';

$_['text_bin_match']                          = 'Depo gözü eşleşmesi:';

$_['text_bin_country']                        = 'Bin Ülkesi:';

$_['text_bin_name_match']                     = 'Kutu Adı Eşleşmesi:';

$_['text_bin_name']                           = 'Depo Adı:';

$_['text_bin_phone_match']                    = 'Bin Telefon Eşleşmesi:';

$_['text_bin_phone']                          = 'Depo gözü telefonu:';

$_['text_customer_phone_in_billing_location'] = 'Fatura Konumundaki Müşteri Telefon Numarası:';

$_['text_ship_forward']                       = 'Nakliye nakliye:';

$_['text_city_postal_match']                  = 'City Postal Match:';

$_['text_ship_city_postal_match']             = 'Gönderi Şehri Posta Maçı:';

$_['text_score']                              = 'Gol:';

$_['text_explanation']                        = 'Gol:';

$_['text_risk_score']                         = 'Risk puanı:';

$_['text_queries_remaining']                  = 'Kalan Sorgular:';

$_['text_maxmind_id']                         = 'Kalan Sorgular:';

$_['text_error']                              = 'Hata:';



// Entry

$_['entry_key']                               = 'MaxMind Lisans Anahtarı';

$_['entry_score']                             = 'Risk puanı';

$_['entry_order_status']                      = 'Sipariş durumu';

$_['entry_status']                            = 'durum';



// Help

$_['help_order_status']                       = 'Belirlediğiniz risk puanının üzerinde bir puana sahip olan siparişler bu sipariş durumuna atanır ve otomatik olarak tam duruma ulaşmasına izin verilmez.';

$_['help_country_match']                      = 'IP adresi ülkesinin fatura adresi ülkesiyle uyuşup uyuşmadığı (uyumsuzluk = yüksek risk).';

$_['help_country_code']                       = 'IP adresinin ülke kodu.';

$_['help_high_risk_country']                  = 'IP adresinin veya fatura adresi ülkesinin Gana, Nijerya veya Vietnam'da olup olmadığı.';

$_['help_distance']                           = 'IP adresinden Faturalandırma Konumuna kilometre cinsinden uzaklık (büyük mesafe = daha yüksek risk).';

$_['help_ip_region']                          = 'IP adresinin Tahmini Eyalet / Bölge.';

$_['help_ip_city']                            = 'IP adresinin Tahmini Şehir.';

$_['help_ip_latitude']                        = 'IP adresinin Tahmini Enlemi.';

$_['help_ip_longitude']                       = 'IP adresinin Tahmini Boylamı.';

$_['help_ip_isp']                             = 'IP adresinin ISS'si.';

$_['help_ip_org']                             = 'IP adresinin organizasyonu.';

$_['help_ip_asnum']                           = 'IP adresinin Tahmini Özerk Sistem Numarası.';

$_['help_ip_user_type']                       = 'IP adresinin tahmini kullanıcı tipi.';

$_['help_ip_country_confidence']              = 'Ülkenin konumunun doğru olduğuna dair güvenimizi göstermek.';

$_['help_ip_region_confidence']               = 'Bölge konumunun doğru olduğuna dair güvenimizi göstermek.';

$_['help_ip_city_confidence']                 = 'Şehrin konumunun doğru olduğuna dair güvenimizi göstermek.';

$_['help_ip_postal_confidence']               = 'Posta kodu konumunun doğru olduğuna güvenimizi göstermek.';

$_['help_ip_postal_code']                     = 'IP adresinin Tahmini Posta Kodu.';

$_['help_ip_accuracy_radius']                 = 'IP adresini kullanan son kullanıcının gerçek konumu ile GeoIP City veri tabanı tarafından döndürülen konum arasındaki ortalama mesafe, mil cinsinden.';

$_['help_ip_net_speed_cell']                  = 'IP adresinin tahmini ağ tipi.';

$_['help_ip_metro_code']                      = 'IP adresinin Tahmini Metro Kodu.';

$_['help_ip_area_code']                       = 'IP adresinin Tahmini Alan Kodu.';

$_['help_ip_time_zone']                       = 'IP adresinin Tahmini Saat Dilimi.';

$_['help_ip_region_name']                     = 'IP adresinin Tahmini Bölge adı.';

$_['help_ip_domain']                          = 'IP adresinin tahmini etki alanı.';

$_['help_ip_country_name']                    = 'IP adresinin tahmini etki alanı....';

$_['help_ip_continent_code']                  = 'IP adresinin Tahmini Ülke adı....';

$_['help_ip_corporate_proxy']                 = 'IP'nin veritabanında Kurumsal bir Proxy olup olmadığı.';

$_['help_anonymous_proxy']                    = 'IP adresinin Anonim Proxy olup olmadığı (anonim proxy = çok yüksek risk).';

$_['help_proxy_score']                        = 'IP Adresinin Açık Proxy Olması Olasılığı.';

$_['help_is_trans_proxy']                     = 'IP adresinin bilinen saydam vekil sunucu veri tabanımızda olup olmadığı, eğer iletilen bir girdi olarak iletilirse döndürülür.';

$_['help_free_mail']                          = 'E-postanın ücretsiz e-posta sağlayıcısından olup olmadığı (ücretsiz e-posta = daha yüksek risk).';

$_['help_carder_email']                       = 'E-postanın yüksek riskli e-postaların veritabanında olup olmadığı.';

$_['help_high_risk_username']                 = 'UsernameMD5 girdisinin yüksek riskli kullanıcı isimlerinin veri tabanında olup olmadığı. Yalnızca kullanıcı adıMD5 girişlere dahilse döndürülür
.';

$_['help_high_risk_password']                 = 'PasswordMD5 girişinin yüksek riskli şifreler veritabanında olup olmadığı. Yalnızca girişlere passwordMD5 eklenmişse döndürülür.';

$_['help_bin_match']                          = 'BIN numarasına göre banka düzenleyen ülkenin fatura adres ülkesiyle eşleşip eşleşmediği.';

$_['help_bin_country']                        = 'BIN numarasına göre kredi kartını veren bankanın ülke kodu.';

$_['help_bin_name_match']                     = 'İhraç eden bankanın adının girilen BIN adıyla eşleşip eşleşmediği. Evet iadesi değeri, kart sahibinin kredi kartına sahip olduğuna dair olumlu bir gösterge sağlar.';

$_['help_bin_name']                           = 'BIN numarasına göre kredi kartını veren bankanın adı. BIN numaralarının yaklaşık% 96'sı için kullanılabilir.';

$_['help_bin_phone_match']                    = 'Müşteri servis telefon numarasının girilen BIN Telefon ile aynı olup olmadığı. Evet iadesi değeri, kart sahibinin kredi kartına sahip olduğuna dair olumlu bir gösterge sağlar..';

$_['help_bin_phone']                          = 'Kredi kartı arkasında listelenen müşteri hizmetleri telefon numarası. BIN numaralarının yaklaşık% 75'i için kullanılabilir. Bazı durumlarda, iade edilen telefon numarası eski olabilir.';

$_['help_customer_phone_in_billing_location'] = 'Müşteri telefon numarasının fatura posta kodunda olup olmadığı. Evet değerinde bir iade değeri, listelenen telefon numarasının kart sahibine ait olduğunu gösterir. No dönüş değeri telefon numarasının farklı bir alanda olabileceğini veya veritabanında listelenemeyebileceğini belirtir. Telefon numarası öneki veritabanımızda bulunamadığında NotFound döndürülür. Şu anda yalnızca ABD Telefon numaralarını destekliyoruz.';

$_['help_ship_forward']                       = 'Gönderim adresinin bilinen posta damlasının veritabanında olup olmadığı.';

$_['help_city_postal_match']                  = 'Fatura şehir ve eyaletin posta koduyla eşleşip eşleşmeyeceği. Şu anda yalnızca ABD adresleri için kullanılabilir durumda olan ABD dışındaki boş dizeyi döndürür.';

$_['help_ship_city_postal_match']             = 'Nakliye şehir ve eyaletin posta koduyla eşleşip eşleşmeyeceği. Şu anda yalnızca ABD adresleri için kullanılabilir durumda olan ABD dışındaki boş dizeyi döndürür.';

$_['help_score']                              = 'Yukarıda listelenen çıktılara dayanan genel dolandırıcılık puanı. Bu orijinal sahtekarlık puanıdır ve basit bir formüle dayanır. Risk skoruyla değiştirildi (aşağıya bakınız), fakat geriye dönük uyumluluk için saklandı.';

$_['help_explanation']                        = 'Formülümüze göre puanın kısa bir açıklaması, buna hangi faktörlerin katkıda bulunduğunu ayrıntılarıyla açıklamak. Lütfen, riske değil skora karşılık geldiğini unutmayın.';

$_['help_risk_score']                         = 'Geçmişte minFraud işlemlerinin analizine dayanarak, siparişin sahtekarlık olduğu tahmin edilen olasılığı temsil eden yeni dolandırıcılık puanı. Şubat 2007’den önce kaydolan müşteriler için yükseltme gerektirir.';

$_['help_queries_remaining']                  = 'Hesabınızda kalan sorgu sayısı, hesabınıza daha fazla sorgu eklemeniz gerektiğinde sizi uyarmak için kullanılabilir.';

$_['help_maxmind_id']                         = 'Sahte etkinlik rapor edildiğinde MaxMind'e geri bildirilirken işlemlere referans vermek için kullanılan benzersiz tanımlayıcı. Bu rapor MaxMind'in size sunduğu hizmeti iyileştirmesine yardımcı olacak ve geri ödeme geçmişinize dayanarak dolandırıcılık puanlama formülünü özelleştirmek için planlı bir özellik sağlayacaktır.';

$_['help_error']                              = 'Uyarı mesajı içeren bir hata dizesini veya isteğin başarısız olmasının bir nedenini döndürür.';



// Error

$_['error_permission']                        = 'Uyarı: MaxMind sahteciliği önleme konusunda değişiklik yapma izniniz yok.!';

$_['error_key']		                          = 'Lisans Anahtarı Gerekli!';