<?php
// Heading
$_['heading_title']    = 'TMD Kategori Araması';

// Text
$_['text_extension']   = 'Uzantıları';
$_['text_success']     = 'Başarı: Kategori Arama modülünü değiştirdiniz!';
$_['text_edit']        = 'Kategori Arama Modülünü Düzenle';

// Entry
$_['entry_status']     = 'durum';

// Error
$_['error_permission'] = 'Uyarı: Kategori Arama modülünü değiştirme izniniz yok!';