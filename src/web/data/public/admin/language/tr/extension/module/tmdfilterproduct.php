<?php
// Heading
$_['heading_title']    		= 'TMD Blog Filtre Ürünü<b>(TMD)</b>';

// Text
$_['text_extension']        = 'Uzantıları';
$_['text_success']     		= 'ExtensionsSuccess: Bir fliterproduct modülünü değiştirdiniz!';
$_['text_edit']        		= 'Fliterproduct Modülünü Düzenle';
$_['text_tab']         		= 'çıkıntı';
$_['text_vertcal']     		= 'Dikey / Normal';
$_['text_normal']      		= 'Normal';
$_['text_crousal']     		= 'Dikey + Carousel';
$_['text_tabcrousal']  		= 'Sekme + Carousel';

// Entry
$_['entry_name']       		= 'Modül Adı';
$_['entry_layouttype'] 		= 'Düzen türü';
$_['entry_crousaltype']		= 'Carousel Türü';
$_['entry_limit']      		= 'limit';
$_['entry_width']      		= 'Genişlik';
$_['entry_height']     		= 'Yükseklik';
$_['entry_status']     		= 'durum';
$_['entry_moduletitle']     = 'Modül Başlığı';
$_['entry_title']       	= 'Modül Başlığı';
$_['entry_product']     	= 'Ürün';

// Help


$_['tab_recentpost']     = 'Son mesaj Modülü';
$_['tab_popular']     = 'Popüler Modül';
$_['tab_comment']     = 'Yorum Yap';

// Error
$_['error_permission'] = 'Uyarı: fliterproduct modülünü değiştirme izniniz yok!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_width']      = 'Genişlik gerekli!';
$_['error_height']     = 'Gerekli yükseklik!';