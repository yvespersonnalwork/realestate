<?php

// Heading

$_['heading_title']     = 'Son 10 Liste';

// Column

$_['column_order_id']   = 'Sipariş Kimliği';

$_['column_customer']   = 'Müşteri';

$_['column_status']     = 'durum';

$_['column_total']      = 'Genel Toplam';

$_['column_date_added'] = 'Ekleme Tarihi';

$_['column_action']     	    = 'Aksiyon';

///

$_['column_property_status']	= 'Mülkiyet durumu';
$_['column_image']              = 'görüntü';
$_['column_name']            	= 'isim';
$_['column_price']           	= 'Fiyat';
$_['column_approved']        	= 'onaylı';
$_['column_status']         	= 'durum';
$_['column_view']         		= 'Görünüm';
$_['column_viewll']         	= 'Hepsini gör';
$_['text_disable']         		= 'Devre dışı';
$_['text_enable']         		= 'etkinleştirme';


////btn 
$_['button_approve']         		= 'onaylamak';