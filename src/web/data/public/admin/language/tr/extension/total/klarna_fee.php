<?php

// Heading

$_['heading_title']    = 'Klarna Ücreti';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Toplam Klarna ücretini değiştirdiniz!';

$_['text_edit']        = 'Klarna Ücreti Toplamını Düzenle';

$_['text_sweden']      = 'İsveç';

$_['text_norway']      = 'Norveç';

$_['text_finland']     = 'Finlandiya';

$_['text_denmark']     = 'Danimarka';

$_['text_germany']     = 'Almanya';

$_['text_netherlands'] = 'Hollanda';



// Entry

$_['entry_total']      = 'sipariş toplamı';

$_['entry_fee']        = 'Fatura Ücreti';

$_['entry_tax_class']  = 'Vergi Sınıfı';

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Error

$_['error_permission'] = 'Uyarı: Toplam Klarna ücretini değiştirme izniniz yok.!';