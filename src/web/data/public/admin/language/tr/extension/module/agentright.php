<?php

// Heading
$_['heading_title']    = 'Ajan Sağ taraf';

// Text

$_['text_extension']   = 'Uzantıları';
$_['text_success']     = 'Başarı: Ajan modülünü değiştirdiniz!';
$_['text_edit']        = 'Ajan Modülünü Düzenle';

// Lable
$_['lable_name']       = 'Modül Adı';
$_['lable_limit']      = 'Limit';
$_['lable_width']      = 'Genişlik';
$_['lable_height']     = 'Yükseklik';
$_['lable_status']     = 'durum';

// Entry
$_['entry_name']       = 'Modül Adı';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Genişlik';
$_['entry_height']     = 'Yükseklik';
$_['entry_status']     = 'durum';

// Error
$_['error_permission'] = 'Uyarı: Agent modülünü değiştirme izniniz yok!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_width']      = 'Genişlik gerekli!';
$_['error_height']     = 'Gerekli yükseklik!';