<?php

// Heading

$_['heading_title']    = 'Ajanlar Sol çubuk';



// Text

$_['text_extension']   = 'Uzantıları';

$_['text_success']     = 'Başarı: Aracılar modülünü değiştirdiniz!';

$_['text_edit']        = 'Aracı Modülünü Düzenle';



// Entry

$_['entry_status']     = 'durum';



// Error

$_['error_permission'] = 'Uyarı: Aracılar modülünü değiştirme izniniz yok!';

