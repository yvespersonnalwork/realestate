<?php

// Heading

$_['heading_title']    = 'Düşük Sipariş Ücreti';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Toplam düşük sipariş ücretini değiştirdiniz!';

$_['text_edit']        = 'Düşük Sipariş Ücreti Toplamını Düzenle';



// Entry

$_['entry_total']      = 'sipariş toplamı';

$_['entry_fee']        = 'ücret';

$_['entry_tax_class']  = 'Vergi Sınıfı';

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Help

$_['help_total']       = 'Bu sipariş toplamı iptal edilmeden önce siparişin ulaşması gereken toplam ödeme.';



// Error

$_['error_permission'] = 'Uyarı: Toplam düşük sipariş ücretini değiştirme izniniz yok.!';

