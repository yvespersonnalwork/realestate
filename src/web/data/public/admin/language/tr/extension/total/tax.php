<?php

// Heading

$_['heading_title']    = 'Vergiler';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Vergileri toplamı değiştirdiniz!';

$_['text_edit']        = 'Vergi Toplamını Düzenle';



// Entry

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Error

$_['error_permission'] = 'Uyarı: Vergi toplamını değiştirme izniniz yok!';