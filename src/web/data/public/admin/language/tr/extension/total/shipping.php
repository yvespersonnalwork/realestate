<?php

// Heading

$_['heading_title']    = 'Nakliye';



// Text

$_['text_total']       = 'Sipariş Toplamı';

$_['text_success']     = 'Başarı: Gönderim toplamını değiştirdiniz!';

$_['text_edit']        = 'Gönderi Toplamını Düzenle';



// Entry

$_['entry_estimator']  = 'Gönderi Tahmincisi';

$_['entry_status']     = 'durum';

$_['entry_sort_order'] = 'Sıralama düzeni';



// Error

$_['error_permission'] = 'Uyarı: Nakliye toplamını değiştirme izniniz yok.!';