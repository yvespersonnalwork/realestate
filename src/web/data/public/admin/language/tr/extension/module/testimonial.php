<?php

// Heading

$_['heading_title']       = 'bonservis (TMD)';



// Text

$_['text_extension']      = 'Uzantıları';
$_['text_success']        = 'Başarı: En son modülü değiştirdiniz!';
$_['text_content_top']    = 'İçerik Başı';
$_['text_content_bottom'] = 'İçerik Alt';
$_['text_column_left']    = 'Sol Sütun';
$_['text_column_right']   = 'Sağ Sütun';
$_['text_edit']           = 'Referansı Düzenle (TMD)';

//label 
$_['text_limit']         = 'limit'; 
$_['text_status']        = 'durum';
$_['text_name']          = 'Modül Adı';

// Entry
$_['entry_limit']         = 'Limit'; 
$_['entry_status']        = 'durum';
$_['entry_name']          = 'Modül Adı';


/// btn 
$_['button_save']             = 'Kayıt etmek';
$_['button_cancel']           = 'iptal etmek';

// Error

$_['error_permission']    = 'Uyarı: En son modülü değiştirme izniniz yok!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';

?>