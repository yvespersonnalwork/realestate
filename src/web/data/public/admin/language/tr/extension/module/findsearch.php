<?php

// Heading

$_['heading_title']    = 'Arama bul';

// Text
$_['text_extension']   = 'Uzantıları';
$_['text_edit']        = 'Arama Modülünü Bul Düzenle';
$_['text_status']      = 'durum';

///success msg 
$_['text_success']     = 'Başarı: Arama Bul modülünü değiştirdiniz!';

//  input Entry
$_['entry_status']     = 'durum';
// Error
$_['error_permission'] = 'Uyarı: Arama Bul modülünü değiştirme izniniz yok.!';


