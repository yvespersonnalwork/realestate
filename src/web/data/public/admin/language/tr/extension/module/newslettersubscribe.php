<?php

// Heading

$_['heading_title']    = '<span style="color:#33CCFF">Haber mektubu</span>';

$_['heading_title1']    = 'Haber mektubu';



// Text

$_['text_extension']      = 'Uzantıları';
$_['text_success']        = 'Başarı: Modülü değiştirdiniz Haber mektubu!';
$_['text_left']           = 'Ayrıldı';
$_['text_right']          = 'Sağ';
$_['text_edit']           = 'Haber mektubu düzenleme';
$_['text_text_default']   = 'Bu seçeneği etkinleştirdiğinizde, açık sepet kayıtlı kullanıcılar da bunu kullanarak abone olabilir veya abone olabilirler';
$_['text_info']           = '<span class="help">Tarafından geliştirilmiş <a href="http://www.probeseven.com" target="_blank"> Probeseven </a></span>';  
$_['text_module']         = 'Modüller';

//help

$_['help_name']          	= 'Buraya modül adı ekleyin.';
$_['help_status']        	=
$_['help_popup']         	= 'bülten formunu pop-up’da açmak istiyorsanız, Etkinleştir’i seçin;';
$_['help_unsubscribe']   	= 'E-bültende eposta aboneliğinden çıkmak istiyorsanız, buradan Etkinleştir'i seçin.';
$_['help_text']          	= 'Bülten butonu başlık formunu buraya ekle';
$_['help_text_color']    	= 'Haber bülteninin rengini buradan ayarlayın';
$_['help_text_color1']    	= 'Pop-up haber bülteni olmadan rengini ayarlama Metin buradan';
$_['help_bgtop']         	= 'Buradan haber bülteninin pop-up rengini ayarlayabilirsiniz';
$_['help_bgbottom']      	= 'Buradan, Derecelendirme efekti için bülten düğmesinin alt rengini belirleyebilirsiniz.';
$_['help_border']        	= 'Buradan Haber Bülteni Arkaplan rengi ekleyin';
$_['help_hover']         	= 'Bülten Ekle düğmesi Metin buradan';
$_['help_container']     	= 'Kutunun tam arka plan rengini buradan ayarlayın';


// Entry

$_['entry_name']  			= 'Modül Adı';
$_['entry_text_color1']  	= 'Varsayılan Bülten Metni';
$_['entry_unsubscribe'] 	= 'aboneliğini';
$_['entry_status']      	= 'durum';
$_['entry_options']    	 	= 'Seçenekler';
$_['entry_mail']   			= 'Eposta gönder';
$_['entry_popup']   		= 'Açılır';
$_['entry_registered'] 		= 'kayıtlı kullanıcılar';
$_['entry_text_color']  	= 'Bülten Metin rengi';
$_['entry_bgtop_color'] 	= 'Popup Renkleri';
$_['entry_bgbottom_color']  = 'Düğme Metni rengi';
$_['entry_border_color']   	= 'Bülten Düğmesi Bgcolor';
$_['entry_text']  			= 'Düğme Başlığı';
$_['entry_hover_color']   	= 'Düğme Vurgusu Metin rengi';
$_['entry_container_color'] = 'Konteyner bg renk';
$_['tab_general']   		= 'Genel';
$_['tab_data']      		= 'Rengi Özelleştir';


// Error
$_['error_permission']      = 'Uyarı: Modülü değiştirme izniniz yok Haber mektubu!';
$_['error_name']            = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';

?>