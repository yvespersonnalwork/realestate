<?php

// Heading

$_['heading_title']     = 'HTML İçeriği';



// Text

$_['text_extension']    = 'Uzantıları';
$_['text_success']      = 'Başarı: HTML İçerik modülünü değiştirdiniz!';
$_['text_edit']         = 'HTML İçerik Modülünü Düzenle';
// lable
$_['text_name']        = 'Modül Adı';
$_['text_title']       = 'Başlık Başlığı';
$_['text_description'] = 'Açıklama';
$_['text_status']      = 'durum';
// Entry
$_['entry_name']        = 'Modül Adı';
$_['entry_title']       = 'Başlık Başlığı';
$_['entry_description'] = 'Açıklama';
$_['entry_status']      = 'durum';

// Error
$_['error_permission']  = 'Uyarı: HTML İçerik modülünü değiştirme izniniz yok.!';
$_['error_name']        = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';