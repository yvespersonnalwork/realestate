<?php

// Heading

$_['heading_title']      = 'Ajan Düzenleme';



// Text

$_['text_extension']     = 'Uzantıları';

$_['text_success']       = 'Başarı: Sahtekarlık Karşıtı IP'yi değiştirdiniz!';

$_['text_edit']          = 'Dolandırıcılık Karşıtı IP’yi Düzenle';

$_['text_ip_add']        = 'IP Adresi Ekle';

$_['text_ip_list']       = 'IP Adresi Ekle...';



// Column

$_['column_ip']          = 'IP';

$_['column_total']       = 'Toplam hesap';

$_['column_date_added']  = 'Ekleme Tarihi';

$_['column_action']      = 'Aksiyon';



// Entry

$_['entry_ip']           = 'IP';

$_['entry_status']       = 'durum';

$_['entry_order_status'] = 'Sipariş durumu';



// Help

$_['help_order_status']  = 'Hesaplarında IP’si yasaklanmış olan müşterilere bu sipariş durumuna atanacak ve otomatik olarak tam statüye erişmesine izin verilmeyecek.';



// Error

$_['error_permission']   = 'Uyarı: Dolandırıcılık Karşıtı IP’yi değiştirme izniniz yok!';

