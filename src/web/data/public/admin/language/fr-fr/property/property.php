<?php

// Heading 

$_['heading_title']          = 'Propriété ';

// Text
$_['text_success']           = 'avec succès: Ajoutez votre propriété!';
$_['text_successedit']       = 'avec succès: vous avez modifié la propriété';
$_['text_successdelete']     = 'avec succès: vous propriété supprimer';
$_['text_list']              = 'Liste de propriété';
$_['text_add']               = 'Ajouter une propriété';
$_['text_edit']              = 'Editer la propriété';
$_['text_form']              = 'Ajouter une propriété';
$_['text_enable']            = 'Activer';
$_['text_disable']           = 'Désactiver';
$_['text_default']           = 'Défaut';
$_['text_upload']            = 'Votre texte envoyé avec succès';
$_['outdoor_pool']           = 'Piscine extérieure';
$_['tab_customfield']        = 'Douane';
$_['button_desapprove']     = 'Désapprouver';

/// Filter
// Column
$_['filterlablel_name']            = 'Nom';
$_['filterlablel_property_status'] = 'État de la propriété';
$_['filterlablel_price']           = 'Prix';
$_['filterlablel_status']          = 'Statut';
$_['filterlablel_price_range']     = 'Échelle des prix';
$_['filterlablel_price_from']      = 'De';
$_['filterlablel_agent']           = 'Agente';

// Column
$_['column_image']           = 'Image';
$_['column_name']            = 'Nom';
$_['column_property_status'] = 'État de la propriété';
$_['column_price']            = 'Prix';
$_['column_status']          = 'Statut';
$_['column_action']          = 'action';
$_['column_price_range']      = 'Échelle des prix';
$_['column_price_from']      = 'De';
$_['column_agent']            = 'Nom d\'agent';
$_['column_category']         = 'Catégorie';
$_['column_approved']         = 'Approuvée';

// Lable
$_['lable_title']           = 'Titre';
$_['lable_approved']           = 'Approuvée';
$_['lable_video']           = 'Vidéo';
$_['lable_neighborhood']    = 'quartier';
$_['lable_area']    = 'Zone';
$_['lable_bedrooms']    = 'Chambres';
$_['lable_bathrooms']    = 'Salle de bain';
$_['lable_roomcount']    = 'Nombre de chambre';
$_['lable_Parkingspaces'] = 'Places de parking';
$_['lable_builtin']      = 'Construit en ';
$_['lable_amenities']    = 'Équipements';
$_['lable_arealenght']    = 'Longueur';
$_['lable_pricelabel']    = 'Étiquette de prix';
$_['lable_Price']           = 'Prix';
$_['lable_agent']           = 'Agente';
$_['lable_meta_title']      = 'Titre méta';
$_['lable_property_status'] = 'État de la propriété';
$_['lable_country']         = 'Pays';
$_['lable_Zone_region']     = 'Zone / région';
$_['lable_city']            = 'Ville';
$_['lable_destinies']            = 'Destinies';
$_['lable_meta_keyword']            = 'meta_keyword';
$_['lable_local_area']      = 'Zone locale';
$_['lable_pincode']         = 'Code PIN / Zipcode';
$_['lable_latitude']         = 'Latitude';
$_['lable_longitude']         = 'Longitude';
$_['lable_images']            = 'Image';
$_['lable_titles']            = 'Titre';
$_['lable_alt']               = 'Alt';
$_['lable_category']           = 'Catégorie';
$_['text_map']           	= 'Obtenir la carte';
$_['lable_title']         = 'Longitude';
$_['lable_meta_descriptions']= ' Meta Description';
$_['lable_tag']             = 'Étiquette';
$_['lable_image']           = 'Image';
$_['lable_SEO_URL']         = 'URL de référencement';
$_['lable_name']            = 'Nom';
$_['lable_description']     ='La description';
$_['lable_cetogarey']       ='Magasins';
$_['lable_meta_title'] 	     = 'Titre de la balise méta';
$_['lable_meta_keyword'] 	 = 'Mots-clés méta';
$_['lable_meta_description'] = 'Meta Tag Description';
$_['lable_keyword']          = 'URL de référencement';
$_['lable_bottom']           = 'Bas';
$_['lable_status']           = 'StatusStatut';
$_['lable_sort_order']       = 'Ordre de tri';
$_['lable_layout']           = 'Remplacement de mise en page';

// Entry
$_['entry_title']           = 'Titre';
$_['entry_approved']           = 'Approuvée';
$_['entry_video']           = 'Vidéo';
$_['entry_neighborhood']    = 'quartier';
$_['entry_area']    = 'Zone';
$_['entry_bedrooms']    = 'Chambres';
$_['entry_bathrooms']    = 'Salle de bain';
$_['entry_roomcount']    = 'Nombre de chambre';
$_['entry_Parkingspaces'] = 'Places de parking';
$_['entry_builtin']      = 'Construit en ';
$_['entry_amenities']    = 'Équipements';
$_['entry_arealenght']    = 'Longueur';
$_['entry_pricelabel']    = 'Étiquette de prix';
$_['entry_Price']           = 'Prix';
$_['entry_agent']           = 'Agente';
$_['entry_meta_title']      = 'Titre méta';
$_['entry_property_status'] = 'État de la propriété';
$_['entry_country']         = 'Pays';
$_['entry_Zone_region']     = 'Zone / région';
$_['entry_city']            = 'Ville';
$_['entry_destinies']            = 'Destinies';
$_['entry_meta_keyword']            = 'meta_keyword';
$_['entry_local_area']      = 'Zone locale';
$_['entry_pincode']         = 'Code PIN / Zipcode';
$_['entry_latitude']         = 'Latitude';
$_['entry_longitude']         = 'Longitude';
$_['entry_images']            = 'Image';
$_['entry_titles']            = 'Titre';
$_['entry_alt']               = 'Alt';
$_['entry_category']           = 'Catégorie';
$_['text_map']           	= 'Obtenir la carte';
$_['entry_title']         = 'Longitude';
$_['entry_meta_descriptions']= ' Meta Description';
$_['entry_tag']             = 'Étiquette';
$_['entry_image']           = 'Image';
$_['entry_SEO_URL']         = 'URL de référencement';
$_['entry_name']            = 'Nom';
$_['entry_description']     ='La description';
$_['entry_cetogarey']       ='Magasins';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'URL de référencement';
$_['entry_bottom']           = 'Bas';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_layout']           = 'Remplacement de mise en page';


$_['text_yards']           = 'cours';
$_['text_kanal']           = 'Kanal';
$_['text_sqfit']           = 'ajustement carré';


// Help
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que l\'URL de référencement est unique au monde..';
$_['help_bottom']            = 'Afficher en bas de page.';

// Error
$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des informations!';
$_['error_title']            = 'Le titre du formulaire doit comporter entre 3 et 64 caractères.!';
$_['error_name']             = ' Le nom doit comporter entre 2 et 255 caractères.!';
$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'URL de référencement déjà utilisée!';
$_['error_account']          = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de magasin.!';
$_['error_checkout']         = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée en tant que conditions de caisse.!';
$_['error_affiliate']        = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes affiliés du magasin.!';
$_['error_return']           = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour du magasin.!';
$_['error_store']            = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';
$_['error_country_id']       = 'Veuillez remplir le champ du pays';
$_['error_zone_id']          = 'S\'il vous plaît remplir le champ de la zone ';
$_['error_sort_order']       = 'Veuillez remplir l\'ordre de tri ';
$_['error_status']         = 'Sélectionnez le statut de la propriété!';