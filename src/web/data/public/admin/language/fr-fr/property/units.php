<?php

// Heading

$_['heading_title']    = 'Unités de propriété';

// Text

$_['text_success']     = 'Succès: vous avez modifié des unités de propriété!';
$_['text_list']        = 'Liste des unités de propriété';
$_['text_add']         = 'Ajouter des unités de propriété';
$_['text_edit']        = 'Modifier les unités de propriété';
$_['text_success']     = 'avec succès: Ajouter votre unité de propriété!';
$_['text_successedit'] = 'avec succès: votre unité de propriété modifiée';
$_['text_successdelete']= 'avec succès: votre unité de propriété Supprimer';

// Column
$_['column_name']      = 'Nom de l\'unité de propriété';
$_['column_action']    = 'action';

// Lable 
$_['lable_name']       = 'Nom de l\'unité de propriété';

// Entry
$_['entry_name']       = 'Nom de l\'unité de propriété';



// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier des unités de propriété.!';
$_['error_name']       = 'Propriété Le nom de l\'état doit comporter entre 3 et 32 caractères.!';
$_['error_default']    = 'Avertissement: Cet état de propriété ne peut pas être supprimé car il est actuellement affecté en tant que statut de propriété de magasin par défaut.!';
