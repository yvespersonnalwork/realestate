<?php
// Heading
$_['heading_title']     = 'Commentaires';

// Text
$_['text_success']      = 'Succès: vous avez modifié les avis.!';
$_['text_list']         = 'Liste de revue';
$_['text_add']          = 'Ajouter un commentaire';
$_['text_edit']         = 'Modifier la critique';

// Column
$_['column_property']   = 'Propriété';
$_['column_author']     = 'Auteure';
$_['column_rating']     = 'Évaluation';
$_['column_status']     = 'Statut';
$_['column_date_added'] = 'date ajoutée';
$_['column_action']     = 'action';

// Entry
$_['entry_property']    = 'Propriété';
$_['entry_author']      = 'Auteure';
$_['entry_rating']      = 'Évaluation';
$_['entry_status']      = 'Statut';
$_['entry_text']        = 'Texte';
$_['entry_date_added']  = 'date ajoutée';


//13/2/2018/
$_['text_success']           = 'avec succès: Ajoutez vos avis ';
$_['text_successedit']       = 'avec succès: vous avez modifié';
$_['text_successdelete']     = 'avec succès: vous Commentaires Supprimer';
//13/2/2018/


// Help
$_['help_product']      = '(Saisie automatique)';

// Error
$_['error_permission']  = 'Avertissement: vous n\'êtes pas autorisé à modifier les avis.!';
$_['error_product']     = 'Produit requis!';
$_['error_author']      = 'L\'auteur doit comporter entre 3 et 64 caractères.!';
$_['error_text']        = 'Le texte de révision doit comporter au moins 1 caractère.!';
$_['error_rating']      = 'Note d\'évaluation requise!';