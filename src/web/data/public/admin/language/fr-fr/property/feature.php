<?php

// Heading 

$_['heading_title']          = 'Fonctionnalité ';

// Text

$_['text_success']           = 'Succès: vous avez modifié l\'information!';
$_['text_list']              = 'Liste des fonctionnalités';
$_['text_add']               = 'Ajouter un formulaire';
$_['text_edit']              = 'Modifier le formulaire';
$_['text_enable']            = 'Activer';
$_['text_form']           	 = 'Ajouter une fonctionnalité';
$_['text_disable']           = 'Désactiver';
$_['text_default']           = 'Défaut';
$_['text_success']           = 'avec succès: Ajoutez votre fonction';
$_['text_successedit']       = 'avec succès: vous avez modifié';
$_['text_successdelete']     = 'avec succès: vous fonctionnalité Supprimer';

// Column

$_['column_image']           = 'Image';
$_['column_name']            = 'Nom';
$_['column_sort_order']    	 = 'Ordre de tri';
$_['column_status']          = 'Statut';
$_['column_action']          = 'action';

// Lable

$_['lable_name']            = 'Nom';
$_['lable_image']           = 'image';
$_['lable_sort_order']       = 'Ordre de tri';
$_['lable_status']           = 'Statut';

// Entry

$_['entry_name']            = 'Nom';
$_['entry_image']           = 'image';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_status']           = 'Statut';

// Help

$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que l\'URL de référencement est unique au monde..';
$_['help_bottom']            = 'Afficher en bas de page.';

// Error

$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des informations!';
$_['error_title']            = 'Le titre du formulaire doit comporter entre 3 et 64 caractères.!';
$_['error_name']            = ' Le nom doit comporter entre 2 et 255 caractères.!';
$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'URL de référencement déjà utilisée!';
$_['error_account']          = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de site Web.!';
$_['error_checkout']         = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de commande du site Web.!';
$_['error_affiliate']        = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions d’affiliation au site Web.!';
$_['error_return']           = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour du site Web.!';
$_['error_store']            = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';

