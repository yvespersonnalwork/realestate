<?php

// Heading 

$_['heading_title']          = 'Endroit le plus proche';

// Text

$_['text_success']           = 'Succès: vous avez modifié l\'information!';
$_['text_list']              = 'Liste la plus proche';
$_['text_add']               = 'Ajouter un formulaire';
$_['text_edit']              = 'Modifier le formulaire';
$_['text_form']              = 'Ajouter le plus proche';
$_['text_enable']            = 'Activer';
$_['text_disable']           = 'Désactiver';
$_['text_default']           = 'Défaut';
$_['text_success']           = 'avec succès: Ajoutez votre place la plus proche!';
$_['text_successedit']       = 'avec succès: vous avez modifié l\'endroit le plus proche';
$_['text_successdelete']     = 'avec succès: vous Lieu le plus proche Supprimer';

// Column

$_['column_image']        		= 'Image';
$_['column_name']            	= 'Nom';
$_['column_sort_order']      	= 'Ordre de tri';
$_['column_status']           	= 'Statut';
$_['column_action']           	= 'action';

// Lable
$_['lable_name']            	= 'Nom';
$_['lable_image']           	= 'image';
$_['lable_sort_order']       	= 'Ordre de tri';
$_['lable_status']           	= 'Statut';

// Entry
$_['entry_name']            	= 'Nom';
$_['entry_image']           	= 'image';
$_['entry_sort_order']       	= 'Ordre de tri';
$_['entry_status']           	= 'Statut';

// Error

$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des informations!';
$_['error_name']             = 'Nom Le titre doit comporter entre 3 et 64 caractères.!';

