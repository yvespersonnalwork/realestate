<?php
// Heading
$_['heading_title']         = 'Client';

// Text
$_['text_success']          = 'Succès: vous avez modifié des clients!';
$_['text_list']             = 'Liste de clients';
$_['text_add']              = 'Ajouter un client';
$_['text_edit']             = 'Modifier le client';
$_['text_default']          = 'Défaut';
$_['text_balance']          = 'Équilibre';

// Column
$_['column_name']           = 'Nom du client';
$_['column_plans']           = 'plan';
$_['column_email']          = 'Email';
$_['column_customer_group'] = 'Groupe de clients';
$_['column_status']         = 'Statut';
$_['column_date_added']     = 'date ajoutée';
$_['column_comment']        = 'Commentaire';
$_['column_description']    = 'La description';
$_['column_amount']         = 'Montante';
$_['column_points']         = 'Points';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Total des comptes';
$_['column_action']         = 'action';

// Entry
$_['entry_customer_group']  = 'Groupe de clients';
$_['entry_firstname']       = 'Prénom';
$_['entry_plans']           = 'des plans';
$_['entry_lastname']        = 'Nom de famille';
$_['entry_email']           = 'Email';
$_['entry_telephone']       = 'Téléphone';
$_['entry_fax']             = 'Fax';
$_['entry_newsletter']      = 'Newsletter';
$_['entry_status']          = 'Status';
$_['entry_approved']        = 'Approved';
$_['entry_safe']            = 'Safe';
$_['entry_password']        = 'Password';
$_['entry_confirm']         = 'Confirm';
$_['entry_company']         = 'Company';
$_['entry_address_1']       = 'Address 1';
$_['entry_address_2']       = 'Address 2';
$_['entry_city']            = 'City';
$_['entry_postcode']        = 'Postcode';
$_['entry_country']         = 'Country';
$_['entry_zone']            = 'Region / State';
$_['entry_default']         = 'Default Address';
$_['entry_comment']         = 'Comment';
$_['entry_description']     = 'Description';
$_['entry_amount']          = 'Amount';
$_['entry_points']          = 'Points';
$_['entry_name']            = 'Customer Name';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = 'Date Added';

// Help
$_['help_safe']             = 'Set to true to avoid this customer from being caught by the anti-fraud system';
$_['help_points']           = 'Use minus to remove points';

// Error
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']      = 'Warning: You do not have permission to modify customers!';
$_['error_exists']          = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']       = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['error_email']           = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']       = 'Telephone must be between 3 and 32 characters!';
$_['error_password']        = 'Password must be between 4 and 20 characters!';
$_['error_confirm']         = 'Password and password confirmation do not match!';
$_['error_address_1']       = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']            = 'City must be between 2 and 128 characters!';
$_['error_postcode']        = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']         = 'Please select a country!';
$_['error_zone']            = '%s required!';