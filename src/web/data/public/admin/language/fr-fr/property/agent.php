<?php

// Heading



$_['heading_title']    = 'Agente';



// Text

$_['text_success']     = "Succès: vous avez modifié le modèle de lettre d\'information!";

$_['text_export']     = 'Exportation';

$_['text_list']      = "Modèle d\'agent";

$_['text_add']      = 'Ajouter un agent';

$_['text_edit']     = "Agent d\'édition";

$_['text_enable']   = 'Activer';

$_['text_disable']   = 'Désactiver';

// Column

$_['column_name']      		= 'Nom';

$_['column_images']   		= 'Images';

$_['column_sort_order']  	= 'Ordre de tri';

$_['column_email']     		= 'Email';

$_['column_status']     	= 'Statut';

$_['column_action']   	 	= 'action';

$_['column_agent']    		= "Nom d\'agent";

$_['column_approve']    	= 'Approuver';

$_['button_approve']     	= 'Approuver';





//13/2/2018/

$_['text_success']           = "avec succès: ajoutez votre type d\'agent ";

$_['text_successedit']       = "avec succès: vous avez modifié le type d\'agent ";

$_['text_successdelete']     = "avec succès: vous avez modifié le type d\'agent";

//13/2/2018/







// Entry

$_['entry_Agent']        = "Nom d\'agent";

$_['entry_descriptions']   = 'La description';

$_['entry_positions']   = 'Positions';

$_['entry_code']        = 'Email';

$_['entry_description'] = 'La description';

$_['entry_country']      = 'Pays';

$_['entry_email']         = 'Email';

$_['entry_address']        = 'Adresse';

$_['entry_password']        = 'Mot de passe';

$_['entry_city']        = 'Ville';

$_['entry_pincode']        = 'Code PIN';

$_['entry_contact']        = 'Contact';

$_['sort_order']           = 'Ordre de tri';

$_['entry_image'] = 'Image';

$_['entry_subject']     = 'Matière';

$_['entry_sort_order']   = 'Ordre de tri';

$_['entry_status']      = 'Statut';



//button

$_['button_shortcut']       = 'Raccourcie';



// Error

$_['error_email_exist']= 'Email Id existe déjà';

$_['error_email_id']      = "Format d\'email invalide";

$_['error_email']      = "Format d\'email invalide";

$_['error_agentname']            = ' Le nom doit comporter entre 2 et 255 caractères.!';





$_['error_permission'] = "Avertissement: vous n\'êtes pas autorisé à modifier le modèle de newsletter.!";



?>