<?php

// Heading
$_['heading_title']    = ' Enquête';

// Text
$_['text_success']     = 'Succès: vous avez modifié le modèle de lettre d\'information!';
$_['text_export']     = 'Exportation';
$_['text_list']      = 'Liste des propriétés';
$_['text_add']      = 'Ajouter une demande';
$_['text_edit']     = 'Modifier la demande';
$_['text_form']      = 'Formulaire';
$_['text_success']           = 'avec succès: Ajoutez votre demande!';
$_['text_successedit']       = 'avec succès: vous avez modifié l\'enquête';
$_['text_successdelete']     = 'avec succès: vous enquête supprimer';
$_['text_enable']   = 'activer';
$_['text_disable']   = 'désactiver';

// Column
$_['column_property']   = 'Propriété';
$_['column_name']      	= 'Nom';
$_['column_email']     	= 'Email';
$_['column_agentname']  = 'Nom d\'agent';
$_['column_description']= 'Description';
$_['column_action']    	= 'Action';

// Lable
$_['lable_property']   	= 'Propriété';
$_['lable_name']        = 'Nom';
$_['lable_email']       = 'Email';
$_['lable_agent']   	= 'Agente';
$_['lable_description'] = 'La description';

// Entry
$_['entry_property']   	= 'Property';
$_['entry_name']        = 'Name';
$_['entry_email']       = 'Email';
$_['entry_agent']   	= 'Agente';
$_['entry_description'] = 'La description';

//button
$_['button_shortcut']       = 'Raccourcie';

// Error
$_['error_permission'] 	= 'Avertissement: vous n\'êtes pas autorisé à modifier le modèle de newsletter.!';
$_['error_email_exist']	= 'Email Id existe déjà';
$_['error_email_id']    = 'Format d\'email invalide';
$_['error_email']      	= 'Format d\'email invalide';
$_['error_name']        = ' Le nom doit comporter entre 2 et 255 caractères.!';



?>