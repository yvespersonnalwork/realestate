<?php

// Heading

$_['heading_title']        = 'Champ personnalisé';

// Text
$_['text_success']         = 'Succès: vous avez modifié des champs personnalisés!';
$_['text_list']            = 'Liste de champs personnalisés';
$_['text_add']             = 'Ajouter un champ personnalisé';
$_['text_edit']            = 'Modifier le champ personnalisé';
$_['text_choose']          = 'Choisir';
$_['text_select']          = 'Sélectionner';
$_['text_radio']           = 'Radio';
$_['text_checkbox']        = 'Case à cocher';
$_['text_input']           = 'Contribution';
$_['text_text']            = 'Texte';
$_['text_textarea']        = 'Textarea';
$_['text_file']            = 'Fichier';
$_['text_date']            = 'Date';
$_['text_datetime']        = 'Date &amp; Temps';
$_['text_time']            = 'Temps';
$_['text_account']         = 'Compte';
$_['text_address']         = 'Adresse';
$_['text_regex']           = 'Regex';
$_['text_success']           = 'avec succès: Ajoutez votre champ personnalisé';
$_['text_successedit']       = 'avec succès: vous avez modifié le champ personnalisé';
$_['text_successdelete']     = 'avec succès: votre champ personnalisé supprimer';

// Column
$_['column_name']          = 'Nom du champ personnalisé';
$_['column_type']          = 'Type';
$_['column_sort_order']    = 'Ordre de tri';
$_['column_action']        = 'action';

// Lable
$_['lable_name']           = 'Nom du champ personnalisé';
$_['lable_type']           = 'Type';
$_['lable_status']         = 'Statut';
$_['lable_sort_order']     = 'Ordre de tri';
$_['lable_value']          = 'Valeur';
$_['lable_validation']     = 'Validation';
$_['lable_location']       = 'Emplacement';
$_['lable_custom_value']   = 'Nom de la valeur du champ personnalisé';
$_['lable_customer_group'] = 'Groupe de clients';
$_['entry_required']       = 'Champs obligatoires';

// Entry
$_['entry_name']           = 'Nom du champ personnalisé';
$_['entry_type']           = 'Type';
$_['entry_status']         = 'Statut';
$_['entry_sort_order']     = 'Ordre de tri';
$_['entry_value']          = 'Valeur';
$_['entry_validation']     = 'Validation';
$_['entry_location']       = 'Emplacement';
$_['entry_custom_value']   = 'Nom de la valeur du champ personnalisé';
$_['entry_customer_group'] = 'Groupe de clients';
$_['entry_required']       = 'Champs obligatoires';

// Help
$_['help_regex']           = 'Utilisez regex. E.g: /[a-zA-Z0-9_-]/';
$_['help_sort_order']      = 'Utilisez le moins pour compter en arrière à partir du dernier champ de l\'ensemble.';

// Error
$_['error_permission']     = "Avertissement: vous n\'êtes pas autorisé à modifier des champs personnalisés.!";
$_['error_name']           = 'Le nom du champ personnalisé doit comporter entre 1 et 128 caractères.!';
$_['error_type']           = 'Avertissement: valeurs de champ personnalisées requises!';
$_['error_custom_value']   = 'Valeurs de champ personnalisées requises!';
