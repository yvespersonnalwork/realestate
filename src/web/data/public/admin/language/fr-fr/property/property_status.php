<?php

// Heading

$_['heading_title']    = 'Statuts de propriété';

// Text

$_['text_success']     = 'Succès: vous avez modifié les statuts de propriété!';
$_['text_list']        = 'Liste d\'état de la propriété';
$_['text_add']         = 'Ajouter le statut de la propriété';
$_['text_edit']        = 'Modifier le statut de la propriété';
$_['text_success']     = 'avec succès: Ajoutez votre statut de propriété!';
$_['text_successedit'] = 'avec succès: vous avez modifié le statut de la propriété';
$_['text_successdelete']= 'avec succès: votre statut de propriété Supprimer';

// Column
$_['column_name']      = 'Nom de la propriété';
$_['column_action']    = 'action';

// Lable 
$_['lable_name']       = 'Nom de la propriété';

// Entry
$_['entry_name']       = 'Nom de la propriété';



// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier les statuts de propriété!';
$_['error_name']       = 'Propriété Le nom de l\'état doit comporter entre 3 et 32 caractères.!';
$_['error_default']    = 'Avertissement: Cet état de propriété ne peut pas être supprimé car il est actuellement affecté en tant que statut de propriété de magasin par défaut.!';
