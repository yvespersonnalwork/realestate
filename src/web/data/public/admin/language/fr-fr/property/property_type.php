<?php
// Heading 
$_['heading_title']          = 'Type de propriété ';
// Text
$_['text_success']           = 'Succès: vous avez modifié l\'information!';
$_['text_list']              = 'Liste de propriété';
$_['text_add']               = 'Ajouter une propriété';
$_['text_edit']              = 'Modifier le formulaire';
$_['text_form']              = 'Ajouter un type de propriété';
$_['text_enable']              = 'Activer';
$_['text_disable']            = 'Désactiver';
$_['text_default']           = 'Défaut';

//13/2/2018/
$_['text_success']           = 'avec succès: Ajoutez votre type de propriété ';
$_['text_successedit']       = 'avec succès: vous avez modifié le type de propriété ';
$_['text_successdelete']     = 'avec succès: vous Type de propriété Supprimer';
//13/2/2018/


// Column
$_['column_description']     = 'Nom';
$_['column_name']            = 'Nom';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_status']           = 'Statut';
$_['column_action']           = 'action';
// Entry
$_['entry_title']           = 'Titre';
$_['entry_image']           = 'Image';
$_['entry_SEO_URL']         = 'URL de référencement';
$_['entry_name']            = 'Nom';
$_['entry_description']     ='La description';
$_['entry_store']            = 'Magasins';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'URL de référencement';
$_['entry_bottom']           = 'Bas';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_layout']           = 'Remplacement de mise en page';

// Help
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que l\'URL de référencement est unique au monde..';
$_['help_bottom']            = 'Afficher en bas de page.';
// Error
$_['error_warning']          = 'Attention: S\'il vous plaît vérifier le formulaire avec soin pour les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des informations!';
$_['error_name']            = 'Le titre du formulaire doit comporter entre 3 et 64 caractères.!';
$_['error_desription']      = 'La description doit comporter plus de 3 caractères.!';

$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'URL de référencement déjà utilisée!';
$_['error_account']          = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de magasin.!';
$_['error_checkout']         = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée en tant que conditions de caisse.!';
$_['error_affiliate']        = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes affiliés du magasin.!';
$_['error_return']           = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour du magasin.!';
$_['error_store']            = 'Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';