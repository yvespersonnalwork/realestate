<?php

// Heading
$_['heading_title']          = 'Liste de catégories';

// Text

$_['text_success']           = 'Succès: vous avez modifié des catégories!';
$_['text_list']              = 'Liste des catégories de biens';
$_['text_add']               = 'ajouter une catégorie';
$_['text_edit']              = 'Modifier la catégorie';
$_['text_default']           = 'Défaut';

// Column
$_['column_name']            = 'Nom de catégorie';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_action']          = 'action';

/// Category Lable ////

$_['lable_name']             = 'Nom de catégorie';
$_['lable_description']      = 'La description';
$_['lable_meta_title'] 	     = 'Titre de la balise méta';
$_['lable_meta_description'] = 'Meta Tag Description';
$_['lable_meta_keyword']     = 'Mots-clés méta';
$_['lable_parent']           = 'Parente';
$_['lable_filter']           = 'Les filtres';
$_['lable_store']            = 'Site Internet';
$_['lable_keyword']          = 'URL de référencement';
$_['lable_image']            = 'Image';
$_['lable_top']              = 'Haut';
$_['lable_column']           = 'Colonnes';
$_['lable_sort_order']       = 'Ordre de tri';
$_['lable_status']           = 'Statut';
$_['lable_layout']           = 'Remplacement de mise en page';

// Entry

$_['entry_name']             = 'Nom de catégorie';
$_['entry_description']      = 'La description';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword']     = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'URL de référencement';
$_['entry_parent']           = 'Parente';
$_['entry_filter']           = 'Les filtres';
$_['entry_store']            = 'Site Internet';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Haut';
$_['entry_column']           = 'Colonnes';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_status']           = 'Statut';
$_['entry_layout']           = 'Remplacement de mise en page';

// Help

$_['help_filter']            = '(Saisie automatique)';
$_['help_keyword']           = "N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que l\'URL de référencement est unique au monde..";
$_['help_top']               = 'Afficher dans la barre de menu supérieure. Ne fonctionne que pour les catégories parentes les plus populaires.';
$_['help_column']            = 'Nombre de colonnes à utiliser pour les 3 dernières catégories. Ne fonctionne que pour les catégories parentes les plus populaires.';

// Error

$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des catégories.!';
$_['error_name']             = 'Le nom de la catégorie doit comporter entre 2 et 255 caractères!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'URL de référencement déjà utilisée!';
$_['error_parent']           = 'La catégorie parent que vous avez choisie est un enfant de la catégorie actuelle!';

