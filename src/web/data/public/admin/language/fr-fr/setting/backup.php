<?php
// Heading
$_['heading_title']    = 'Sauvegarde &amp; Restaurer';

// Text
$_['text_success']     = 'Succès: vous avez importé avec succès votre base de données!';

// Entry
$_['entry_import']     = 'Importation';
$_['entry_export']     = 'Exportation';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier la sauvegarde. &amp; Restaurer!';
$_['error_export']     = 'Attention: vous devez sélectionner au moins une table à exporter!';
$_['error_empty']      = 'Avertissement: le fichier que vous avez téléchargé était vide!';