<?php

// Heading

$_['heading_title']                = 'Paramètres du site';



// Text

$_['text_stores']                  = 'Site Internet';

$_['tab_store']                  = 'Site Internet';

$_['text_success']                 = 'Succès: vous avez modifié les paramètres du site Web.!';

$_['text_edit']                    = 'Modifier les paramètres du site Web';

$_['text_product']                 = 'Référencement';

$_['text_review']                  = 'Commentaires';

$_['text_voucher']                 = 'Pièces justificatives';

$_['text_tax']                     = 'Les taxes';

$_['text_account']                 = 'Compte';

$_['text_checkout']                = 'Check-out';

$_['text_stock']                   = 'Stock';

$_['text_affiliate']               = 'Les affiliés';

$_['text_captcha']                 = 'Captcha';

$_['text_register']                = 'S\'inscrire';

$_['text_guest']                   = 'Commander des invités';

$_['text_return']                  = 'Résultats';

$_['text_contact']                 = 'Contact';

$_['text_mail']                    = 'Courrier';

$_['text_smtp']                    = 'SMTP';

$_['text_mail_alert']              = 'Alertes mail';

$_['text_mail_account']            = 'S\'inscrire';

$_['text_mail_affiliate']          = 'Ajouter une propriété';

$_['text_mail_order']              = 'Ordres';

$_['text_mail_review']             = 'Commentaires';

$_['text_general']                 = 'Général';

$_['text_security']                = 'Sécurité';

$_['text_upload']                  = 'Uploads';

$_['text_error']                   = 'La gestion des erreurs';


//label name

$_['text_meta_title']             = 'Meta Title';

$_['text_meta_description']       = 'Meta Tag Description';

$_['text_meta_keyword']           = 'Mots-clés méta';

$_['text_layout']                 = 'Mise en page par défaut';

$_['text_theme']                  = 'Thème';

$_['text_name']                   = 'Nom du site';

$_['text_owner']                  = 'Propriétaire du site Web';

$_['text_address']                = 'Adresse';

$_['text_email']                   = 'Email';

$_['text_telephone']               = 'Téléphone';

$_['text_map']                     = 'Code de la carte intégrée';

$_['text_mapkey']              = 'Touche Google Map';

$_['text_image']                  = 'Image';

$_['text_location']               = 'Emplacement du site';

$_['text_country']                = 'Pays';

$_['text_zone']                   = 'Région / Etat';

$_['text_language']               = 'La langue';

$_['text_admin_language']         = 'Langue d\'administration';

$_['text_currency']               = 'Devise';

$_['text_currency_auto']          = 'Mise à jour automatique de la devise';

$_['text_length_class']           = 'Classe de longueur';

$_['text_weight_class']           = 'Catégorie de poids';

$_['text_limit_admin']            = 'Éléments par défaut par page (Admin)';

$_['text_product_count']          = 'Nombre de catégories';

$_['text_customer_online']        = 'Clients en ligne';

$_['text_customer_activity']      = 'Activité Clients';

$_['text_customer_search']        = 'Enregistrer les recherches des clients';

$_['text_customer_group']         = 'Groupe de clients';

$_['text_customer_group_display'] = 'Groupe de clients';

$_['text_customer_price']         = 'Connexion Afficher les prix';

$_['text_login_attempts']         = 'Nombre maximal de tentatives de connexion';

$_['text_account']                = 'Nombre maximal de tentatives de connexion...';

$_['text_captcha']                = 'Captcha';

$_['text_captcha_page']           = 'Page captcha';

$_['text_logo']                   = 'Logo du site';

$_['text_icon']                   = 'Icône';

$_['text_ftp_hostname']           = 'Hôte FTP';

$_['text_ftp_port']               = 'Port FTP';

$_['text_ftp_username']           = 'Nom d\'utilisateur FTP';

$_['text_ftp_password']           = 'Mot de passe FTP';

$_['text_ftp_root']               = 'Racine FTP';

$_['text_ftp_status']             = 'Activer FTP';

$_['text_mail_protocol']          = 'Protocole de messagerie';

$_['text_mail_parameter']         = 'Paramètres de messagerie';

$_['text_mail_smtp_hostname']     = 'Nom d\'hôte SMTP';

$_['text_mail_smtp_username']     = 'Nom d\'utilisateur SMTP';

$_['text_mail_smtp_password']     = 'Mot de passe SMTP';

$_['text_mail_smtp_port']         = 'Port SMTP';

$_['text_mail_smtp_timeout']      = 'Délai d\'attente SMTP';

$_['text_mail_alert']             = 'Courrier d\'alerte';

$_['text_mail_alert_email']       = 'Courrier d\'alerte supplémentaire';

$_['text_secure']                 = 'Utiliser SSL';

$_['text_shared']                 = 'Utiliser des sessions partagées';

$_['text_robots']                 = 'Des robots';

$_['text_seo_url']                = 'Utiliser des URL de référencement';

$_['text_file_max_size']	       = 'Taille maximale du fichier';

$_['text_file_ext_allowed']       = 'Extensions de fichier autorisées';

$_['text_file_mime_allowed']      = 'Types de fichiers MIME autorisés';

$_['text_maintenance']            = 'Mode de Maintenance';

$_['text_password']               = 'Autoriser le mot de passe oublié';

$_['text_encryption']             = 'Clé de cryptage';

$_['text_compression']            = 'Niveau de compression de sortie';

$_['text_error_display']          = 'Erreurs d\'affichage';

$_['text_error_log']              = 'Erreurs de journal';

$_['text_error_filename']         = 'Nom du fichier journal des erreurs';

$_['text_status']                 = 'Statut';

$_['tab_socialmedia']              = 'Footer Social Icons';

$_['text_aboutdes']         	   = 'A propos de nous Description';

$_['text_title']         	       = 'Titre:';

$_['text_phoneno']         	   = 'Pas de téléphone:';

$_['text_mobile']         	       = 'Numéro de mobile:';

$_['text_phoneno']         	   = 'Pas de téléphone:';

$_['text_email']         	       = 'Email:';

$_['text_address2']         	   = 'Adresse:';

$_['text_fburl']         	       = 'L\'adresse URL de Facebook:';

$_['text_google']         	       = 'URL Google:';

$_['text_twet']         	       = 'URL de Twitter:';

$_['text_in']         	           = 'LinkedIn:';

$_['text_instagram']         	   = 'Instagram:';

$_['text_pinterest']         	   = 'Pinterest';

$_['text_youtube']         	   = 'Youtube';

$_['text_blogger']         	   = 'Blogger';

$_['text_footericon']             = 'Logo de pied de page';

// Entry  input  name 

$_['entry_meta_title']             = 'Meta Title';

$_['entry_meta_description']       = 'Meta Tag Description';

$_['entry_meta_keyword']           = 'Mots-clés méta';

$_['entry_layout']                 = 'Mise en page par défaut';

$_['entry_theme']                  = 'Thème';

$_['entry_name']                   = 'Nom du site';

$_['entry_owner']                  = 'Propriétaire du site Web';

$_['entry_address']                = 'Adresse';

$_['entry_email']                   = 'Email';

$_['entry_telephone']               = 'Téléphone';

$_['entry_map']                     = 'Code de la carte intégrée';

$_['entry_mapkey']              = 'Touche Google Map';

$_['entry_image']                  = 'Image';

$_['entry_location']               = 'Emplacement du site';

$_['entry_country']                = 'Pays';

$_['entry_zone']                   = 'Région / Etat';

$_['entry_language']               = 'La langue';

$_['entry_admin_language']         = 'Langue d\'administration';

$_['entry_currency']               = 'Devise';

$_['entry_currency_auto']          = 'Mise à jour automatique de la devise';

$_['entry_length_class']           = 'Classe de longueur';

$_['entry_weight_class']           = 'Catégorie de poids';

$_['entry_limit_admin']            = 'Éléments par défaut par page (Admin)';

$_['entry_product_count']          = 'Nombre de catégories';

$_['entry_customer_online']        = 'Clients en ligne';

$_['entry_customer_activity']      = 'Activité Clients';

$_['entry_customer_search']        = 'Enregistrer les recherches des clients';

$_['entry_customer_group']         = 'Groupe de clients';

$_['entry_customer_group_display'] = 'Groupes de clients';

$_['entry_customer_price']         = 'Connexion Afficher les prix';

$_['entry_login_attempts']         = 'Nombre maximal de tentatives de connexion';

$_['entry_account']                = 'Conditions du compte';

$_['entry_captcha']                = 'Captcha';

$_['entry_captcha_page']           = 'Page captcha';

$_['entry_logo']                   = 'Logo du site';

$_['entry_icon']                   = 'Icône';

$_['entry_ftp_hostname']           = 'Hôte FTP';

$_['entry_ftp_port']               = 'Port FTP';

$_['entry_ftp_username']           = 'Nom d\'utilisateur FTP';

$_['entry_ftp_password']           = 'Mot de passe FTP';

$_['entry_ftp_root']               = 'Racine FTP';

$_['entry_ftp_status']             = 'Activer FTP';

$_['entry_mail_protocol']          = 'Protocole de messagerie';

$_['entry_mail_parameter']         = 'Paramètres de messagerie';

$_['entry_mail_smtp_hostname']     = 'Nom d\'hôte SMTP';

$_['entry_mail_smtp_username']     = 'Nom d\'utilisateur SMTP';

$_['entry_mail_smtp_password']     = 'Mot de passe SMTP';

$_['entry_mail_smtp_port']         = 'Port SMTP';

$_['entry_mail_smtp_timeout']      = 'Délai d\'attente SMTP';

$_['entry_mail_alert']             = 'Courrier d\'alerte';

$_['entry_mail_alert_email']       = 'Courrier d\'alerte supplémentaire';

$_['entry_secure']                 = 'Utiliser SSL';

$_['entry_shared']                 = 'Utiliser des sessions partagées';

$_['entry_robots']                 = 'Des robots';

$_['entry_seo_url']                = 'Utiliser des URL de référencement';

$_['entry_file_max_size']	       = 'Taille maximale du fichier';

$_['entry_file_ext_allowed']       = 'Extensions de fichier autorisées';

$_['entry_file_mime_allowed']      = 'Types de fichiers MIME autorisés';

$_['entry_maintenance']            = 'Mode de Maintenance';

$_['entry_password']               = 'Autoriser le mot de passe oublié';

$_['entry_encryption']             = 'Clé de cryptage';

$_['entry_compression']            = 'Niveau de compression de sortie';

$_['entry_error_display']          = 'Erreurs d\'affichage';

$_['entry_error_log']              = 'Erreurs de journal';

$_['entry_error_filename']         = 'Nom du fichier journal des erreurs';

$_['entry_status']                 = 'Statut';

$_['tab_socialmedia']              = 'Footer Social Icons';

$_['entry_aboutdes']         	   = 'A propos de nous Description';

$_['entry_title']         	       = 'Titre:';

$_['entry_phoneno']         	   = 'Pas de téléphone:';

$_['entry_mobile']         	       = 'Numéro de mobile:';

$_['entry_phoneno']         	   = 'Pas de téléphone:';

$_['entry_email']         	       = 'Email:';

$_['entry_address2']         	   = 'Adresse:';

$_['entry_fburl']         	       = 'L\'adresse URL de Facebook:';

$_['entry_google']         	       = 'URL Google:';

$_['entry_twet']         	       = 'URL de Twitter:';

$_['entry_in']         	           = 'LinkedIn:';

$_['entry_instagram']         	   = 'Instagram:';

$_['entry_pinterest']         	   = 'Pinterest';

$_['entry_youtube']         	   = 'Youtube';

$_['entry_blogger']         	   = 'Blogger';

$_['entry_footericon']             = 'Logo de pied de page';
$_['entry_order_status']                   = 'Statut de la commande';



// Help

$_['help_location']                = 'Les différents emplacements de site Web que vous souhaitez afficher sur le formulaire de contact.';

$_['help_currency']                = 'Changer la devise par défaut. Effacez le cache de votre navigateur pour voir la modification et réinitialiser votre cookie existant..';

$_['help_currency_auto']           = 'Configurez votre site Web pour mettre à jour automatiquement les devises quotidiennement.';

$_['help_limit_admin']   	       = 'Détermine le nombre d\'éléments d\'administration affichés par page (commandes, clients, etc.).';

$_['help_product_count']           = 'Affichez le nombre de produits dans les sous-catégories dans le menu de catégorie d\'en-tête de site Web. Soyez averti, cela entraînera des pertes de performances extrêmes pour les magasins comportant de nombreuses sous-catégories.!';

$_['help_customer_online']         = 'Suivre les clients en ligne via la section des rapports clients.';

$_['help_customer_activity']       = 'Suivre l\'activité des clients via la section des rapports clients.';

$_['help_customer_group']          = 'Groupe de clients par défaut.';

$_['help_customer_group_display']  = 'Afficher les groupes de clients que les nouveaux clients peuvent choisir d\'utiliser, tels que le commerce de gros et le commerce, lors de l\'inscription.';

$_['help_customer_price']          = 'Afficher les prix uniquement lorsqu\'un client est connecté.';

$_['help_login_attempts']          = 'Nombre maximal de tentatives de connexion autorisées avant que le compte ne soit verrouillé pendant 1 heure. Le compte client et affilié peut être déverrouillé sur les pages d\'administrateur du client ou de l\'affilié.';

$_['help_account']                 = 'Force les gens à accepter les conditions avant qu\'un compte puisse être créé.';









$_['help_captcha']                 = 'Captcha à utiliser pour l'enregistrement, la connexion, les contacts et les avis.';

$_['help_icon']                    = 'L'icône doit être un fichier PNG de 16 pixels sur 16 pixels..';

$_['help_ftp_root']                = 'Le répertoire dans lequel votre installation RealEstate est stockée. Normalement \'public_html/\'.';

$_['help_mail_protocol']           = 'Choisissez seulement \ 'Mail \' sauf si votre hôte a désactivé la fonction de messagerie php.';

$_['help_mail_parameter']          = 'Lorsque vous utilisez \ 'Mail \', des paramètres de messagerie supplémentaires peuvent être ajoutés ici (par exemple -f email@storeaddress.com).';

$_['help_mail_smtp_hostname']      = 'Add \'tls://\' or \'ssl://\' prefix if security connection is required. (e.g. tls://smtp.gmail.com, ssl://smtp.gmail.com).';

$_['help_mail_smtp_password']      = 'Pour gmail, vous devrez peut-être configurer un mot de passe spécifique à l'application ici: https://security.google.com/settings/security/apppasswords.';

$_['help_mail_alert']              = 'Sélectionnez les fonctionnalités pour lesquelles vous souhaitez recevoir un e-mail d'alerte lorsqu'un client les utilise..';

$_['help_mail_alert_email']        = 'Tous les courriers électroniques supplémentaires que vous souhaitez recevoir, ainsi que le courrier électronique du magasin principal. (séparées par des virgules).';

$_['help_secure']                  = 'Pour utiliser SSL, vérifiez auprès de votre hôte si un certificat SSL est installé et ajoutez l'URL SSL aux fichiers de configuration catalog et admin..';

$_['help_shared']                  = 'Essayez de partager le cookie de session entre magasins afin que le panier puisse être transmis entre différents domaines..';

$_['help_robots']                  = 'Une liste des agents utilisateurs du robot Web avec lesquels les sessions partagées ne seront pas utilisées. Utilisez des lignes distinctes pour chaque agent d'utilisateur.';

$_['help_seo_url']                 = 'Pour utiliser les URL SEO, le module apache mod-rewrite doit être installé et vous devez renommer le fichier htaccess.txt en .htaccess...';

$_['help_file_max_size']		   = 'La taille maximale du fichier image que vous pouvez télécharger dans le Gestionnaire d’images. Entrez comme octet.';

$_['help_file_ext_allowed']        = 'Ajoutez les extensions de fichier autorisées à être téléchargées. Utilisez une nouvelle ligne pour chaque valeur.';

$_['help_file_mime_allowed']       = 'Ajouter quels types de fichiers MIME sont autorisés à télécharger. Utilisez une nouvelle ligne pour chaque valeur.';

$_['help_maintenance']             = 'Empêche les clients de naviguer dans votre magasin. Au lieu de cela, ils verront un message de maintenance. Si vous êtes connecté en tant qu'administrateur, vous verrez le magasin comme d'habitude.';

$_['help_password']                = 'Autoriser l’utilisation d’un mot de passe oublié pour l’administrateur. Ceci sera automatiquement désactivé si le système détecte une tentative de piratage.';

$_['help_encryption']              = 'Veuillez fournir une clé secrète qui sera utilisée pour chiffrer des informations confidentielles lors du traitement des commandes..';

$_['help_compression']             = 'GZIP pour un transfert plus efficace vers les clients demandeurs. Le niveau de compression doit être compris entre 0 et 9.';



// Error

$_['error_warning']                = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';

$_['error_permission']             = 'Avertissement: vous n'êtes pas autorisé à modifier les paramètres!';

$_['error_meta_title']             = 'Le titre doit comporter entre 3 et 32 caractères.!';

$_['error_name']                   = 'Le nom du site Web doit comporter entre 3 et 32 caractères.!';

$_['error_owner']                  = 'Le propriétaire du site doit comporter entre 3 et 64 caractères.!';

$_['error_address']                = 'L'adresse du site Web doit comporter entre 10 et 256 caractères.!';

$_['error_mapkey']                 = 'S'il vous plaît entrer la clé de la carte';

$_['error_map']                    = 'S'il vous plaît entrer la carte google map code d'intégration';

$_['error_email']                  = 'Adresse électronique ne semble pas être valide!';

$_['error_telephone']              = 'Le téléphone doit comporter entre 3 et 32 caractères.!';

$_['error_limit']       	       = 'Limite requise!';

$_['error_login_attempts']         = 'Les tentatives de connexion doivent être supérieures à 0!';

$_['error_customer_group_display'] = 'Vous devez inclure le groupe de clients par défaut si vous souhaitez utiliser cette fonctionnalité.!';



$_['error_processing_status']      = 'Vous devez choisir au moins 1 statut de processus de commande';



$_['error_ftp_hostname']           = 'Hôte FTP requis!';

$_['error_ftp_port']               = 'Port FTP requis!';

$_['error_ftp_username']           = 'Nom d'utilisateur FTP requis!';

$_['error_ftp_password']           = 'Mot de passe FTP requis!';

$_['error_error_filename']         = 'Nom du fichier journal des erreurs requis!';

$_['error_malformed_filename']	   = 'Erreur Nom du fichier journal mal formé!';

$_['error_encryption']             = 'La clé de cryptage doit comporter entre 32 et 1024 caractères.!';

