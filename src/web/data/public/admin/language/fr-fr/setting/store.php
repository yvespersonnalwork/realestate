<?php
// Heading
$_['heading_title']                = 'Magasins';

// Text
$_['text_settings']                = 'Paramètres';
$_['text_success']                 = 'Succès: vous avez modifié les magasins!';
$_['text_list']                    = 'Liste de magasin';
$_['text_add']                     = 'Ajouter un magasin';
$_['text_edit']                    = 'Modifier le magasin';
$_['text_items']                   = 'Articles';
$_['text_tax']                     = 'Les taxes';
$_['text_account']                 = 'Compte';
$_['text_checkout']                = 'Check-out';
$_['text_stock']                   = 'Stock';
$_['text_shipping']                = 'Adresse de livraison';
$_['text_payment']                 = 'adresse de facturation';

// Column
$_['column_name']                  = 'Nom du magasin';
$_['column_url']	               = 'URL du magasin';
$_['column_action']                = 'action';

// Entry
$_['entry_url']                    = 'URL du magasin';
$_['entry_ssl']                    = 'URL SSL';
$_['entry_meta_title']             = 'Meta Title';
$_['entry_meta_description']       = 'Meta Tag Description';
$_['entry_meta_keyword']           = 'Mots-clés méta';
$_['entry_layout']                 = 'Mise en page par défaut';
$_['entry_theme']                  = 'Thème';
$_['entry_name']                   = 'Nom du magasin';
$_['entry_owner']                  = 'Propriétaire du magasin';
$_['entry_address']                = 'Adresse';
$_['entry_email']                  = 'Email';
$_['entry_telephone']              = 'Téléphone';
$_['entry_image']                  = 'Image';
$_['entry_location']               = 'Emplacement du magasin';
$_['entry_country']                = 'Pays';
$_['entry_zone']                   = 'Région / Etat';
$_['entry_language']               = 'La langue';
$_['entry_currency']               = 'Devise';
$_['entry_tax']                    = 'Afficher les prix avec taxe';
$_['entry_tax_default']            = 'Utiliser l\'adresse fiscale du magasin';
$_['entry_tax_customer']           = 'Utiliser l\'adresse fiscale du client';
$_['entry_customer_group']         = 'Groupe de clients';
$_['entry_customer_group_display'] = 'Groupes de clients';
$_['entry_customer_price']         = 'Connexion Afficher les prix';
$_['entry_account']                = 'Conditions du compte';
$_['entry_cart_weight']            = 'Afficher le poids sur la page du panier';
$_['entry_checkout_guest']         = 'Commander des invités';
$_['entry_checkout']               = 'Conditions de caisse';
$_['entry_order_status']           = 'Statut de la commande';
$_['entry_stock_display']          = 'Afficher le stock';
$_['entry_stock_checkout']         = 'Commande de stock';
$_['entry_logo']                   = 'Logo du magasin';
$_['entry_icon']                   = 'Icône';
$_['entry_secure']                 = 'Utiliser SSL';

// Help
$_['help_url']                     = 'Incluez l\'URL complète de votre magasin. Assurez-vous d’ajouter \ '/ \' à la fin. Exemple: http://www.votredomaine.com/path/ <br /> <br /> N’utilisez pas de répertoires pour créer un nouveau magasin. Vous devez toujours indiquer un autre domaine ou sous-domaine à votre hébergement..';
$_['help_ssl']                     = 'URL SSL vers votre magasin. Assurez-vous d’ajouter \ '/ \' à la fin. Exemple: http://www.votredomaine.com/path/ <br /> <br /> N’utilisez pas de répertoires pour créer un nouveau magasin. Vous devez toujours indiquer un autre domaine ou sous-domaine à votre hébergement.';
$_['help_location']                = 'Les différents magasins que vous souhaitez afficher sur le formulaire de contact.';
$_['help_currency']                = 'Changer la devise par défaut. Effacez le cache de votre navigateur pour voir la modification et réinitialiser votre cookie existant..';
$_['help_tax_default']             = 'Utilisez l\'adresse du magasin pour calculer les taxes si le client n\'est pas connecté. Vous pouvez choisir d\'utiliser l\'adresse du magasin pour l\'adresse de livraison ou de paiement du client..';
$_['help_tax_customer']            = 'Utilisez l\'adresse par défaut du client lorsqu\'il se connecte pour calculer les taxes. Vous pouvez choisir d’utiliser l’adresse par défaut pour l’adresse de livraison ou de paiement du client..';
$_['help_customer_group']          = 'Groupe de clients par défaut.';
$_['help_customer_group_display']  = 'Afficher les groupes de clients que les nouveaux clients peuvent choisir d\'utiliser, tels que le commerce de gros et le commerce, lors de l\'inscription.';
$_['help_customer_price']          = 'Afficher les prix uniquement lorsqu\'un client est connecté.';
$_['help_account']                 = 'Force les gens à accepter les conditions avant qu\'un compte puisse être créé.';
$_['help_checkout_guest']          = 'Autoriser les clients à passer à la caisse sans créer de compte. Ce ne sera pas disponible lorsqu\'un produit téléchargeable est dans le panier.';
$_['help_checkout']                = 'Force les gens à accepter les conditions avant qu\'un client puisse passer à la caisse.';
$_['help_order_status']            = 'Définir le statut de commande par défaut lors du traitement d\'une commande.';
$_['help_stock_display']           = 'Afficher la quantité en stock sur la page du produit.';
$_['help_stock_checkout']          = 'Autoriser les clients à passer à la caisse si les produits qu\'ils commandent ne sont pas en stock.';
$_['help_icon']                    = 'L\'icône doit être un fichier PNG de 16 pixels sur 16 pixels..';
$_['help_secure']                  = 'Pour utiliser SSL, vérifiez auprès de votre hôte si un certificat SSL est installé.';

// Error
$_['error_warning']                = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']             = 'Avertissement: vous n\'êtes pas autorisé à modifier les magasins.!';
$_['error_url']                    = 'URL de magasin requise!';
$_['error_meta_title']             = 'Le titre doit comporter entre 3 et 32 caractères.!';
$_['error_name']                   = 'Le nom du magasin doit comporter entre 3 et 32 caractères.!';
$_['error_owner']                  = 'Le propriétaire du magasin doit comporter entre 3 et 64 caractères.!';
$_['error_address']                = 'L\'adresse du magasin doit comporter entre 10 et 256 caractères.!';
$_['error_email']                  = 'Adresse électronique ne semble pas être valide!';
$_['error_telephone']              = 'Le téléphone doit comporter entre 3 et 32 caractères.!';
$_['error_customer_group_display'] = 'Vous devez inclure le groupe de clients par défaut si vous souhaitez utiliser cette fonctionnalité.!';
$_['error_default']                = 'Attention: vous ne pouvez pas supprimer votre magasin par défaut!';
$_['error_store']                  = 'Avertissement: Ce magasin ne peut pas être supprimé car il est actuellement affecté aux commandes% s.!';
