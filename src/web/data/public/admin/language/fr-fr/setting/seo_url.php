<?php
// Heading
$_['heading_title']    = 'URL de référencement';

// Text
$_['text_success']     = 'Succès: vous avez modifié l\'URL du référencement!';
$_['text_list']        = 'Liste d\'URL de référencement';
$_['text_add']         = 'Ajouter une URL de référencement';
$_['text_edit']        = 'Modifier l\'URL du référencement';
$_['text_filter']      = 'Filtre';
$_['text_default']     = 'Défaut';

// Column
$_['column_query']     = 'Requete';
$_['column_keyword']   = 'Mot-clé';
$_['column_store']     = 'le magasin';
$_['column_language']  = 'La langue';
$_['column_action']    = 'Action';

// Entry
$_['entry_query']      = 'Requete';
$_['entry_keyword']    = 'Mot-clé';
$_['entry_store']      = 'le magasin';
$_['entry_language']   = 'La langue';

// Error
$_['error_permission'] = 'Attention: vous n\'êtes pas autorisé à modifier des bannières!';
$_['error_query']      = 'La requête doit comporter entre 3 et 64 caractères.!';
$_['error_keyword']    = 'Le mot clé doit comporter entre 3 et 64 caractères.!';
$_['error_exists']     = 'Mot clé déjà utilisé!';
