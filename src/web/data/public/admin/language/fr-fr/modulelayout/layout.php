<?php

// Heading

$_['heading_title']       		 = 'Layouts';



// Text

$_['text_success']        		 = 'Succès: vous avez modifié les mises en page.!';

$_['text_list']           		 = 'Réglage de la mise en page';

$_['text_add']            		 = 'Ajouter une mise en page';

$_['text_edit']           		 = 'Modifier la mise en page';

$_['text_remove']         		 = 'Retirer';

$_['text_route']          		 = 'Choisissez le magasin et les itinéraires à utiliser avec cette présentation';

$_['text_module']         		 = 'Choisissez la position des modules';

$_['text_default']        		 = 'Défaut';

//xml

$_['text_column_slider']         = 'Curseur de colonne';

$_['text_column_testimonial']    = 'Témoignage de colonne';

$_['text_content_propty']        = 'Propriété de contenu';

$_['text_column_footer']         = 'Pied de colonne';

$_['text_column_footer4']         = 'Galerie de pied de colonne';

$_['text_column_footer3']         = 'Bulletin de pied de colonne';

$_['text_column_footer5']         = 'Pied de colonne5';

$_['text_content_top']    		 = 'Contenu haut';

$_['text_content_bottom'] 		 = 'Contenu bas';

$_['text_column_left']    		 = 'Colonne de gauche';

$_['text_column_right']   		 = 'Colonne de droite';



// Column

$_['column_name']         		 = 'Nom de la mise en page';

$_['column_action']       		 = 'action';


// label 

$_['text_name']         		 = 'Nom de la mise en page';
$_['text_store']         		 = 'le magasin';
$_['text_route']        		 = 'Route';
$_['text_module']       		 = 'Module';


// Entry

$_['entry_name']         		 = 'Nom de la mise en page';

$_['entry_store']         		 = 'le magasin';

$_['entry_route']        		 = 'Route';

$_['entry_module']       		 = 'Module';



// Error

$_['error_permission']   		 = "Avertissement: vous n'êtes pas autorisé à modifier les mises en page.!";

$_['error_name']         		 = 'Le nom de la mise en page doit comporter entre 3 et 64 caractères.!';

$_['error_default']     		 = 'Avertissement: cette présentation ne peut pas être supprimée car elle est actuellement affectée à la présentation par défaut du magasin.!';

$_['error_store']        		 = 'Attention: cette mise en page ne peut pas être supprimée car elle est actuellement attribuée aux magasins% s!';

$_['error_product']      		 = 'Attention: cette mise en page ne peut pas être supprimée car elle est actuellement affectée aux produits% s!';

$_['error_category']     		 = 'Attention: cette mise en page ne peut pas être supprimée car elle est actuellement affectée aux catégories% s!';

$_['error_information']  		 = 'Attention: cette mise en page ne peut pas être supprimée car elle est actuellement affectée aux pages d’informations% s.!';

