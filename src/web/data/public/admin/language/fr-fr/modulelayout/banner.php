<?php

// Heading

$_['heading_title']     = 'Bannières';



// Text

$_['text_success']      = 'Succès: vous avez modifié des bannières!';

$_['text_list']         = 'Liste de bannières';

$_['text_add']          = 'Ajouter une bannière';

$_['text_edit']         = 'Modifier la bannière';

$_['text_default']      = 'Défaut';




// Column

$_['column_name']       = 'Nom de la bannière';
$_['column_status']     = 'Statut';
$_['column_action']     = 'action';
//label

$_['text_name']        = 'Nom de la bannière';
$_['text_title']       = 'Titre';
$_['text_link']        = 'Lien';
$_['text_image']       = 'Image';
$_['text_status']      = 'Statut';
$_['text_sort_order']  = 'Ordre de tri';
// Entry
$_['entry_name']        = 'Nom de la bannière';
$_['entry_title']       = 'Titre';
$_['entry_link']        = 'Lien';
$_['entry_image']       = 'Image';
$_['entry_status']      = 'Statut';
$_['entry_sort_order']  = 'Ordre de tri';

// Error

$_['error_permission'] = "Attention: vous n'êtes pas autorisé à modifier des bannières!";

$_['error_name']       = 'Le nom de la bannière doit comporter entre 3 et 64 caractères.!';

$_['error_title']      = 'Le titre de la bannière doit comporter entre 2 et 64 caractères.!';