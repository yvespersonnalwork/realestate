<?php
// header
$_['heading_title']  = 'réinitialisez votre mot de passe';

// Text
$_['text_password']  = 'Entrez le nouveau mot de passe que vous souhaitez utiliser.';
$_['text_success']   = 'Succès: votre mot de passe a été mis à jour avec succès.';

// Entry
$_['entry_password'] = 'Mot de passe';
$_['entry_confirm']  = 'Confirmer';

// Error
$_['error_password'] = 'Le mot de passe doit comporter entre 4 et 20 caractères.!';
$_['error_confirm']  = 'Mot de passe et confirmation du mot de passe ne correspondent pas!';