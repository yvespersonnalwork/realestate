<?php

// Text

$_['text_blogdashboard']               = 'Blog Dashboard';
///left side menu
$_['text_setting']                   = 'Paramètres';
$_['text_themecontrol'] 		 	 = 'Thème de contrôle';
$_['text_backup']                    = 'Restauration de sauvegarde';
$_['text_seo_url']                     = 'URL de référencement';
$_['text_property']                  = 'Propriété'; 
$_['text_category']                  = "Type d'annonce / Catégorie";
$_['text_order_status']              = 'Statuts de propriété';
$_['text_feature'] 		 			 = 'Caractéristiques';
$_['text_nearest'] 		 			 = 'Lieu le plus proche';
$_['text_units'] 		 			 = 'Unités';
$_['text_enquiry'] 		 			 = 'Demande de propriété';
$_['text_agent']                     = 'Agente';
$_['text_custom_field']              = 'Champ personnalisé';
$_['text_contact']                   = 'Courrier';
$_['text_catalog']                   = 'Catalogue';
$_['text_country']                   = 'Des pays';
$_['text_coupon']                    = 'Coupons';
$_['text_currency']                  = 'Monnaies';
$_['text_customer_group']            = 'Groupes de clients';
$_['text_dashboard']                 = 'Tableau de bord';
$_['text_design']                    = 'Conception';
$_['text_download']                  = 'Téléchargements';
$_['text_log']                       = 'Journaux d'erreur';
$_['text_event']                     = 'Événements';
$_['text_extension']                 = 'Les extensions';
$_['text_filter']                    = 'Les filtres';
$_['text_geo_zone']                  = 'Zones géographiques';
$_['text_information']               = "Pages d'information";
$_['text_installer']                 = 'Installateur d'extension";
$_['text_language']                  = 'Langues';
$_['text_localisation']              = 'Localisation';
$_['text_location']                  = 'Emplacement du site';
$_['text_contact']                   = 'Courrier';
$_['text_marketing']                 = 'Commercialisation';
$_['text_menu']                      = 'Gestionnaire de menu';
$_['text_modification']              = 'Modifications';
$_['text_manufacturer']              = 'Les fabricants';
$_['text_option']                    = 'Les options';
$_['text_order']                     = 'Ordres';
$_['text_seo_url']                     = 'URL de référencement';

$_['text_product']                   = 'Des produits';

$_['text_reports']                   = 'Rapports';

$_['text_report_sale']               = 'Ventes';

$_['text_report_sale_order']         = 'Ordres';

$_['text_report_sale_tax']           = 'Impôt';

$_['text_report_sale_shipping']      = 'livraison';

$_['text_report_sale_return']        = 'Résultats';

$_['text_report_sale_coupon']        = 'Coupons';

$_['text_report_product']            = 'Des produits';

$_['text_report_product_viewed']     = 'Vue';

$_['text_report_product_purchased']  = 'Achetée';

$_['text_report_customer']           = 'Les clients';

$_['text_report_customer_activity']  = 'Activité du client';

$_['text_report_customer_search']    = 'Recherches de clients';

$_['text_report_customer_online']    = 'Clients en ligne';

$_['text_report_customer_order']     = 'Ordres';

$_['text_report_customer_reward']    = 'Points de récompense';

$_['text_report_customer_credit']    = 'Crédit';

$_['text_report_marketing']          = 'Commercialisation';

$_['text_report_affiliate']          = 'Les affiliés';

$_['text_report_affiliate_activity'] = "Activité d'affiliation";

$_['text_review']                    = 'Commentaires';

$_['text_return']                    = 'Résultats';

$_['text_return_action']             = 'Actions de retour';

$_['text_return_reason']             = 'Raisons de retour';

$_['text_return_status']             = 'États de retour';

$_['text_sale']                      = 'Ventes';

$_['text_setting']                   = 'Paramètres';

$_['text_store']                     = "Site Web d'extension";

$_['text_stock_status']              = 'Stock Statuses';



$_['text_tax']                       = 'Les taxes';

$_['text_tax_class']                 = "Classes d'impôt";

$_['text_tax_rate']                  = "Les taux d'imposition";

$_['text_translation']               = 'Éditeur de langue';

$_['text_theme']                     = 'Éditeur de thème';

$_['text_tools']                     = 'Outils';

$_['text_upload']                    = 'Uploads';

$_['text_user']                      = 'Utilisatrices';

$_['text_voucher']                   = 'Chèques cadeaux';

$_['text_voucher_theme']             = 'Thèmes des bons';

$_['text_weight_class']              = 'Catégories de poids';

$_['text_length_class']              = 'Classes de longueur';

$_['text_zone']                      = 'Zones';

$_['text_recurring']                 = 'Profils récurrents';

$_['text_order_recurring']           = 'Commandes récurrentes';

$_['text_openbay_extension']         = 'OpenBay Pro';

$_['text_openbay_dashboard']         = 'Tableau de bord';

$_['text_openbay_orders']            = 'Mise à jour de la commande en gros';

$_['text_openbay_items']             = 'Gérer les objets';

$_['text_openbay_ebay']              = 'eBay';

$_['text_openbay_amazon']            = 'Amazone (EU)';

$_['text_openbay_amazonus']          = 'Amazone (US)';

$_['text_openbay_etsy']            	 = 'Etsy';

$_['text_openbay_settings']          = 'Paramètres';

$_['text_openbay_links']             = "Liens d'articles";

$_['text_openbay_report_price']      = 'Rapport de prix';

$_['text_openbay_order_import']      = 'Importation de commande';

$_['text_paypal']                    = 'Pay Pal';

$_['text_paypal_search']             = 'Chercher';

$_['text_complete_status']           = 'Commandes complétées'; 

$_['text_processing_status']         = 'Traitement des commandes'; 

$_['text_other_status']              = 'Autres statuts'; 





$_['text_property']                  = 'Propriété'; 

$_['text_order_status']              = 'Statuts de propriété';

$_['text_category']                  = 'Catégorie Liste / Catégorie';

$_['text_customer']                  = 'Liste des propriétés du client';

$_['text_custom_field']              = 'Champ personnalisé';

$_['text_agent']                     = 'Agente';
$_['text_mail']                      = 'Modèle de courrier d'agent';

$_['text_users']                     = 'Utilisatrice';

$_['text_user_group']                = "Groupes d'utilisateurs";

$_['text_pagebuilder']              = 'Page Builder';

$_['text_layout']                    = 'Réglage de la mise en page';

$_['text_banner']                    = 'Bannière';

$_['text_module']                    = 'Paramétrage du module';

$_['text_system']                    = 'Réglage';

$_['text_backup']                    = 'Restauration de sauvegarde';

$_['text_pages']                     = 'Des pages';

$_['text_membership']                = 'Adhésion';

$_['text_plans']                     = 'Des plans';

$_['text_variation']                 = 'Paquet';

$_['text_megaheader1']               = 'Mega Header';

$_['text_megaheader']                = 'Mega Header Menu';

$_['text_newssubscribe']             = 'Abonnés à la newsletter';

$_['text_faqcat']   				 = 'Catégorie de FAQ';		

$_['text_faq']     					 = 'Questions - réponses';

$_['text_testimonial']               = 'Témoignage';

$_['text_photo']          			 = 'Photos';

$_['text_gallerydashboard'] 		 = 'Tableau de bord de la galerie';

$_['text_feature'] 		 			 = ' Caractéristiques';

$_['text_nearest'] 		 			 = 'Lieu le plus proche';

$_['text_enquiry'] 		 			 = 'Demande de propriété';

$_['text_agents'] 		 			 = 'Agents immobiliers';



$_['members_plan'] 		 			  = 'Plan de membre';
$_['setting_text'] 		 			  = 'Réglage';
$_['text_members'] 		 			  = 'Membre';


/* lanuge add */
$_['text_analytics'] 		 			 = 'google & fb analytics';



