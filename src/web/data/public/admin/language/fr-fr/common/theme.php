<?php

// Heading 
$_['heading_title']          = 'Thème';

// Text
$_['text_success']           = "Succès: vous avez modifié l'information!";
$_['text_list']              = 'Liste thématique';
$_['text_form']              = 'Contrôleur de thème';
$_['text_edit']              = 'Modifier le thème';
$_['text_default']           = 'Défaut';
$_['text_header1']           = 'Entête1';
$_['text_header2']           = 'Entête2';
$_['text_header3']           = 'Entête3';
$_['text_header4']           = 'Entête4';
$_['text_header5']           = 'Entête5';
$_['text_footer1']           = 'Bas de page1';
$_['text_footer2']           = 'Bas de page2';
$_['text_footer3']           = 'Bas de page3';
$_['text_footer4']           = 'Bas de page4';
$_['text_agent1']            = 'Agente1';
$_['text_agent2']            = 'Agente2';
$_['text_agent3']            = 'Agente3';
$_['text_proprties1']        = 'Propriétés1';
$_['text_proprties2']        = 'Propriétés2';
$_['text_proprties3']        = 'Propriétés3';
$_['text_search1']        	 = 'Chercher1';
$_['text_search2']        	 = 'Chercher2';
$_['text_search3']        	 = 'Chercher3';

$_['tab_setting']            = 'Réglage';
$_['tab_header']             = 'Entête';
$_['tab_footer']             = 'Bas de page';
$_['tab_prolayout']          = 'Page d'accueil';
$_['tab_agent']              = "Notre agent page d'accueil";
$_['tab_properites']         = 'Mise en page des propriétés';
$_['tab_search']             = 'Mise en page de recherche';
$_['tab_socialmedia']        = 'Footer Social Icons';

// Lable
$_['lable_twittercode']            = 'Code Twitter';
$_['lable_fburl']         	       = 'L'adresse URL de Facebook:';
$_['lable_google']         	       = 'URL Google:';
$_['lable_twet']         	       = 'URL de Twitter:';
$_['lable_in']         	           = 'LinkedIn:';
$_['lable_instagram']         	   = 'Instagram:';
$_['lable_pinterest']         	   = 'Pinterest';
$_['lable_youtube']         	   = 'Youtube';
$_['lable_blogger']         	   = 'Blogger';
$_['lable_footericon']             = 'Logo de pied de page';
$_['lable_aboutdes']         	   = 'A propos de nous Description';
$_['lable_status']                 = 'Statut';
$_['lable_aboutdes']         	   = 'A propos de nous Description';
$_['lable_title']         	       = 'Titre:';
$_['lable_phoneno']         	   = 'Pas de téléphone:';
$_['lable_mobile']         	       = 'Numéro de mobile:';
$_['lable_phoneno']         	   = 'Pas de téléphone:';
$_['lable_email']         	       = 'Email:';
$_['lable_address2']         	   = 'Adresse:';

// Entry
$_['entry_twittercode']            = 'Code Twitter';
$_['entry_fburl']         	       = 'L'adresse URL de Facebook:';
$_['entry_google']         	       = 'URL Google:';
$_['entry_twet']         	       = 'URL de Twitter:';
$_['entry_in']         	           = 'LinkedIn:';
$_['entry_instagram']         	   = 'Instagram:';
$_['entry_pinterest']         	   = 'Pinterest';
$_['entry_youtube']         	   = 'Youtube';
$_['entry_blogger']         	   = 'Blogger';
$_['entry_footericon']             = 'Logo de pied de page';
$_['entry_aboutdes']         	   = 'A propos de nous Description';
$_['entry_title']         	       = 'Titre:';
$_['entry_mobile']         	       = 'Numéro de mobile:';
$_['entry_phoneno']         	   = 'Pas de téléphone:';
$_['entry_email']         	       = 'Email:';
$_['entry_address2']         	   = 'Adresse:';

// Error
$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = 'Avertissement: vous n'êtes pas autorisé à modifier des informations!';
$_['error_title']            = 'le titre du formulaire doit comporter entre 3 et 64 caractères!';
$_['error_description']      = 'La description doit comporter plus de 400 caractères.!';
$_['error_name']             = 'Le nom doit comporter plus de 3 caractères et moins de 64 caractères.!';											   

