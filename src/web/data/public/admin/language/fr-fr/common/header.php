<?php
// Heading
$_['heading_title']          = 'Immobilier';

// Text
$_['text_order']             = 'Ordres';
$_['text_processing_status'] = 'En traitement';
$_['text_complete_status']   = 'Terminé';
$_['text_customer']          = 'Terminé';
$_['text_online']            = 'Clients en ligne';
$_['text_approval']          = 'Validation en attente';
$_['text_product']           = 'Des produits';
$_['text_stock']             = 'En rupture de stock';
$_['text_review']            = 'Commentaires';
$_['text_return']            = 'Résultats';
$_['text_affiliate']         = 'Les affiliés';
$_['text_store']             = 'Magasins';
$_['text_front']             = 'Site web';
$_['text_help']              = 'Aidez-moi';
$_['text_homepage']          = "RealEstate Page d'accueil";
$_['text_support']           = "Forum d'entraide";
$_['text_documentation']     = 'Documentation';
$_['text_logout']            = 'Se déconnecter';
