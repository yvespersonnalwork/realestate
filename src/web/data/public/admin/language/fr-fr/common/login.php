<?php
// header
$_['heading_title']  = 'Administration';

// Text
$_['text_heading']   = 'Administration';
//xml update
//$_['text_login']     = 'Veuillez saisir vos identifiants..';
$_['text_login']     = 'Identifiez vous pour accéder à votre compte!';
//xml update
$_['text_forgotten'] = 'mot de passe oublié';

// Entry
$_['entry_username'] = "Nom d'utilisateur";
$_['entry_password'] = 'Mot de passe';

// Button
$_['button_login']   = "S'identifier";

// Error
$_['error_login']    = 'Aucune correspondance pour le nom d'utilisateur et / ou le mot de passe.';
$_['error_token']    = 'Session de jeton invalide. Veuillez vous reconnecter.';