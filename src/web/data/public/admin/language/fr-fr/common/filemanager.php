<?php
// Heading
$_['heading_title']    = "Gestionnaire d'images";

// Text
$_['text_uploaded']    = "Succès: votre fichier a été téléchargé!";
$_['text_directory']   = 'Succès: répertoire créé!';
$_['text_delete']      = 'Succès: votre fichier ou répertoire a été supprimé.!';

// Entry
$_['entry_search']     = 'Chercher..';
$_['entry_folder']     = 'Nom de dossier';

// Error
$_['error_permission'] = 'Avertissement: autorisation refusée!';
$_['error_filename']   = 'Attention: le nom du fichier doit être compris entre 3 et 255!';
$_['error_folder']     = 'Attention: le nom du dossier doit être compris entre 3 et 255!';
$_['error_exists']     = 'Attention: un fichier ou répertoire du même nom existe déjà!';
$_['error_directory']  = "Avertissement: le répertoire n'existe pas!";
$_['error_filesize']   = 'Avertissement: taille de fichier incorrecte!';
$_['error_filetype']   = 'Avertissement: type de fichier incorrect!';
$_['error_upload']     = "Avertissement: le fichier n'a pas pu être téléchargé pour une raison inconnue!";
$_['error_delete']     = 'Attention: vous ne pouvez pas supprimer ce répertoire!';
