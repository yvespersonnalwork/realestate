<?php
// Heading
$_['heading_title']    		= 'Agente';

// Text
$_['text_success']     		= "Succès: vous avez modifié l'agent!";
$_['text_export']     		= 'Exportation';
$_['text_list']      		= 'Liste d'agents';
$_['text_add']      		= 'Ajouter un agent';
$_['text_edit']     		= 'Agent d'édition';
$_['text_enable']   		= 'Activer';
$_['text_disable']   		= 'Désactiver';
$_['text_invoice']   		= 'Facture d'achat';
$_['text_print']   			= 'Impression';

// Filter
$_['filterlable_name']    	= 'Nom';
$_['filterlable_status']    = 'Statut';

// Column
$_['column_images']   		= 'Images';
$_['column_name']      		= 'Nom';
$_['column_email']     		= 'Email';
$_['column_sort_order']  	= 'Ordre de tri';
$_['column_status']     	= 'Statut';
$_['column_action']   	 	= 'action';
$_['column_approve']    	= 'Approuver';
$_['column_plans']    		= 'Des plans';
$_['column_planlimit']    	= 'Plan de propriété limite';
$_['column_planexpiry']    	= 'Plan d'expiration';
$_['button_approve']     	= 'Approuver';
$_['button_desapprove']     = 'Désapprouver';

// Lable
$_['lable_Agent']        	= "Nom d'agent";
$_['lable_positions']   	= 'Positions';
$_['lable_image'] 			= 'Image';
$_['lable_email']         	= 'Email';
$_['lable_address']        	= 'Adresse';
$_['lable_password']        = 'Mot de passe';
$_['lable_city']        	= 'Ville';
$_['lable_contact']        	= 'Contact';
$_['lable_pincode']        	= 'Code PIN';
$_['lable_sort_order']   	= 'Ordre de tri';
$_['lable_descriptions']   	= 'La description';
$_['lable_country']      	= 'Pays';
$_['lable_status']      	= 'Statut';
$_['label_plans']      		= 'Des plans';

// Entry
$_['entry_Agent']        	= "Nom d'agent";
$_['entry_positions']   	= 'Positions';
$_['entry_image'] 			= 'Image';
$_['entry_email']         	= 'Email';
$_['entry_address']        	= 'Adresse';
$_['entry_password']        = 'Mot de passe';
$_['entry_city']        	= 'Ville';
$_['entry_contact']        	= 'Contact';
$_['entry_pincode']        	= 'Code PIN';
$_['entry_sort_order']   	= 'Ordre de trir';
$_['entry_descriptions']   	= 'La description';
$_['entry_country']      	= 'Pays';
$_['entry_status']      	= 'Statut';
$_['text_price']      	    = 'Prix';
$_['text_orderstatus']      = 'Statut de la commande';

//button
$_['button_shortcut']       = 'Raccourcie';
$_['button_view']       	= 'Vue de la facture';

//invoice
$_['entry_plans']       	= 'Des plans';
$_['entry_propertylimit']    	= 'Plan de propriété limite';
$_['entry_top']    	= ' Haut';
$_['entry_feature']    	= ' Fonctionnalité';
$_['entry_bottom']    	= ' Bas';
$_['entry_latest']    	= ' Dernière';
$_['entry_pstatus']    	= 'Statut du plan';
$_['entry_planstart']    	= 'Date de début du plan';
$_['entry_planend']    	= 'Date de fin du plan';

// Error
$_['error_email_exist']		= 'Email Id existe déjà';
$_['error_email']      		= "Format d'email invalide";
$_['error_agentname']            = ' Le nom doit comporter entre 2 et 255 caractères.!';
$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier le modèle de newsletter.!";

