<?php
// Heading
$_['heading_title']     	 = "Modèle de courrier d'agent";

// Text
$_['text_success']       	 = 'Succès: vous avez modifié !';
$_['text_list']           	 = "Liste de modèles d'e-mail à envoyer à l'agent";
$_['text_add']          	 = 'Ajouter un mail';
$_['text_edit']         	 = 'Modifier le courrier';
$_['text_default']      	 = 'Défaut';
$_['text_enable']      	     = 'Activer';
$_['text_disable']      	 = 'Désactiver';
$_['text_agentregister']     = "agente_S'inscrire_courrier";
$_['text_expiryplan']     	 = 'agente_expiration_plan';
$_['text_agentapproved']     = 'agente_approuvée_courrier';
$_['text_agentproperty']     = 'agente_ajouter_propriété_admin_courrier';
$_['text_propertyapp']       = 'property_approuvée_courrier';
$_['text_agentforgotte']     = 'agente_oubliée_courrier';
$_['text_paymount']     = 'agente_montant_courrier';

// Column
$_['column_name']       	 = 'Nom';
$_['column_date']        	 = 'date ajoutée';
$_['column_action']      	 = 'action';

// Lable
$_['lable_subject']          = 'Matière';
$_['lable_message']          = 'Message';
$_['lable_name']             = 'Nom';
$_['lable_status']           = 'Statut';
$_['lable_type']             = 'Type';

// Entry
$_['entry_subject']          = 'Matière';
$_['entry_message']          = 'Message';
$_['entry_name']             = 'Nom';
$_['entry_status']           = 'Statut';
$_['entry_type']             = 'Type';

/// Tab
$_['tab_mail']               = 'Courrier';
$_['tab_info']               = 'Info';

// Error
$_['error_warning'] 		 = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission'] 		 = 'Avertissement: vous n'êtes pas autorisé à modifier les fabricants.!';
$_['error_name']             = 'Le nom doit comporter entre 2 et 64 caractères.!';
$_['error_type']             = 'Veuillez sélectionner le type de courrier';