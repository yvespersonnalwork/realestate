<?php
// Heading
$_['heading_title']        = 'Apis';

// Text
$_['text_success']         = 'Succès: vous avez modifié les API!';
$_['text_list']            = "Liste d'API";
$_['text_add']             = 'Ajouter une API';
$_['text_edit']            = "Editer l'API";
$_['text_ip']              = "Vous pouvez créer ci-dessous une liste des adresses IP autorisées à accéder à l’API. Votre adresse IP actuelle est %s";

// Column
$_['column_name']          = "Nom de l'API";
$_['column_status']        = 'Statut';
$_['column_date_added']    = 'date ajoutée';
$_['column_date_modified'] = 'Date modifiée';
$_['column_token']         = 'Jeton';
$_['column_ip']            = 'IP';
$_['column_action']        = 'action';

// Entry
$_['entry_name']           = "Nom de l'API";
$_['entry_key']            = 'clé API';
$_['entry_status']         = 'Statut';
$_['entry_ip']             = 'IP';

// Error
$_['error_permission']     = "Avertissement: vous n'êtes pas autorisé à modifier les API!";
$_['error_name']           = "Le nom de l'API doit comporter entre 3 et 20 caractères.!";
$_['error_key']            = 'La clé API doit comporter entre 64 et 256 caractères.!';
