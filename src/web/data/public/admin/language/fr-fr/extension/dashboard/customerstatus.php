<?php

// Heading

$_['heading_title']     = 'Dernières 10 annonces';





// Column

$_['column_order_id']   = 'numéro de commande';

$_['column_customer']   = 'Client';

$_['column_status']     = 'Statut';

$_['column_total']      = 'Totale';

$_['column_date_added'] = 'date ajoutée';

$_['column_action']     	    = 'action';

///

$_['column_property_status']	= 'État de la propriété';
$_['column_image']              = 'Image';
$_['column_name']            	= 'Nom';
$_['column_price']           	= 'Prix';
$_['column_approved']        	= 'Approuvée';
$_['column_status']         	= 'Statut';
$_['column_view']         		= 'Vue';
$_['column_viewll']         	= 'Voir tout';
$_['text_disable']         		= 'Désactiver';
$_['text_enable']         		= 'Activer';


////btn 
$_['button_approve']         		= 'Approuver';
