<?php
// Heading
$_['heading_title']    = 'Recherche par catégorie TMD';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de recherche par catégorie!';
$_['text_edit']        = 'Modifier le module de recherche par catégorie';

// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de recherche par catégorie!';