<?php

// Heading

$_['heading_title']    = 'Rechercher une recherche';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_edit']        = 'Modifier le module de recherche de recherche';
$_['text_status']      = 'Statut';

///success msg 
$_['text_success']     = 'Succès: vous avez modifié le module de recherche de recherche!';

//  input Entry
$_['entry_status']     = 'Statut';
// Error
$_['error_permission'] = 'Avertissement: Vous n\'êtes pas autorisé à modifier le module Rechercher une recherche!';


