<?php

// Heading

$_['heading_title']    = 'Promotions';



// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié les promotions de module!';
$_['text_edit']        = 'Module de modification des promotions';


// Entry

$_['entry_name']       = 'Nom du module';
$_['entry_limit']      = 'Limite';
$_['entry_width']      = 'Largeur';
$_['entry_height']     = 'la taille';
$_['entry_status']     = 'Statut';

// Error

$_['error_permission'] = 'Avertissement: Vous n\'êtes pas autorisé à modifier le module des promotions!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';
$_['error_width']      = 'Largeur requise!';
$_['error_height']     = 'Hauteur requise!';