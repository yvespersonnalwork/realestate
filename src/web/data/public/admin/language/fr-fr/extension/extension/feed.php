<?php
// Heading
$_['heading_title']    = 'Les flux';

// Text
$_['text_success']     = 'Succès: vous avez modifié les flux!';
$_['text_list']        = 'Liste de flux';

// Column
$_['column_name']      = 'Nom du produit';
$_['column_status']    = 'Statut';
$_['column_action']    = 'action';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier les flux!';