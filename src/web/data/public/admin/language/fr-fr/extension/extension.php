<?php

// Heading

$_['heading_title']  = 'Modules';



// Text

$_['text_success']   = 'Succès: vous avez modifié des extensions!';

$_['text_list']      = 'Liste de modules';

$_['text_type']      = 'Choisissez le type d\'extension';

$_['text_filter']    = 'Filtre';

$_['text_analytics'] = 'Analytique';

$_['text_captcha']   = 'Captcha';

$_['text_dashboard'] = 'Tableau de bord';

$_['text_feed']      = 'Les flux';

$_['text_fraud']     = 'Anti fraude';

$_['text_module']    = 'Modules';

$_['text_content']   = 'Modules de contenu';

$_['text_menu']      = 'Modules de menu';

$_['text_payment']   = 'Payments';

$_['text_shipping']  = 'livraison';

$_['text_theme']     = 'Des thèmes';

$_['text_total']     = 'Total des commandes';