<?php
// Heading
$_['heading_title']     = 'Modifications';

// Text
$_['text_success']      = 'Succès: vous avez modifié les modifications!';
$_['text_refresh']      = 'Chaque fois que vous activez / désactivez ou supprimez une modification, vous devez cliquer sur le bouton Actualiser pour reconstruire votre cache de modifications.!';
$_['text_list']         = 'Liste de modification';

// Column
$_['column_name']       = 'Nom de la modification';
$_['column_author']     = 'Auteure';
$_['column_version']    = 'Version';
$_['column_status']     = 'Statut';
$_['column_date_added'] = 'date ajoutée';
$_['column_action']     = 'action';

// Error
$_['error_permission']  = 'Avertissement: vous n\'êtes pas autorisé à modifier les modifications!';