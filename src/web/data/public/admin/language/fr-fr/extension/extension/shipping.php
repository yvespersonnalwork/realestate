<?php
// Heading
$_['heading_title']     = 'livraison';

// Text
$_['text_success']      = 'Succès: vous avez modifié l\'expédition!';
$_['text_list']         = 'Liste d\'expédition';

// Column
$_['column_name']       = 'Mode de livraison';
$_['column_status']     = 'Statut';
$_['column_sort_order'] = 'Ordre de tri';
$_['column_action']     = 'action';

// Error
$_['error_permission']  = 'Attention: vous n\'avez pas la permission de modifier l\'expédition!';