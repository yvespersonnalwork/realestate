<?php
// Heading
$_['heading_title']    = 'Carrousel';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module carrousel.!';
$_['text_edit']        = 'Module de carrousel d\'édition';

// Entry
$_['entry_name']       = 'Nom du module';
$_['entry_banner']     = 'Bannière';
$_['entry_width']      = 'Largeur';
$_['entry_height']     = 'la taille';
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de carrousel!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères!';
$_['error_width']      = 'Largeur requise!';
$_['error_height']     = 'Hauteur requise!';