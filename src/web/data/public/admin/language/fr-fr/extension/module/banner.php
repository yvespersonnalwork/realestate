<?php

// Heading

$_['heading_title']    = 'Bannière';



// lable

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module bannière!';
$_['text_edit']        = 'Modifier le module de bannière';
$_['text_name']        = 'Nom du module';
$_['text_banner']      = 'Bannière';
$_['text_dimension']   = 'Dimension (W x H) et type de redimensionnement';
$_['text_width']       = 'Largeur';
$_['text_height']      = 'la taille';
$_['text_status']      = 'Statut';


// Entry

$_['entry_name']       = 'Nom du module';

$_['entry_banner']     = 'Bannière';

$_['entry_dimension']  = 'Dimension (W x H) et type de redimensionnement';

$_['entry_width']      = 'Largeur';

$_['entry_height']     = 'la taille';

$_['entry_status']     = 'Statut';



// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de bannière.!';

$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';

$_['error_width']      = 'Largeur requise!';

$_['error_height']     = 'Hauteur requise!';