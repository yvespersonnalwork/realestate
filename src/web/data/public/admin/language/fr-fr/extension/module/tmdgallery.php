<?php

// Heading

$_['heading_title']       = 'Galerie de photos (TMD)';



// Lable

$_['text_extension']      = 'Les extensions';
$_['text_success']        = 'Succès: vous avez modifié le module TMD Photo Gallery!';
$_['text_content_top']    = 'Contenu haut';
$_['text_content_bottom'] = 'Contenu bas';
$_['text_column_left']    = 'Colonne de gauche';
$_['text_column_right']   = 'Colonne de droite';
$_['text_edit']           = 'Modifier la galerie de photos (TMD)';

$_['text_name']           = 'Nom du module';
$_['text_limit']          = 'Limite'; 
$_['text_image']          = 'Image (L x H) et type de redimensionnement';
$_['text_layout']         = 'Disposition';
$_['text_position']       = 'Position';
$_['text_status']         = 'Statut';
$_['text_sort_order']     = 'Ordre de tri:';
$_['text_album']    	  = 'Album';
$_['text_width']          = 'Largeur';
$_['text_height']         = 'la taille';

// input value Entry
$_['entry_name']          = 'Nom du module';
$_['entry_limit']         = 'Limite'; 
$_['entry_image']         = 'Image (L x H) et type de redimensionnement';
$_['entry_layout']        = 'Disposition';
$_['entry_position']      = 'Position';
$_['entry_status']        = 'Statut';
$_['entry_sort_order']    = 'Ordre de tri:';
$_['entry_album']    	  = 'Album';
$_['entry_width']         = 'Largeur';
$_['entry_height']        = 'la taille';

// Error

$_['error_permission']    = 'Attention: vous n\'êtes pas autorisé à modifier le module TMD Photo Gallery!';

$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';

$_['error_width']      = 'Largeur requise!';

$_['error_height']     = 'Hauteur requise!';

?>