<?php
// Heading
$_['heading_title']    = 'Klarna Fee';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total des frais Klarna.!';
$_['text_edit']        = 'Modifier le total des frais Klarna';
$_['text_sweden']      = 'Suède';
$_['text_norway']      = 'Norvège';
$_['text_finland']     = 'Finlande';
$_['text_denmark']     = 'Danemark';
$_['text_germany']     = 'Allemagne';
$_['text_netherlands'] = 'Les Pays-Bas';

// Entry
$_['entry_total']      = 'Total de la commande';
$_['entry_fee']        = 'Frais de facture';
$_['entry_tax_class']  = 'Classe d\'impôt';
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Attention: vous n’avez pas l’autorisation de modifier le total des frais Klarna!';