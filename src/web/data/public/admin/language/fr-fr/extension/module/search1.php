<?php

// Heading

$_['heading_title']    = 'Chercher';



// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de recherche!';
$_['text_edit']        = 'Modifier le module de recherche';
// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de recherche!';