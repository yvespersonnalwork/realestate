<?php
// Heading
$_['heading_title']    = 'Chèque cadeau';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total du chèque-cadeau.!';
$_['text_edit']        = 'Modifier le chèque-cadeau total';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Attention: vous n\'êtes pas autorisé à modifier le total du chèque-cadeau.!';