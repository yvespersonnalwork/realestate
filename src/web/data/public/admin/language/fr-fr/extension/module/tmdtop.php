<?php

// Heading

$_['heading_title']    = 'tmdtop';



// Text

$_['text_extension']   = 'Les extensions';

$_['text_success']     = 'Succès: vous avez modifié le module Tmd Top!';

$_['text_edit']        = 'Modifier le module en vedette';



// Entry

$_['entry_name']       = 'Nom du module';

$_['entry_property']    = 'Propriétés';

$_['entry_limit']      = 'Limite';

$_['entry_width']      = 'Largeur';

$_['entry_height']     = 'la taille';

$_['entry_status']     = 'Statut';



// Help

$_['help_product']     = '(Saisie automatique)';



// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module sélectionné.!';

$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';

$_['error_width']      = 'Largeur requise!';

$_['error_height']     = 'Hauteur requise!';