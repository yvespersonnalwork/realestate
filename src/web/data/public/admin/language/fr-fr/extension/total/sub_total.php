<?php
// Heading
$_['heading_title']    = 'Total';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total partiel!';
$_['text_edit']        = 'Modifier le total partiel';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le total partiel!';