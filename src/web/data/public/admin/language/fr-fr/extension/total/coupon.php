<?php
// Heading
$_['heading_title']    = 'Coupon';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total du coupon!';
$_['text_edit']        = 'Modifier le coupon';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Attention: vous n\'êtes pas autorisé à modifier le total des coupons!';