<?php

// Heading

$_['heading_title']       = 'Témoignage (TMD)';



// Text

$_['text_extension']      = 'Les extensions';
$_['text_success']        = 'Succès: vous avez modifié le dernier module!';
$_['text_content_top']    = 'Contenu haut';
$_['text_content_bottom'] = 'Contenu bas';
$_['text_column_left']    = 'Colonne de gauche';
$_['text_column_right']   = 'Colonne de droite';
$_['text_edit']           = 'Modifier le témoignage (TMD)';

//label 
$_['text_limit']         = 'Limite'; 
$_['text_status']        = 'Statut';
$_['text_name']          = 'Nom du module';

// Entry
$_['entry_limit']         = 'Limite'; 
$_['entry_status']        = 'Statut';
$_['entry_name']          = 'Nom du module';


/// btn 
$_['button_save']             = 'sauver';
$_['button_cancel']           = 'Annuler';

// Error

$_['error_permission']    = 'Avertissement: vous n\'êtes pas autorisé à modifier le dernier module!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';

?>