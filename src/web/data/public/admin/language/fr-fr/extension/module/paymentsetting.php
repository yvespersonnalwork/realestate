<?php

// Heading
$_['heading_title']    = 'Paramètre de paiement';

// Text

$_['text_extension']   = 'Paramètre de paiement';
$_['text_success']     = 'Succès: vous avez modifié le module de configuration du paiement!';
$_['text_edit']        = 'Module de modification des paramètres de paiement';
$_['text_authorization']			 = 'Autorisation';
$_['text_sale']						 = 'Vente'; 
$_['text_complete']						 = 'Achevée'; 
$_['text_select']						 = '--Veuillez choisir--'; 
$_['tab_orderstatus']						 = 'Statut de la commande'; 

// Lable
$_['label_email']					 = 'Email';
$_['label_test']					 = 'Mode bac à sable';
$_['label_transaction']				 = 'Méthode de transaction';
$_['label_debug']					 = 'Mode débogage';
$_['label_total']					 = 'Totale';
$_['label_canceled_reversal_status'] = 'Annulation du statut d\'annulation';
$_['label_completed_status']		 = 'Statut terminé';
$_['label_denied_status']			 = 'Statut refusé';
$_['label_expired_status']			 = 'Statut expiré';
$_['label_failed_status']			 = 'Statut échoué';
$_['label_pending_status']			 = 'Statut en attente';
$_['label_processed_status']		 = 'Statut traité';
$_['label_refunded_status']			 = 'Statut remboursé';
$_['label_reversed_status']			 = 'Statut inversé';
$_['label_voided_status']			 = 'Statut annulé';
$_['label_geo_zone']				 = 'Zone géographique';
$_['label_status']					 = 'Statut';
$_['label_sort_order']				 = 'Ordre de tri';
$_['label_orderstatus']				 = 'Statut de la commande';

// Entry
$_['entry_email']					 = 'Email';
$_['entry_test']					 = 'Mode bac à sable';
$_['entry_transaction']				 = 'Méthode de transaction';
$_['entry_debug']					 = 'Mode débogage';
$_['entry_total']					 = 'Totale';
$_['entry_canceled_reversal_status'] = 'Annulation du statut d\'annulation';
$_['entry_completed_status']		 = 'Statut terminé';
$_['entry_denied_status']			 = 'Statut refusé';
$_['entry_expired_status']			 = 'Statut expiré';
$_['entry_failed_status']			 = 'Statut échoué';
$_['entry_pending_status']			 = 'Statut en attente';
$_['entry_processed_status']		 = 'Statut traité';
$_['entry_refunded_status']			 = 'Statut remboursé';
$_['entry_reversed_status']			 = 'Statut inversé';
$_['entry_voided_status']			 = 'Statut annulé';
$_['entry_geo_zone']				 = 'Zone géographique';
$_['entry_status']					 = 'Statut';
$_['entry_sort_order']				 = 'Ordre de tri';
$_['entry_orderstatus']				 = 'Statut de la commande';

// Help
$_['help_test']						 = 'Utiliser le serveur de passerelle en direct ou en test (bac à sable) pour traiter les transactions?';
$_['help_debug']			    	 = 'Enregistre des informations supplémentaires dans le journal système';
$_['help_total']					 = 'Le total de la commande que la commande doit atteindre avant que ce mode de paiement devienne actif';

// Error
$_['error_permission']				 = 'Attention: vous n\'êtes pas autorisé à modifier le paiement PayPal!';
$_['error_email']					 = 'Email (requis!';

