<?php

// Heading

$_['heading_title']    = 'Information';
// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module d\'information!';
$_['text_edit']        = 'Module d\'information d\'édition';

// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module d\'information!';