<?php
$_['heading_title']    = 'Captcha de base';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']	   = 'Succès: vous avez modifié Basic Captcha!';
$_['text_edit']        = 'Éditer le captcha de base';

// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier Basic Captcha!';
