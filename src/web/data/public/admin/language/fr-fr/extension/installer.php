<?php
// Heading
$_['heading_title']        = 'Installateur d\'extension';

// Text
$_['text_upload']          = 'Téléchargez vos extensions';
$_['text_success']         = 'Succès: l\'extension a été installée!';
$_['text_unzip']           = 'Extraction des fichiers!';
$_['text_ftp']             = 'Copier des fichiers!';
$_['text_sql']             = 'Running SQL!';
$_['text_xml']             = 'Appliquer des modifications!';
$_['text_php']             = 'Exécuter PHP!';
$_['text_remove']          = 'Supprimer des fichiers temporaires!';
$_['text_clear']           = 'Succès: vous avez effacé tous les fichiers temporaires!';

// Entry
$_['entry_upload']         = 'Téléverser un fichier';
$_['entry_overwrite']      = 'Fichiers qui seront écrasés';
$_['entry_progress']       = 'Le progrès';

// Help
$_['help_upload']          = 'Nécessite un fichier de modification avec l\'extension ".ocmod.zip" ou ".ocmod.xml".';

// Error
$_['error_permission']     = 'Avertissement: vous n\'êtes pas autorisé à modifier des extensions!';
$_['error_temporary']      = 'Avertissement: Certains fichiers temporaires doivent être supprimés. Cliquez sur le bouton d\'effacement pour les supprimer!';
$_['error_upload']         = 'Le fichier n\'a pas pu être téléchargé!';
$_['error_filetype']       = 'type de fichier invalide!';
$_['error_file']           = 'Fichier introuvable!';
$_['error_unzip']          = 'Le fichier zip n\'a pas pu être ouvert!';
$_['error_code']           = 'La modification nécessite un code d\'identification unique!';
$_['error_exists']         = 'La modification% s utilise le même code d\'identification que celui que vous essayez de télécharger.!';
$_['error_directory']      = 'Répertoire contenant les fichiers à télécharger introuvable.!';
$_['error_ftp_status']     = 'FTP doit être activé dans les paramètres';
$_['error_ftp_connection'] = 'Impossible de se connecter en tant que %s:%s';
$_['error_ftp_login']      = 'Ne pouvait pas se connecter as %s';
$_['error_ftp_root']       = 'Impossible de définir la racine directory as %s';
$_['error_ftp_directory']  = 'Impossible de changer de répertoire %s';
$_['error_ftp_file']       = 'Impossible de télécharger le fichier %s';
