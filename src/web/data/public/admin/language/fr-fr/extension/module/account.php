<?php
// Heading
$_['heading_title']    = 'Compte';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de compte!';
$_['text_edit']        = 'Module de modification de compte';

// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module du compte!';