<?php
// Heading
$_['heading_title']    = 'Les taxes';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total des taxes!';
$_['text_edit']        = 'Modifier le total des taxes';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le total des taxes!';