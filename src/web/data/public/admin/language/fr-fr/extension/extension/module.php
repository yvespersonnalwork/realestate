<?php
// Heading
$_['heading_title']    = 'Modules';

// Text
$_['text_success']     = 'Succès: vous avez modifié des modules!';
$_['text_layout']      = 'Une fois que vous avez installé et configuré un module, vous pouvez l’ajouter à un modèle <a href="%s" class="alert-link"> ici.</a>!';
$_['text_add']         = 'Ajouter un module';
$_['text_list']        = 'Liste de modules';

// Column
$_['column_name']      = 'Nom du module';
$_['column_action']    = 'action';

// Entry
$_['entry_code']       = 'Module';
$_['entry_name']       = 'Nom du module';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier des modules!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';
$_['error_code']       = 'Extension requise!';