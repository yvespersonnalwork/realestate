<?php
// Heading
$_['heading_title']    = 'livraison';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total des envois!';
$_['text_edit']        = 'Modifier le total d\'expédition';

// Entry
$_['entry_estimator']  = 'Estimateur d\'expédition';
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le total des envois!';