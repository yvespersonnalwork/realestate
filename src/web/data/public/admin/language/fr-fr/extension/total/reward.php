<?php
// Heading
$_['heading_title']    = 'Points de récompense';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total des points de fidélité!';
$_['text_edit']        = 'Modifier le total des points de récompense';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le total des points de récompense!';