<?php
// Heading
$_['heading_title']    = 'Catégories';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le client du tableau de bord.!';
$_['text_edit']        = 'Modifier le client du tableau de bord';
$_['text_view']        = 'Voir plus...';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';
$_['entry_width']      = 'Largeur';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le client du tableau de bord.!';