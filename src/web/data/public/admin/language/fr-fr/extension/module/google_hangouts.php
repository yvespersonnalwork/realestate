<?php

// Heading

$_['heading_title']    = 'Google Hangouts';


// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module Google Hangouts!';
$_['text_edit']        = 'Modifier le module Google Hangouts';

// Entry

$_['entry_code']       = 'Code Google Talk';
$_['entry_status']     = 'Statut';

// Help

$_['help_code']        = 'Aller à <a href="https://developers.google.com/+/hangouts/button" target="_blank">Créer un badge de chatback Google Hangout</a> et copier &amp; coller le code généré dans la zone de texte.';

// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module Google Hangouts.!';

$_['error_code']       = 'Code requis';