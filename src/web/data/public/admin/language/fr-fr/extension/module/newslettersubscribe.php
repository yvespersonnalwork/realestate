<?php

// Heading

$_['heading_title']    = '<span style="color:#33CCFF">Lettre d\'information</span>';

$_['heading_title1']    = 'Lettre d\'information';



// Text

$_['text_extension']      = 'Les extensions';
$_['text_success']        = 'Succès: Vous avez modifié le module Lettre d’information!';
$_['text_left']           = 'La gauche';
$_['text_right']          = 'Droite';
$_['text_edit']           = 'Lettre d\'information Modifier';
$_['text_text_default']   = 'Lorsque vous activez cette option, les utilisateurs enregistrés dans le panier ouvert peuvent également s’abonner ou se désabonner à l’aide de cette option';
$_['text_info']           = '<span class="help">Développé par <a href="http://www.probeseven.com" target="_blank"> Probeseven </a></span>';  
$_['text_module']         = 'Modules';

//help

$_['help_name']          	= 'Ajoutez le nom du module ici.';
$_['help_status']        	=
$_['help_popup']         	= 'si vous souhaitez créer un bulletin d\'information dans une fenêtre contextuelle, sélectionnez Activer, sinon Désactiver';
$_['help_unsubscribe']   	= 'Si vous souhaitez vous désabonner par e-mail du bulletin d\'information, sélectionnez Activer à partir d\'ici.';
$_['help_text']          	= 'Ajoutez le formulaire de titre du bouton Newsletter ici';
$_['help_text_color']    	= 'Définir la couleur du bulletin d\'information Texte d\'ici';
$_['help_text_color1']    	= 'Définir la couleur de sans newsletter popup Text from here';
$_['help_bgtop']         	= 'De là, vous pouvez définir la couleur de la newsletter Popup';
$_['help_bgbottom']      	= 'À partir de là, vous pouvez définir la couleur inférieure du bouton newsletter pour l’effet Dégradé.';
$_['help_border']        	= 'Ajouter la couleur de fond du bulletin d\'information d\'ici';
$_['help_hover']         	= 'Bouton Ajouter Newslater La couleur de survol du texte à partir d\'ici';
$_['help_container']     	= 'Définir la couleur de fond de la boîte à partir d\'ici';


// Entry

$_['entry_name']  			= 'Nom du module';
$_['entry_text_color1']  	= 'Default Newslater TextTexte de Newslater par défaut';
$_['entry_unsubscribe'] 	= 'Se désabonner';
$_['entry_status']      	= 'Statut';
$_['entry_options']    	 	= 'Les options';
$_['entry_mail']   			= 'Envoyer un email';
$_['entry_popup']   		= 'Apparaitre';
$_['entry_registered'] 		= 'Utilisateurs enregistrés';
$_['entry_text_color']  	= 'Lettre d\'information Couleur du texte';
$_['entry_bgtop_color'] 	= 'Couleurs Popup';
$_['entry_bgbottom_color']  = 'Couleur du texte du bouton';
$_['entry_border_color']   	= 'Bouton Newsletter Bgcolor';
$_['entry_text']  			= 'Titre du bouton';
$_['entry_hover_color']   	= 'Bouton Survol Couleur du texte';
$_['entry_container_color'] = 'Conteneur couleur bg';
$_['tab_general']   		= 'Général';
$_['tab_data']      		= 'Personnaliser la couleur';


// Error
$_['error_permission']      = 'Avertissement: Vous n\'êtes pas autorisé à modifier le module. Lettre d\'information!';
$_['error_name']            = 'Le nom du module doit comporter entre 3 et 64 caractères.!';

?>