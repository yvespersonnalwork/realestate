<?php
// Heading
$_['heading_title']    = 'Totale';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié les totaux!';
$_['text_edit']        = 'Editer Total Total';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier les totaux!';