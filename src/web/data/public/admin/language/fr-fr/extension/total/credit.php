<?php
// Heading
$_['heading_title']    = 'Crédit du site';

// Text
$_['text_total']       = 'Ordre Total';
$_['text_success']     = 'Succès: Du har ændret butikken Site web Total!';
$_['text_edit']        = 'Rediger Store Site Web Total';

// Entry
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Sorteringsrækkefølge';

// Error
$_['error_permission'] = 'Advarsel: Du har ikke tilladelse til at ændre Crédit au site Web Total!';