<?php

// Heading

$_['heading_title']     = 'Contenu HTML';



// Text

$_['text_extension']    = 'Les extensions';
$_['text_success']      = 'Succès: vous avez modifié le module de contenu HTML!';
$_['text_edit']         = 'Modifier le module de contenu HTML';
// lable
$_['text_name']        = 'Nom du module';
$_['text_title']       = 'Titre de titre';
$_['text_description'] = 'La description';
$_['text_status']      = 'Statut';
// Entry
$_['entry_name']        = 'Nom du module';
$_['entry_title']       = 'Titre de titre';
$_['entry_description'] = 'La description';
$_['entry_status']      = 'Status';

// Error
$_['error_permission']  = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de contenu HTML!';
$_['error_name']        = 'Le nom du module doit comporter entre 3 et 64 caractères!';