<?php
// Heading
$_['heading_title']              = 'FraudLabs Pro';

// Text
$_['text_extension']             = 'Les extensions';
$_['text_success']               = 'Succès: vous avez modifié les paramètres de FraudLabs Pro!';
$_['text_edit']                  = 'Paramètres';
$_['text_signup']                = 'FraudLabs Pro est un service de détection de fraude. Vous pouvez <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"> <u> vous inscrire ici </ u> </a> pour obtenir une clé d\'API gratuite.';
$_['text_id']                    = 'FraudLabs Pro ID';
$_['text_ip_address']            = 'Adresse IP';
$_['text_ip_net_speed']          = 'Vitesse nette IP';
$_['text_ip_isp_name']           = 'Nom du FAI IP';
$_['text_ip_usage_type']         = 'Type d\'utilisation IP';
$_['text_ip_domain']             = 'Domaine IP';
$_['text_ip_time_zone']          = 'Fuseau horaire IP';
$_['text_ip_location']           = 'Emplacement IP';
$_['text_ip_distance']           = 'Distance IP';
$_['text_ip_latitude']           = 'Latitude IP ';
$_['text_ip_longitude']          = 'Longitude IP';
$_['text_risk_country']          = 'Pays à haut risque IP';
$_['text_free_email']            = 'Email gratuit';
$_['text_ship_forward']          = 'Expédier';
$_['text_using_proxy']           = 'Utiliser un proxy';
$_['text_bin_found']             = 'BIN Trouvé';
$_['text_email_blacklist']       = 'Email Liste noire';
$_['text_credit_card_blacklist'] = 'Liste noire des cartes de crédit';
$_['text_score']                 = 'FraudLabs Pro Score';
$_['text_status']                = 'État FraudLabs Pro';
$_['text_message']               = 'Message';
$_['text_transaction_id']        = 'identifiant de transaction';
$_['text_credits']               = 'Équilibre';
$_['text_error']                 = 'Erreur:';
$_['text_flp_upgrade']           = '<a href = "http://www.fraudlabspro.com/plan" target = "_ blank"> [Mettre à niveau]</a>';
$_['text_flp_merchant_area']     = 'Veuillez vous connecter à <a href="http://www.fraudlabspro.com/merchant/login" target="_blank"> Espace marchands FraudLabs Pro </a> pour plus d\'informations sur cette commande.';


// Entry
$_['entry_status']               = 'Statut';
$_['entry_key']                  = 'clé API';
$_['entry_score']                = 'Score de risque';
$_['entry_order_status']         = 'Statut de la commande';
$_['entry_review_status']        = 'Statut de révision';
$_['entry_approve_status']       = 'Approuver le statut';
$_['entry_reject_status']        = 'Rejeter le statut';
$_['entry_simulate_ip']          = 'Simuler IP';

// Help
$_['help_order_status']          = 'Les ordres dont le score est supérieur au score de risque défini se voient attribuer le statut de commande.';
$_['help_review_status']         = 'Les commandes marquées comme révisées par FraudLabs Pro se verront attribuer ce statut..';
$_['help_approve_status']        = 'Les commandes marquées comme approuvées par FraudLabs Pro se verront attribuer ce statut.';
$_['help_reject_status']         = 'Les commandes marquées comme rejetées par FraudLabs Pro se verront attribuer ce statut de commande.';
$_['help_simulate_ip']           = 'Simulez l\'adresse IP du visiteur pour le test. Laisser en blanc pour le désactiver.';
$_['help_fraudlabspro_id']       = 'Identifiant unique pour une transaction contrôlée par le système FraudLabs Pro.';
$_['help_ip_address']            = 'Adresse IP.';
$_['help_ip_net_speed']          = 'Vitesse de connexion.';
$_['help_ip_isp_name']           = 'ISP de l\'adresse IP.';
$_['help_ip_usage_type']         = 'Type d\'utilisation de l\'adresse IP. EG, ISP, Commercial, Résidentiel.';
$_['help_ip_domain']             = 'Nom de domaine de l\'adresse IP.';
$_['help_ip_time_zone']          = 'Fuseau horaire de l\'adresse IP.';
$_['help_ip_location']           = 'Localisation de l\'adresse IP.';
$_['help_ip_distance']           = 'Distance entre l\'adresse IP et le lieu de facturation.';
$_['help_ip_latitude']           = 'Latitude de l\'adresse IP.';
$_['help_ip_longitude']          = 'Longitude de l\'adresse IP.';
$_['help_risk_country']          = 'Si le pays d\'adresse IP figure dans la dernière liste de pays à risque élevé.';
$_['help_free_email']            = 'Si le courrier électronique provient d\'un fournisseur de messagerie gratuit.';
$_['help_ship_forward']          = 'Si l\'adresse d\'expédition est une adresse de transitaire.';
$_['help_using_proxy']           = 'Si l\'adresse IP provient de Anonymous Proxy Server.';
$_['help_bin_found']             = 'Si les informations BIN correspondent à notre liste BIN.';
$_['help_email_blacklist']       = 'Si l\'adresse e-mail est dans notre base de données de liste noire.';
$_['help_credit_card_blacklist'] = 'Si la carte de crédit est dans notre base de données de liste noire.';
$_['help_score']                 = 'Score de risque, 0 (risque faible) - 100 (risque élevé).';
$_['help_status']                = 'Statut FraudLabs Pro.';
$_['help_message']               = 'FraudLabs Pro description du message d\'erreur.';
$_['help_transaction_id']        = 'Identifiant unique pour une transaction contrôlée par le système FraudLabs Pro.';
$_['help_credits']               = 'Solde des crédits disponibles après cette transaction.';

// Error
$_['error_permission']           = 'Avertissement: vous n\'êtes pas autorisé à modifier les paramètres de FraudLabs Pro!';
$_['error_key']		             = 'Clé API requise!';