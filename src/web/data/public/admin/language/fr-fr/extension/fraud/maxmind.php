<?php
// Heading
$_['heading_title']                           = 'MaxMind Anti-Fraud';

// Text
$_['text_extension']                          = 'Les extensions';
$_['text_success']                            = 'Succès: vous avez modifié MaxMind anti-fraude!';
$_['text_edit']                               = 'Modifier MaxMind Anti-Fraud';
$_['text_signup']                             = 'MaxMind est un service de détection de fraude. Si vous n\'avez pas de clé de licence, vous pouvez <a href="http://www.maxmind.com/?rId=opencart" target="_blank"> <u> vous inscrire ici</u></a>.';
$_['text_country_match']                      = 'Match de pays:';
$_['text_country_code']                       = 'Code postal:';
$_['text_high_risk_country']                  = 'Pays à haut risque:';
$_['text_distance']                           = 'Distance:';
$_['text_ip_region']                          = 'Région IP:';
$_['text_ip_city']                            = 'Ville IP:';
$_['text_ip_latitude']                        = 'Latitude IP:';
$_['text_ip_longitude']                       = 'Longitude IP:';
$_['text_ip_isp']                             = 'FAI:';
$_['text_ip_org']                             = 'Organisation de la propriété intellectuelle:';
$_['text_ip_asnum']                           = 'ASNUM:';
$_['text_ip_user_type']                       = 'Type d\'utilisateur IP:';
$_['text_ip_country_confidence']              = 'Confiance du pays IP:';
$_['text_ip_region_confidence']               = 'Confiance dans la région IP:';
$_['text_ip_city_confidence']                 = 'Confiance en ville IP:';
$_['text_ip_postal_confidence']               = 'Confiance postale IP:';
$_['text_ip_postal_code']                     = 'Code postal IP:';
$_['text_ip_accuracy_radius']                 = 'Rayon de précision IP:';
$_['text_ip_net_speed_cell']                  = 'Cellule de vitesse nette IP';
$_['text_ip_metro_code']                      = 'Code de métro IP:';
$_['text_ip_area_code']                       = 'Code de zone IP:';
$_['text_ip_time_zone']                       = 'Fuseau horaire IP:';
$_['text_ip_region_name']                     = 'Nom de région IP:';
$_['text_ip_domain']                          = 'Domaine IP:';
$_['text_ip_country_name']                    = 'Nom du pays IP:';
$_['text_ip_continent_code']                  = 'Code de continent IP:';
$_['text_ip_corporate_proxy']                 = 'Proxy d\'entreprise IP:';
$_['text_anonymous_proxy']                    = 'proxy anonyme:';
$_['text_proxy_score']                        = 'Score de procuration:';
$_['text_is_trans_proxy']                     = 'Est un proxy transparent:';
$_['text_free_mail']                          = 'Mail gratuit:';
$_['text_carder_email']                       = 'Email du cardeur:';
$_['text_high_risk_username']                 = 'Nom d\'utilisateur à risque élevé:';
$_['text_high_risk_password']                 = 'Mot de passe à haut risque:';
$_['text_bin_match']                          = 'Bin Match:';
$_['text_bin_country']                        = 'Bin Country:';
$_['text_bin_name_match']                     = 'Correspondance Nom Bin:';
$_['text_bin_name']                           = 'Nom du bac:';
$_['text_bin_phone_match']                    = 'Match de téléphone bin:';
$_['text_bin_phone']                          = 'Bin Téléphone:';
$_['text_customer_phone_in_billing_location'] = 'Numéro de téléphone du client dans le lieu de facturation:';
$_['text_ship_forward']                       = 'Envoi à terme:';
$_['text_city_postal_match']                  = 'Match postal de ville:';
$_['text_ship_city_postal_match']             = 'Correspondance postale ville d\'expédition:';
$_['text_score']                              = 'But:';
$_['text_explanation']                        = 'Explication:';
$_['text_risk_score']                         = 'Score de risque:';
$_['text_queries_remaining']                  = 'Requêtes restantes:';
$_['text_maxmind_id']                         = 'ID Maxmind:';
$_['text_error']                              = 'Erreur:';

// Entry
$_['entry_key']                               = 'Clé de licence MaxMind';
$_['entry_score']                             = 'Score de risque';
$_['entry_order_status']                      = 'Statut de la commande';
$_['entry_status']                            = 'Statut';

// Help
$_['help_order_status']                       = 'Les ordres dont le score est supérieur au score de risque défini se verront attribuer ce statut et ne pourront pas atteindre automatiquement le statut complet..';
$_['help_country_match']                      = 'Si le pays de l\'adresse IP correspond au pays de l\'adresse de facturation (non-correspondance = risque plus élevé).';
$_['help_country_code']                       = 'Code de pays de l\'adresse IP.';
$_['help_high_risk_country']                  = 'Si l\'adresse IP ou le pays de facturation sont situés au Ghana, au Nigeria ou au Vietnam.';
$_['help_distance']                           = 'Distance entre l\'adresse IP et le lieu de facturation en kilomètres (grande distance = risque plus élevé).';
$_['help_ip_region']                          = 'Etat / région estimé de l\'adresse IP.';
$_['help_ip_city']                            = 'Ville estimée de l\'adresse IP.';
$_['help_ip_latitude']                        = 'Latitude estimée de l\'adresse IP.';
$_['help_ip_longitude']                       = 'Longitude estimée de l\'adresse IP.';
$_['help_ip_isp']                             = 'ISP de l\'adresse IP.';
$_['help_ip_org']                             = 'Organisation de l\'adresse IP.';
$_['help_ip_asnum']                           = 'Nombre estimé de systèmes autonomes de l\'adresse IP.';
$_['help_ip_user_type']                       = 'Type d\'utilisateur estimé de l\'adresse IP.';
$_['help_ip_country_confidence']              = 'Représentant notre confiance que l\'emplacement du pays est correct.';
$_['help_ip_region_confidence']               = 'Représentant notre confiance que l\'emplacement de la région est correct.';
$_['help_ip_city_confidence']                 = 'Représentant notre confiance que l\'emplacement de la ville est correct.';
$_['help_ip_postal_confidence']               = 'Représentant notre confiance que l\'emplacement du code postal est correct.';
$_['help_ip_postal_code']                     = 'Code postal estimé de l\'adresse IP.';
$_['help_ip_accuracy_radius']                 = 'Distance moyenne entre l\'emplacement réel de l\'utilisateur final utilisant l\'adresse IP et l\'emplacement renvoyé par la base de données GeoIP City, en miles.';
$_['help_ip_net_speed_cell']                  = 'Type de réseau estimé de l\'adresse IP.';
$_['help_ip_metro_code']                      = 'Code de métro estimé de l\'adresse IP.';
$_['help_ip_area_code']                       = 'Code régional estimé de l\'adresse IP.';
$_['help_ip_time_zone']                       = 'Fuseau horaire estimé de l\'adresse IP.';
$_['help_ip_region_name']                     = 'Nom de région estimé de l\'adresse IP.';
$_['help_ip_domain']                          = 'Domaine estimé de l\'adresse IP.';
$_['help_ip_country_name']                    = 'Nom de pays estimé de l\'adresse IP.';
$_['help_ip_continent_code']                  = 'Code de continent estimé de l\'adresse IP.';
$_['help_ip_corporate_proxy']                 = 'Si l\'IP est un proxy d\'entreprise dans la base de données ou non.';
$_['help_anonymous_proxy']                    = 'Indique si l\'adresse IP est un proxy anonyme (proxy anonyme = risque très élevé).';
$_['help_proxy_score']                        = 'Probabilité que l\'adresse IP soit un proxy ouvert.';
$_['help_is_trans_proxy']                     = 'Indique si l\'adresse IP est dans notre base de données de serveurs proxy transparents connus, renvoyée si l\'IP transmis est transmis en tant qu\'entrée.';
$_['help_free_mail']                          = 'Si le courrier électronique provient d\'un fournisseur de messagerie gratuit (courrier électronique gratuit = risque plus élevé).';
$_['help_carder_email']                       = 'Si le courrier électronique est dans la base de données des courriers électroniques à haut risque.';
$_['help_high_risk_username']                 = 'Si le nom d\'utilisateurMD5 est entré dans la base de données de noms d\'utilisateur à haut risque. Renvoyé uniquement si nomutilisateurMD5 est inclus dans les entrées.';
$_['help_high_risk_password']                 = 'Si le mot de passe MD5 est entré dans la base de données de mots de passe à haut risque. Renvoyé uniquement si passwordMD5 est inclus dans les entrées.';
$_['help_bin_match']                          = 'Si le pays de la banque émettrice basé sur le numéro BIN correspond au pays de l\'adresse de facturation.';
$_['help_bin_country']                        = 'Code de pays de la banque qui a émis la carte de crédit sur la base du numéro BIN.';
$_['help_bin_name_match']                     = 'Indique si le nom de la banque émettrice correspond au nom BIN saisi. Une valeur de retour de Oui indique positivement que le titulaire de carte est en possession d\'une carte de crédit.';
$_['help_bin_name']                           = 'Nom de la banque qui a émis la carte de crédit sur la base du numéro BIN. Disponible pour environ 96% des numéros BIN.';
$_['help_bin_phone_match']                    = 'Si le numéro de téléphone du service client correspond au numéro de téléphone BIN entré. Une valeur de retour de Oui indique positivement que le titulaire de carte est en possession d\'une carte de crédit.';
$_['help_bin_phone']                          = 'Numéro de téléphone du service clientèle indiqué au dos de la carte de crédit. Disponible pour environ 75% des numéros BIN. Dans certains cas, le numéro de téléphone renvoyé peut être obsolète.';
$_['help_customer_phone_in_billing_location'] = 'Si le numéro de téléphone du client est dans le code postal de facturation. Une valeur de retour de Oui indique positivement que le numéro de téléphone indiqué appartient au titulaire de la carte. Une valeur renvoyée de Non indique que le numéro de téléphone peut se trouver dans une zone différente ou ne pas figurer dans notre base de données. NotFound est renvoyé lorsque le préfixe du numéro de téléphone est introuvable dans notre base de données. Actuellement, nous ne prenons en charge que les numéros de téléphone américains.';
$_['help_ship_forward']                       = 'Si l\'adresse de livraison est dans la base de données de courrier gouttes connus.';
$_['help_city_postal_match']                  = 'Si la ville et l\'état de facturation correspondent au code postal. Actuellement disponible pour les adresses américaines uniquement, renvoie une chaîne vide en dehors des États-Unis.';
$_['help_ship_city_postal_match']             = 'Si la ville et l\'état d\'expédition correspondent au code postal. Actuellement disponible pour les adresses américaines uniquement, renvoie une chaîne vide en dehors des États-Unis.';
$_['help_score']                              = 'Note globale de fraude basée sur les résultats énumérés ci-dessus. Il s\'agit du score de fraude original, basé sur une formule simple. Il a été remplacé par le score de risque (voir ci-dessous), mais est conservé pour des raisons de compatibilité ascendante.';
$_['help_explanation']                        = 'Une brève explication du score, détaillant les facteurs qui y ont contribué, selon notre formule. Veuillez noter que cela correspond au score, pas à l\'RiskScore.';
$_['help_risk_score']                         = 'Nouveau score de fraude représentant la probabilité estimée que la commande est une fraude, basé sur l\'analyse des transactions passées de minFraud. Nécessite une mise à niveau pour les clients qui se sont inscrits avant février 2007.';
$_['help_queries_remaining']                  = 'Nombre de requêtes restantes dans votre compte, peut être utilisé pour vous avertir lorsque vous devrez peut-être ajouter d\'autres requêtes à votre compte.';
$_['help_maxmind_id']                         = 'Identifiant unique, utilisé pour référencer les transactions lors du signalement d\'activités frauduleuses à MaxMind. Ces rapports aideront MaxMind à améliorer ses services et permettront à une fonctionnalité planifiée de personnaliser la formule de décompte des fraudes en fonction de votre historique de facturation.';
$_['help_error']                              = 'Returns an error string with a warning message or a reason why the request failed.';

// Error
$_['error_permission']                        = 'Avertissement: vous n\'êtes pas autorisé à modifier MaxMind anti-fraude!';
$_['error_key']		                          = 'Clé de licence requise!';