<?php
// Heading
$_['heading_title']    = 'Des thèmes';

// Text
$_['text_success']     = 'Succès: vous avez modifié des thèmes!';
$_['text_list']        = 'Liste thématique';

// Column
$_['column_name']      = 'Nom du thème';
$_['column_status']    = 'Statut';
$_['column_action']    = 'action';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier des thèmes!';