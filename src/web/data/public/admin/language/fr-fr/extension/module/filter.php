<?php
// Heading
$_['heading_title']    = 'Filtre';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de filtrage.!';
$_['text_edit']        = 'Module de filtre d\'édition';

// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de filtrage!';