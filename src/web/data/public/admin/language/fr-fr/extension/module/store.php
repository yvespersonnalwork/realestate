<?php

// Heading

$_['heading_title']    = 'Site Internet';



// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de site Web!';
$_['text_edit']        = 'Modifier le module de site Web';

// Entry

$_['entry_admin']      = 'Utilisateurs Admin seulement';
$_['entry_status']     = 'Statut';

// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de site Web.!';