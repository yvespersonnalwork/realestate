<?php

// Heading
$_['heading_title']    = 'Agente';

// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de l\'agent!';
$_['text_edit']        = 'Module d\'agent d\'édition';

// Lable
$_['lable_name']       = 'Nom du module';
$_['lable_limit']      = 'Limite';
$_['lable_width']      = 'Largeur';
$_['lable_height']     = 'la taille';
$_['lable_status']     = 'Statut';

// Entry
$_['entry_name']       = 'Nom du module';
$_['entry_limit']      = 'Limite';
$_['entry_width']      = 'Largeur';
$_['entry_height']     = 'la taille';
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de l\'agent!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères!';
$_['error_width']      = 'Largeur requise!';
$_['error_height']     = 'Hauteur requise!';