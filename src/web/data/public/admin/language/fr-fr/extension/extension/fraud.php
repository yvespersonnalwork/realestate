<?php
// Heading
$_['heading_title']    = 'Anti fraude';

// Text
$_['text_success']     = 'Succès: vous avez modifié la protection anti-fraude!';
$_['text_list']        = 'Liste anti-fraude';

// Column
$_['column_name']      = 'Nom anti-fraude';
$_['column_status']    = 'Statut';
$_['column_action']    = 'action';

// Error
$_['error_permission'] = 'Attention: vous n\'avez pas la permission de modifier anti-fraude!';