<?php
// Heading
$_['heading_title']     = 'Événements';

// Text
$_['text_success']      = 'Succès: vous avez modifié des événements!';
$_['text_list']         = 'Liste des événements';
$_['text_event']        = 'Les événements sont utilisés par les extensions pour remplacer les fonctionnalités par défaut de votre magasin. Si vous avez des problèmes, vous pouvez désactiver ou activer les événements ici.';

// Column
$_['column_code']       = 'Code de l\'événement';
$_['column_trigger']    = 'Déclencheur';
$_['column_action']     = 'action';
$_['column_status']     = 'Statut';
$_['column_date_added'] = 'date ajoutée';
$_['column_action']     = 'action';

// Error
$_['error_permission']  = 'Avertissement: vous n\'êtes pas autorisé à modifier des extensions!';