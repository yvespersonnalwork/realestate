<?php
// Heading
$_['heading_title']    = 'Agents à gauche';

// Text
$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module Agents!';
$_['text_edit']        = 'Module de modification des agents';

// Entry
$_['entry_status']     = 'Statut';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module Agents!';
