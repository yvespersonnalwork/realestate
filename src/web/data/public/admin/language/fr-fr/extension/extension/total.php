<?php
// Heading
$_['heading_title']     = 'Total des commandes';

// Text
$_['text_success']      = 'Succès: vous avez modifié les totaux!';
$_['text_list']         = 'Liste de commande totale';

// Column
$_['column_name']       = 'Total des commandes';
$_['column_status']     = 'Statut';
$_['column_sort_order'] = 'Ordre de tri';
$_['column_action']     = 'action';

// Error
$_['error_permission']  = 'Avertissement: vous n\'êtes pas autorisé à modifier les totaux!';