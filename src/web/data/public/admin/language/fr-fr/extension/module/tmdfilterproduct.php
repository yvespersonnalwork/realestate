<?php
// Heading
$_['heading_title']    		= 'Produit de filtre de blog TMD<b>(TMD)</b>';

// Text
$_['text_extension']        = 'Les extensions';
$_['text_success']     		= 'Succès: vous avez modifié le module fliterproduct!';
$_['text_edit']        		= 'Edit fliterproduct Module';
$_['text_tab']         		= 'Editer le module fliterproduct';
$_['text_vertcal']     		= 'Verticale / Ordinaire';
$_['text_normal']      		= 'Ordinaire';
$_['text_crousal']     		= 'Verticale+Carrousel';
$_['text_tabcrousal']  		= 'Languette+Carrousel';

// Entry
$_['entry_name']       		= 'Nom du module';
$_['entry_layouttype'] 		= 'Type de disposition';
$_['entry_crousaltype']		= 'Type de carrousel';
$_['entry_limit']      		= 'Limite';
$_['entry_width']      		= 'Largeur';
$_['entry_height']     		= 'la taille';
$_['entry_status']     		= 'Statut';
$_['entry_moduletitle']     = 'Titre de l\'en-tête du module';
$_['entry_title']       	= 'Titre du module';
$_['entry_product']     	= 'Produit';

// Help


$_['tab_recentpost']     = 'Post récent Module';
$_['tab_popular']     = 'Module populaire';
$_['tab_comment']     = 'Commentaire';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module fliterproduct!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères!';
$_['error_width']      = 'Largeur requise!';
$_['error_height']     = 'Hauteur requise!';