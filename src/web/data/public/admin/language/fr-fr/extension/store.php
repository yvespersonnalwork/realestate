<?php
// Heading
$_['heading_title']    = 'Magasin d\'extension';

// Text
$_['text_success']     = 'Succès: vous avez modifié des extensions!';
$_['text_list']        = 'Liste d\'extension';
$_['text_license']     = 'Licence';
$_['text_free']        = 'Libre';
$_['text_commercial']  = 'Commerciale';
$_['text_category']    = 'toutes catégories';
$_['text_theme']       = 'Des thèmes';
$_['text_payment']     = 'Passerelles de paiement';
$_['text_shipping']    = 'méthodes de livraison';
$_['text_module']      = 'Modules';
$_['text_total']       = 'Total des commandes';
$_['text_feed']        = 'Flux de produits';
$_['text_report']      = 'Rapports';
$_['text_other']       = 'Autre';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier des extensions!';