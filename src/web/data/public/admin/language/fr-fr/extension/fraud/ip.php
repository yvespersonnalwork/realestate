<?php
// Heading
$_['heading_title']      = 'IP anti-fraude';

// Text
$_['text_extension']     = 'Les extensions';
$_['text_success']       = 'Succès: vous avez modifié l\'IP anti-fraude.!';
$_['text_edit']          = 'Modifier l\'IP anti-fraude';
$_['text_ip_add']        = 'Ajouter une adresse IP';
$_['text_ip_list']       = 'Liste d\'adresses IP de fraude';

// Column
$_['column_ip']          = 'IP';
$_['column_total']       = 'Total des comptes';
$_['column_date_added']  = 'date ajoutée';
$_['column_action']      = 'action';

// Entry
$_['entry_ip']           = 'IP';
$_['entry_status']       = 'Statut';
$_['entry_order_status'] = 'Statut de la commande';

// Help
$_['help_order_status']  = 'Les clients qui ont une adresse IP interdite sur leurs comptes se verront attribuer ce statut de commande et ne seront pas autorisés à atteindre le statut complet automatiquement.';

// Error
$_['error_permission']   = 'Avertissement: vous n’avez pas l’autorisation de modifier l’anti-fraude IP!';
