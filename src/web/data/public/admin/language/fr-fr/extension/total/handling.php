<?php
// Heading
$_['heading_title']    = 'Frais de gestion';

// Text
$_['text_total']       = 'Total des commandes';
$_['text_success']     = 'Succès: vous avez modifié le total des frais de traitement!';
$_['text_edit']        = 'Modifier le total des frais de traitement';

// Entry
$_['entry_total']      = 'Total de la commande';
$_['entry_fee']        = 'Frais';
$_['entry_tax_class']  = 'Classe d\'impôt';
$_['entry_status']     = 'Statut';
$_['entry_sort_order'] = 'Ordre de tri';

// Help
$_['help_total']       = 'Le total de paiement que la commande doit atteindre avant que ce total ne devienne actif.';

// Error
$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le total des frais de traitement.!';