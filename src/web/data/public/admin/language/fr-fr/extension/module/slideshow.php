<?php

// Heading

$_['heading_title']    = 'Diaporama';


// Text

$_['text_extension']   = 'Les extensions';
$_['text_success']     = 'Succès: vous avez modifié le module de diaporama!';
$_['text_edit']        = 'Module d\'édition de diaporama';

/// Lable text
$_['text_name']       = 'Nom du module';
$_['text_banner']     = 'Bannière';
$_['text_width']      = 'Largeur';
$_['text_height']     = 'la taille';
$_['text_status']     = 'Statut';


// Entry
$_['entry_named']      = 'Nom du module';
$_['entry_banner']     = 'Bannière';
$_['entry_width']      = 'Largeur';
$_['entry_height']     = 'la taille';
$_['entry_status']     = 'Statut';

// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier le module de diaporama.!';
$_['error_name']       = 'Le nom du module doit comporter entre 3 et 64 caractères.!';
$_['error_width']      = 'Largeur requise!';
$_['error_height']     = 'Hauteur requise!';