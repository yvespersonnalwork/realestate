<?php
// Heading
$_['heading_title']                    = 'Thème de site Web par défaut';

// Text
$_['text_theme']                       = 'Des thèmes';
$_['text_success']                     = 'Succès: vous avez modifié le thème de site Web par défaut.!';
$_['text_edit']                        = 'Modifier le thème de site Web par défaut';
$_['text_general']                     = 'Général';
$_['text_product']                     = 'Des produits';
$_['text_image']                       = 'Images';

// Entry
$_['entry_directory']                  = 'Répertoire thématique';
$_['entry_status']                     = 'Statut';
$_['entry_product_limit']              = 'Éléments par défaut par page';
$_['entry_product_description_length'] = 'Liste Description Limite';
$_['entry_image_category']             = 'Catégorie Taille de l\'image (L x H))';
$_['entry_image_thumb']                = 'Image du produit Taille du pouce (lx H)';
$_['entry_image_popup']                = 'Image de produit Popup Taille (W x H)';
$_['entry_image_product']              = 'Taille de l\'image de produit (W x H)';
$_['entry_image_additional']           = 'Taille de l’image produit supplémentaire (L x H)';
$_['entry_image_related']              = 'Image du produit connexe Taille (W x H)';
$_['entry_image_compare']              = 'Comparer la taille de l\'image (L x H)';
$_['entry_image_wishlist']             = 'Liste de souhaits Taille de l\'image (L x H)';
$_['entry_image_cart']                 = 'Panier Taille de l\'image (W x H)';
$_['entry_image_location']             = 'Taille de l\'image du site Web (L x H)';
$_['entry_width']                      = 'Largeur';
$_['entry_height']                     = 'la taille';

// Help
$_['help_directory'] 	               = 'Ce champ est uniquement destiné à permettre aux thèmes plus anciens d’être compatibles avec le nouveau système de thèmes. Vous pouvez définir le répertoire de thèmes à utiliser pour les paramètres de taille d\'image définis ici.';
$_['help_product_limit'] 	           = 'Détermine le nombre d’articles du catalogue affichés par page (produits, catégories, etc.)';
$_['help_product_description_length']  = 'Dans la liste, brève description du nombre de caractères (catégories, fonctions spéciales, etc.)';

// Error
$_['error_permission']                 = 'Avertissement: vous n\'êtes pas autorisé à modifier le thème de magasin par défaut.!';
$_['error_limit']       	           = 'Limite de produit requise!';
$_['error_image_thumb']                = 'Image du produit Dimensions du pouce Dimensions requises!';
$_['error_image_popup']                = 'Product Image Popup Taille Dimensions requises!';
$_['error_image_product']              = 'Liste de produit Taille Dimensions requises!';
$_['error_image_category']             = 'Catégorie Liste Taille Dimensions requises!';
$_['error_image_additional']           = 'Image produit supplémentaire Taille Dimensions requises!';
$_['error_image_related']              = 'Image du produit connexe Taille dimensions requises!';
$_['error_image_compare']              = 'Comparer Image Taille Dimensions requises!';
$_['error_image_wishlist']             = 'Liste de souhaits Image Taille dimensions requises!';
$_['error_image_cart']                 = 'Panier Image Taille Dimensions requises!';
$_['error_image_location']             = 'Image du site web Dimensions requises!';