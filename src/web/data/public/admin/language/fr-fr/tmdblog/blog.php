<?php
// Heading
$_['heading_title']          = 'Blog';

// Text
$_['text_success']           = 'Succès: vous avez modifié le blog!';
$_['text_list']              = 'Liste de blogs';
$_['text_add']               = 'Ajouter un blog';
$_['text_edit']              = 'Modifier le blog';
$_['text_default']           = 'Défaut';
$_['entry_username']         = 'Nom d\'utilisateur';
$_['entry_blogcoment']       = 'Afficher les commentaires';
$_['tab_comment']            = 'commentaires';
$_['column_customer']        = 'Client';
$_['column_comment']         = 'Client';
$_['text_no_comment']        = 'Sans commentaires ';
$_['text_dash']                     = 'Tableau de bord';
$_['text_blog']                    = 'Les blogs';
$_['text_cate']                   = 'Catégorie';
$_['text_comm']                    = 'Client';
$_['text_sett']                    = 'Paramètres';
$_['text_addmodule']                    = 'Ajouter un module';

// Column
$_['column_date_added']      = 'date ajoutée';
$_['entry_date_added']       = 'date ajoutée';
$_['column_status']          = 'Statut';
$_['column_image']           = 'Image';
$_['column_title']           = 'Nom du blog';
$_['column_sort_order']	     = 'Ordre de tri';
$_['column_action']          = 'action';
$_['text_enabled']           = 'Activée';
$_['text_disabled']          = 'désactivé';
$_['column_name']            = 'Nom du blog';
$_['column_view']            = 'Les vues';


// Entry
$_['entry_name']            = 'Nom du blog';
$_['entry_description']      = 'La description';
$_['entry_store']            = 'Magasins';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO Mot clé';
$_['entry_bottom']           = 'Bas';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_layout']           = 'Remplacement de mise en page';
$_['entry_image']            = 'Image';
$_['entry_tag']              = 'Mots clés';
$_['text_none']              = '--Aucune--';
$_['entry_tmdblogcategory']  = 'Tmd BlogCatégorie';
$_['help_tmdblogcategory']   = 'Saisie automatique';

// Help
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..';
$_['help_bottom']            = 'Afficher en bas de page.';

// Error
$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier l\'article!';
$_['error_title']            = 'Le nom du blog doit comporter entre 3 et 64 caractères.!';
$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'Mot clé SEO déjà utilisé!';
$_['error_account']          = 'Avertissement: cette page d\'article ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de magasin.!';
$_['error_checkout']         = 'Avertissement: cette page d\'article ne peut pas être supprimée, car elle est actuellement affectée en tant que conditions de paiement en magasin.!';
$_['error_affiliate']        = 'Avertissement: cette page d\'article ne peut pas être supprimée car elle est actuellement affectée aux termes d\'affiliation du magasin.!';
$_['error_return']           = 'Avertissement: cette page d\'article ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour en magasin.!';
$_['error_store']            = 'Avertissement: Cette page artistique ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';