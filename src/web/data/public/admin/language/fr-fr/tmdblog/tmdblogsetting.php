<?php
// Heading
$_['heading_title']          = 'Paramétrage du blog';

// Text
$_['text_success']           = 'Succès: vous avez modifié des catégories!';
$_['text_list']              = 'Tmd Blog Setting List';
$_['text_add']               = 'Ajouter un paramètre de blog Tmd';
$_['text_edit']              = 'Modifier les paramètres du blog Tmd';
$_['text_default']           = 'Défaut';
$_['text_searchpage']        = 'Page de recherche';
$_['text_categorypage']      = 'Catégorie de blog';
$_['text_relatedblog']       = 'Page connexe';
$_['text_blogpage']          = 'Page de blog';
$_['text_listpage']          = 'Page de liste';
$_['text_page']              = 'Page';

// Column
$_['column_name']            = 'Titre du Blog';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_action']          = 'action';

// Entry
$_['entry_commentlimit']     = 'Limite';
$_['entry_name']             = 'Titre du Blog';
$_['entry_description']      = 'La description';
$_['entry_meta_title'] 	     = 'Titre';
$_['entry_meta_keyword'] 	 = 'Mots clés';
$_['entry_meta_description'] = 'La description';
$_['entry_keyword']          = 'SEO Mot clé';
$_['entry_parent']           = 'Parente';
$_['entry_filter']           = 'Les filtres';
$_['entry_store']            = 'Magasins';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Haut';
$_['entry_column']           = 'Colonnes';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_titlesize']        = 'Titre du blog';
$_['entry_titlecolor']       = 'Titre du blog Couleur du texte';
$_['entry_thumbimg']         = 'Commentaire Banner Thumb Size';
$_['entry_comntbanner']      = 'Taille de la bannière de commentaire';
$_['text_blogcommentsglobal']= 'Globaly Afficher / Masquer les commentaires du blog';
$_['entry_blogcomments']     = 'Commentaires du blog';
$_['help_blogcomment']       = 'Sélectionnez l\'option pour masquer et afficher tous les commentaires de blogs';

$_['entry_article']          = 'Blog';
$_['entry_google']           = 'Partage Google';
$_['entry_pinterest']        = 'Partager Pinterest';
$_['entry_twitter']          = 'Part Twitter';
$_['entry_facebook']         = 'Partage facebook';
$_['entry_feedbackrow']      = 'Boîte de commentaires';
$_['entry_descp']            = 'La description';
$_['entry_articleimg']       = 'Image du blog';
$_['entry_width']            = 'Largeur';
$_['entry_height']           = 'la taille';

$_['entry_descptextcolor']      = 'Description Texte Couleur';
$_['entry_articletextcolor']    = 'Couleur du texte du blog';
$_['entry_postboxbgcolor']      = 'Couleur de fond de la boîte de blog';
$_['entry_containerbgcolor']    = 'Couleur de fond du conteneur de blog';
$_['entry_titlebgcolor']     	= 'Couleur du thème du blog';
$_['entry_themehovercolor']     = 'Blog Thème Hover Couleur';
$_['entry_titletextcolor']      = 'Couleur du texte d\'en-tête';
$_['entry_blogtextcolor']       = 'Titre Survol Couleur du texte';
$_['entry_bloglayout']    	    = 'Disposition';
$_['entry_blogperpage']      	= 'Page';
$_['entry_blogperrow']    	    = 'Rangée';
$_['entry_dispalytitle']    	= 'Titre';
$_['entry_dispalydes']       	= 'La description';
$_['entry_blogthumbsize']    	= 'Taille du pouce ';
$_['entry_dispalyblogtimg']    	= 'Image';
$_['entry_keyword']          = 'SEO Mot clé';

$_['help_meta_title'] 	    	= 'Titre de la balise méta';
$_['help_meta_keyword'] 		= 'Mots-clés méta';
$_['help_meta_description'] 	= 'Meta Tag Description';
$_['help_blogthumbsize']   		= 'Blog Blog Thumb <br/> Taille(W*H)';

$_['help_article']           	= 'Activer ou désactiver le blog';
$_['help_google']           	= 'Activer ou désactiver le partage Google';
$_['help_pinterest']            = 'Activer ou désactiver le partage Pinterest';
$_['help_twitter']           	= 'Activer ou désactiver Twitter';
$_['help_facebook']           	= 'Activer ou désactiver Facebook';
$_['help_feedbackrow']          = 'Activer ou désactiver la zone de commentaires';
$_['help_descp']           		= 'Activer ou désactiver Description';
$_['help_articleimg']           = 'Activer ou désactiver l\'image du blog';
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..';

$_['tab_setting']           	= 'Réglage';
$_['tab_color']             	= 'Ajuster la couleur';
$_['tab_main']             		= 'Général';
$_['tab_blogseting']            = 'Paramétrage du blog';
$_['tab_support']               = 'Soutien';

// Help
$_['help_filter']            	= '(Saisie automatique)';
$_['help_keyword']           	= 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..';
$_['help_top']               	= 'Afficher dans la barre de menu supérieure. Ne fonctionne que pour les catégories parentes les plus populaires.';
$_['help_column']            	= 'Nombre de colonnes à utiliser pour les 3 dernières catégories. Ne fonctionne que pour les catégories parentes les plus populaires.';

// Error
$_['error_warning']          	= 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       	= 'Avertissement: vous n\'êtes pas autorisé à modifier des catégories.!';
$_['error_name']             	= 'Le titre du blog doit comporter entre 2 et 32 caractères.!';
$_['error_meta_title']       	= 'Blog Meta Title doit comporter plus de 3 caractères et moins de 255 caractères.!';
$_['error_keyword']          = 'Mot clé SEO déjà utilisé!';