<?php
// Heading
$_['heading_title']                = 'Commentaires récents';

// Column
$_['column_sr_no']  	 = 'Sr.No.';
$_['column_post']  		 = 'Poster';
$_['column_author']      = 'Auteure';
$_['column_status']     = 'Statut';
$_['column_action']     = 'action';
