<?php
// Heading
$_['heading_title']          = 'Catégorie de blog';

// Text
$_['text_success']           = 'Succès: vous avez modifié tmdblogcategory!';
$_['text_list']              = 'Liste des catégories de blogs TMD';
$_['text_add']               = 'Ajouter une catégorie de blog TMD';
$_['text_edit']              = 'Modifier la catégorie de blog TMD';
$_['text_default']           = 'DefaultDéfaut';

// Column
$_['column_date_added']      = 'date ajoutée';
$_['entry_date_added']       = 'date ajoutée';
$_['column_status']          = 'Statut';
$_['column_image']           = 'Image';
$_['column_title']           = 'Titre de la catégorie de blog TMD';
$_['column_sort_order']	     = 'Ordre de tri';
$_['column_action']          = 'action';
$_['text_enabled']           = 'Activée';
$_['text_disabled']          = 'désactivé';
$_['column_name']            = 'Nom de la catégorie du blog';
$_['column_count']           = 'Compter';


// Entry
$_['entry_name']            = 'Nom de la catégorie du blog';
$_['entry_description']      = 'La description';
$_['entry_store']            = 'Magasins';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO Mot clé';
$_['entry_bottom']           = 'Bas';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_layout']           = 'Remplacement de mise en page';
$_['entry_image']            = 'Image';
$_['entry_tag']              = 'Mots clés';
$_['text_none']              = '--Aucune--';
$_['entry_parent']           = 'Parente';

// Help
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..';
$_['help_bottom']            = 'Afficher en bas de page.';

// Error
$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier tmdblogcategory!';
$_['error_title']            = 'Le titre de la catégorie de blog TMD doit comporter entre 3 et 64 caractères!';
$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'Mot clé SEO déjà utilisé!';
$_['error_account']          = 'Avertissement: Cette page tmdblogcategory ne peut pas être supprimée car elle est actuellement affectée en tant que termes du compte de magasin.!';
$_['error_checkout']         = 'Avertissement: Cette page tmdblogcategory ne peut pas être supprimée car elle est actuellement affectée en tant que conditions de paiement en magasin.!';
$_['error_affiliate']        = 'Avertissement: Cette page tmdblogcategory ne peut pas être supprimée car elle est actuellement affectée en tant que conditions d’affiliation.!';
$_['error_return']           = 'Avertissement: Cette page tmdblogcategory ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour du magasin.!';
$_['error_store']            = 'Avertissement: Cette page tmdblogcategory ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';