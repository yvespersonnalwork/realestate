<?php
// Heading
$_['heading_title']      = 'Blogs récents';

// Column
$_['column_totalcoment'] = 'Commentaire total';
$_['column_sr_no']  	 = 'Identifiant de blog';
$_['column_post']  		 = 'Blog';
$_['column_author']      = 'Client';
$_['column_status']      = 'Statut';
$_['column_action']      = 'action';

