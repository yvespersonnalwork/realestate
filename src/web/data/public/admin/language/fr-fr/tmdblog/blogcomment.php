<?php
// Heading
$_['heading_title']          = 'Commentaire du blog';

// Text
$_['text_success']           = 'Succès: vous avez modifié le commentaire de blog.!';
$_['text_list']              = 'Liste des commentaires du blog';
$_['text_add']               = 'Ajouter un commentaire de blog';
$_['text_edit']              = 'Modifier le commentaire du blog';
$_['text_default']           = 'Défaut';
$_['text_no_comment']        = 'Sans commentaires ';
$_['text_dash']              = 'Tableau de bord';
$_['text_cate']              = 'Catégorie';
$_['text_comm']              = 'Commentaire';
$_['text_sett']              = 'Paramètres';
$_['text_addmodule']         = 'Ajouter un module';
$_['text_none']              = '--Aucune--';
$_['text_enabled']           = 'Activée';
$_['text_disabled']          = 'désactivé';

// Tab
$_['tab_comment']            = 'commentaires';

// Column

$_['column_date_added']      = 'date ajoutée';
$_['column_status']          = 'Statut';
$_['column_image']           = 'Image';
$_['column_title']           = 'Nom du commentaire de blog';
$_['column_sort_order']	     = 'Ordre de tri';
$_['column_action']          = 'action';

$_['column_name']            = 'Nom du client';
$_['column_view']            = 'Les vues';
$_['column_customer']        = 'Client';
$_['column_comment']         = 'Commentaire';


// Entry
$_['entry_name']             = 'Nom du commentaire de blog';
$_['entry_customername']     = 'Client';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_image']            = 'Image';
$_['entry_tmdblog']          = 'Sélectionner un blog';
$_['entry_date_added']       = 'date ajoutée';
$_['entry_comment']          = 'Commentaire';


// Help
$_['help_tmdblog']           = 'Saisie automatique';
$_['help_bottom']            = 'Afficher en bas de page.';

// Error
$_['error_warning']          = 'Attention: Veuillez vérifier le formulaire avec soin pour déceler les erreurs!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier l\'article!';
$_['error_title']            = 'Le nom du commentaire de blog doit comporter entre 3 et 64 caractères.!';
$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'Mot clé SEO déjà utilisé!';
$_['error_account']          = 'Avertissement: cette page d\'article ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de magasin.!';
$_['error_checkout']         = 'Avertissement: cette page d\'article ne peut pas être supprimée, car elle est actuellement affectée en tant que conditions de paiement en magasin.!';
$_['error_affiliate']        = 'Avertissement: cette page d\'article ne peut pas être supprimée car elle est actuellement affectée aux termes d\'affiliation du magasin.!';
$_['error_return']           = 'Avertissement: cette page d\'article ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour en magasin.!';
$_['error_store']            = 'Avertissement: Cette page artistique ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!';