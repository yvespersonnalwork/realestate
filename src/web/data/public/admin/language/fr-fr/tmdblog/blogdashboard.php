<?php
// Heading
$_['heading_title']                = 'BlogDashboard';

// Text
$_['text_order_total']             = 'Total des commandes';
$_['text_customer_total']          = 'Total clients';
$_['text_sale_total']              = 'Ventes totales';
$_['text_online_total']            = 'Personnes en ligne';
$_['text_map']                     = 'Carte du monde';
$_['text_sale']                    = 'Analyse des ventes';
$_['text_activity']                = 'Activité récente';
$_['text_recent']                  = 'Dernières commandes';
$_['text_order']                   = 'Ordres';
$_['text_customer']                = 'Les clients';
$_['text_day']                     = 'Aujourd\'hui';
$_['text_week']                    = 'La semaine';
$_['text_month']                   = 'Mois';
$_['text_year']                    = 'Année';
$_['text_view']                    = 'Voir plus...';
$_['text_dash']                    = 'Tableau de bord';
$_['text_blog']                    = 'Blogs';
$_['text_cate']                    = 'Catégorie';
$_['text_comm']                    = 'Commentaire';
$_['text_sett']                    = 'Paramètres';
$_['text_addmodule']               = 'Ajouter un module';

// Error
$_['error_install']                = 'Avertissement: le dossier d\'installation existe toujours et doit être supprimé pour des raisons de sécurité!';