<?php

// Heading

$_['heading_title']     = 'Langues';
// Text

$_['text_success']      = 'Succès: vous avez modifié les langues!';
$_['text_list']         = 'Liste de langue';
$_['text_add']          = 'Ajouter une langue';
$_['text_edit']         = 'Modifier la langue';


//label name 

$_['text_name']        = 'Nom de la langue';
$_['text_code']        = 'Code';
$_['text_locale']      = 'Lieu';
$_['text_status']      = 'Statut';
$_['text_sort_order']  = 'Ordre de tri';



// Column

$_['column_name']       = 'Nom de la langue';
$_['column_code']       = 'Code';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry

$_['entry_name']        = 'Nom de la langue';
$_['entry_code']        = 'Code';
$_['entry_locale']      = 'Lieu';
$_['entry_status']      = 'Statut';
$_['entry_sort_order']  = 'Ordre de tri';
$_['text_disabled']           = 'désactivé';
$_['text_enabled']           = 'Activée';

// btn 
$_['button_save']               = 'sauver';
$_['button_cancel']             = 'Annuler';

// Help

$_['help_locale']       = 'Exemple: en_US.UTF-8, en_US, en-gb, en_gb, anglais';
$_['help_status']       = 'Masquer / afficher dans la liste déroulante des langues';

// Error

$_['error_permission']  = "Avertissement: vous n'êtes pas autorisé à modifier les langues!";
$_['error_exists']      = 'Attention: vous avez ajouté avant la langue!';
$_['error_name']        = 'Le nom de la langue doit comporter entre 3 et 32 caractères!';
$_['error_code']        = 'Le code de langue doit comporter au moins 2 caractères.!';
$_['error_locale']      = 'Locale requis!';
$_['error_default']     = 'Avertissement: Cette langue ne peut pas être supprimée car elle est actuellement attribuée comme langue de stockage par défaut!';
$_['error_admin']       = 'Avertissement: Cette langue ne peut pas être supprimée car elle est actuellement attribuée à la langue d\'administration!';
$_['error_store']       = 'Avertissement: Cette langue ne peut pas être supprimée car elle est actuellement affectée aux magasins% s.!';
$_['error_order']       = 'Attention: cette langue ne peut pas être supprimée car elle est actuellement affectée aux commandes% s!';