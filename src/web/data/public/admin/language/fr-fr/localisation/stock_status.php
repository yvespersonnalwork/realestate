<?php

// Heading

$_['heading_title']    = 'État des stocks';
// Text
$_['text_success']     = 'Succès: vous avez modifié les statuts de stock!';
$_['text_list']        = "Liste d'état des stocks";
$_['text_add']         = "Ajouter l'état de stock";
$_['text_edit']        = "Modifier l'état du stock";

// Column
$_['column_name']      = 'Stock Statut Nom';
$_['column_action']    = 'action';

// Entry
$_['entry_name']       = 'Stock Statut Nom';
// Error
$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier les statuts des stocks.!";
$_['error_name']       = 'Stock Le nom doit comporter entre 3 et 32 caractères.!';
$_['error_product']    = 'Avertissement: Cet état de stock ne peut pas être supprimé car il est actuellement affecté aux produits% s.!';