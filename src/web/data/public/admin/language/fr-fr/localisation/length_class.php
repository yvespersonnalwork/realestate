<?php

// Heading

$_['heading_title']    = 'Classes de longueur';

// Text

$_['text_success']     = 'Succès: vous avez modifié les classes de longueur!';
$_['text_list']        = 'Liste des classes de longueur';
$_['text_add']         = 'Ajouter une classe de longueur';
$_['text_edit']        = 'Modifier la classe de longueur';

// Column

$_['column_title']     = 'Titre de longueur';
$_['column_unit']      = 'Unité de longueur';
$_['column_value']     = 'Valeur';
$_['column_action']    = 'action';


// Entry

$_['entry_title']      = 'Titre de longueur';
$_['entry_unit']       = 'Unité de longueur';
$_['entry_value']      = 'Valeur';

// Help
$_['help_value']       = "Définir à 1.00000 s'il s'agit de la longueur par défaut.";
// Error
$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier les classes de longueur!";
$_['error_title']      = 'Longueur Le titre doit comporter entre 3 et 32 caractères.!';
$_['error_unit']       = 'Longueur L\'unité doit comporter entre 1 et 4 caractères.!';
$_['error_default']    = 'Avertissement: Cette classe de longueur ne peut pas être supprimée car elle est actuellement attribuée à la classe de longueur de magasin par défaut.!';
$_['error_product']    = 'Avertissement: Cette classe de longueur ne peut pas être supprimée car elle est actuellement affectée aux produits% s.!';