<?php

// Heading

$_['heading_title']    = 'Catégories de poids';



// Text

$_['text_success']     = 'Succès: vous avez modifié les classes de poids!';
$_['text_list']        = 'Liste des classes de poids';
$_['text_add']         = 'Ajouter une classe de poids';
$_['text_edit']        = 'Modifier la classe de poids';
// Column

$_['column_title']     = 'Titre du poids';
$_['column_unit']      = 'Unité de poids';
$_['column_value']     = 'Poids UnitValue';
$_['column_action']    = 'action';

// Entry

$_['entry_title']      = 'Titre du poids';
$_['entry_unit']       = 'Unité de poids';
$_['entry_value']      = 'Valeur';

// Help

$_['help_value']       = 'Réglez à 1.00000 s\'il s\'agit de votre poids par défaut.';

// Error

$_['error_permission'] = "Attention: vous n'êtes pas autorisé à modifier les classes de poids!";
$_['error_title']      = 'Le titre du poids doit comporter entre 3 et 32 caractères.!';

$_['error_unit']       = "L'unité de poids doit comporter entre 1 et 4 caractères!";

$_['error_default']    = 'Avertissement: Cette classe de pondération ne peut pas être supprimée car elle est actuellement affectée comme classe de pondération de magasin par défaut.!';

$_['error_product']    = 'Avertissement: Cette classe de poids ne peut pas être supprimée car elle est actuellement affectée aux produits% s.!';