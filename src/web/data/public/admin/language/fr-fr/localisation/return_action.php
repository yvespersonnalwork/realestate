<?php

// Heading

$_['heading_title']    = 'Actions de retour';

// Text

$_['text_success']     = 'Succès: vous avez modifié les actions de retour!';
$_['text_list']        = 'Retour de la liste d\'actions';
$_['text_add']         = 'Ajouter une action de retour';
$_['text_edit']        = 'Modifier l\'action de retour';
// Column
$_['column_name']      = "Nom de l'action de retour";
$_['column_action']    = 'action';

// Entry
$_['entry_name']       = "Nom de l'action de retour";
// Error
$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier les actions de retour!";
$_['error_name']       = "Le nom de l'action de retour doit comporter entre 3 et 64 caractères.!";
$_['error_return']     = 'Avertissement: Cette action de retour ne peut pas être supprimée car elle est actuellement affectée aux% s produits retournés.!';