<?php

// Heading

$_['heading_title']        = 'Monnaies';
// Text

$_['text_success']         = 'Succès: vous avez modifié les devises!';
$_['text_list']            = 'Liste des devises';
$_['text_add']             = 'Ajouter une devise';
$_['text_edit']            = 'Modifier la devise';
$_['text_iso']             = 'Vous pouvez trouver une liste complète des codes de devise et des paramètres ISO <a href="http://www.xe.com/iso4217.php" target="_blank">here</a>.';
// Column

$_['column_title']         = 'Titre de la monnaie';
$_['column_code']          = 'Code';
$_['column_value']         = 'Valeur';
$_['column_date_modified'] = 'Dernière mise à jour';
$_['column_action']        = 'action';


///label name 

$_['text_title']          = 'Titre de la monnaie';
$_['text_code']           = 'Code';
$_['text_value']          = 'Valeur';
$_['text_symbol_left']    = 'Symbole gauche';
$_['text_symbol_right']   = 'Symbole Droite';
$_['text_decimal_place']  = 'Décimales';
$_['text_status']         = 'Statut';

// Entry input name 

$_['entry_title']          = 'Titre de la monnaie';
$_['entry_code']           = 'Code';
$_['entry_value']          = 'Valeur';
$_['entry_symbol_left']    = 'Symbole gauche';
$_['entry_symbol_right']   = 'Symbole Droite';
$_['entry_decimal_place']  = 'Décimales';
$_['entry_status']         = 'Statut';

// Help

$_['help_code']            = 'Ne changez pas s\'il s\'agit de votre devise par défaut.';
$_['help_value']           = 'Réglez à 1.00000 s\'il s\'agit de votre devise par défaut.';

// btn 
$_['button_save']         = 'sauver';
$_['button_cancel']         = 'Annuler';




// Error

$_['error_permission']     = "Avertissement: vous n'êtes pas autorisé à modifier les devises!";

$_['error_title']          = 'Le titre de la devise doit comporter entre 3 et 32 caractères.!';

$_['error_code']           = 'Le code de devise doit contenir 3 caractères!';

$_['error_default']        = 'Avertissement: Cette devise ne peut pas être supprimée car elle est actuellement affectée comme devise de magasin par défaut.!';

$_['error_store']          = 'Attention: This legs is can be to be a supprimé% s.!';

$_['error_order']          = 'Attention: Cette devise ne peut pas être supprimée car elle est actuellement affectée aux commandes% s!';