<?php

// Heading

$_['heading_title']    = 'Emplacements de site Web';



// Text

$_['text_success']     = "Succès: vous avez modifié l'emplacement des sites Web.!";
$_['text_list']        = "Liste d'emplacement de site Web";
$_['text_add']         = "Ajouter l'emplacement du site";
$_['text_edit']        = "Modifier l'emplacement du site";
$_['text_default']     = 'Défaut';
$_['text_time']        = 'Horaires d\'ouverture';
$_['text_geocode']     = "Le géocode n'a pas abouti pour la raison suivante:";
// Column

$_['column_name']      = 'Nom du site';
$_['column_address']   = 'Adresse';
$_['column_action']    = 'action';

// Entry

$_['entry_name']       = 'Nom du site';
$_['entry_address']    = 'Adresse';
$_['entry_geocode']    = 'Géocodage';
$_['entry_telephone']  = 'Téléphone';
$_['entry_fax']        = 'Fax';
$_['entry_image']      = 'Image';
$_['entry_open']       = "Horaires d'ouverture";
$_['entry_comment']    = 'Commentaire';
// Help

$_['help_geocode']     = 'Veuillez saisir manuellement le géocode de votre site Web.';
$_['help_open']        = "Renseignez les heures d'ouverture de vos magasins.";
$_['help_comment']     = "Ce champ concerne les notes spéciales que vous souhaitez informer le client, c’est-à-dire que le magasin n’accepte pas les chèques.";
// Error

$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier les emplacements des magasins.!";
$_['error_name']       = 'Le nom du magasin doit comporter entre 3 et 32 caractères.!';
$_['error_address']    = "L'adresse doit comporter entre 3 et 128 caractères!";
$_['error_telephone']  = 'Le téléphone doit comporter entre 3 et 32 caractères.!';