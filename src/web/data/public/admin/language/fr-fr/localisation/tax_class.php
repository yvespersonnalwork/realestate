<?php

// Heading

$_['heading_title']     = "Classes d'impôt";

// Text

$_['text_success']      = "Succès: vous avez modifié les classes d'imposition!";
$_['text_list']         = "Liste des classes d'impôt";
$_['text_add']          = 'Ajouter une classe de taxe';
$_['text_edit']         = 'Modifier la classe de taxe';
$_['text_shipping']     = 'Adresse de livraison';
$_['text_payment']      = 'adresse de facturation';
$_['text_store']        = 'Adresse du site Web';
// Column

$_['column_title']      = "Titre de la classe d'impôt";
$_['column_action']     = 'action';

// Entry

$_['entry_title']       = "Titre de la classe d'impôt";
$_['entry_description'] = 'La description';
$_['entry_rate']        = "Taux d'imposition";
$_['entry_based']       = 'Basé sur';
$_['entry_geo_zone']    = 'Zone géographique';
$_['entry_priority']    = 'Priorité';

// Error

$_['error_permission']  = "Avertissement: vous n'êtes pas autorisé à modifier les classes de taxe!";
$_['error_title']       = 'Le titre de la classe de taxe doit comporter entre 3 et 32 caractères.!';
$_['error_description'] = 'La description doit comporter entre 3 et 255 caractères!';
$_['error_product']     = 'Avertissement: Cette classe de taxe ne peut pas être supprimée car elle est actuellement affectée aux produits% s.!';