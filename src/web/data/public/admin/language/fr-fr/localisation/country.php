<?php

// Heading

$_['heading_title']           = 'Des pays';

// Text

$_['text_success']            = 'Succès: vous avez modifié des pays!';
$_['text_list']               = 'Liste de pays';
$_['text_add']                = 'Ajouter un pays ';
$_['text_edit']               = 'Modifier le pays';

// label name

$_['text_name']              = 'Nom du pays';
$_['text_iso_code_2']        = 'Code ISO (2)';
$_['text_iso_code_3']        = 'Code ISO (3)';
$_['text_address_format']    = 'Format d\'adresse';
$_['text_postcode_required'] = 'Code postal requis';
$_['text_status']            = 'Statut';


// Column
$_['column_name']             = 'Nom du pays';
$_['column_iso_code_2']       = 'Code ISO (2)';
$_['column_iso_code_3']       = 'Code ISO (3)';
$_['column_action']           = 'action';

/// btn 
$_['button_save']               = 'sauver';
$_['button_cancel']             = 'Annuler';



// Entry
$_['entry_name']              = 'Nom du pays';
$_['entry_iso_code_2']        = 'Code ISO (2)';
$_['entry_iso_code_3']        = 'Code ISO (3)';
$_['entry_address_format']    = 'Format d\'adresse';
$_['entry_postcode_required'] = 'Code postal requis';
$_['entry_status']            = 'Statut';

$_['text_disabled']           = 'désactivé';
$_['text_enabled']           = 'Activée';
/// btn 



// Help

$_['help_address_format']     = 'Prénom = {firstname}<br />Nom de famille = {lastname}<br />Compagnie = {company}<br />Adresse 1 = {address_1}<br />Adresse 2 = {address_2}<br />Ville = {city}<br />Code postal = {postcode}<br />Zone = {zone}<br />Code de zone = {zone_code}<br />Pays = {country}';

// Error
$_['error_permission']        = "Avertissement: vous n'êtes pas autorisé à modifier des pays!";
$_['error_name']              = 'Le nom du pays doit comporter entre 3 et 128 caractères!';
$_['error_default']           = 'Avertissement: Ce pays ne peut pas être supprimé car il est actuellement attribué comme pays de stockage par défaut.!';
$_['error_store']             = 'Avertissement: Ce pays ne peut pas être supprimé car il est actuellement attribué aux magasins% s.!';
$_['error_address']           = 'Avertissement: Ce pays ne peut pas être supprimé car il est actuellement affecté aux entrées du carnet d’adresses% s.!';
$_['error_affiliate']         = 'Attention: ce pays ne peut pas être supprimé car il est actuellement attribué aux affiliés% s!';
$_['error_zone']              = 'Avertissement: Ce pays ne peut pas être supprimé car il est actuellement attribué aux zones% s.!';
$_['error_zone_to_geo_zone']  = 'Avertissement: Ce pays ne peut pas être supprimé car il est actuellement attribué aux zones% s aux zones géographiques.!';