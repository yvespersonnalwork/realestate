<?php

// Heading

$_['heading_title']    = 'Raisons de retour';

// Text

$_['text_success']     = 'Succès: vous avez modifié les motifs de retour!';
$_['text_list']        = 'Liste des motifs de retour';
$_['text_add']         = 'Ajouter une raison de retour';
$_['text_edit']        = 'Modifier le motif de retour';

// Column

$_['column_name']      = 'Nom du motif de retour';
$_['column_action']    = 'action';
// Entry

$_['entry_name']       = 'Nom du motif de retour';
// Error

$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier les motifs de retour!";
$_['error_name']       = 'Le nom de la raison renvoyée doit comporter entre 3 et 128 caractères.!';
$_['error_return']     = 'Avertissement: Ce motif de retour ne peut pas être supprimé car il est actuellement attribué aux produits retournés% s.!';