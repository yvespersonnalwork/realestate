<?php

// Heading

$_['heading_title']      = 'Zones géographiques';

// Text

$_['text_success']       = 'Succès: vous avez modifié les zones géographiques!';
$_['text_list']          = 'Liste des zones géographiques';
$_['text_add']           = 'Ajouter une zone géographique';
$_['text_edit']          = 'Modifier la zone géographique';

//label name
$_['text_name']         = 'Nom de la zone géographique';
$_['text_description']  = 'La description';
$_['text_country']      = 'Pays';
$_['text_zone']         = 'Zone';

// Column

$_['column_name']        = 'Nom de la zone géographique';
$_['column_description'] = 'La description';
$_['column_action']      = 'action';
// Entry

$_['entry_name']         = 'Nom de la zone géographique';
$_['entry_description']  = 'La description';
$_['entry_country']      = 'Pays';
$_['entry_zone']         = 'Zone';
$_['text_disabled']           = 'désactivé';
$_['text_enabled']           = 'Activée';

/// btn 
$_['button_save']               = 'sauver';
$_['button_cancel']             = 'Annuler';
$_['button_geo_zone_add']       = 'géo_zone_ajouter';
$_['button_remove']             = 'Retirer';



// Error

$_['error_permission']   = "Avertissement: vous n'êtes pas autorisé à modifier des zones géographiques.!";
$_['error_name']         = 'Le nom de la zone géographique doit comporter entre 3 et 32 caractères.!';
$_['error_description']  = 'Description Le nom doit comporter entre 3 et 255 caractères.!';
$_['error_tax_rate']     = 'Avertissement: cette zone géographique ne peut pas être supprimée car elle est actuellement affectée à un ou plusieurs taux de taxe.!';