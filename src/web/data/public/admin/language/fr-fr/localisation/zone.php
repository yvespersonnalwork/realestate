<?php

// Heading

$_['heading_title']          = 'Zones';


// Text

$_['text_success']           = 'Succès: vous avez modifié des zones!';
$_['text_list']              = 'Liste de zones';
$_['text_add']               = 'Ajouter une zone';
$_['text_edit']              = 'Zone d\'édition';

///label name

$_['text_name']             = 'Nom de la zone';
$_['text_code']             = 'Code de zone';
$_['text_country']          = 'Pays';
$_['text_status']           = 'Statut';

// Column

$_['column_name']            = 'Nom de la zone';
$_['column_code']            = 'Code de zone';
$_['column_country']         = 'Pays';
$_['column_action']          = 'action';

// Entry input value name 

$_['entry_name']             = 'Nom de la zone';
$_['entry_code']             = 'Code de zone';
$_['entry_country']          = 'Pays';
$_['entry_status']           = 'Statut';
$_['text_disabled']           = 'désactivé';
$_['text_enabled']           = 'Activée';

/// btn 
$_['button_save']               = 'sauver';
$_['button_cancel']             = 'Annuler';
$_['button_geo_zone_add']       = 'géo_zone_ajouter';
$_['button_remove']             = 'Retirer';


// Error


$_['error_permission']       = "Avertissement: vous n'êtes pas autorisé à modifier des zones!";
$_['error_name']             = 'Le nom de la zone doit comporter entre 3 et 128 caractères!';
$_['error_default']          = 'Avertissement: Cette zone ne peut pas être supprimée car elle est actuellement attribuée à la zone de stockage par défaut!';
$_['error_store']            = 'Avertissement: cette zone ne peut pas être supprimée car elle est actuellement affectée aux magasins% s.!';
$_['error_address']          = 'Avertissement: Cette zone ne peut pas être supprimée car elle est actuellement affectée aux entrées du carnet d’adresses% s.!';
$_['error_affiliate']        = 'Attention: cette zone ne peut pas être supprimée car elle est actuellement assignée aux affiliés% s!';
$_['error_zone_to_geo_zone'] = 'Avertissement: cette zone ne peut pas être supprimée car elle est actuellement affectée aux zones% s aux zones géographiques.!';