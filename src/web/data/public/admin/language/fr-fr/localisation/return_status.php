<?php

// Heading

$_['heading_title']    = 'États de retour';


// Text

$_['text_success']     = 'Succès: vous avez modifié les statuts de retour!';
$_['text_list']        = 'Retour Statut Liste';
$_['text_add']         = 'Ajouter le statut de retour';
$_['text_edit']        = 'Modifier le statut de retour';

// Column

$_['column_name']      = 'Nom du statut de retour';
$_['column_action']    = 'action';

// Entry

$_['entry_name']       = 'Nom du statut de retour';

// Error

$_['error_permission'] = "Avertissement: vous n'êtes pas autorisé à modifier les statuts de retour!";

$_['error_name']       = 'Le nom de statut de retour doit comporter entre 3 et 32 caractères.!';

$_['error_default']    = 'Avertissement: Cet état de retour ne peut pas être supprimé car il est actuellement affecté comme état de retour par défaut.!';

$_['error_return']     = 'Avertissement: Cet état de retour ne peut pas être supprimé car il est actuellement affecté aux retours% s.!';

