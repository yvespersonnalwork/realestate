<?php

// Heading

$_['heading_title']    = 'Abonnés à la newsletter';



// Text

$_['text_success']     = 'Succès: vous avez modifié les abonnés à la newsletter!';

$_['text_export']     = 'Exportation';

$_['text_list']     = 'Liste des abonnés à la newsletter';

$_['text_add']     = 'Ajouter des abonnés à la newsletter';

$_['text_edit']     = 'Modifier les abonnés à la newsletter';



// Column

$_['column_name']      = 'Nom';

$_['column_email']     = 'Email';

$_['column_action']    = 'action';



// Entry

$_['entry_name']       = 'Nom';

$_['entry_code']       = 'Email';







// Error

$_['error_email_exist']= 'Email Id existe déjà';

$_['error_email_id']      = "Format d\'email invalide";

$_['error_permission'] = "Attention: vous n\'êtes pas autorisé à modifier les abonnés à la newsletter!";

?>