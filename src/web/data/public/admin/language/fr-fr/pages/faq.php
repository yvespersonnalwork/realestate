<?php
// Heading
$_['heading_title']          = 'questions - réponses';

// Text
$_['text_success']           = 'Succès: vous avez modifié la FAQ!';
$_['text_list']              = 'Liste de FAQ';
$_['text_add']               = 'Ajouter une FAQ';
$_['text_edit']              = 'Modifier la FAQ';
$_['text_default']           = 'Défaut';

// Column
$_['column_name']            = 'FAQ Nom';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_action']          = 'action';


///

$_['text_category']         = 'Catégorie';
$_['text_name']             = 'Question';
$_['text_description']      = 'Répondre';
$_['text_meta_title'] 	     = 'Titre de la balise méta';
$_['text_meta_keyword'] 	 = 'Mots-clés méta';
$_['text_meta_description'] = 'Meta Tag Description';
$_['text_keyword']          = 'SEO Mot clé';
$_['text_parent']           = 'Parente';
$_['text_filter']           = 'Les filtres';
$_['text_store']            = 'Site Internet';
$_['text_image']            = 'Image';
$_['text_top']              = 'Haut';
$_['text_column']           = 'Colonnes';
$_['text_sort_order']       = 'Ordre de tri';
$_['text_status']           = 'Statut';
$_['text_layout']           = 'Remplacement de mise en page';



// Entry
$_['entry_category']         = 'Catégorie';
$_['entry_name']             = 'Question';
$_['entry_description']      = 'Répondre';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO Mot clé';
$_['entry_parent']           = 'Parente';
$_['entry_filter']           = 'Les filtres';
$_['entry_store']            = 'Site Internet';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Haut';
$_['entry_column']           = 'Colonnes';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_status']           = 'Statut';
$_['entry_layout']           = 'Remplacement de mise en page';

/// btn
$_['button_save']             = 'sauver';
$_['button_cancel']           = 'Annuler';


// Help
$_['help_filter']            = '(Saisie automatique)';
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..';
$_['help_top']               = 'Afficher dans la barre de menu supérieure. Ne fonctionne que pour la FAQ des meilleurs parents.';
$_['help_column']            = 'Nombre de colonnes à utiliser pour les 3 questions les plus fréquentes. Ne fonctionne que pour la FAQ des meilleurs parents.';

// Error
$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier la FAQ!';
$_['error_name']             = 'FAQ Le nom doit comporter entre 2 et 32 caractères!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'Mot clé SEO déjà utilisé!';
$_['error_category']          = 'Veuillez sélectionner une catégorie si les catégories ne sont pas présentes, puis ajoutez d\'abord une catégorie.!	';