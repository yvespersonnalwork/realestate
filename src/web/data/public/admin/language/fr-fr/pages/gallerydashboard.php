<?php
// Heading
$_['heading_title']                = 'Tableau de bord de la galerie';

// Text
$_['text_order_total']             = 'Total des commandes';
$_['text_customer_total']          = 'Total clients';
$_['text_sale_total']              = 'Ventes totales';
$_['text_online_total']            = 'Personnes en ligne';
$_['text_map']                     = 'Carte du monde';
$_['text_sale']                    = 'Analyse des ventes';
$_['text_activity']                = 'Activité récente';
$_['text_recent']                  = 'Dernières commandes';
$_['text_order']                   = 'Ordres';
$_['text_customer']                = 'Les clients';
$_['text_day']                     = 'Aujourd\'hui';
$_['text_week']                    = 'La semaine';
$_['text_month']                   = 'Mois';
$_['text_year']                    = 'Année';
$_['text_view']                    = 'Voir plus...';


// Error
$_['error_install']                = "Avertissement: le dossier d\'installation existe toujours et doit être supprimé pour des raisons de sécurité!";