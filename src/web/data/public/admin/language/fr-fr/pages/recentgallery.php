<?php
// Heading
$_['heading_title']      = 'Galerie récente';

// Column
$_['column_galleryid']   = 'Identifiant de la galerie';
$_['column_galleryname'] = 'Nom de la galerie';
$_['column_totalalbum']  = 'Album total';
$_['column_status']      = 'Statut';
$_['column_action']      = 'action';

