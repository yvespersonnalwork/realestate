<?php

// Heading

$_['heading_title']          = "Pages d\'information";



// Text

$_['text_success']           = "Succès: vous avez modifié l\'information!";

$_['text_list']              = "Liste des pages d\'information";

$_['text_add']               = 'Ajouter des informations';

$_['text_edit']              = 'Modifier les informations';

$_['text_default']           = 'Défaut';



// Column

$_['column_title']           = "Titre de la page d\'information";

$_['column_sort_order']	     = 'Ordre de tri';

$_['column_action']          = 'action';



//label name
$_['text_title']            = "Titre de l\'information";

$_['text_description']      = 'La description';

$_['text_store']            = 'Magasins';

$_['text_meta_title'] 	     = 'Titre de la balise méta';

$_['text_meta_keyword'] 	 = 'Mots-clés méta';

$_['text_meta_description'] = 'Meta Tag Description';

$_['text_keyword']          = 'URL de référencement';

$_['text_bottom']           = 'Bas';

$_['text_status']           = 'Statut';

$_['text_sort_order']       = 'Ordre de tri';

$_['text_layout']           = 'Remplacement de mise en page';





// Entry

$_['entry_title']            = 'Titre de l\'information';

$_['entry_description']      = 'La description';

$_['entry_store']            = 'Magasins';

$_['entry_meta_title'] 	     = 'Titre de la balise méta';

$_['entry_meta_keyword'] 	 = 'Mots-clés méta';

$_['entry_meta_description'] = 'Meta Tag Description';

$_['entry_keyword']          = 'URL de référencement';

$_['entry_bottom']           = 'Bas';

$_['entry_status']           = 'Statut';

$_['entry_sort_order']       = 'Ordre de tri';

$_['entry_layout']           = 'Remplacement de mise en page';






// Help

$_['help_keyword']           = "N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que l\'URL de référencement est unique au monde.";

$_['help_bottom']            = 'Afficher en bas de page.';



// Error

$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';

$_['error_permission']       = "Avertissement: vous n\'êtes pas autorisé à modifier des informations!";

$_['error_title']            = "Le titre de l\'information doit comporter entre 3 et 64 caractères.!";

$_['error_description']      = 'La description doit comporter plus de 3 caractères.!';

$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';

$_['error_keyword']          = 'URL de référencement déjà utilisée!';

$_['error_account']          = "Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux termes du compte de site Web.!";

$_['error_checkout']         = "Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de commande du site Web.!";

$_['error_affiliate']        = "Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions d’affiliation au site Web.!";

$_['error_return']           = "Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement affectée aux conditions de retour du site Web.!";

$_['error_store']            = "Avertissement: Cette page d’information ne peut pas être supprimée car elle est actuellement utilisée par les magasins% s.!";

