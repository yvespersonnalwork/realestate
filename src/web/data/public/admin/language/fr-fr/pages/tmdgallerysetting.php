<?php
// Heading
$_['heading_title']          = 'Réglage de la galerie';

// Text
$_['text_success']           = 'Succès: vous avez modifié des catégories!';
$_['text_list']              = 'Tmd GallerySetting List';
$_['text_add']               = 'Ajouter Tmd GallerySetting';
$_['text_edit']              = 'Éditer Tmd GallerySetting';
$_['text_default']           = 'Défaut';

// Column
$_['column_name']            = 'Titre de la galerie';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_action']          = 'action';

// Entry
$_['entry_width']       	 = 'Largeur';
$_['entry_height']       	 = 'la taille';
$_['entry_image_popup']  	 = 'Taille Popup';
$_['entry_thumb_popup']  	 = 'Taille du pouce';
$_['entry_limtofgallery']  	 = 'Définir la limite de photo';
$_['entry_namecolor']  		 = 'Photo Nom Couleur';
$_['entry_desccolor'] 	     = 'Description couleur';
$_['entry_totalphotocolor']  = 'Couleur totale de la photo';

$_['tab_color']              = 'Ajuster la couleur';
$_['tab_setting']            = 'Réglage de la galerie';

// Error
$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = "Avertissement: vous n\'êtes pas autorisé à modifier des catégories.!";
$_['error_name']             = 'Le titre de la galerie doit comporter entre 2 et 32 caractères.!';
$_['error_meta_title']       = 'Le méta-titre de la galerie doit comporter plus de 3 caractères et moins de 255 caractères.!';