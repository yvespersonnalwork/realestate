<?php

// Heading

$_['heading_title']      = 'Photo';



// Text

$_['text_success']       = 'Succès: vous avez modifié l\'album photos!';

$_['text_default']       = 'Défaut';

$_['text_image_manager'] = "Gestionnaire d'images";

$_['text_browse']        = 'Feuilleter';

$_['text_clear']         = 'Claire';

$_['text_percent']       = 'Pourcentage';

$_['text_amount']        = 'Montant fixé';

$_['text_list']          = 'Liste Photo';

$_['text_add']           = 'Ajouter une photo';

$_['text_edit']          = 'Modifier photo';

$_['entry_url']          = 'URL de référencement';



// Column

$_['column_name']        = "Nom de l'album";
$_['column_image']        = 'Image';

$_['column_title']       = 'Titre';

$_['column_sort_order']  = 'Sort Order';

$_['column_action']      = 'action';

$_['button_insert']      = 'Ajouter un nouveau';

$_['button_multiple']    = 'Ajouter plusieurs images';



// Entry

$_['entry_albumname']    = "Nom de l'album";

$_['entry_name']         = 'Titre de la photo';

$_['entry_title']        = 'Titre';

$_['entry_image']        = 'Image';

$_['entry_images']       = 'Ajouter plusieurs images';

$_['entry_sort_order']   = 'Ordre de tri';

$_['entry_description']  = 'La description';

$_['entry_width']        = 'Largeur';

$_['entry_height']       = 'la taille';

$_['entry_image_popup']  = 'Taille Popup';

$_['entry_thumb_popup']  = 'Taille du pouce';



// Error

$_['error_permission']   = "Avertissement: vous n'êtes pas autorisé à modifier les photos.!";

$_['error_name']         = 'Le nom de l\'album doit être Sélectionner !';

$_['error_title']        = 'Le nom de la galerie doit comporter entre 2 et 32 caractères.!';

$_['error_product']      = 'Avertissement: Cet album ne peut pas être supprimé car il est actuellement attribué aux produits% s!';

$_['error_images']       = 'Minimum une image requise!';

?>