<?php
// Heading
$_['heading_title']          = 'Catégories';

// Text
$_['text_success']           = 'Succès: vous avez modifié des catégories!';
$_['text_list']              = 'Liste de catégories';
$_['text_add']               = 'ajouter une catégorie';
$_['text_edit']              = 'Modifier la catégorie';
$_['text_default']           = 'Défaut';

// Column
$_['column_name']            = 'Nom de catégorie';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_action']          = 'action';

//label name

$_['text_name']             = 'Nom de catégorie';
$_['text_description']      = 'La description';
$_['text_meta_title'] 	     = 'Titre de la balise méta';
$_['text_meta_keyword'] 	 = 'Mots-clés méta';
$_['text_meta_description'] = 'Meta Tag Description';
$_['text_keyword']          = 'SEO Mot clé';
$_['text_parent']           = 'Parente';
$_['text_filter']           = 'Les filtres';
$_['text_store']            = 'Site Internet';
$_['text_image']            = 'Image';
$_['text_top']              = 'Haut';
$_['text_column']           = 'Colonnes';
$_['text_sort_order']       = 'Ordre de tri';
$_['text_status']           = 'Statut';
$_['text_layout']           = 'Remplacement de mise en page';


// Entry input name
$_['entry_name']             = 'Nom de catégorie';
$_['entry_description']      = 'La description';
$_['entry_meta_title'] 	     = 'Titre de la balise méta';
$_['entry_meta_keyword'] 	 = 'Mots-clés méta';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO Mot clé';
$_['entry_parent']           = 'Parente';
$_['entry_filter']           = 'Les filtres';
$_['entry_store']            = 'Site Internet';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Haut';
$_['entry_column']           = 'Colonnes';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_status']           = 'Statut';
$_['entry_layout']           = 'Remplacement de mise en page';

///btn 
$_['button_save']             = 'sauver';
$_['button_cancel']           = 'Annuler';


// Help
$_['help_filter']            = '(Saisie automatique)';
$_['help_keyword']           = 'N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..';
$_['help_top']               = 'Afficher dans la barre de menu supérieure. Ne fonctionne que pour les catégories parentes les plus populaires.';
$_['help_column']            = 'Nombre de colonnes à utiliser pour les 3 dernières catégories. Ne fonctionne que pour les catégories parentes les plus populaires.';

// Error
$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = 'Avertissement: vous n\'êtes pas autorisé à modifier des catégories.!';
$_['error_name']             = 'Le nom de la catégorie doit comporter entre 2 et 32 caractères.!';
$_['error_meta_title']       = 'Le méta-titre doit comporter plus de 3 caractères et moins de 255 caractères!';
$_['error_keyword']          = 'Mot clé SEO déjà utilisé!';