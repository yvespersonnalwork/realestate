<?php
// Heading
$_['heading_title']    		= 'Mega en-tête';

$_['text_megaheader']      	= 'Mega en-tête';
$_['text_success']     		= "Succès: vous avez modifié le module de définition d\'en-tête méga!";
$_['text_add']        		= 'Ajouter un en-tête Mega';

// Entry
$_['entry_bgtitle']     	= 'Titre Bg Couleur';
$_['entry_bghovtitle']     	= 'Titre Bg Hover Color';
$_['entry_bg_color']     	= 'En-tête Bg Couleur';
$_['entry_title']     	 	= 'Titre Titre Couleur';
$_['entry_hovtitle']     	= "Titre d\'en-tête Survoler la couleur";
$_['entry_link']         	= "Couleur du lien d\'en-tête";
$_['entry_link_hover']   	= "Couleur du lien d’en-tête (survol)";
$_['entry_powered']      	= 'Couleur du texte en-tête';
$_['entry_title_font']   	= "Titre de l\'en-tête Taille de la police";
$_['entry_sub_link']     	= 'Lien secondaire Taille de la police';
$_['entry_product_limit']	= 'Limite de produit';
$_['entry_category_limit'] 	= 'Limite de catégorie';
$_['entry_product_image']  	= 'Image du produit';
$_['entry_height']         	= 'la taille';
$_['entry_width']          	= 'Largeur';
$_['entry_category_image']  = 'Image de catégorie';
$_['entry_manufacture_image'] = 'Image de fabrication';
$_['entry_drpmebg']			  = 'Menu déroulant Couleur bg';
$_['help_category_image']			  = 'Définir la taille de l\'image de la catégorie';
/* update code */
$_['entry_menuexpend']	      = 'Menu Dépenses';
$_['text_no']	              = 'Non';
$_['text_yes']	              = 'Oui';
/* update code */


// Error
$_['error_permission'] 		  = "Avertissement: vous n\'êtes pas autorisé à modifier le module de définition d\'en-tête mega!";

// help
$_['help_bg_color'] 		  = "Couleur de fond de l\'en-tête";
$_['help_title'] 			  = "Couleur du texte de l\'en-tête";
$_['help_link'] 		      = "Couleur du texte des liens d\'en-tête";
$_['help_link_hover'] 		  = "Couleur du texte des liens d’en-tête (survolé)";
$_['help_powered'] 			  = "Couleur du texte du bas de l\'en-tête";
$_['help_title_font'] 		  = 'Taille de la police du titre des en-têtes';
$_['help_sub_link'] 		  = "Taille de la police du lien d\'en-tête";
$_['help_product_limit'] 	  = 'La limite de produit est uniquement pour la catégorie et la fabrication';
$_['help_category_limit'] 	  = 'Limite de catégorie';
$_['help_product_image'] 	  = "Product Image Hauteur, largeur";
$_['help_category_image'] 	  = "Catégorie Image Hauteur, largeur";
$_['help_manufacture_image']  = "Manufacture Image Hauteur, largeur";
$_['help_hovtitle'] 		  = "Survol du texte Couleur du titre de l\'en-tête";
$_['help_bgtitle'] 		      = 'Titre du menu Bg Couleur';
$_['help_bghovetitle'] 		  = 'Menu Titre Survol Couleur';
$_['help_drpmebg'] 		      = 'Sélectionnez le menu déroulant Couleur bg';