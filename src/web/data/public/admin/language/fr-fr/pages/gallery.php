<?php
// Heading
$_['heading_title']          = 'Galerie';

// Text
$_['text_success']           = 'Succès: vous avez modifié la galerie!';
$_['text_default']           = 'Défaut';
$_['text_image_manager']     = 'Gestionnaire d\'images';
$_['text_browse']            = 'Feuilleter';
$_['text_clear']             = 'Claire';
$_['text_list']              = 'Liste des galeries';
$_['text_add']               = 'Ajouter une liste de galerie';
$_['text_edit']              = 'Editer la liste de la galerie';
$_['entry_url']          	 = 'URL de référencement';

// Column
$_['column_name']            = 'Nom de l\'album';
$_['column_sort_order']      = 'Nom de l\'album';
$_['column_image']           = 'Image de couverture';
$_['column_status']          = 'Statut';
$_['column_action']          = 'action';

// Entry
$_['entry_name']             = 'Nom de l'album:';
$_['entry_description']      = 'La description:';
$_['entry_image']            = 'Cadre de l'album:';
$_['entry_sort_order']       = 'Ordre de tri:';
$_['entry_status']           = 'Statut:';

// Error 
$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = "Avertissement: vous n'êtes pas autorisé à modifier Gallery.!";
$_['error_name']             = 'Le nom de la galerie doit comporter entre 2 et 32 caractères.!';
?>