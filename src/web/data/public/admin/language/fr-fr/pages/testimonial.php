<?php

// Heading

$_['heading_title']       = 'Témoignages';



// Text

$_['text_success']      = 'Succès: vous avez modifié les témoignages!';

$_['text_list']      = 'Liste des témoignages';

$_['text_add']      = 'Ajouter des témoignages';

$_['text_edit']      = 'Modifier les témoignages';



// Column

$_['column_author']     = 'Auteure';

$_['column_rating']     = 'Pays';

$_['column_status']     = 'Statut';

$_['column_date_added'] = 'date ajoutée';

$_['column_action']     = 'action';

$_['entry_image']     = 'Image';

$_['button_insert']     = 'Ajouter';




///label

$_['text_product']     = 'Pays';
$_['text_author']      = 'Auteure';
$_['text_country']     = 'Pays';
$_['text_rating']      = 'Évaluation:';
$_['text_status']      = 'Statut';
$_['text_text']        = 'Texte';
$_['text_good']        = 'Bien';
$_['text_bad']         = 'Mauvais';
$_['text_image']         = 'image';


// Entry

$_['entry_product']     = 'Pays';
$_['entry_author']      = 'Auteure';
$_['entry_country']     = 'Pays';
$_['entry_rating']      = 'Évaluation:';
$_['entry_status']      = 'Statut';
$_['entry_text']        = 'Texte';
$_['entry_good']        = 'Bien';
$_['entry_bad']         = 'Mauvais';


/// btn

$_['button_save']          = 'enregistrer';
$_['button_cancel']          = 'Annuler';

// Error

$_['error_permission']  = "Avertissement: vous n\'êtes pas autorisé à modifier des témoignages.!";

$_['error_author']      = "L\'auteur doit comporter entre 3 et 64 caractères.!";

$_['error_text']        = 'Le texte du témoignage doit être supérieur à 1 caractère!';

?>