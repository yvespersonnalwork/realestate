<?php
// Heading
$_['heading_title']          = 'Mega Header Menu';

//Text
$_['text_list']         	 = 'Liste des menus';
$_['text_form']              = 'Mega en-tête';
$_['text_category']          = 'Catégorie';
$_['text_info']              = 'Information';
$_['text_manufact']          = 'Fabricante';
$_['text_custom']            = 'Douane';
$_['text_select']            = 'Sélectionner';
$_['text_editor']            = 'Éditrice';
$_['text_product']           = 'Produit';
$_['text_enable']            = 'Activée';
$_['text_disable']           = 'désactivé';
$_['text_no_results']        = 'pas de résultat';
$_['text_category_image']    = 'Image de catégorie';
$_['text_category_description'] = 'description de la catégorie';
$_['text_manufatureimage']   = 'Image du fabricant';
$_['text_patternimage']   	= "Motif d\'image";
$_['entry_patternimage']     = 'Fond de modèle';
$_['entry_selectimagetype']  = "Sélectionnez le type d\'arrière-plan";
//// new changes  27-10-2016///
$_['entry_store']            = 'Site Internet';
//// new changes  27-10-2016///


$_['button_edit']            = 'Modifier';
$_['button_add']             = 'Ajouter un nouveau';
$_['button_delete']          = 'Effacer';
$_['button_save']            = 'sauver';
$_['button_setting']         = 'Mega header Réglage';
$_['button_cancel']          = 'Canel';
$_['text_success']           = 'Succès: vous avez modifié les en-têtes Mega! ';
$_['button_custom']          = 'Ajouter un menu personnalisé';

$_['text_product1']          = 'Propriété';
$_['text_subcategory']       = 'Sous catégorie';
$_['text_price']         	 = 'Prix';
$_['text_pname']         	 = 'Nom de la propriété';
$_['text_image']         	 = 'Image';
$_['text_model']        	 = 'Modèle';
$_['text_upc']        	     = 'UPC';
$_['text_sku']         		 = 'SKU';
$_['text_description']       = 'La description';


/// label name 
$_['text_image']          	 = 'Image de fond';
$_['text_status']           = 'Statut';
$_['text_sort_order']       = 'Ordre de tri';
$_['text_title']            = "Titre d\'en-tête personnalisé";
$_['text_row']           	 = 'Rangée';
$_['text_col']           	 = 'Cols';
$_['text_enable']           = 'Activer le titre de la catégorie de sous-menu';
$_['text_url']           	 = 'URL';
$_['text_open']           	 = 'Ouvrir dans un nouvel onglet';
$_['text_type']           	 = 'Type';
$_['text_custname']         = "Nom d\'usage";
$_['text_custurl'] 	     = 'URL personnalisée';
$_['text_parent'] 	         = 'Parente';
$_['text_submenu'] 		 = 'Sous Megamenu';
$_['text_description']      = 'Description du MegaMenu';
$_['text_product']          = 'Produit';
$_['text_category']         = 'Catégorie';
$_['text_row']         	 = 'Rangée';
$_['text_customcode']       = 'Code personnalisé';
$_['button_setting']         = 'Paramètres';
$_['text_information']      = 'Information';
$_['text_manufacturer']     = 'Fabricante';
$_['text_editor']            = 'Éditrice';
$_['text_icontitle']		 = 'Icône';
$_['text_showicon']		 = "Activer l\'icône";

$_['text_selectimagetype']  = "Sélectionnez le type d\'arrière-plan";
//// new changes  27-10-2016///
$_['text_store']            = 'Site Internet';

// Entry
$_['entry_image']          	 = 'Image de fond';
$_['entry_status']           = 'Statut';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_title']            = "Titre d\'en-tête personnalisé";
$_['entry_row']           	 = 'Rangée';
$_['entry_col']           	 = 'Cols';
$_['entry_enable']           = 'Activer le titre de la catégorie de sous-menu';
$_['entry_url']           	 = 'URL';
$_['entry_open']           	 = 'Ouvrir dans un nouvel onglet';
$_['entry_type']           	 = 'Type';
$_['entry_custname']         = "Nom d\'usage";
$_['entry_custurl'] 	     = 'URL personnalisée';
$_['entry_parent'] 	         = 'Parente';
$_['entry_submenu'] 		 = 'Sous Megamenu';
$_['entry_description']      = 'Description du MegaMenu';
$_['entry_product']          = 'Produit';
$_['entry_category']         = 'Catégorie';
$_['entry_row']         	 = 'Rangée';
$_['entry_customcode']       = 'Code personnalisé';
$_['button_setting']         = 'Paramètres';
$_['entry_information']      = 'Information';
$_['entry_manufacturer']     = 'Fabricante';
$_['text_editor']            = 'Éditrice';
$_['entry_icontitle']		 = 'Icône';
$_['entry_showicon']		 = "Activer l\'icône";

// column_edit
$_['column_title']           = 'Titre du menu';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_status']          = 'Statut';
$_['column_edit']            = 'action';

 // Error
$_['error_title']      		 = 'Le titre doit comporter entre 3 et 21 caractères.'; 
$_['error_image']      		 = 'Veuillez sélectionner une image'; 

//filter
$_['button_filter']      	 = 'Filtre';
$_['entry_title']      		 = "Titre d\'en-tête personnalisé";
$_['entry_status']      	 = 'Statut';
$_['text_enabled']      	 = 'Activer';
$_['text_disabled']      	 = 'Désactiver';

//help
$_['help_keyword']           = "N\'utilisez pas d\'espaces, remplacez-les par des espaces et assurez-vous que le mot clé est unique..";
$_['help_bottom']            = 'Afficher en bas de page.';
$_['help_category']          = 'Saisie automatique';
$_['help_product']           = 'Saisie automatique';
$_['help_showicon']          = "Activer uniquement l\'icône dans le menu";
