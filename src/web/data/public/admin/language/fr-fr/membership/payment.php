<?php
// Heading
$_['heading_title']					 = 'Paiements PayPal Standard';

// Text
$_['text_extension']				 = 'Les extensions';
$_['text_success']					 = 'Succès: vous avez modifié les détails du compte PayPal.!';
$_['text_edit']                      = 'Modifier les paiements PayPal standard';
$_['text_pp_standard']				 = '<une cible = "_ BLANK" href = "https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"> <img src = "vue / image / paiement / paypal.png" alt = "PayPal Paiement de site Web Pro "title =" Paiement sur le site PayPal Pro iFrame "style =" border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Autorisation';
$_['text_sale']						 = 'Vente'; 

// Entry
$_['entry_email']					 = 'Email';
$_['entry_test']					 = 'Mode bac à sable';
$_['entry_transaction']				 = 'Méthode de transaction';
$_['entry_debug']					 = 'Mode débogage';
$_['entry_total']					 = 'Totale';
$_['entry_canceled_reversal_status'] = "Annulation du statut d'annulation";
$_['entry_completed_status']		 = 'Statut terminé';
$_['entry_denied_status']			 = 'Statut refusé';
$_['entry_expired_status']			 = 'Statut expiré';
$_['entry_failed_status']			 = 'Statut échoué';
$_['entry_pending_status']			 = 'Statut en attente';
$_['entry_processed_status']		 = 'Statut traité';
$_['entry_refunded_status']			 = 'Statut remboursé';
$_['entry_reversed_status']			 = 'Statut inversé';
$_['entry_voided_status']			 = 'Statut annulé';
$_['entry_geo_zone']				 = 'Zone géographique';
$_['entry_status']					 = 'Statut';
$_['entry_sort_order']				 = 'Ordre de tri';

// Tab
$_['tab_general']					 = 'Général';
$_['tab_order_status']       		 = 'Statut de la commande';

// Help
$_['help_test']						 = 'Utiliser le serveur de passerelle en direct ou en test (bac à sable) pour traiter les transactions?';
$_['help_debug']			    	 = 'Enregistre des informations supplémentaires dans le journal système';
$_['help_total']					 = 'Le total de la commande que la commande doit atteindre avant que ce mode de paiement devienne actif';

// Error
$_['error_permission']				 = "Attention: vous n'êtes pas autorisé à modifier le paiement PayPal!";
$_['error_email']					 = 'Email (requis!';