<?php
// Heading
$_['heading_title']          = 'Paquet';

// Text
$_['text_success']           = 'Succès: vous avez modifié le package!';
$_['text_list']              = 'Liste de colis';
$_['text_add']               = 'Ajouter un paquet';
$_['text_edit']              = 'Editer le paquet';
$_['text_default']           = 'Défaut';

// Column
$_['column_name']            = 'Nom du paquet';
$_['column_sort_order']      = 'Ordre de tri';
$_['column_action']          = 'action';

// Entry
$_['entry_name']             = 'Nom du paquet';
$_['entry_description']      = 'La description';
$_['entry_keyword']          = 'URL de référencement';
$_['entry_image']            = 'Image';
$_['entry_sort_order']       = 'Ordre de tri';
$_['entry_status']           = 'Statut';


// Help
$_['help_keyword']           = "N'utilisez pas d'espaces, remplacez-les par des espaces et assurez-vous que l'URL de référencement est unique au monde..";


// Error
$_['error_warning']          = 'Avertissement: Veuillez vérifier soigneusement le formulaire pour rechercher les erreurs.!';
$_['error_permission']       = "Avertissement: Vous n'êtes pas autorisé à modifier un package.!";
$_['error_name']             = 'Le nom du package doit comporter entre 2 et 255 caractères.!';
$_['error_keyword']          = 'URL de référencement déjà utilisée!';
