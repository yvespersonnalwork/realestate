<?php

// Heading

$_['heading_title']    = 'Groupes d\'utilisateurs';



// Text

$_['text_success']     = 'Succès: vous avez modifié des groupes d\'utilisateurs!';

$_['text_list']        = 'Groupe d\'utilisateurs';

$_['text_add']         = 'Ajouter un groupe d\'utilisateurs';

$_['text_edit']        = 'Editer le groupe d\'utilisateurs';



// Column

$_['column_name']      = 'Nom du groupe d\'utilisateurs';

$_['column_action']    = 'action';



///label name 

$_['text_name']       = 'Nom du groupe d\'utilisateurs';

$_['text_access']     = 'Droit d\'accès';

$_['text_modify']     = 'Modifier l\'autorisation';


// Entry

$_['entry_name']       = 'Nom du groupe d\'utilisateurs';

$_['entry_access']     = 'Droit d\'accès';

$_['entry_modify']     = 'Modifier l\'autorisation';


///btn 
$_['button_save']           = 'sauver';
$_['button_cancel']         = 'Annuler';


// Error

$_['error_permission'] = 'Avertissement: vous n\'êtes pas autorisé à modifier des groupes d\'utilisateurs.!';

$_['error_name']       = 'Le nom du groupe d\'utilisateurs doit comporter entre 3 et 64 caractères.!';

$_['error_user']       = 'Avertissement: Ce groupe d’utilisateurs ne peut pas être supprimé car il est actuellement attribué aux utilisateurs de% s.!';