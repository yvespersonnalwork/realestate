<?php

// Heading

$_['heading_title']         = 'Utilisatrices';



// Text

$_['text_success']          = 'Succès: vous avez modifié des utilisateurs!';

$_['text_list']             = 'liste d\'utilisateur';

$_['text_add']              = 'Ajouter un utilisateur';

$_['text_edit']             = 'Modifier l\'utilisateur';



// Column

$_['column_username']       = 'Nom d\'utilisateur';

$_['column_status']         = 'Statut';

$_['column_date_added']     = 'date ajoutée';

$_['column_action']         = 'action';

/// label name 

$_['text_username']        = 'Nom d\'utilisateur';

$_['text_user_group']      = 'groupe d\'utilisateurs';

$_['text_password']        = 'Mot de passe';

$_['text_confirm']         = 'Confirmer';

$_['text_firstname']       = 'Prénom';

$_['text_lastname']        = 'Nom de famille';

$_['text_email']           = 'Email';

$_['text_image']           = 'Image';

$_['text_status']          = 'Statut';




// Entry

$_['entry_username']        = 'Nom d\'utilisateur';
$_['entry_user_group']      = 'groupe d\'utilisateurs';
$_['entry_password']        = 'Mot de passe';
$_['entry_confirm']         = 'Confirmer';
$_['entry_firstname']       = 'Prénom';
$_['entry_lastname']        = 'Nom de famille';
$_['entry_email']           = 'Email';
$_['entry_image']           = 'Image';
$_['entry_status']          = 'Statut';
$_['text_enabled']          = 'Activée';
$_['text_disabled']         = 'désactivé';


///btn 
$_['button_save']           = 'sauver';
$_['button_cancel']         = 'Annuler';

// Error

$_['error_permission']      = 'Avertissement: vous n\'êtes pas autorisé à modifier les utilisateurs.!';

$_['error_account']         = 'Attention: vous ne pouvez pas supprimer votre propre compte!';

$_['error_exists_username'] = 'Attention: le nom d\'utilisateur est déjà utilisé!';

$_['error_username']        = 'Le nom d\'utilisateur doit comporter entre 3 et 20 caractères.!';

$_['error_password']        = 'Le mot de passe doit comporter entre 4 et 20 caractères.!';

$_['error_confirm']         = 'Mot de passe et confirmation du mot de passe ne correspondent pas!';

$_['error_firstname']       = 'Le prénom doit comporter entre 1 et 32 caractères!';

$_['error_lastname']        = 'Le nom de famille doit comporter entre 1 et 32 caractères!';

$_['error_email']           = 'Adresse électronique ne semble pas être valide!';

$_['error_exists_email']    = 'Avertissement: l\'adresse e-mail est déjà enregistrée!';