<?php

// Heading
$_['heading_title']    = 'Payment Setting';

// Text

$_['text_extension']   = 'Payment Setting';
$_['text_success']     = 'Success: You have modified Payment Setting module!';
$_['text_edit']        = 'Edit Payment Setting Module';
$_['text_authorization']			 = 'Authorization';
$_['text_sale']						 = 'Sale'; 
$_['text_complete']						 = 'Complete'; 
$_['text_select']						 = '--Please Select--'; 
$_['tab_orderstatus']						 = 'Order Status'; 

// Lable
$_['label_email']					 = 'E-Mail';
$_['label_test']					 = 'Sandbox Mode';
$_['label_transaction']				 = 'Transaction Method';
$_['label_debug']					 = 'Debug Mode';
$_['label_total']					 = 'Total';
$_['label_canceled_reversal_status'] = 'Canceled Reversal Status';
$_['label_completed_status']		 = 'Completed Status';
$_['label_denied_status']			 = 'Denied Status';
$_['label_expired_status']			 = 'Expired Status';
$_['label_failed_status']			 = 'Failed Status';
$_['label_pending_status']			 = 'Pending Status';
$_['label_processed_status']		 = 'Processed Status';
$_['label_refunded_status']			 = 'Refunded Status';
$_['label_reversed_status']			 = 'Reversed Status';
$_['label_voided_status']			 = 'Voided Status';
$_['label_geo_zone']				 = 'Geo Zone';
$_['label_status']					 = 'Status';
$_['label_sort_order']				 = 'Sort Order';
$_['label_orderstatus']				 = 'Order Status';

// Entry
$_['entry_email']					 = 'E-Mail';
$_['entry_test']					 = 'Sandbox Mode';
$_['entry_transaction']				 = 'Transaction Method';
$_['entry_debug']					 = 'Debug Mode';
$_['entry_total']					 = 'Total';
$_['entry_canceled_reversal_status'] = 'Canceled Reversal Status';
$_['entry_completed_status']		 = 'Completed Status';
$_['entry_denied_status']			 = 'Denied Status';
$_['entry_expired_status']			 = 'Expired Status';
$_['entry_failed_status']			 = 'Failed Status';
$_['entry_pending_status']			 = 'Pending Status';
$_['entry_processed_status']		 = 'Processed Status';
$_['entry_refunded_status']			 = 'Refunded Status';
$_['entry_reversed_status']			 = 'Reversed Status';
$_['entry_voided_status']			 = 'Voided Status';
$_['entry_geo_zone']				 = 'Geo Zone';
$_['entry_status']					 = 'Status';
$_['entry_sort_order']				 = 'Sort Order';
$_['entry_orderstatus']				 = 'Order Status';

// Help
$_['help_test']						 = 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_debug']			    	 = 'Logs additional information to the system log';
$_['help_total']					 = 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission']				 = 'Warning: You do not have permission to modify payment PayPal!';
$_['error_email']					 = 'E-Mail required!';

