<?php
// Heading
$_['heading_title']          = 'Blog commentaar';

// Text
$_['text_success']           = 'Succes: je hebt de blogreactie aangepast!';
$_['text_list']              = 'Blog Commentarenlijst';
$_['text_add']               = 'Voeg blog commentaar toe';
$_['text_edit']              = 'Bewerk blog commentaar';
$_['text_default']           = 'Standaard';
$_['text_no_comment']        = 'Geen commentaar ';
$_['text_dash']              = 'Dashboard';
$_['text_cate']              = 'Categorie';
$_['text_comm']              = 'Commentaar';
$_['text_sett']              = 'instellingen';
$_['text_addmodule']         = 'Module toevoegen';
$_['text_none']              = '--Geen--';
$_['text_enabled']           = 'Ingeschakeld';
$_['text_disabled']          = 'invalide';

// Tab
$_['tab_comment']            = 'Comments';

// Column

$_['column_date_added']      = 'Datum toegevoegd';
$_['column_status']          = 'staat';
$_['column_image']           = 'Beeld';
$_['column_title']           = 'Blog Comment Name';
$_['column_sort_order']	     = 'sorteervolgorde';
$_['column_action']          = 'Actie';

$_['column_name']            = 'klantnaam';
$_['column_view']            = 'Keer bekeken';
$_['column_customer']        = 'Klant';
$_['column_comment']         = 'Commentaar';


// Entry
$_['entry_name']             = 'Blog Comment Name';
$_['entry_customername']     = 'Klant';
$_['entry_status']           = 'staat';
$_['entry_sort_order']       = 'sorteervolgorde';
$_['entry_image']            = 'Beeld';
$_['entry_tmdblog']          = 'Selecteer Blog';
$_['entry_date_added']       = 'Datum toegevoegd';
$_['entry_comment']          = 'Commentaar';


// Help
$_['help_tmdblog']           = 'autocomplete';
$_['help_bottom']            = 'Weergave in de onderste voettekst.';

// Error
$_['error_warning']          = 'Waarschuwing: controleer het formulier zorgvuldig op fouten!';
$_['error_permission']       = 'Waarschuwing: je hebt geen toestemming om het artikel te wijzigen!';
$_['error_title']            = 'Blog Comment naam moet tussen de 3 en 64 karakters zijn!';
$_['error_description']      = 'Beschrijving moet uit meer dan 3 tekens bestaan!';
$_['error_meta_title']       = 'Metatitel moet groter zijn dan 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-sleutelwoord al in gebruik!';
$_['error_account']          = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd omdat deze momenteel is toegewezen als de voorwaarden van de winkelaccount!';
$_['error_checkout']         = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd, omdat deze op dit moment is toegewezen als de winkelbetalingsvoorwaarden!';
$_['error_affiliate']        = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd, omdat deze op dit moment is toegewezen als voorwaarden voor de winkelfiliaal!';
$_['error_return']           = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd omdat deze momenteel is toegewezen als de winkelretourvoorwaarden!';
$_['error_store']            = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd omdat deze momenteel wordt gebruikt door% s-winkels!';