<?php
// Heading
$_['heading_title']                = 'BlogDashboard';

// Text
$_['text_order_total']             = 'Totaal bestellingen';
$_['text_customer_total']          = 'Totale klanten';
$_['text_sale_total']              = 'Totale verkoop';
$_['text_online_total']            = 'Mensen online';
$_['text_map']                     = 'Wereldkaart';
$_['text_sale']                    = 'Verkoopanalyses';
$_['text_activity']                = 'Recente activiteit';
$_['text_recent']                  = 'Laatste bestellingen';
$_['text_order']                   = 'bestellingen';
$_['text_customer']                = 'Klanten';
$_['text_day']                     = 'Vandaag';
$_['text_week']                    = 'Week';
$_['text_month']                   = 'Maand';
$_['text_year']                    = 'Jaar';
$_['text_view']                    = 'Bekijk meer...';
$_['text_dash']                    = 'Dashboard';
$_['text_blog']                    = 'blogs';
$_['text_cate']                    = 'Categorie';
$_['text_comm']                    = 'Commentaar';
$_['text_sett']                    = 'instellingen';
$_['text_addmodule']               = 'Module toevoegen';

// Error
$_['error_install']                = 'Waarschuwing: de map Installeren bestaat nog steeds en moet om veiligheidsredenen worden verwijderd!';