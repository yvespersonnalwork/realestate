<?php
// Heading
$_['heading_title']          = 'blog';

// Text
$_['text_success']           = 'Succes: je hebt Blog aangepast!';
$_['text_list']              = 'Blog lijst';
$_['text_add']               = 'Voeg een blog toe';
$_['text_edit']              = 'Bewerk blog';
$_['text_default']           = 'Standaard';
$_['entry_username']         = 'Gebruikersnaam';
$_['entry_blogcoment']       = 'Opmerkingen tonen';
$_['tab_comment']            = 'Comments';
$_['column_customer']        = 'Klant';
$_['column_comment']         = 'Commentaar';
$_['text_no_comment']        = 'Geen commentaar ';
$_['text_dash']                     = 'Dashboard';
$_['text_blog']                    = 'blogs';
$_['text_cate']                   = 'Categorie';
$_['text_comm']                    = 'Commentaar';
$_['text_sett']                    = 'instellingen';
$_['text_addmodule']                    = 'Module toevoegen';

// Column
$_['column_date_added']      = 'Datum toegevoegd';
$_['entry_date_added']       = 'Datum toegevoegd';
$_['column_status']          = 'staat';
$_['column_image']           = 'Beeld';
$_['column_title']           = 'Blognaam';
$_['column_sort_order']	     = 'sorteervolgorde';
$_['column_action']          = 'Actie';
$_['text_enabled']           = 'Ingeschakeld';
$_['text_disabled']          = 'invalide';
$_['column_name']            = 'Blognaam';
$_['column_view']            = 'Keer bekeken';


// Entry
$_['entry_name']            = 'Blognaam';
$_['entry_description']      = 'Omschrijving';
$_['entry_store']            = 'winkel';
$_['entry_meta_title'] 	     = 'Metatagtitel';
$_['entry_meta_keyword'] 	 = 'Metatag Trefwoorden';
$_['entry_meta_description'] = 'Metatag beschrijving';
$_['entry_keyword']          = 'SEO-sleutelwoord';
$_['entry_bottom']           = 'Bodem';
$_['entry_status']           = 'staat';
$_['entry_sort_order']       = 'sorteervolgorde';
$_['entry_layout']           = 'Layout Override';
$_['entry_image']            = 'Beeld';
$_['entry_tag']              = 'Tags';
$_['text_none']              = '--Geen--';
$_['entry_tmdblogcategory']  = 'Tmd BlogCategory';
$_['help_tmdblogcategory']   = 'autocomplete';

// Help
$_['help_keyword']           = 'Gebruik geen spaties, maar vervang spaties door - en zorg ervoor dat het zoekwoord wereldwijd uniek is.';
$_['help_bottom']            = 'Weergave in de onderste voettekst.';

// Error
$_['error_warning']          = 'Waarschuwing: controleer het formulier zorgvuldig op fouten!';
$_['error_permission']       = 'Waarschuwing: je hebt geen toestemming om het artikel te wijzigen!';
$_['error_title']            = 'Blognaam moet tussen 3 en 64 tekens lang zijn!';
$_['error_description']      = 'Beschrijving moet uit meer dan 3 tekens bestaan!';
$_['error_meta_title']       = 'Metatitel moet groter zijn dan 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-sleutelwoord al in gebruik!';
$_['error_account']          = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd omdat deze momenteel is toegewezen als de voorwaarden van de winkelaccount!';
$_['error_checkout']         = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd, omdat deze op dit moment is toegewezen als de winkelbetalingsvoorwaarden!';
$_['error_affiliate']        = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd, omdat deze op dit moment is toegewezen als voorwaarden voor de winkelfiliaal!';
$_['error_return']           = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd omdat deze momenteel is toegewezen als de winkelretourvoorwaarden!';
$_['error_store']            = 'Waarschuwing: deze artikelpagina kan niet worden verwijderd omdat deze momenteel wordt gebruikt door% s-winkels!';