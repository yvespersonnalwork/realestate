<?php
// Heading
$_['heading_title']          = 'Blog instelling';

// Text
$_['text_success']           = 'Succes: u heeft gewijzigde categorieën!';
$_['text_list']              = 'Tmd Blog instellingenlijst';
$_['text_add']               = 'Voeg Tmd Blog Setting toe';
$_['text_edit']              = 'Bewerk Tmd Blog-instelling';
$_['text_default']           = 'Standaard';
$_['text_searchpage']        = 'Zoekpagina';
$_['text_categorypage']      = 'Blogcategorie';
$_['text_relatedblog']       = 'Gerelateerde pagina';
$_['text_blogpage']          = 'Blogpagina';
$_['text_listpage']          = 'Listingpagina';
$_['text_page']              = 'Pagina';

// Column
$_['column_name']            = 'Blog Titel';
$_['column_sort_order']      = 'sorteervolgorder';
$_['column_action']          = 'Actie';

// Entry
$_['entry_commentlimit']     = 'Begrenzing';
$_['entry_name']             = 'Blog Titel';
$_['entry_description']      = 'Omschrijving';
$_['entry_meta_title'] 	     = 'Titel';
$_['entry_meta_keyword'] 	 = 'trefwoorden';
$_['entry_meta_description'] = 'Omschrijving';
$_['entry_keyword']          = 'SEO Keyword';
$_['entry_parent']           = 'Ouder';
$_['entry_filter']           = 'filters';
$_['entry_store']            = 'winkel';
$_['entry_image']            = 'Beeld';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'columns';
$_['entry_sort_order']       = 'sorteervolgorde';
$_['entry_titlesize']        = 'Blog titel grootte';
$_['entry_titlecolor']       = 'Tekstkleur blogtitel';
$_['entry_thumbimg']         = 'Commentaar Banner Duimgrootte';
$_['entry_comntbanner']      = 'Bannergrootte opmerking';
$_['text_blogcommentsglobal']= 'Globaly Blogreacties weergeven / verbergen';
$_['entry_blogcomments']     = 'Blog Reacties';
$_['help_blogcomment']       = 'Selecteer een optie om alle blogs te verbergen en weer te geven';

$_['entry_article']          = 'blog';
$_['entry_google']           = 'Google delen';
$_['entry_pinterest']        = 'Pinterest delen';
$_['entry_twitter']          = 'Twitter delen';
$_['entry_facebook']         = 'Facebook delen';
$_['entry_feedbackrow']      = 'Feedback doos';
$_['entry_descp']            = 'Omschrijving';
$_['entry_articleimg']       = 'BeschrijvingBlogafbeelding';
$_['entry_width']            = 'Breedte';
$_['entry_height']           = 'Hoogte';

$_['entry_descptextcolor']      = 'Beschrijving Tekstkleur';
$_['entry_articletextcolor']    = 'Blog Tekstkleur';
$_['entry_postboxbgcolor']      = 'Blog Box achtergrondkleur';
$_['entry_containerbgcolor']    = 'Blog Container achtergrondkleur';
$_['entry_titlebgcolor']     	= 'Blogthema Kleur';
$_['entry_themehovercolor']     = 'Blogthema Hover Color';
$_['entry_titletextcolor']      = 'Tekstkleur aanwijzen';
$_['entry_blogtextcolor']       = 'Kop zweven Tekstkleur';
$_['entry_bloglayout']    	    = 'lay-out';
$_['entry_blogperpage']      	= 'Pagina';
$_['entry_blogperrow']    	    = 'Rij';
$_['entry_dispalytitle']    	= 'Titel';
$_['entry_dispalydes']       	= 'Omschrijving';
$_['entry_blogthumbsize']    	= 'Duimgrootte';
$_['entry_dispalyblogtimg']    	= 'Beeld';
$_['entry_keyword']          = 'SEO Keyword';

$_['help_meta_title'] 	    	= 'Metatagtitel';
$_['help_meta_keyword'] 		= 'Metatag Trefwoorden';
$_['help_meta_description'] 	= 'Metatag beschrijving';
$_['help_blogthumbsize']   		= 'Blog Blog Thumb<br/>Size(W*H)';

$_['help_article']           	= 'Schakel blog in of uit';
$_['help_google']           	= 'Schakel Google Share in of uit';
$_['help_pinterest']            = 'Schakel Pinterest share in of uit';
$_['help_twitter']           	= 'Schakel Twitter in of uit';
$_['help_facebook']           	= 'Schakel Facebook in of uit';
$_['help_feedbackrow']          = 'Feedback box in- of uitschakelen';
$_['help_descp']           		= 'Schakel Beschrijving in of uit';
$_['help_articleimg']           = 'Schakel blogafbeelding in of uit';
$_['help_keyword']           = 'Gebruik geen spaties, maar vervang spaties door - en zorg ervoor dat het zoekwoord wereldwijd uniek is.';

$_['tab_setting']           	= 'omgeving';
$_['tab_color']             	= 'Kleur aanpassen';
$_['tab_main']             		= 'Algemeen';
$_['tab_blogseting']            = 'Blog instelling';
$_['tab_support']               = 'Ondersteuning';

// Help
$_['help_filter']            	= '(autocomplete)';
$_['help_keyword']           	= 'Gebruik geen spaties, maar vervang spaties door - en zorg ervoor dat het zoekwoord wereldwijd uniek is.';
$_['help_top']               	= 'Weergeven in de bovenste menubalk. Werkt alleen voor de beste oudercategorieën.';
$_['help_column']            	= 'Aantal kolommen dat moet worden gebruikt voor de onderste 3 categorieën. Werkt alleen voor de beste oudercategorieën.';

// Error
$_['error_warning']          	= 'Waarschuwing: controleer het formulier zorgvuldig op fouten!';
$_['error_permission']       	= 'Waarschuwing: u bent niet gemachtigd om categorieën te wijzigen!';
$_['error_name']             	= 'De blogtitel moet tussen de 2 en 32 tekens lang zijn!';
$_['error_meta_title']       	= 'Meta-titel van blog moet groter zijn dan 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-sleutelwoord al in gebruik!';