<?php
// Heading
$_['heading_title']          = 'Blogcategorie';

// Text
$_['text_success']           = 'Succes: u hebt de tmdblogcategorie aangepast!';
$_['text_list']              = 'TMD Blog Categorielijst';
$_['text_add']               = 'Voeg TMD-blogcategorie toe';
$_['text_edit']              = 'Bewerk TMD-blogcategorie';
$_['text_default']           = 'Standaard';

// Column
$_['column_date_added']      = 'Datum toegevoegd';
$_['entry_date_added']       = 'Datum toegevoegd';
$_['column_status']          = 'staat';
$_['column_image']           = 'Beeld';
$_['column_title']           = 'TMD Blog Categorie Titel';
$_['column_sort_order']	     = 'sorteervolgorde';
$_['column_action']          = 'Actie';
$_['text_enabled']           = 'Ingeschakeld';
$_['text_disabled']          = 'invalide';
$_['column_name']            = 'Blog Categorienaam';
$_['column_count']           = 'tellen';


// Entry
$_['entry_name']            = 'Blog Categorienaam';
$_['entry_description']      = 'Omschrijving';
$_['entry_store']            = 'winkel';
$_['entry_meta_title'] 	     = 'Metatagtitel';
$_['entry_meta_keyword'] 	 = 'Metatag Trefwoorden';
$_['entry_meta_description'] = 'Metatag beschrijving';
$_['entry_keyword']          = 'SEO-sleutelwoord';
$_['entry_bottom']           = 'Bodem';
$_['entry_status']           = 'staat';
$_['entry_sort_order']       = 'sorteervolgorde';
$_['entry_layout']           = 'Layout Override';
$_['entry_image']            = 'Beeld';
$_['entry_tag']              = 'Tags';
$_['text_none']              = '--Geen--';
$_['entry_parent']           = 'Ouder';

// Help
$_['help_keyword']           = 'Gebruik geen spaties, maar vervang spaties door - en zorg ervoor dat het zoekwoord wereldwijd uniek is.';
$_['help_bottom']            = 'Weergave in de onderste voettekst.';

// Error
$_['error_warning']          = 'Waarschuwing: controleer het formulier zorgvuldig op fouten!';
$_['error_permission']       = 'Waarschuwing: u bent niet gemachtigd om tmdblogcategory te wijzigen!';
$_['error_title']            = 'TMD Blog Categorie Titel moet tussen 3 en 64 tekens lang zijn!';
$_['error_description']      = 'Beschrijving moet uit meer dan 3 tekens bestaan!';
$_['error_meta_title']       = 'Metatitel moet groter zijn dan 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-sleutelwoord al in gebruik!';
$_['error_account']          = 'Waarschuwing: deze tmdblogcategory-pagina kan niet worden verwijderd omdat deze momenteel is toegewezen als de voorwaarden van de winkelaccount!';
$_['error_checkout']         = 'Waarschuwing: deze tmdblogcategory-pagina kan niet worden verwijderd, omdat deze momenteel is toegewezen als de winkelbetalingsvoorwaarden!';
$_['error_affiliate']        = 'Waarschuwing: deze tmdblogcategory-pagina kan niet worden verwijderd, omdat deze momenteel is toegewezen als voorwaarden voor de winkelfiliaal!';
$_['error_return']           = 'Warning: This tmdblogcategory page cannot be deleted as it is currently assigned as the store return terms!';
$_['error_store']            = 'Waarschuwing: deze tmdblogcategory-pagina kan niet worden verwijderd omdat deze momenteel wordt gebruikt door% s-winkels!';