<?php
// Heading
$_['heading_title']    = 'SEO URL';

// Text
$_['text_success']     = 'Succes: u heeft de SEO-URL gewijzigd!';
$_['text_list']        = 'URL-URL van zoekmachine';
$_['text_add']         = 'Voeg een SEO URL toe';
$_['text_edit']        = 'Bewerk de URL van de zoekmachine';
$_['text_filter']      = 'Filter';
$_['text_default']     = 'Standaard';

// Column
$_['column_query']     = 'vraag';
$_['column_keyword']   = 'keyword';
$_['column_store']     = 'Op te slaan';
$_['column_language']  = 'Taal';
$_['column_action']    = 'Action';

// Entry
$_['entry_query']      = 'Query';
$_['entry_keyword']    = 'Keyword';
$_['entry_store']      = 'Op te slaan';
$_['entry_language']   = 'Taal';

// Error
$_['error_permission'] = 'Waarschuwing: u bent niet gemachtigd om banners aan te passen!';
$_['error_query']      = 'Zoekopdracht moet tussen 3 en 64 tekens zijn!';
$_['error_keyword']    = 'Zoekwoord moet tussen 3 en 64 tekens lang zijn!';
$_['error_exists']     = 'Zoekwoord al in gebruik!';
