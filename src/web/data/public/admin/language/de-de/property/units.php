<?php

// Heading

$_['heading_title']    = 'Property Units';

// Text

$_['text_success']     = 'Succes: u hebt eigenschapseenheden gewijzigd!';
$_['text_list']        = 'Lijst met eigenschapseenheden';
$_['text_add']         = 'Voeg eigendommen toe';
$_['text_edit']        = 'Bewerk eigendommen';
$_['text_success']     = 'succesvol: voeg uw eigenschapseenheid toe!';
$_['text_successedit'] = 'met succes: uw aangepaste eigenschapseenheid';
$_['text_successdelete']= 'succesvol: uw property-unit Delete';

// Column
$_['column_name']      = 'Naam van eigenschapeenheid';
$_['column_action']    = 'Actie';

// Lable 
$_['lable_name']       = 'Naam van eigenschapeenheid';

// Entry
$_['entry_name']       = 'Naam van eigenschapeenheid';



// Error

$_['error_permission'] = 'Waarschuwing: u bent niet gemachtigd om property-eenheden te wijzigen!';
$_['error_name']       = 'Eigenschap Status Naam moet tussen 3 en 32 karakters zijn!';
$_['error_default']    = 'Waarschuwing: deze eigenschap kan niet worden verwijderd omdat deze momenteel is toegewezen als de standaard winkeleigenschapstatus!';
