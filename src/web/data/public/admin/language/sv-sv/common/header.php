<?php
// Heading
$_['heading_title']          = 'Fastighet';

// Text
$_['text_order']             = 'Order';
$_['text_processing_status'] = 'bearbetning';
$_['text_complete_status']   = 'Avslutad';
$_['text_customer']          = 'kunder';
$_['text_online']            = 'Kunder Online';
$_['text_approval']          = 'Väntar på godkännande';
$_['text_product']           = 'Produkter';
$_['text_stock']             = 'Slut i centrallagret';
$_['text_review']            = 'recensioner';
$_['text_return']            = 'Returer';
$_['text_affiliate']         = 'Affiliates';
$_['text_store']             = 'butiker';
$_['text_front']             = 'Webbsida Front';
$_['text_help']              = 'Hjälp';
$_['text_homepage']          = 'RealEstate Hemsida';
$_['text_support']           = 'Supportforum';
$_['text_documentation']     = 'Dokumentation';
$_['text_logout']            = 'Logga ut';
