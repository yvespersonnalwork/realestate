<?php
// Heading
$_['heading_title']    = 'Bildhanteraren';

// Text
$_['text_uploaded']    = 'Framgång: Din fil har laddats upp!';
$_['text_directory']   = 'Framgång: Katalog skapad!';
$_['text_delete']      = 'Framgång: Din fil eller katalog har tagits bort!';

// Entry
$_['entry_search']     = 'Sök..';
$_['entry_folder']     = 'Mapp namn';

// Error
$_['error_permission'] = 'Varning: Tillstånd nekad!';
$_['error_filename']   = 'Varning: Filnamnet måste vara mellan 3 och 255!';
$_['error_folder']     = 'Varning: Mappnamn måste vara mellan 3 och 255!';
$_['error_exists']     = 'Varning: En fil eller katalog med samma namn finns redan!';
$_['error_directory']  = 'Varning: Katalog finns inte!';
$_['error_filesize']   = 'Varning: Felaktig filstorlek!';
$_['error_filetype']   = 'Varning: Fel filtyp!';
$_['error_upload']     = 'Varning: Filen kunde inte laddas upp av okänd anledning!';
$_['error_delete']     = 'Varning: Du kan inte radera den här katalogen!';
