<?php

// Text

$_['text_blogdashboard']               = 'Blog Dashboard';
///left side menu
$_['text_setting']                   = 'inställningar';
$_['text_themecontrol'] 		 	 = 'Temakontroll';
$_['text_backup']                    = 'Säkerhetskopiering / återställning';
$_['text_seo_url']                     = 'Seo url';
$_['text_property']                  = 'Fast egendom'; 
$_['text_category']                  = 'Listningstyp / Kategori';
$_['text_order_status']              = 'Fastighetsstatistik';
$_['text_feature'] 		 			 = 'Funktioner';
$_['text_nearest'] 		 			 = 'Närmaste plats';
$_['text_units'] 		 			 = 'Enheter';
$_['text_enquiry'] 		 			 = 'Fastighetsförfrågan';
$_['text_agent']                     = 'Ombud';
$_['text_custom_field']              = 'Custom Field';
$_['text_contact']                   = 'Post';
$_['text_catalog']                   = 'Katalog';
$_['text_country']                   = 'Länder';
$_['text_coupon']                    = 'kuponger';
$_['text_currency']                  = 'Valutor';
$_['text_customer_group']            = 'Kundgrupper';
$_['text_dashboard']                 = 'instrumentbräda';
$_['text_design']                    = 'Design';
$_['text_download']                  = 'Nedladdningar';
$_['text_log']                       = 'Felloggar';
$_['text_event']                     = 'evenemang';
$_['text_extension']                 = 'Extensions';
$_['text_filter']                    = 'filter';
$_['text_geo_zone']                  = 'Geo Zones';
$_['text_information']               = 'Informationssidor';
$_['text_installer']                 = 'Extension Installer';
$_['text_language']                  = 'språk';
$_['text_localisation']              = 'Lokalisering';
$_['text_location']                  = 'Webbplats Plats';
$_['text_contact']                   = 'Post';
$_['text_marketing']                 = 'marknadsföring';
$_['text_menu']                      = 'marknadsföring...';
$_['text_modification']              = 'ändringar';
$_['text_manufacturer']              = 'tillverkare';
$_['text_option']                    = 'alternativ';
$_['text_order']                     = 'Order';
$_['text_seo_url']                     = 'Seo url';

$_['text_product']                   = 'Produkter';

$_['text_reports']                   = 'Produkter';

$_['text_report_sale']               = 'försäljning';

$_['text_report_sale_order']         = 'Order';

$_['text_report_sale_tax']           = 'Beskatta';

$_['text_report_sale_shipping']      = 'Frakt';

$_['text_report_sale_return']        = 'Returer';

$_['text_report_sale_coupon']        = 'kuponger';

$_['text_report_product']            = 'Produkter';

$_['text_report_product_viewed']     = 'Tittade';

$_['text_report_product_purchased']  = 'Köpt';

$_['text_report_customer']           = 'kunder';

$_['text_report_customer_activity']  = 'Kundaktivitet';

$_['text_report_customer_search']    = 'Kundsökningar';

$_['text_report_customer_online']    = 'Kunder Online';

$_['text_report_customer_order']     = 'Order';

$_['text_report_customer_reward']    = 'Belöningspoäng';

$_['text_report_customer_credit']    = 'Kreditera';

$_['text_report_marketing']          = 'marknadsföring';

$_['text_report_affiliate']          = 'Affiliates';

$_['text_report_affiliate_activity'] = 'Affiliateaktivitet';

$_['text_review']                    = 'recensioner';

$_['text_return']                    = 'Returer';

$_['text_return_action']             = 'Returåtgärder';

$_['text_return_reason']             = 'Återvända skäl';

$_['text_return_status']             = 'Returer';

$_['text_sale']                      = 'försäljning';

$_['text_setting']                   = 'inställningar';

$_['text_store']                     = 'Extensionswebbplats';

$_['text_stock_status']              = 'Lagerstatus';



$_['text_tax']                       = 'Skatter';

$_['text_tax_class']                 = 'Skatteklasser';

$_['text_tax_rate']                  = 'Skatteklasser';

$_['text_translation']               = 'Språkredigerare';

$_['text_theme']                     = 'Tema Editor';

$_['text_tools']                     = 'Verktyg';

$_['text_upload']                    = 'uppladdningar';

$_['text_user']                      = 'användare';

$_['text_voucher']                   = 'Presentkort';

$_['text_voucher_theme']             = 'Kupong teman';

$_['text_weight_class']              = 'Viktklasser';

$_['text_length_class']              = 'Längd klasser';

$_['text_zone']                      = 'zoner';

$_['text_recurring']                 = 'Återkommande profiler';

$_['text_order_recurring']           = 'Återkommande beställningar';

$_['text_openbay_extension']         = 'OpenBay Pro';

$_['text_openbay_dashboard']         = 'instrumentbräda';

$_['text_openbay_orders']            = 'Bulkorderuppdatering';

$_['text_openbay_items']             = 'Hantera objekt';

$_['text_openbay_ebay']              = 'eBay';

$_['text_openbay_amazon']            = 'amason (EU)';

$_['text_openbay_amazonus']          = 'amason (US)';

$_['text_openbay_etsy']            	 = 'Etsy';

$_['text_openbay_settings']          = 'inställningar';

$_['text_openbay_links']             = 'Artikellänkar';

$_['text_openbay_report_price']      = 'Prissättningsrapport';

$_['text_openbay_order_import']      = 'Beställ import';

$_['text_paypal']                    = 'PayPal';

$_['text_paypal_search']             = 'Sök';

$_['text_complete_status']           = 'Beställningar Slutförda'; 

$_['text_processing_status']         = 'Order Processing'; 

$_['text_other_status']              = 'Övriga Statusser'; 





$_['text_property']                  = 'Fast egendom'; 

$_['text_order_status']              = 'Fastighetsstatistik';

$_['text_category']                  = 'Kategori Lista / Kategori';

$_['text_customer']                  = 'Fastighetsnoteringskund';

$_['text_custom_field']              = 'Anpassat fält';

$_['text_agent']                     = 'Ombud';
$_['text_mail']                      = 'Agent Mail Template';

$_['text_users']                     = 'Användare';

$_['text_user_group']                = 'Användargrupper';

$_['text_pagebuilder']              = 'Page Builder';

$_['text_layout']                    = 'Inställning av sidlayout';

$_['text_banner']                    = 'Baner';

$_['text_module']                    = 'Modul inställning';

$_['text_system']                    = 'Miljö';

$_['text_backup']                    = 'Säkerhetskopiering / återställning';

$_['text_pages']                     = 'sidor';

$_['text_membership']                = 'Medlemskap';

$_['text_plans']                     = 'Plans';

$_['text_variation']                 = 'Paket';

$_['text_megaheader1']               = 'Mega Header';

$_['text_megaheader']                = 'Mega Header Meny';

$_['text_newssubscribe']             = 'Nyhetsbrev Abonnenter';

$_['text_faqcat']   				 = 'Faq Kategori';		

$_['text_faq']     					 = 'faq';

$_['text_testimonial']               = 'Rekommendation';

$_['text_photo']          			 = 'foton';

$_['text_gallerydashboard'] 		 = 'Galleri Dashboard';

$_['text_feature'] 		 			 = 'Funktioner';

$_['text_nearest'] 		 			 = 'Närmaste plats';

$_['text_enquiry'] 		 			 = 'Fastighetsförfrågan';

$_['text_agents'] 		 			 = 'Fastighetsmäklare';

$_['text_themecontrol'] 		 			 = 'Temakontroll';


/* lanuge add */
$_['text_analytics'] 		 			 = 'google & fb analytics';



