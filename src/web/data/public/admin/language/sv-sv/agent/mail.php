<?php
// Heading
$_['heading_title']     	 = 'Agent Mails Mall';

// Text
$_['text_success']       	 = 'Framgång: Du har ändrat !';
$_['text_list']           	 = 'E-postmallista för att skicka Agent';
$_['text_add']          	 = 'Lägg till Mail';
$_['text_edit']         	 = 'Redigera Mail';
$_['text_default']      	 = 'Standard';
$_['text_enable']      	     = 'Gör det möjligt';
$_['text_disable']      	 = 'inaktivera';
$_['text_agentregister']     = 'ombud_Registrera_post';
$_['text_agentapproved']     = 'ombud_godkänd_post';
$_['text_agentproperty']     = 'ombud_Lägg till_fast egendom_administration_post';
$_['text_propertyapp']       = 'fast egendom_godkänd_post';
$_['text_agentforgotte']     = 'ombud_glömt_post';

// Column
$_['column_name']       	 = 'namn';
$_['column_date']        	 = 'datum tillagt';
$_['column_action']      	 = 'Verkan';

// Lable
$_['lable_subject']          = 'Ämne';
$_['lable_message']          = 'Meddelande';
$_['lable_name']             = 'namn';
$_['lable_status']           = 'Status';
$_['lable_type']             = 'Typ';

// Entry
$_['entry_subject']          = 'Ämne';
$_['entry_message']          = 'Meddelande';
$_['entry_name']             = 'namn';
$_['entry_status']           = 'Status';
$_['entry_type']             = 'Typ';

/// Tab
$_['tab_mail']               = 'Post';
$_['tab_info']               = 'Info';

// Error
$_['error_warning'] 		 = 'Varning: Vänligen kontrollera formuläret noggrant för fel!';
$_['error_permission'] 		 = 'Varning: Du har inte behörighet att ändra tillverkare!';
$_['error_name']             = 'Namnet måste vara mellan 2 och 64 tecken!';
$_['error_type']             = 'Vänligen välj posttyp';