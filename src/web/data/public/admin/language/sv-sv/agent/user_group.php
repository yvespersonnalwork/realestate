<?php
// Heading
$_['heading_title']    = 'Användargrupper';

// Text
$_['text_success']     = 'Framgång: Du har ändrat användargrupper!';
$_['text_list']        = 'Användargrupp';
$_['text_add']         = 'Lägg till användargrupp';
$_['text_edit']        = 'Redigera användargrupp';

// Column
$_['column_name']      = 'Användargruppsnamn';
$_['column_action']    = 'Verkan';

// Entry
$_['entry_name']       = 'Användargruppsnamn';
$_['entry_access']     = 'Access Permission';
$_['entry_modify']     = 'Ändra Tillstånd';

// Error
$_['error_permission'] = 'Varning: Du har inte behörighet att ändra användargrupper!';
$_['error_name']       = 'Användarnamnenamn måste vara mellan 3 och 64 tecken!';
$_['error_user']       = 'Varning: Den här användargruppen kan inte raderas som den för närvarande är tilldelad till% s-användare!';