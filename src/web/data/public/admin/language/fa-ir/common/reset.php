<?php
// header
$_['heading_title']  = 'Ferfange jo wachtwurd';

// Text
$_['text_password']  = 'Fier it nije wachtwurd yn wêr jo wolle brûke.';
$_['text_success']   = 'Sesje: Jo wachtwurd is tagonklik makke.';

// Entry
$_['entry_password'] = 'Wachtwurd';
$_['entry_confirm']  = 'Befêstigje';

// Error
$_['error_password'] = 'Wachtwurd moat tusken 4 en 20 karakters wêze!';
$_['error_confirm']  = 'Wachtwurd en wachtwurd befetsje net oerien!';