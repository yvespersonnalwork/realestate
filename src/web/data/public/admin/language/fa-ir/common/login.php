<?php
// header
$_['heading_title']  = 'Bestjoer';

// Text
$_['text_heading']   = 'Bestjoer';
//xml update
//$_['text_login']     = 'Fier jo login details yn.';
$_['text_login']     = 'Oanmelde foar jo tagong ta jo akkount!';
//xml update
$_['text_forgotten'] = 'Forgotten Password';

// Entry
$_['entry_username'] = 'Brûkersnamme';
$_['entry_password'] = 'Wachtwurd';

// Button
$_['button_login']   = 'Ynlogge';

// Error
$_['error_login']    = 'Gjin match foar brûkers en / of wachtwurd.';
$_['error_token']    = 'Unjildich token sesje. Registrearje asjebleaft.';