<?php

// Text

$_['text_blogdashboard']               = 'Blog Dashboard';
///left side menu
$_['text_setting']                   = 'Ynstellings';
$_['text_themecontrol'] 		 	 = 'Theme Control';
$_['text_backup']                    = 'Backup / Restore';
$_['text_seo_url']                     = 'Seo url';
$_['text_property']                  = 'Besit'; 
$_['text_category']                  = 'Listing type / kategory';
$_['text_order_status']              = 'Eigenskippen Statuses';
$_['text_feature'] 		 			 = 'Eigenskippen';
$_['text_nearest'] 		 			 = 'Wetter - Agrarwetter';
$_['text_units'] 		 			 = 'Units';
$_['text_enquiry'] 		 			 = 'Property inquiry';
$_['text_agent']                     = 'Property Agent';
$_['text_custom_field']              = 'Custom Field';
$_['text_contact']                   = 'Post';
$_['text_catalog']                   = 'Katalogus';
$_['text_country']                   = 'Lannen';
$_['text_coupon']                    = 'Coupons';
$_['text_currency']                  = 'Currencies';
$_['text_customer_group']            = 'Kwaliteitsgroepen';
$_['text_dashboard']                 = 'Dashboard';
$_['text_design']                    = 'DashboardDesign';
$_['text_download']                  = 'Downloads';
$_['text_log']                       = 'Flater logs';
$_['text_event']                     = 'Eveneminten';
$_['text_extension']                 = 'Extensions';
$_['text_filter']                    = 'Filters';
$_['text_geo_zone']                  = 'Geo Zonen';
$_['text_information']               = 'Ynformaasje siden';
$_['text_installer']                 = 'Extension Installer';
$_['text_language']                  = 'Talen';
$_['text_localisation']              = 'Lokaasje';
$_['text_location']                  = 'Webside lokaasje';
$_['text_contact']                   = 'Post';
$_['text_marketing']                 = 'Marketing';
$_['text_menu']                      = 'Menu Manager';
$_['text_modification']              = 'Feroarings';
$_['text_manufacturer']              = 'Makkers';
$_['text_option']                    = 'Opsjes';
$_['text_order']                     = 'Orders';
$_['text_seo_url']                     = 'Seo url';

$_['text_product']                   = 'Products';

$_['text_reports']                   = 'Reports';

$_['text_report_sale']               = 'Ferkeap';

$_['text_report_sale_order']         = 'Orders';

$_['text_report_sale_tax']           = 'Belesting';

$_['text_report_sale_shipping']      = 'Wetter - Agrarwetter';

$_['text_report_sale_return']        = 'Returns';

$_['text_report_sale_coupon']        = 'Coupons';

$_['text_report_product']            = 'Products';

$_['text_report_product_viewed']     = 'Viewed';

$_['text_report_product_purchased']  = 'Purchased';

$_['text_report_customer']           = 'Klanten';

$_['text_report_customer_activity']  = 'Klantaktiviteit';

$_['text_report_customer_search']    = 'Customer Searches';

$_['text_report_customer_online']    = 'Klanten Online';

$_['text_report_customer_order']     = 'Orders';

$_['text_report_customer_reward']    = 'Belangpunten';

$_['text_report_customer_credit']    = 'Kredyt';

$_['text_report_marketing']          = 'Marketing';

$_['text_report_affiliate']          = 'Affiliates';

$_['text_report_affiliate_activity'] = 'Affiliate Activity';

$_['text_review']                    = 'Reaksjes';

$_['text_return']                    = 'Returns';

$_['text_return_action']             = 'Return Actions';

$_['text_return_reason']             = 'Return Reasons';

$_['text_return_status']             = 'Return Statuses';

$_['text_sale']                      = 'Ferkeap';

$_['text_setting']                   = 'Ynstellings';

$_['text_store']                     = 'Extension Website';

$_['text_stock_status']              = 'Stock Statuses';



$_['text_tax']                       = 'Wetter - Agrarwetter';

$_['text_tax_class']                 = 'Tax Classes';

$_['text_tax_rate']                  = 'Tax Rates';

$_['text_translation']               = 'Language Editor';

$_['text_theme']                     = 'Tema-editor';

$_['text_tools']                     = 'Tools';

$_['text_upload']                    = 'Uploads';

$_['text_user']                      = 'Users';

$_['text_voucher']                   = 'Gift Gutschees';

$_['text_voucher_theme']             = 'Gûverneur Tema's';

$_['text_weight_class']              = 'Gewichtklassen';

$_['text_length_class']              = 'Length Classes';

$_['text_zone']                      = 'Sônes';

$_['text_recurring']                 = 'Wurkwize profilen';

$_['text_order_recurring']           = 'Wurkwize';

$_['text_openbay_extension']         = 'OpenBay Pro';

$_['text_openbay_dashboard']         = 'Dashboard';

$_['text_openbay_orders']            = 'Bulk oarder update';

$_['text_openbay_items']             = 'Kontrôle items';

$_['text_openbay_ebay']              = 'eBay';

$_['text_openbay_amazon']            = 'Amazon (EU)';

$_['text_openbay_amazonus']          = 'Amazon (US)';

$_['text_openbay_etsy']            	 = 'Etsy';

$_['text_openbay_settings']          = 'Ynstellings';

$_['text_openbay_links']             = 'Item links';

$_['text_openbay_report_price']      = 'Pricing rapport';

$_['text_openbay_order_import']      = 'Bestjoeren ymportearje';

$_['text_paypal']                    = 'PayPal';

$_['text_paypal_search']             = 'Sykje';

$_['text_complete_status']           = 'Orden foltôge'; 

$_['text_processing_status']         = 'Orders Processing'; 

$_['text_other_status']              = 'Oare Statuses'; 





$_['text_property']                  = 'Besit'; 

$_['text_order_status']              = 'Eigenskippen Statuses';

$_['text_category']                  = 'List fan kategory / kategory';

$_['text_customer']                  = 'Eigenskippen Listing Kunde';

$_['text_custom_field']              = 'Custom Field';

$_['text_agent']                     = 'Agint';
$_['text_mail']                      = 'Agent Mail Template';

$_['text_users']                     = 'Brûker';

$_['text_user_group']                = 'Brûkersgroepen';

$_['text_pagebuilder']              = 'Page Builder';

$_['text_layout']                    = 'Side-ôfbylding ynstelle';

$_['text_banner']                    = 'Banner';

$_['text_module']                    = 'Module Setting';

$_['text_system']                    = 'Setting';

$_['text_backup']                    = 'Backup / Restore';

$_['text_pages']                     = 'Siden';

$_['text_membership']                = 'Lidmaatskip';

$_['text_plans']                     = 'Plannen';

$_['text_variation']                 = 'Pakket';

$_['text_megaheader1']               = 'Mega Header';

$_['text_megaheader']                = 'Mega Header Menu';

$_['text_newssubscribe']             = 'Newsletter Subscribers';

$_['text_faqcat']   				 = 'Faq Category';		

$_['text_faq']     					 = 'Faq';

$_['text_testimonial']               = 'Testimonial';

$_['text_photo']          			 = 'Foto's';

$_['text_gallerydashboard'] 		 = 'Gallery Dashboard';

$_['text_feature'] 		 			 = 'Eigenskippen';

$_['text_nearest'] 		 			 = 'Wetter - Agrarwetter';

$_['text_enquiry'] 		 			 = 'Property inquiry';

$_['text_agents'] 		 			 = 'Property inquiry';

$_['text_themecontrol'] 		 			 = 'Theme Control';


/* lanuge add */
$_['text_analytics'] 		 			 = 'google & fb analytics';



