<?php
// Heading
$_['heading_title']    = 'Image Manager';

// Text
$_['text_uploaded']    = 'Súkses: Jo bestân is uploadd!';
$_['text_directory']   = 'Súkses: Directory is makke!';
$_['text_delete']      = 'Súkses: jo triem of map is wiske!';

// Entry
$_['entry_search']     = 'Sykje..';
$_['entry_folder']     = 'Mapnamme';

// Error
$_['error_permission'] = 'Warskôging: tastimming jûn!';
$_['error_filename']   = 'Warskôging: triemnamme moat tusken 3 en 255 wêze!';
$_['error_folder']     = 'Warskôging: Mapnamme moat tusken 3 en 255 wêze!';
$_['error_exists']     = 'Warskôging: In triem of triemtafel mei deselde namme bestiet al!';
$_['error_directory']  = 'Warskôging: Directory bestiet net!';
$_['error_filesize']   = 'Warskôging: Unjildige triemgrutte!';
$_['error_filetype']   = 'Warskôging: Unjildige triemtype!';
$_['error_upload']     = 'Warskôging: Triem koe net opnommen wurde foar in ûnbekende reden!';
$_['error_delete']     = 'Warskôging: Jo kinne dizze map net wiskje!';
