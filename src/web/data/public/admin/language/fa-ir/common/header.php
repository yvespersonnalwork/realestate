<?php
// Heading
$_['heading_title']          = 'Ûnreplik goed';

// Text
$_['text_order']             = 'Orders';
$_['text_processing_status'] = 'Bewurking';
$_['text_complete_status']   = 'Fertelt';
$_['text_customer']          = 'Klanten';
$_['text_online']            = 'Klanten Online';
$_['text_approval']          = 'Wachtsjend op goedkarring';
$_['text_product']           = 'Products';
$_['text_stock']             = 'Sunder foarried';
$_['text_review']            = 'Reaksjes';
$_['text_return']            = 'Returns';
$_['text_affiliate']         = 'Affiliates';
$_['text_store']             = 'Stores';
$_['text_front']             = 'Website Front';
$_['text_help']              = 'Help';
$_['text_homepage']          = 'RealEstate Homepage';
$_['text_support']           = 'Support Forum';
$_['text_documentation']     = 'Dokumintaasje';
$_['text_logout']            = 'Útlogge';
