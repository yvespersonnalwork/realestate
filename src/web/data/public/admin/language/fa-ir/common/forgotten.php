<?php
// header
$_['heading_title']   = 'Ferjit jo wachtwurd?';

// Text
$_['text_forgotten']  = 'Forgotten Password';
$_['text_your_email'] = 'Jo e-mailadres';
$_['text_email']      = 'Fier it e-postadres yn ferbân mei jo akkount yn. Klikje as jo in e-postadres resets e-post nei jo hawwe.';
$_['text_success']    = 'In e-post mei in befêstigingslink is jo e-postadres adminstjoerd.';

// Entry
$_['entry_email']     = 'E-postadres';
$_['entry_password']  = 'Nij Wachtwurd';
$_['entry_confirm']   = 'Befêstigje';

// Error
$_['error_email']     = 'Warskôging: It adres fan e-post is net fûn yn ús registers, besykje it nochris te besykjen!';
$_['error_password']  = 'Wachtwurd moat tusken 4 en 20 karakters wêze!';
$_['error_confirm']   = 'Wachtwurd en wachtwurd befetsje net oerien!';