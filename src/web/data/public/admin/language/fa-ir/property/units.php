<?php

// Heading

$_['heading_title']    = 'Property Units';

// Text

$_['text_success']     = 'Sesje: Jo hawwe eigenskip-ienheden feroare!';
$_['text_list']        = 'Eigenskippen Untwerpen List';
$_['text_add']         = 'Add Property Units';
$_['text_edit']        = 'Eigenskippen feroarje';
$_['text_success']     = 'mei suksesfolle: tafoegje jo eigendom!';
$_['text_successedit'] = 'mei sukses: jo feroare eigenskip-ienheid';
$_['text_successdelete']= 'mei súksesfolle: jo eigenskip-uniel Fuortsmite';

// Column
$_['column_name']      = 'Eigenskippen namme';
$_['column_action']    = 'Aksje';

// Lable 
$_['lable_name']       = 'Eigenskippen namme';

// Entry
$_['entry_name']       = 'Eigenskippen namme';



// Error

$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om eigenskip-ienheden te feroarjen!';
$_['error_name']       = 'Eigenskipstatusnamme moat tusken 3 en 32 tekens wêze!';
$_['error_default']    = 'Warskôging: Dizze eigenskipstatus kin net wiske wurde as it op dit stuit as de standert wachtwurde statusstatint is!';
