<?php
// Heading
$_['heading_title']          = 'Plannen';

// Text
$_['text_success']           = 'Súkses: Jo hawwe bewurke plannen!';
$_['text_list']              = 'Plannen List';
$_['text_add']               = 'Plannen taheakje';
$_['text_edit']              = 'Bewurkje plannen';
$_['text_default']           = 'Standert';
$_['text_day']          	 = 'Dei';
$_['text_month']          	 = 'Moanne';
$_['text_years']           	 = 'Jier';

$_['text_select']           = '---Selektearje opsje-----';


$_['tab_variation']          = 'Ôfwikseling';


$_['tab_extraoptions']       = 'Extra Opsjes';
$_['button_remove']          = 'Weinimme';
$_['button_variation_add']   = 'Add Variation';
$_['button_extraoption_add'] = 'Add Option';

// Column
$_['column_name']            = 'Plannenamme';
$_['column_image']           = 'Byld';
$_['column_price']           = 'Priis';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Aksje';

// Entry
$_['entry_name']             = 'Plannenamme';
$_['entry_description']      = 'Beskriuwing';
$_['entry_keyword']          = 'SEO URL';
$_['entry_image']            = 'Byld';
$_['entry_price']            = 'Priis';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';
$_['entry_variation']        = 'ôfwikseling';


$_['enter_validate']        = 'validearje';
$_['enter_number']        = 'Nûmer';
$_['entry_title']            = 'Titel';

// Help
$_['help_keyword']           = 'Bliuw gjin romten brûke, yn plak fan ferfange ferfiers troch - en soargje derfoar dat it SEO URL is unike unike.';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warskôging: Jo hawwe gjin tastimming om plannen te wizigjen!';
$_['error_name']             = 'Plannen Namme moat tusken 2 en 255 tekens wêze!';
$_['error_keyword']          = 'SEO URL al brûkt!';
