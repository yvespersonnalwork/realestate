<?php
// Heading
$_['heading_title']        = 'APIs';

// Text
$_['text_success']         = 'Sesje: Jo hawwe feroare APIs!';
$_['text_list']            = 'API List';
$_['text_add']             = 'Add API';
$_['text_edit']            = 'API bewurkje';
$_['text_ip']              = 'Hjirûnder kinne jo in list fan IP's meitsje tagong ta de API. Jo hjoeddeiske IP is %s';

// Column
$_['column_name']          = 'API namme';
$_['column_status']        = 'Status';
$_['column_date_added']    = 'Datum tafoege';
$_['column_date_modified'] = 'Datum tafoege';
$_['column_token']         = 'Datum tafoege';
$_['column_ip']            = 'IP';
$_['column_action']        = 'Aksje';

// Entry
$_['entry_name']           = 'API Name';
$_['entry_key']            = 'API-kaai';
$_['entry_status']         = 'Status';
$_['entry_ip']             = 'IP';

// Error
$_['error_permission']     = 'Warskôging: Jo hawwe gjin tastimming om APIs te wizigjen!';
$_['error_name']           = 'API-namme moat tusken 3 en 20 karakters wêze!';
$_['error_key']            = 'API-kaai moat tusken 64 en 256 tekens wêze!';
