<?php
// Heading
$_['heading_title']    = 'Brûkersgroepen';

// Text
$_['text_success']     = 'Sesje: Jo hawwe gebrûk makke fan groepen!';
$_['text_list']        = 'User Group';
$_['text_add']         = 'Brûk User Group';
$_['text_edit']        = 'Benutzergrutte bewurkje';

// Column
$_['column_name']      = 'Brûkersgroepnamme';
$_['column_action']    = 'Aksje';

// Entry
$_['entry_name']       = 'Brûkersgroepnamme';
$_['entry_access']     = 'Tagonklikheid tagong';
$_['entry_modify']     = 'Fergunning feroarje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om brûkersgroepen te wizigjen!';
$_['error_name']       = 'User Group Name moat tusken 3 en 64 tekens wêze!';
$_['error_user']       = 'Warskôging: Dizze brûkersgroep kin net wiske wurde as it no oanwêzich is oan brûkers fan% s!';