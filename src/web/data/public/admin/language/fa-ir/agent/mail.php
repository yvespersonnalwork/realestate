<?php
// Heading
$_['heading_title']     	 = 'Agent Mails Template';

// Text
$_['text_success']       	 = 'Súkses: Jo hawwe wizige !';
$_['text_list']           	 = 'E-postlistlist om Agent te ferstjoeren';
$_['text_add']          	 = 'Add Mail';
$_['text_edit']         	 = 'E-post bewurkje';
$_['text_default']      	 = 'Standert';
$_['text_enable']      	     = 'Mooglik meitsje';
$_['text_disable']      	 = 'Disable';
$_['text_agentregister']     = 'agint_register_post';
$_['text_agentapproved']     = 'agint_goedkard_post';
$_['text_agentproperty']     = 'agint_add_besit_admin_post';
$_['text_propertyapp']       = 'besit_goedkard_post';
$_['text_agentforgotte']     = 'agint_fergetten_post';

// Column
$_['column_name']       	 = 'Namme';
$_['column_date']        	 = 'Datum tafoege';
$_['column_action']      	 = 'Aksje';

// Lable
$_['lable_subject']          = 'Ûnderwerp';
$_['lable_message']          = 'Berjocht';
$_['lable_name']             = 'Namme';
$_['lable_status']           = 'Status';
$_['lable_type']             = 'Type';

// Entry
$_['entry_subject']          = 'Ûnderwerp';
$_['entry_message']          = 'Berjocht';
$_['entry_name']             = 'Namme';
$_['entry_status']           = 'Status';
$_['entry_type']             = 'Type';

/// Tab
$_['tab_mail']               = 'Post';
$_['tab_info']               = 'Info';

// Error
$_['error_warning'] 		 = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission'] 		 = 'Warskôging: Jo hawwe gjin tastimming om modellen te feroarjen!';
$_['error_name']             = 'Namme moat tusken 2 en 64 tekens wêze!';
$_['error_type']             = 'Selektearje de posttype';