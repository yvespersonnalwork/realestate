<?php
// Heading
$_['heading_title']          = 'Kategoryen';

// Text
$_['text_success']           = 'Súkses: Jo hawwe wizige kategoryen!';
$_['text_list']              = 'List fan kategoryen';
$_['text_add']               = 'Add Category';
$_['text_edit']              = 'Edit Category';
$_['text_default']           = 'Standert';

// Column
$_['column_name']            = 'Kategory Namme';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Aksje';

//label name

$_['text_name']             = 'Kategory Namme';
$_['text_description']      = 'Beskriuwing';
$_['text_meta_title'] 	     = 'Meta Tag Titel';
$_['text_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['text_meta_description'] = 'Meta Tag Omskriuwing';
$_['text_keyword']          = 'SEO kaaiwurd';
$_['text_parent']           = 'Parent';
$_['text_filter']           = 'Filters';
$_['text_store']            = 'Webstee';
$_['text_image']            = 'Byld';
$_['text_top']              = 'Top';
$_['text_column']           = 'Pylder';
$_['text_sort_order']       = 'Sort Order';
$_['text_status']           = 'Status';
$_['text_layout']           = 'Layout Override';


// Entry input name
$_['entry_name']             = 'Kategory Namme';
$_['entry_description']      = 'Beskriuwing';
$_['entry_meta_title'] 	     = 'Meta Tag Titel';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Omskriuwing';
$_['entry_keyword']          = 'SEO kaaiwurd';
$_['entry_parent']           = 'Parent';
$_['entry_filter']           = 'Filters';
$_['entry_store']            = 'Webstee';
$_['entry_image']            = 'Byld';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Pylder';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';
$_['entry_layout']           = 'Layout Override';

///btn 
$_['button_save']             = 'Rêde';
$_['button_cancel']           = 'Ôfbrekke';


// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'AutocompleteDo gjin spaasjes brûke, yn plak fan ferfange romten mei - en moatte derfoar soargje dat it kaaiwurd is unike unike.';
$_['help_top']               = 'Werje yn de topmenu bar. Allinnich wurket foar de boppesteamde kategoryen.';
$_['help_column']            = 'Kies kolommen om te brûken foar de ûnderkant 3 kategoryen. Allinnich wurket foar de boppesteamde kategoryen.';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warskôging: Jo hawwe gjin tastimming om kategoryen te wizigjen!';
$_['error_name']             = 'Category Name must be between 2 and 32 characters!';
$_['error_meta_title']       = 'Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-kaaiwurd al yn gebrûk!';