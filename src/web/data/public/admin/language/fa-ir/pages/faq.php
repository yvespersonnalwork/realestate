<?php
// Heading
$_['heading_title']          = 'FAQ';

// Text
$_['text_success']           = 'Súkses: Jo hawwe feroare FAQ!';
$_['text_list']              = 'FAQlist';
$_['text_add']               = 'Add FAQ';
$_['text_edit']              = 'FAQ bewurkje';
$_['text_default']           = 'Standert';

// Column
$_['column_name']            = 'FAQ Namme';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Aksje';


///

$_['text_category']         = 'Kategory';
$_['text_name']             = 'Fraach';
$_['text_description']      = 'Antwurdzje';
$_['text_meta_title'] 	     = 'Meta Tag Titel';
$_['text_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['text_meta_description'] = 'Meta Tag Omskriuwing';
$_['text_keyword']          = 'SEO kaaiwurd';
$_['text_parent']           = 'Parent';
$_['text_filter']           = 'Filters';
$_['text_store']            = 'Webstee';
$_['text_image']            = 'Byld';
$_['text_top']              = 'Top';
$_['text_column']           = 'Pylder';
$_['text_sort_order']       = 'Sort Order';
$_['text_status']           = 'Status';
$_['text_layout']           = 'Layout Override';



// Entry
$_['entry_category']         = 'Kategory';
$_['entry_name']             = 'Fraach';
$_['entry_description']      = 'Antwurdzje';
$_['entry_meta_title'] 	     = 'Meta Tag Titel';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Omskriuwing';
$_['entry_keyword']          = 'SEO kaaiwurd';
$_['entry_parent']           = 'Parent';
$_['entry_filter']           = 'Filters';
$_['entry_store']            = 'Webstee';
$_['entry_image']            = 'Byld';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Pylder';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';
$_['entry_layout']           = 'Layout Override';

/// btn
$_['button_save']             = 'Rêde';
$_['button_cancel']           = 'Ôfbrekke';


// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Bliuw gjin romten brûke, yn plak fan ferfange romten ferfange - en soargje derfoar dat it kaaiwurd wrâldwide unyk is';
$_['help_top']               = 'Werje yn de topmenu bar. Allinne wurket foar de boppesteamde FAQ.';
$_['help_column']            = 'Tal punten brûke foar de bottom 3 FAQ. Allinne wurket foar de boppesteamde FAQ.';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warskôging: Jo hawwe net tastimming om FAQ te feroarjen!';
$_['error_name']             = 'FAQ Namme moat tusken 2 en 32 tekens wêze!';
$_['error_meta_title']       = 'Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-kaaiwurd al yn gebrûk!';
$_['error_category']          = 'Selektearje asjebleaft as kategoryen net oanwêzich binne foegje earst ta kategory!	';