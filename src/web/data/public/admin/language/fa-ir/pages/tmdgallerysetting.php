<?php
// Heading
$_['heading_title']          = 'Gallery Setting';

// Text
$_['text_success']           = 'Súkses: Jo hawwe wizige kategoryen!';
$_['text_list']              = 'Tmd GallerySetting List';
$_['text_add']               = 'Tmd GallerySetting taheakje';
$_['text_edit']              = 'Tmd GallerySetting bewurkje';
$_['text_default']           = 'Standert';

// Column
$_['column_name']            = 'Gallery Title';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Aksje';

// Entry
$_['entry_width']       	 = 'Breedte';
$_['entry_height']       	 = 'Hichte';
$_['entry_image_popup']  	 = 'Popupgrutte';
$_['entry_thumb_popup']  	 = 'Thumbgrutte';
$_['entry_limtofgallery']  	 = 'Set foto limit';
$_['entry_namecolor']  		 = 'Photo Name Kleur';
$_['entry_desccolor'] 	     = 'Omskriuwing kleur';
$_['entry_totalphotocolor']  = 'Totale Fotokleur';

$_['tab_color']              = 'Kleure oanpasse';
$_['tab_setting']            = 'Gallery Setting';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warskôging: Jo hawwe gjin tastimming om kategoryen te wizigjen!';
$_['error_name']             = 'Gallery Titel moat tusken 2 en 32 persoanen wêze!';
$_['error_meta_title']       = 'Gallery Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';