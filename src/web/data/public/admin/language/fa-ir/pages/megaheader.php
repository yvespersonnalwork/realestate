<?php
// Heading
$_['heading_title']          = 'Mega Header Menu';

//Text
$_['text_list']         	 = 'List fan menu's';
$_['text_form']              = 'Mega header';
$_['text_category']          = 'Category';
$_['text_info']              = 'Ynformaasje';
$_['text_manufact']          = 'Fabrikant';
$_['text_custom']            = 'Gewoante';
$_['text_select']            = 'Útkieze';
$_['text_editor']            = 'Redakteur';
$_['text_product']           = 'Produkt';
$_['text_enable']            = 'Mooglik meitsje';
$_['text_disable']           = 'Ynvalide';
$_['text_no_results']        = 'gjin resultaat';
$_['text_category_image']    = 'Category Image';
$_['text_category_description'] = 'Kategory Description';
$_['text_manufatureimage']   = 'Wetter - Agrarwetter';
$_['text_patternimage']   	= 'Ofbyldmuster';
$_['entry_patternimage']     = 'Pattern Background';
$_['entry_selectimagetype']  = 'Selektearje eftergrûntype';
//// new changes  27-10-2016///
$_['entry_store']            = 'Webstee';
//// new changes  27-10-2016///


$_['button_edit']            = 'Bewurkje';
$_['button_add']             = 'Foegje nij ta';
$_['button_delete']          = 'Wiskje';
$_['button_save']            = 'Rêde';
$_['button_setting']         = 'Mega Header Setting';
$_['button_cancel']          = 'Canel';
$_['text_success']           = 'Sesje: Jo hawwe mega-headers feroare! ';
$_['button_custom']          = 'Oanpaste menu taheakje';

$_['text_product1']          = 'Property';
$_['text_subcategory']       = 'Sub kategory';
$_['text_price']         	 = 'Priis';
$_['text_pname']         	 = 'Property Name';
$_['text_image']         	 = 'Byld';
$_['text_model']        	 = 'Model';
$_['text_upc']        	     = 'UPC';
$_['text_sku']         		 = 'SKU';
$_['text_description']       = 'Beskriuwing';


/// label name 
$_['text_image']          	 = 'Êftergrûnplaatsje';
$_['text_status']           = 'Status';
$_['text_sort_order']       = 'Sort order';
$_['text_title']            = 'Oanpaste koptekst';
$_['text_row']           	 = 'Rigel';
$_['text_col']           	 = 'Cols';
$_['text_enable']           = 'Submenu Category Title ynskeakelje';
$_['text_url']           	 = 'URL';
$_['text_open']           	 = 'Iepenje yn nije ljepper';
$_['text_type']           	 = 'Type';
$_['text_custname']         = 'Oanpaste namme';
$_['text_custurl'] 	     = 'Oanpaste url';
$_['text_parent'] 	         = 'Parent';
$_['text_submenu'] 		 = 'Sub Megamenu';
$_['text_description']      = 'MegaMenu Omskriuwing';
$_['text_product']          = 'Produkt';
$_['text_category']         = 'kategory';
$_['text_row']         	 = 'Rigel';
$_['text_customcode']       = 'Custom Code';
$_['button_setting']         = 'Ynstellings';
$_['text_information']      = 'Ynformaasje';
$_['text_manufacturer']     = 'Fabrikant';
$_['text_editor']            = 'Redakteur';
$_['text_icontitle']		 = 'Icon';
$_['text_showicon']		 = 'Ikon ynskeakelje';

$_['text_selectimagetype']  = 'Selektearje background type ';
//// new changes  27-10-2016///
$_['text_store']            = 'Webstee';

// Entry
$_['entry_image']          	 = 'Êftergrûnplaatsje';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort order';
$_['entry_title']            = 'Oanpaste koptekst';
$_['entry_row']           	 = 'Rigel';
$_['entry_col']           	 = 'Cols';
$_['entry_enable']           = 'Submenu Category Title ynskeakelje';
$_['entry_url']           	 = 'URL';
$_['entry_open']           	 = 'Iepenje yn nije ljepper';
$_['entry_type']           	 = 'Type';
$_['entry_custname']         = 'Oanpaste namme';
$_['entry_custurl'] 	     = 'Oanpaste url';
$_['entry_parent'] 	         = 'Parent';
$_['entry_submenu'] 		 = 'Sub Megamenu';
$_['entry_description']      = 'MegaMenu Description';
$_['entry_product']          = 'Produkt';
$_['entry_category']         = 'Kategory';
$_['entry_row']         	 = 'Rigel';
$_['entry_customcode']       = 'Custom Code';
$_['button_setting']         = 'Ynstellings';
$_['entry_information']      = 'Ynformaasje';
$_['entry_manufacturer']     = 'Fabrikant';
$_['text_editor']            = 'Redakteur';
$_['entry_icontitle']		 = 'Icon';
$_['entry_showicon']		 = 'Ikon ynskeakelje';

// column_edit
$_['column_title']           = 'Menu Title';
$_['column_sort_order']      = 'Sort order';
$_['column_status']          = 'Status';
$_['column_edit']            = 'Aksje';

 // Error
$_['error_title']      		 = 'Titel moat 3 oant 21 karakters wêze'; 
$_['error_image']      		 = 'Selektearje byld'; 

//filter
$_['button_filter']      	 = 'Filter';
$_['entry_title']      		 = 'Oanpaste koptekst';
$_['entry_status']      	 = 'Status';
$_['text_enabled']      	 = 'Mooglik meitsje';
$_['text_disabled']      	 = 'Disable';

//help
$_['help_keyword']           = 'Bliuw gjin romten brûke, yn plak fan ferfange romten ferfange - en soargje derfoar dat it kaaiwurd wrâldwide unyk is.';
$_['help_bottom']            = 'Spesjale yn 'e ûnderste fuot.';
$_['help_category']          = 'Autocomplete';
$_['help_product']           = 'Autocomplete';
$_['help_showicon']          = 'Allinne byldkaike ynskeakelje';
