<?php
// Heading
$_['heading_title']          = 'Blog';

// Text
$_['text_success']           = 'Súkses: Jo hawwe feroare Blog!';
$_['text_list']              = 'Bloglist';
$_['text_add']               = 'Add Blog';
$_['text_edit']              = 'Bewurkje Blog';
$_['text_default']           = 'Standert';
$_['entry_username']         = 'Brûkersnamme';
$_['entry_blogcoment']       = 'Kommentaar sjen litte';
$_['tab_comment']            = 'Comments';
$_['column_customer']        = 'Klant';
$_['column_comment']         = 'Comment';
$_['text_no_comment']        = 'Gjin kommentaar ';
$_['text_dash']                     = 'Dashboard';
$_['text_blog']                    = 'Blogs';
$_['text_cate']                   = 'Kategory';
$_['text_comm']                    = 'Comment';
$_['text_sett']                    = 'Ynstellings';
$_['text_addmodule']                    = 'Module taheakje';

// Column
$_['column_date_added']      = 'Datum tafoege';
$_['entry_date_added']       = 'Datum tafoege';
$_['column_status']          = 'Status';
$_['column_image']           = 'Byld';
$_['column_title']           = 'Blognamme';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Aksje';
$_['text_enabled']           = 'Mooglik meitsje';
$_['text_disabled']          = 'Ynvalide';
$_['column_name']            = 'Blognamme';
$_['column_view']            = 'Views';


// Entry
$_['entry_name']            = 'Blognamme';
$_['entry_description']      = 'Beskriuwing';
$_['entry_store']            = 'Stores';
$_['entry_meta_title'] 	     = 'Meta Tag Titel';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Omskriuwing';
$_['entry_keyword']          = 'SEO kaaiwurd';
$_['entry_bottom']           = 'Boaiem';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_layout']           = 'Layout Override';
$_['entry_image']            = 'Byld';
$_['entry_tag']              = 'Tags';
$_['text_none']              = '--Gjin--';
$_['entry_tmdblogcategory']  = 'Tmd BlogCategory';
$_['help_tmdblogcategory']   = 'Autocomplete';

// Help
$_['help_keyword']           = 'Bliuw gjin romten brûke, yn plak fan ferfange romten ferfange - en soargje derfoar dat it kaaiwurd wrâldwide unyk is.';
$_['help_bottom']            = 'Spesjale yn 'e ûnderste fuot.';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warskôging: Jo hawwe gjin rjocht om artikel te feroarjen!';
$_['error_title']            = 'Blêdnamme moat tusken 3 en 64 karakters wêze!';
$_['error_description']      = 'Beskriuwing moat mear as 3 tekens wêze!';
$_['error_meta_title']       = 'Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-kaaiwurd al yn gebrûk!';
$_['error_account']          = 'Warskôging: Dizze artikel side kin net wiske wurde as it op 'e hichte is as de winkelkonte terminen!';
$_['error_checkout']         = 'Warskôging: Dizze artikel side kin net wiske wurde as it op 'e hichte is as de winkelskontraasjebegripen!';
$_['error_affiliate']        = 'Warskôging: Dizze artikel side kin net wiske wurde as it op dit stuit as websaffiliatebegrip oanjûn is!';
$_['error_return']           = 'Warskôging: Dizze artikel side kin net wiske wurde as it op dit stuit as de winkel weromkommende termen is!';
$_['error_store']            = 'Warskôging: Dizze artikel side kin net wiske wurde as it op dit stuit brûkt wurdt troch% s-winkels!';