<?php
// Heading
$_['heading_title']          = 'Blog Comment';

// Text
$_['text_success']           = 'Súkses: Jo hawwe feroare fan Blog Comment!';
$_['text_list']              = 'Blog Comment List';
$_['text_add']               = 'Add Comment Comment';
$_['text_edit']              = 'Edit Comment Comment';
$_['text_default']           = 'Standert';
$_['text_no_comment']        = 'Gjin kommentaar ';
$_['text_dash']              = 'Dashboard';
$_['text_cate']              = 'Kategory';
$_['text_comm']              = 'Comment';
$_['text_sett']              = 'Ynstellings';
$_['text_addmodule']         = 'Module taheakje';
$_['text_none']              = '--Gjin--';
$_['text_enabled']           = 'Mooglik meitsje';
$_['text_disabled']          = 'Ynvalide';

// Tab
$_['tab_comment']            = 'Comments';

// Column

$_['column_date_added']      = 'Datum tafoege';
$_['column_status']          = 'Status';
$_['column_image']           = 'Byld';
$_['column_title']           = 'Blog Comment Namme';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Aksje';

$_['column_name']            = 'Klant namme';
$_['column_view']            = 'Views';
$_['column_customer']        = 'Klant';
$_['column_comment']         = 'Comment';


// Entry
$_['entry_name']             = 'Blog Comment Namme';
$_['entry_customername']     = 'Klant';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_image']            = 'Byld';
$_['entry_tmdblog']          = 'Selektearje Blog';
$_['entry_date_added']       = 'Datum tafoege';
$_['entry_comment']          = 'Datum tafoege';


// Help
$_['help_tmdblog']           = 'Autocomplete';
$_['help_bottom']            = 'Spesjale yn 'e ûnderste fuot.';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warskôging: Jo hawwe gjin rjocht om artikel te feroarjen!';
$_['error_title']            = 'Namme fan blêd Comment moat tusken 3 en 64 karakters wêze!';
$_['error_description']      = 'Beskriuwing moat mear as 3 tekens wêze!';
$_['error_meta_title']       = 'Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-kaaiwurd al yn gebrûk!';
$_['error_account']          = 'Warskôging: Dizze artikel side kin net wiske wurde as it op 'e hichte is as de winkelkonte terminen!';
$_['error_checkout']         = 'Warskôging: Dizze artikel side kin net wiske wurde as it op 'e hichte is as de winkelskontraasjebegripen!';
$_['error_affiliate']        = 'Warskôging: Dizze artikel side kin net wiske wurde as it op dit stuit as websaffiliatebegrip oanjûn is!';
$_['error_return']           = 'Warskôging: Dizze artikel side kin net wiske wurde as it op dit stuit as de winkel weromkommende termen is!';
$_['error_store']            = 'Warskôging: Dizze artikel side kin net wiske wurde as it op dit stuit brûkt wurdt troch% s-winkels!';