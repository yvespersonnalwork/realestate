<?php
// Heading
$_['heading_title']                = 'BlogDashboard';

// Text
$_['text_order_total']             = 'Total Orders';
$_['text_customer_total']          = 'Totaal klanten';
$_['text_sale_total']              = 'Total Sales';
$_['text_online_total']            = 'Minsken online';
$_['text_map']                     = 'Wrâldkaart';
$_['text_sale']                    = 'Sales Analytics';
$_['text_activity']                = 'resinte aktiviteit';
$_['text_recent']                  = 'Latest Orders';
$_['text_order']                   = 'Orders';
$_['text_customer']                = 'Klanten';
$_['text_day']                     = 'Hjoed';
$_['text_week']                    = 'Wike';
$_['text_month']                   = 'Moanne';
$_['text_year']                    = 'Jier';
$_['text_view']                    = 'Sjoch mear...';
$_['text_dash']                    = 'Dashboard';
$_['text_blog']                    = 'Blogs';
$_['text_cate']                    = 'Kategory';
$_['text_comm']                    = 'Comment';
$_['text_sett']                    = 'Ynstellings';
$_['text_addmodule']               = 'Module taheakje';

// Error
$_['error_install']                = 'Warskôging: Map bestiet noch bestiet en moat foar feiligens redenen wiske wurde!';