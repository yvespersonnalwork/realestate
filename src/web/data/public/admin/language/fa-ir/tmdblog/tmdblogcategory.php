<?php
// Heading
$_['heading_title']          = 'Blog Category';

// Text
$_['text_success']           = 'Success: You have modified tmdblogcategory!';
$_['text_list']              = 'TMD Blog Category List';
$_['text_add']               = 'Add TMD Blog Category';
$_['text_edit']              = 'Bewurkje TMD-Blog-kategory';
$_['text_default']           = 'Standert';

// Column
$_['column_date_added']      = 'Datum tafoege';
$_['entry_date_added']       = 'Datum tafoege';
$_['column_status']          = 'Status';
$_['column_image']           = 'Byld';
$_['column_title']           = 'TMD Blog Category Title';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Aksje';
$_['text_enabled']           = 'Mooglik meitsje';
$_['text_disabled']          = 'Ynvalide';
$_['column_name']            = 'Blog Category Name';
$_['column_count']           = 'Telle';


// Entry
$_['entry_name']            = 'Blog Category Name';
$_['entry_description']      = 'Beskriuwing';
$_['entry_store']            = 'Stores';
$_['entry_meta_title'] 	     = 'Meta Tag Titel';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Omskriuwing';
$_['entry_keyword']          = 'SEO kaaiwurd';
$_['entry_bottom']           = 'Boaiem';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_layout']           = 'Layout Override';
$_['entry_image']            = 'Byld';
$_['entry_tag']              = 'Tags';
$_['text_none']              = '--Gjin--';
$_['entry_parent']           = 'Parent';

// Help
$_['help_keyword']           = 'Bliuw gjin romten brûke, yn plak fan ferfange romten ferfange - en soargje derfoar dat it kaaiwurd wrâldwide unyk is.';
$_['help_bottom']            = 'Spesjale yn 'e ûnderste fuot.';

// Error
$_['error_warning']          = 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       = 'Warning: You do not have permission to modify tmdblogcategory!';
$_['error_title']            = 'TMD-Blog-kategory Titel moat tusken 3 en 64 karakters wêze!';
$_['error_description']      = 'Beskriuwing moat mear as 3 tekens wêze!';
$_['error_meta_title']       = 'Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-kaaiwurd al yn gebrûk!';
$_['error_account']          = 'Warskôging: Dizze side tmdblogcategory kin net wiske wurde as it no opjûn is as de wachtkontoadoarten!';
$_['error_checkout']         = 'Warskôging: Dizze side tmdblogcategory kin net wiske wurde as it op 'e hichte is as de winkelskontrôtbegripen!';
$_['error_affiliate']        = 'Warskôging: Dizze side tmdblogcategory kin net wiske wurde as it op dit stuit as websaffiliatebegrip oanjûn is!';
$_['error_return']           = 'Warskôging: dizze side tmdblogcategory kin net wiske wurde as it no opjûn is as de winkel weromkomst!';
$_['error_store']            = 'Warskôging: Dizze side tmdblogcategory kin net wiske wurde as it no brûkt wurdt troch% s-winkels!';