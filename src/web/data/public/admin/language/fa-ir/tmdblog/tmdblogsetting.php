<?php
// Heading
$_['heading_title']          = 'Blog Setting';

// Text
$_['text_success']           = 'Súkses: Jo hawwe wizige kategoryen!';
$_['text_list']              = 'Tmd-Blog-ynstellingslist';
$_['text_add']               = 'Tmd-Blog-ynstelling taheakje';
$_['text_edit']              = 'Bewurkje Tmd Blog Setting';
$_['text_default']           = 'Standert';
$_['text_searchpage']        = 'Sykje side';
$_['text_categorypage']      = 'Blog Category';
$_['text_relatedblog']       = 'Related Page';
$_['text_blogpage']          = 'Blog Page';
$_['text_listpage']          = 'Listing side';
$_['text_page']              = 'Side';

// Column
$_['column_name']            = 'Blog Title';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Aksje';

// Entry
$_['entry_commentlimit']     = 'Beheine';
$_['entry_name']             = 'Blog Title';
$_['entry_description']      = 'Beskriuwing';
$_['entry_meta_title'] 	     = 'Titel';
$_['entry_meta_keyword'] 	 = 'Keywords';
$_['entry_meta_description'] = 'Beskriuwing';
$_['entry_keyword']          = 'SEO kaaiwurd';
$_['entry_parent']           = 'Parent';
$_['entry_filter']           = 'Filters';
$_['entry_store']            = 'Stores';
$_['entry_image']            = 'Byld';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Pylder';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_titlesize']        = 'Blog Titelgrutte';
$_['entry_titlecolor']       = 'Blêdwizert tekstkleur';
$_['entry_thumbimg']         = 'Comment Banner Thumbgrutte';
$_['entry_comntbanner']      = 'Comment Bannergrutte';
$_['text_blogcommentsglobal']= 'Globaly toane / ferbergje Blogkommintaar';
$_['entry_blogcomments']     = 'Blog Comments';
$_['help_blogcomment']       = 'Selektearje de opsje om te ferbergjen en sjen te litten fan alle reaksjes fan blogs';

$_['entry_article']          = 'Blog';
$_['entry_google']           = 'Google share';
$_['entry_pinterest']        = 'Pinterest share';
$_['entry_twitter']          = 'Twitter share';
$_['entry_facebook']         = 'Facebook share';
$_['entry_feedbackrow']      = 'Feedback Box';
$_['entry_descp']            = 'Beskriuwing';
$_['entry_articleimg']       = 'Blogôfbylding';
$_['entry_width']            = 'Breedte';
$_['entry_height']           = 'Hichte';

$_['entry_descptextcolor']      = 'Beskriuwing Tekstkleur';
$_['entry_articletextcolor']    = 'Blog Tekstkleur';
$_['entry_postboxbgcolor']      = 'Blêdwizer fan kleur foar blok';
$_['entry_containerbgcolor']    = 'Blêder foar kontainer foar blêdwizer';
$_['entry_titlebgcolor']     	= 'Blog Theme Color';
$_['entry_themehovercolor']     = 'Blog Theme Hover Color';
$_['entry_titletextcolor']      = 'Tekstkleur kopiearje';
$_['entry_blogtextcolor']       = 'Kopiearje Hover Tekstkleur';
$_['entry_bloglayout']    	    = 'Opmaak';
$_['entry_blogperpage']      	= 'Side';
$_['entry_blogperrow']    	    = 'Rigel';
$_['entry_dispalytitle']    	= 'Titel';
$_['entry_dispalydes']       	= 'Beskriuwing';
$_['entry_blogthumbsize']    	= 'Thumbgrutte';
$_['entry_dispalyblogtimg']    	= 'Byld';
$_['entry_keyword']          = 'SEO kaaiwurd';

$_['help_meta_title'] 	    	= 'Meta Tag Titel';
$_['help_meta_keyword'] 		= 'Meta Tag Keywords';
$_['help_meta_description'] 	= 'Meta Tag Omskriuwing';
$_['help_blogthumbsize']   		= 'Blog Blog Thumb <br/> Grutte(W*H)';

$_['help_article']           	= 'Blêdwizer ynskeakelje of útsette';
$_['help_google']           	= 'Aktivearje Google of share';
$_['help_pinterest']            = 'Aktivearjen of Untwerp ynskeakelje';
$_['help_twitter']           	= 'Twitter ynskeakelje of útskeakelje';
$_['help_facebook']           	= 'Aktivearje Facebook of útskeakelje';
$_['help_feedbackrow']          = 'Ynbrekke Feedback Box';
$_['help_descp']           		= 'Aktivearje of útskeakelje Description';
$_['help_articleimg']           = 'Blêdwizerôfbylding ynskeakelje of ôfbrekke';
$_['help_keyword']           = 'Bliuw gjin romten brûke, yn plak fan ferfange romten ferfange - en soargje derfoar dat it kaaiwurd wrâldwide unyk is.';

$_['tab_setting']           	= 'Setting';
$_['tab_color']             	= 'Kleure oanpasse';
$_['tab_main']             		= 'Algemien';
$_['tab_blogseting']            = 'Blog Setting';
$_['tab_support']               = 'Stypje';

// Help
$_['help_filter']            	= '(Autocomplete)';
$_['help_keyword']           	= 'Bliuw gjin romten brûke, yn plak fan ferfange romten ferfange - en soargje derfoar dat it kaaiwurd wrâldwide unyk is.';
$_['help_top']               	= 'Werje yn de topmenu bar. Allinnich wurket foar de boppesteamde kategoryen.';
$_['help_column']            	= 'Kies kolommen om te brûken foar de ûnderkant 3 kategoryen. Allinnich wurket foar de boppesteamde kategoryen.';

// Error
$_['error_warning']          	= 'Warskôging: Kontrolearje asjebleaft it formulier sjoch foar fouten!';
$_['error_permission']       	= 'Warskôging: Jo hawwe gjin tastimming om kategoryen te wizigjen!';
$_['error_name']             	= 'Blessertitel moat tusken 2 en 32 persoanen wêze!';
$_['error_meta_title']       	= 'Blêd Meta Titel moat grutter wêze as 3 en minder dan 255 tekens!';
$_['error_keyword']          = 'SEO-kaaiwurd al yn gebrûk!';