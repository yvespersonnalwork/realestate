<?php
// Heading
$_['heading_title']    = 'Backup & amp; Restore';

// Text
$_['text_success']     = 'Sesje: Jo hawwe jo databank suksesfol ymportearre!';

// Entry
$_['entry_import']     = 'Ymport';
$_['entry_export']     = 'Eksportearje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming foar it wizigjen fan Backup & amp; Restore!';
$_['error_export']     = 'Warskôging: Jo moatte op syn minst ien tabel selektearje om te eksportearjen!';
$_['error_empty']      = 'Warskôging: De triem dy't jo uploadt wie leech!';