<?php
// Heading
$_['heading_title']                    = 'Standert Webside Theme';

// Text
$_['text_theme']                       = 'Tema's';
$_['text_success']                     = 'Sesje: Jo hawwe it standert Website-tema feroare!';
$_['text_edit']                        = 'Tema Standert standert besykje';
$_['text_general']                     = 'Algemien';
$_['text_product']                     = 'Products';
$_['text_image']                       = 'Ofbyldings';

// Entry
$_['entry_directory']                  = 'Tema Directory';
$_['entry_status']                     = 'Status';
$_['entry_product_limit']              = 'Standert Items per side';
$_['entry_product_description_length'] = 'List Description Limyt';
$_['entry_image_category']             = 'Category Image Size (W x H)';
$_['entry_image_thumb']                = 'Product Image Thumb Size (W x H)';
$_['entry_image_popup']                = 'Product Image Popupgrutte (W x H)';
$_['entry_image_product']              = 'Product Image Listgrutte (W x H)';
$_['entry_image_additional']           = 'Ekstra produktôfbyldinggrutte (W x H)';
$_['entry_image_related']              = 'Beskikbere produktôfbyldingsgrutte (W x H)';
$_['entry_image_compare']              = 'Fergelykje de ôfbyldingsgrutte (W x H)';
$_['entry_image_wishlist']             = 'Wish List-sjabloan (W x H)';
$_['entry_image_cart']                 = 'Cart Image Size (W x H)';
$_['entry_image_location']             = 'Website Image Size (W x H)';
$_['entry_width']                      = 'Breedte';
$_['entry_height']                     = 'Hichte';

// Help
$_['help_directory'] 	               = 'Dit fjild is allinich om âldere tema's te aktivearjen te kompatibel mei it nije tema-systeem. Jo kinne it tema-map sette om te brûken op de hjirgrûn beskreaune byldgrutte-ynstellings.';
$_['help_product_limit'] 	           = 'Determine hoefolle katalog items siden per side (produkten, kategoryen, ensfh)';
$_['help_product_description_length']  = 'Yn 'e listwerjefte koarte tekenrige limyt (kategory, spesjale ensfh)';

// Error
$_['error_permission']                 = 'Warskôging: Jo hawwe net tastimming om it standert opsetsema te feroarjen!';
$_['error_limit']       	           = 'Produktlimyt nedich!';
$_['error_image_thumb']                = 'Product Image Thumb Grutte dimensjes nedich!';
$_['error_image_popup']                = 'Produktfoarbyld Popupgrutte ôfmjittingen ferplicht!';
$_['error_image_product']              = 'Produktlist Grutte dimensjes nedich!';
$_['error_image_category']             = 'Category List Grutte dimensjes nedich!';
$_['error_image_additional']           = 'Oanfoljende produktôfbylding Grutte dimensjes nedich!';
$_['error_image_related']              = 'Beskikbere produktenôfbylding Grutte dimensjes nedich!';
$_['error_image_compare']              = 'Fergelykje de ôfbyldingsgrutte dimensjes ferplicht!';
$_['error_image_wishlist']             = 'Wish List-ôfbylding dimensjes nedich!';
$_['error_image_cart']                 = 'Cart Image Grutte dimensjes nedich!';
$_['error_image_location']             = 'Website-ôfbylding Grutte dimensjes nedich!';