<?php
// Heading
$_['heading_title']    = 'TMD Category Search';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe mooglike sykmasjine foar siden makke!';
$_['text_edit']        = 'Edit Category Search Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om te modulearjen fan Category Search module!';