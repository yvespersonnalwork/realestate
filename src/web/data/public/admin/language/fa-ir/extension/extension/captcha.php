<?php
// Heading
$_['heading_title']    = 'Captchas';

// Text
$_['text_success']     = 'Súkses: Jo hawwe feroare captchas!';
$_['text_list']        = 'Captcha List';

// Column
$_['column_name']      = 'Captcha Namme';
$_['column_status']    = 'Status';
$_['column_action']    = 'Aksje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming om de captchas te feroarjen!';