<?php
// Heading
$_['heading_title']    = 'Feeds';

// Text
$_['text_success']     = 'Sesje: Jo hawwe gegevens oanpast!';
$_['text_list']        = 'Feedlist';

// Column
$_['column_name']      = 'Produkt Feed Name';
$_['column_status']    = 'Status';
$_['column_action']    = 'Aksje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om feeds te feroarjen!';