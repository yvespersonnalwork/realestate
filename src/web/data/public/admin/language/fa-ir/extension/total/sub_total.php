<?php
// Heading
$_['heading_title']    = 'Sub-Totaal';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: jo hawwe totale totale modifikaasje feroare!';
$_['text_edit']        = 'Sub-Total bewurkje bewurkje seksje edit source';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin rjochten om it totale totale total te feroarjen!';