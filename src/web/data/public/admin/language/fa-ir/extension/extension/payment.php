<?php
// Heading
$_['heading_title']     = 'Payments';

// Text
$_['text_success']      = 'Súkses: Jo hawwe feroare betellingen!';
$_['text_list']         = 'Payment List';

// Column
$_['column_name']       = 'Wize fan beteljen';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Aksje';

// Error
$_['error_permission']  = 'Warskôging: Jo hawwe net tastimming om betellingen te feroarjen!';