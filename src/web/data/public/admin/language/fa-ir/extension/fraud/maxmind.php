<?php
// Heading
$_['heading_title']                           = 'MaxMind Anti-fraud';

// Text
$_['text_extension']                          = 'Extensions';
$_['text_success']                            = 'Súkses: Jo hawwe MaxMind feroarjen fan anty-fraude!';
$_['text_edit']                               = 'Bewurkje MaxMind Anti-Fraud';
$_['text_signup']                             = 'MaxMind is in fraude-deteksje tsjinst. As jo net in lisinsje-kaai hawwe, kinne jo kinne <a href="http://www.maxmind.com/?rId=opencart" target="_blank"><u>Oanmelde hjir</u></a>.';
$_['text_country_match']                      = 'Country Match:';
$_['text_country_code']                       = 'Lânkoade:';
$_['text_high_risk_country']                  = 'Heech Risiko Lân:';
$_['text_distance']                           = 'Ôfstân:';
$_['text_ip_region']                          = 'IP Regio:';
$_['text_ip_city']                            = 'IP Stêd:';
$_['text_ip_latitude']                        = 'IP Breedte:';
$_['text_ip_longitude']                       = 'IP Longitude:';
$_['text_ip_isp']                             = 'ISP:';
$_['text_ip_org']                             = 'IP Organisaasje:';
$_['text_ip_asnum']                           = 'ASNUM:';
$_['text_ip_user_type']                       = 'IP User Type:';
$_['text_ip_country_confidence']              = 'IP Lânfertrouwen:';
$_['text_ip_region_confidence']               = 'IP Region Vertrouwen:';
$_['text_ip_city_confidence']                 = 'IP City Vertrouwen:';
$_['text_ip_postal_confidence']               = 'IP Postfertrouwen:';
$_['text_ip_postal_code']                     = 'IP Postkoade:';
$_['text_ip_accuracy_radius']                 = 'IP Rjochting:';
$_['text_ip_net_speed_cell']                  = 'IP Net Speed Cell';
$_['text_ip_metro_code']                      = 'IP Metro Code:';
$_['text_ip_area_code']                       = 'IP Postkoade:';
$_['text_ip_time_zone']                       = 'IP Tiidsône:';
$_['text_ip_region_name']                     = 'IP Region Name:';
$_['text_ip_domain']                          = 'IP Domein:';
$_['text_ip_country_name']                    = 'IP Namme fan it lân:';
$_['text_ip_continent_code']                  = 'IP Continent Code:';
$_['text_ip_corporate_proxy']                 = 'IP Corporate Proxy:';
$_['text_anonymous_proxy']                    = 'Anonymous Proxy:';
$_['text_proxy_score']                        = 'Proxy Score:';
$_['text_is_trans_proxy']                     = 'Is Transparent Proxy:';
$_['text_free_mail']                          = 'Free Mail:';
$_['text_carder_email']                       = 'Carder Email:';
$_['text_high_risk_username']                 = 'Heech risiko-brûkersnamme:';
$_['text_high_risk_password']                 = 'Heech risiko wachtwurd:';
$_['text_bin_match']                          = 'Bin Match:';
$_['text_bin_country']                        = 'Bin Land:';
$_['text_bin_name_match']                     = 'Bin Namme Match:';
$_['text_bin_name']                           = 'Bin Namme:';
$_['text_bin_phone_match']                    = 'Bin Phone Match:';
$_['text_bin_phone']                          = 'Bin Telefoan:';
$_['text_customer_phone_in_billing_location'] = 'Kunde Tillefoannûmer yn Billinglokaasje:';
$_['text_ship_forward']                       = 'Ferstjoeren nei foaren:';
$_['text_city_postal_match']                  = 'City Postal Match:';
$_['text_ship_city_postal_match']             = 'Wetter City Postal Match:';
$_['text_score']                              = 'Skoare:';
$_['text_explanation']                        = 'Ferklearring:';
$_['text_risk_score']                         = 'Risk Score:';
$_['text_queries_remaining']                  = 'Queries oerbleaun:';
$_['text_maxmind_id']                         = 'Maxmind ID:';
$_['text_error']                              = 'Fersin:';

// Entry
$_['entry_key']                               = 'MaxMind License Key';
$_['entry_score']                             = 'Risk Score';
$_['entry_order_status']                      = 'Bestânstatus';
$_['entry_status']                            = 'Status';

// Help
$_['help_order_status']                       = 'Orden dy't in skoare hawwe oer jo set risiketoertifikaat wurde dizze bestellingstatus oanbean en sil de folsleine status automatysk net berikke.';
$_['help_country_match']                      = 'Of lannen fan IP-adressen oerienkomt mei billingadres lân (mismatch = hegere risiko).';
$_['help_country_code']                       = 'Lânkoade fan it IP adres.';
$_['help_high_risk_country']                  = 'Of IP-adres of faktueradres is yn Ghana, Nigearia, of Fietnam.';
$_['help_distance']                           = 'Ofstân fan IP-adressen nei Farianten Plak yn kilometer (grutte ôfstân = heger risiko).';
$_['help_ip_region']                          = 'Estimearre steat / regio fan it IP adres.';
$_['help_ip_city']                            = 'Estimearre stêd fan it IP adres.';
$_['help_ip_latitude']                        = 'Estimearre Latitude fan it IP-adres.';
$_['help_ip_longitude']                       = 'Estimearre lingte fan it IP adres.';
$_['help_ip_isp']                             = 'ISP fan it IP adres.';
$_['help_ip_org']                             = 'Organisaasje fan it IP adres.';
$_['help_ip_asnum']                           = 'Estimearre autonome systeemnûmer fan it IP adres.';
$_['help_ip_user_type']                       = 'Estimearre brûkersstype fan it IP adres.';
$_['help_ip_country_confidence']              = 'Fertsjintwurdigje fan ús betrouwen dat de lokaasje fan 'e lannen goed is.';
$_['help_ip_region_confidence']               = 'Fertsjintwurdigje ús fertrouwen dat de regio-lokaasje korrekt is.';
$_['help_ip_city_confidence']                 = 'Fertsjintwurdigje ús fertrouwen dat de stedsposysje korrekt is.';
$_['help_ip_postal_confidence']               = 'Fertsjintwurdigje ús fertrouwen dat de postkoade-lokaasje korrekt is.';
$_['help_ip_postal_code']                     = 'Estimearre postkoade fan it IP adres.';
$_['help_ip_accuracy_radius']                 = 'De trochsneed ôfstân tusken de werklike lokaasje fan 'e einbehearder mei it IP-adres en de lokaasje weromjûn troch de GeoIP City-database, yn milen.';
$_['help_ip_net_speed_cell']                  = 'Estimearre netwurkstype fan it IP adres.';
$_['help_ip_metro_code']                      = 'Estimated Metro Code fan it IP adres.';
$_['help_ip_area_code']                       = 'Estimated Area Code fan it IP adres.';
$_['help_ip_time_zone']                       = 'Estimated Time Zone fan it IP adres.';
$_['help_ip_region_name']                     = 'Estimearre streeknamme fan it IP adres.';
$_['help_ip_domain']                          = 'Estimearre domein fan it IP adres.';
$_['help_ip_country_name']                    = 'Estimated country name of the IP address.';
$_['help_ip_continent_code']                  = 'Estimearre kontinintkoade fan it IP adres.';
$_['help_ip_corporate_proxy']                 = 'Of de IP is in Corporate Proxy yn 'e database of net.';
$_['help_anonymous_proxy']                    = 'Of IP-adres is Anonymous Proxy (anonym proxy = tige hege risiko).';
$_['help_proxy_score']                        = 'Wizigje fan IP-adres as in iepen proxy.';
$_['help_is_trans_proxy']                     = 'Of IP-adres is yn ús databank fan bekende transparante proxy-tsjinners, weromjûn as foardreaun isIPIP as in ynfier.';
$_['help_free_mail']                          = 'Of e-mail is fan fergese e-mailprovider (fergees e-post = heger risiko).';
$_['help_carder_email']                       = 'Of e-post is yn databank fan hege risiko-e-mails.';
$_['help_high_risk_username']                 = 'Oft ynterneMD5-ynfier is yn databank fan heech risiko brûkersnammen. Allinnich allinne weromjûn as brûknammeMD5 yn yngongen is opnaam.';
$_['help_high_risk_password']                 = 'Oft inputMD5-input is yn databank fan hege risiko-wachtwurden. Allinnich allinne weromjûn as passwordMD5 yn ynfier is opnaam.';
$_['help_bin_match']                          = 'Of lannen fan útjûne bank binne basearre op BIN nûmer mei biedingsadres lân.';
$_['help_bin_country']                        = 'Lânkoade fan 'e bank dy't de creditcard útjûn hat basearre op BIN nûmer.';
$_['help_bin_name_match']                     = 'Of namme fan de útfier fan bankpegels ynfierde BIN-namme. In weromkommende wearde jout jo in positive yndikaasje dat cardholder yn krêft fan krêftkaart is.';
$_['help_bin_name']                           = 'Namme fan de bank dy't de creditcard útjûn hat basearre op BIN nûmer. Beskikber foar sawat 96% fan BIN nûmers.';
$_['help_bin_phone_match']                    = 'Oft kundenservice tillefoan oerienkomt mei ynfierd BIN Telefoan. In weromkommende wearde jout jo in positive yndikaasje dat cardholder yn krêft fan krêftkaart is.';
$_['help_bin_phone']                          = 'Kwaliteitssjinst tillefoannûmerlist op 'e rêch fan creditcard. Beskikber foar sawat 75% fan BIN nûmers. Yn guon gefallen kin it tillefoannûmer weromjûn wurde.';
$_['help_customer_phone_in_billing_location'] = 'Oft de klant tillefoannûmer is yn de postkoade post. In weromkommende wearde fan Ja jout in positive yndikaasje dat it tillefoannûmer opnij is oan de cardholder. In weromkommende wearde fan Nee jout oan dat it tillefoannûmer op in oare gebiet wêze kin, of miskien net yn ús databank opnommen wurde. NotFound is weromjûn as it prefix tillefoan net fûn wurde yn ús database. Op dit stuit stypje wy allinich fûnsen fan.';
$_['help_ship_forward']                       = 'Of ferstjoeradres is yn databank fan bekende maildruppels.';
$_['help_city_postal_match']                  = 'Of kliïnte fan steds- en statepykcode. Op it stuit beskikber foar allinich US-adressen, jout lege string werom bûten de US.';
$_['help_ship_city_postal_match']             = 'Of streek fan steds- en statepyktekens. Op it stuit beskikber foar allinich US-adressen, jout lege string werom bûten de US.';
$_['help_score']                              = 'Oerflak fraude op basis fan útfieringen dy't hjirboppe neamd binne. Dit is de orizjinele fraude, en basearre op in ienfâldige formule. It is ferfongen troch risiko-skoare (sjoch hjirûnder), mar wurdt bewarre foar efterkompatibiliteit.';
$_['help_explanation']                        = 'In koarte ferklearring fan 'e skoare, detaillearre hokker faktors bydroegen, neffens ús formule. Tink derom dat dit is lyk oan de skoare, net de risikoScore.';
$_['help_risk_score']                         = 'Nije fraudekorporaasje dy't de beskate problemen fertsjintwurdiget dat de opdracht bedriging is, basearre fan 'e analyze fan' e ferline minraven-transaksjes. Ferplichtet in upgrade foar kliïnten dy't foar febrewaris 2007 opskreaun binne.';
$_['help_queries_remaining']                  = 'Oantal oanfragen dy't yn jo akkount bliuwe, kinne brûkt wurde om jo te warskôgjen as jo nedich binne om mear fraach te meitsjen oan jo akkount.';
$_['help_maxmind_id']                         = 'Unike identiteit, brûkt om ferwizing transaksjes by it rapportearjen fan frauduleusaktiviteit werom nei MaxMind. Dizze rapportaazje sil MaxMind helpe om jo tsjinstferliening ferbetterje te kinnen en sil in bepaalde funksje ynskeakelje om de fraudulearringsformulier oan te passen op basis fan jo fergoedingshistoarje.';
$_['help_error']                              = 'Gout in flater tekenrige mei in warskôgings-berjocht of in reden wêrom't it fersyk mislearre is.';

// Error
$_['error_permission']                        = 'Warskôging: Jo hawwe net tastimming om MaxMind anti-fraud te feroarjen!';
$_['error_key']		                          = 'Lisinsjeskaart ferplicht!';