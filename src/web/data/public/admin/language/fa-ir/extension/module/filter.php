<?php
// Heading
$_['heading_title']    = 'Filter';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Sesje: Jo hawwe it filtermodul feroare!';
$_['text_edit']        = 'Filterfilter bewurkje';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming foar it oanpassen fan filtermodul!';