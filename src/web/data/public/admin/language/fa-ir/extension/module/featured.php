<?php
// Heading
$_['heading_title']    = 'Featured';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Sesje: Jo hawwe feroare modul!';
$_['text_edit']        = 'Meitsje bepaalde modul bewurkje';

// Entry
$_['entry_name']       = 'Module namme';
$_['entry_product']    = 'Eigenskippen';
$_['entry_limit']      = 'Beheine';
$_['entry_width']      = 'Breedte';
$_['entry_height']     = 'Hichte';
$_['entry_status']     = 'Status';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om de module te bewurkjen!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_width']      = 'Breedte nedich!';
$_['error_height']     = 'Hichte ferplichte!';