<?php
// Heading
$_['heading_title']    = 'Wetter - Agrarwetter';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe gegevens ferstjoerd!';
$_['text_edit']        = 'Ferwiderje tafoegd';

// Entry
$_['entry_estimator']  = 'Fergunning Estimator';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om de skipfeart total te feroarjen!';