<?php
// Heading
$_['heading_title']    = 'Hannelingskosten';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Sesje: Jo hawwe feroare ôfhannele hantlieding!';
$_['text_edit']        = 'Bewurking fan 'e handling fan' e gearhing';

// Entry
$_['entry_total']      = 'Bestelling Totaal';
$_['entry_fee']        = 'Honorarium';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Help
$_['help_total']       = 'De kastiel totaal de opdracht moat berikke foardat dizze opdracht allinich aktyf wurdt.';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om de fergoedingskosten te feroarjen!';