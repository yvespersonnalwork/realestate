<?php
$_['heading_title']    = 'Google reCAPTCHA';

// Text
$_['text_captcha']     = 'Captcha';
$_['text_success']	   = 'Sesje: Jo hawwe Google reCAPTCHA feroare!';
$_['text_edit']        = 'Bewurkje Google reCAPTCHA';
$_['text_signup']      = 'Gean nei <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>Google reCAPTCHA page</u></a> en registrearje jo webside.';

// Entry
$_['entry_key']        = 'Side-kaai';
$_['entry_secret']     = 'Geheime kaai';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om Google reCAPTCHA te feroarjen!';
$_['error_key']	       = 'Key nedich!';
$_['error_secret']	   = 'Geheim nedich!';
