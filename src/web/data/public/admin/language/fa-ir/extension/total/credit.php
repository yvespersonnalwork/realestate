<?php
// Heading
$_['heading_title']    = 'Webside kredyt';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe wizige Webside total makke!';
$_['text_edit']        = 'Webside Webside Totaal bewurkje';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om Webside kredyt totaal te wizigjen!';