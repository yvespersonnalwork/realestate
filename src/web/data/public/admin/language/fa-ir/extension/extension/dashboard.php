<?php
// Heading
$_['heading_title']     = 'Dashboard';

// Text
$_['text_success']      = 'Súkses: Jo hawwe Dashboards feroare!';
$_['text_list']         = 'Dashboard List';

// Column
$_['column_name']       = 'Dashboardnamme';
$_['column_width']      = 'Breedte';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Aksje';
// Error
$_['error_permission']  = 'Warskôging: Jo hawwe gjin rjochten om Dashboards te feroarjen!';