<?php
// Heading
$_['heading_title']     = 'Wetter - Agrarwetter';

// Text
$_['text_success']      = 'Súkses: Jo hawwe ferfangen ferfangen!';
$_['text_list']         = 'Shipping List';

// Column
$_['column_name']       = 'Ferstjoermetoade';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Aksje';

// Error
$_['error_permission']  = 'Warskôging: Jo hawwe gjin tastimming om de skip te feroarjen!';