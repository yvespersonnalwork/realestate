<?php
// Heading
$_['heading_title']    = 'Google Hangouts';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Sesje: Jo hawwe it modul fan Google Hangouts feroare!';
$_['text_edit']        = 'Bewurkje Google Hangouts-module';

// Entry
$_['entry_code']       = 'Google Talk Code';
$_['entry_status']     = 'Status';

// Help
$_['help_code']        = 'Gean nei <a href="https://developers.google.com/+/hangouts/button" target="_blank">Meitsje in Google Hangout chatback badge </a> en kopiearje & amp; Paste de generearre koade yn it tekstfak.';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om modul Google Hangouts te feroarjen!';
$_['error_code']       = 'Code ferplicht';