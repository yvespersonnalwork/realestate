<?php
// Heading
$_['heading_title']    = 'Modules';

// Text
$_['text_success']     = 'Súkses: Jo hawwe modifisearre modules!';
$_['text_layout']      = 'Nei't jo in module ynstalleare en ynsteld hawwe kinne jo it oanmeitsje oan in layout <a href="%s" class="alert-link">here</a>!';
$_['text_add']         = 'Module taheakje';
$_['text_list']        = 'Module List';

// Column
$_['column_name']      = 'Module namme';
$_['column_action']    = 'Aksje';

// Entry
$_['entry_code']       = 'Module';
$_['entry_name']       = 'Module namme';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin rjochten om modules te wizigjen!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_code']       = 'Ekstra nedich!';