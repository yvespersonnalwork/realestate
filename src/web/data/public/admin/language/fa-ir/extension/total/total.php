<?php
// Heading
$_['heading_title']    = 'Totaal';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Sesje: Jo hawwe totale totalen feroare!';
$_['text_edit']        = 'Totaal Totaal bewurkjen';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin rjochten om de totale totalen te feroarjen!';