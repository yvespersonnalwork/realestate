<?php
// Heading
$_['heading_title']              = 'FraudLabs Pro';

// Text
$_['text_extension']             = 'Extensions';
$_['text_success']               = 'Súkses: Jo hawwe de FraudLabs Pro ynstellingen feroare!';
$_['text_edit']                  = 'Ynstellings';
$_['text_signup']                = 'FraudLabs Pro is in fraude-deteksje tsjinst. Do kinst <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"><u>sign up here</u></a> for a free API Key.';
$_['text_id']                    = 'FraudLabs Pro ID';
$_['text_ip_address']            = 'IP Adres';
$_['text_ip_net_speed']          = 'IP Net Speed';
$_['text_ip_isp_name']           = 'IP ISP Namme';
$_['text_ip_usage_type']         = 'IP Utjeftyp';
$_['text_ip_domain']             = 'IP Domein';
$_['text_ip_time_zone']          = 'IP Tiidsône';
$_['text_ip_location']           = 'IP Lokaasje';
$_['text_ip_distance']           = 'IP Ôfstân';
$_['text_ip_latitude']           = 'IP Breedte';
$_['text_ip_longitude']          = 'IP Longitude';
$_['text_risk_country']          = 'High Risiko Lân';
$_['text_free_email']            = 'Free Email';
$_['text_ship_forward']          = 'Ship Forward';
$_['text_using_proxy']           = 'Proksy brûke';
$_['text_bin_found']             = 'BIN fûn';
$_['text_email_blacklist']       = 'Email Blacklist';
$_['text_credit_card_blacklist'] = 'Credit Card Blacklist';
$_['text_score']                 = 'FraudLabs Pro Score';
$_['text_status']                = 'FraudLabs Pro Status';
$_['text_message']               = 'Berjocht';
$_['text_transaction_id']        = 'Transaksje ID';
$_['text_credits']               = 'Lykwicht';
$_['text_error']                 = 'Fersin:';
$_['text_flp_upgrade']           = '<a href="http://www.fraudlabspro.com/plan" target="_blank">[Upgrade]</a>';
$_['text_flp_merchant_area']     = 'Meitsje jo op <a href="http://www.fraudlabspro.com/merchant/login" target="_blank"> FraudLabs Pro Merchant Area </a> foar mear ynformaasje oer dizze oarder.';


// Entry
$_['entry_status']               = 'Status';
$_['entry_key']                  = 'API-kaai';
$_['entry_score']                = 'Risk Score';
$_['entry_order_status']         = 'Bestânstatus';
$_['entry_review_status']        = 'Oersjoch Status';
$_['entry_approve_status']       = 'Befêstigje Status';
$_['entry_reject_status']        = 'Stjoeringstatus';
$_['entry_simulate_ip']          = 'Simulate IP';

// Help
$_['help_order_status']          = 'Orden dy't in skoare hawwe oer jo set risiketoertifikaat wurde dizze bestjoerstatus oanbean.';
$_['help_review_status']         = 'Orden dy't as beoardieling fan FraudLabs Pro markearre wurde sil dizze bestjoerstatus oanjûn wurde.';
$_['help_approve_status']        = 'Orden dy 't as goedkard wurde troch FraudLabs Pro wurde dizze bestjoerstatus oanbean.';
$_['help_reject_status']         = 'Orden dy 't as wegere wurde troch FraudLabs Pro wurde dizze bestjoerstatus oanjûn.';
$_['help_simulate_ip']           = 'Simulearje it besikersadres foar it testen. Lit leech dat it útskeakelje sil.';
$_['help_fraudlabspro_id']       = 'Unike identiteit foar in transaksje dy't troch FraudLabs Pro systeem skreaun is.';
$_['help_ip_address']            = 'IP-adres.';
$_['help_ip_net_speed']          = 'Ferbetterdheid.';
$_['help_ip_isp_name']           = 'ISP fan it IP adres.';
$_['help_ip_usage_type']         = 'Brûktype fan it IP-adres. E, ISP, Commercial, Residential.';
$_['help_ip_domain']             = 'Domein namme fan it IP adres.';
$_['help_ip_time_zone']          = 'Tiidsône fan it IP adres.';
$_['help_ip_location']           = 'Lokaasje fan it IP adres.';
$_['help_ip_distance']           = 'Ofstân fan IP-adressen nei Billing-lokaasje.';
$_['help_ip_latitude']           = 'Latitude fan it IP-adres.';
$_['help_ip_longitude']          = 'Lengte fan it IP-adres.';
$_['help_risk_country']          = 'Of IP-adres lân is yn 'e lêste list fan heechste risiko's.';
$_['help_free_email']            = 'Of e-mail is fan 'e fergeze e-postprovider.';
$_['help_ship_forward']          = 'Of ferstjoeradres is in frachtfrageradres.';
$_['help_using_proxy']           = 'Of IP-adres is fan Anonymous Proxy Server.';
$_['help_bin_found']             = 'Oft de BIN-ynformaasje oerienkommt mei ús BIN list.';
$_['help_email_blacklist']       = 'Oft it e-mailadres yn ús blacklistlist is.';
$_['help_credit_card_blacklist'] = 'Of de creditcard is yn ús blacklistlistedaten.';
$_['help_score']                 = 'Risiko-score, 0 (leech risiko) - 100 (hege risiko).';
$_['help_status']                = 'FraudLabs Pro status.';
$_['help_message']               = 'FraudLabs Pro error message description.';
$_['help_transaction_id']        = 'Unike identiteit foar in transaksje dy't troch FraudLabs Pro systeem skreaun is.';
$_['help_credits']               = 'Balâns fan de credits beskikber nei dizze transaksje.';

// Error
$_['error_permission']           = 'Warskôging: Jo hawwe gjin rjochten om de ynstellings fan FraudLabs Pro te feroarjen!';
$_['error_key']		             = 'API kaai ferklearre!';