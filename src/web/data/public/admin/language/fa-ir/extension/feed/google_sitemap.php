<?php
// Heading
$_['heading_title']    = 'Google Sitemap';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Sesje: Jo hawwe it Google Sitemap feed feroare!';
$_['text_edit']        = 'Bewurkje Google Sitemap';

// Entry
$_['entry_status']     = 'Status';
$_['entry_data_feed']  = 'Data Feed Url';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om Google Sitemap feed te feroarjen!';