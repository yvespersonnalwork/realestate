<?php
// Heading
$_['heading_title']    = 'Carousel';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe bewurke karosjele module!';
$_['text_edit']        = 'Karosel-modul bewurkje';

// Entry
$_['entry_name']       = 'Module namme';
$_['entry_banner']     = 'Banner';
$_['entry_width']      = 'Breedte';
$_['entry_height']     = 'Hichte';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming om modus-modus te feroarjen!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_width']      = 'Breedte nedich!';
$_['error_height']     = 'Hichte ferplichte!';