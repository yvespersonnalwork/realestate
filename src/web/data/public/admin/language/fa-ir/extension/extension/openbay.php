<?php
// Heading
$_['heading_title']        				= 'OpenBay Pro';

// Buttons
$_['button_retry']						= 'Gean op 'e nij';
$_['button_update']						= 'Update';
$_['button_patch']						= 'Patch';
$_['button_faq']						= 'View FAQ topic';

// Tab
$_['tab_setting']						= 'Ynstellings';
$_['tab_update']						= 'Software updates';
$_['tab_update_v1']						= 'Easy updater';
$_['tab_patch']							= 'Patch';
$_['tab_developer']						= 'Untwikkelder';

// Text
$_['text_dashboard']         			= 'Dashboard';
$_['text_success']         				= 'Súkses: Ynstellings binne bewarre';
$_['text_products']          			= 'Items';
$_['text_orders']          				= 'Orders';
$_['text_manage']          				= 'Manage';
$_['text_help']                     	= 'Help';
$_['text_tutorials']                    = 'Tutorials';
$_['text_suggestions']                  = 'Ideeën';
$_['text_version_latest']               = 'Jo binne de lêste ferzje';
$_['text_version_check']     			= 'Checking software version';
$_['text_version_installed']    		= 'Ynstallearre ferzje fan OpenBay Pro: v';
$_['text_version_current']        		= 'Jo ferzje is';
$_['text_version_available']        	= 'de lêste is';
$_['text_language']             		= 'API antwurd taal';
$_['text_getting_messages']     		= 'Berjochten fan OpenBay Pro krije';
$_['text_complete']     				= 'Kompleet';
$_['text_patch_complete']           	= 'Patch is tapast';
$_['text_connection_ok']				= 'Ferbûn mei server OK. Real estate folders fûn';
$_['text_updated']						= 'Module is fernijd (v.%s)';
$_['text_update_description']			= 'It updateprogramma sil feroaringen meitsje fan jo webside-systeem. Soargje derfoar dat jo in folsleine triem-en databank-reservaasje hawwe foardat jo aktivearje.';
$_['text_patch_description']			= 'As jo de aktive triemmen manuell oplaad hawwe, moatte jo de patch útfiere om it update te foltôgjen';
$_['text_clear_faq']                    = 'Wiskje ferburgen FAQ popups';
$_['text_clear_faq_complete']           = 'Notifikaasjes sille no wer wer sjen';
$_['text_install_success']              = 'Marketplace is ynstallearre';
$_['text_uninstall_success']            = 'Marketplace is fuortsmiten';
$_['text_title_messages']               = 'Berjochten & amp; Notifikaasjes';
$_['text_marketplace_shipped']			= 'De bestellingstatus wurdt aktualisearre om te leverjen op 'e merkplaats';
$_['text_action_warning']				= 'Dizze aksje is gefaarlik sa is wachtwurd beskerme.';
$_['text_check_new']					= 'Kontrolearje foar nije ferzje';
$_['text_downloading']					= 'Herunterladen der Aktualisierungsdateien';
$_['text_extracting']					= 'Triemmen útfiere';
$_['text_running_patch']				= 'Rinnende patchbestannen';
$_['text_fail_patch']					= 'It bestannen fan fernijde bestannen net mooglik te meitsjen';
$_['text_updated_ok']					= 'Update folslein ynstallearre ferzje is no ';
$_['text_check_server']					= 'Kontrolearjen fan serverbedriuwen';
$_['text_version_ok']					= 'Software is al op 'e hichte, ynstalleare ferzje is ';
$_['text_remove_files']					= 'Triemen wiskje net mear nedich';
$_['text_confirm_backup']				= 'Asjebleaft dat jo in folsleine reservekopy hawwe foardat jo trochgean';

// Column
$_['column_name']          				= 'Pluginnamme';
$_['column_status']        				= 'Status';
$_['column_action']        				= 'Aksje';

// Entry
$_['entry_patch']            			= 'Hânlieding update patch';
$_['entry_courier']						= 'Courier';
$_['entry_courier_other']           	= 'Oare courier';
$_['entry_tracking']                	= 'Tracking #';
$_['entry_empty_data']					= 'Lege gegevens fan 'e webside?';
$_['entry_password_prompt']				= 'Fier hjirûnder de gegevens wekke wachtwurd';
$_['entry_update']						= 'Easy 1 klik opnij';
$_['entry_beta']						= 'Beta ferzje brûke';

// Error
$_['error_admin']             			= 'Admin-ferwacht ferwachte';
$_['error_no_admin']					= 'Ferbining OK mar jo directoryEverate admin directory waard net fûn';
$_['error_no_files']					= 'Ferbining OK mar RealEstate mappen fûnen net fûn! Is jo rootpaad korrekt?';
$_['error_failed']						= 'It is mislearre om te laden, werneare?';
$_['error_tracking_id_format']			= 'Jo tracking-ID kin de karakters net befetsje > or <';
$_['error_tracking_courier']			= 'Jo moatte in courier selektearje as jo in tracking-ID taheakje wolle';
$_['error_tracking_custom']				= 'Ferjit jo korrierich fjild leech as jo gewoane brûker brûke wolle';
$_['error_permission']					= 'Jo hawwe net tastimming om de útwreiding fan OpenBay Pro te feroarjen';
$_['error_mkdir']						= 'PHP mkdir funksje is útskeakele, kontakt jo host';
$_['error_file_delete']					= 'Dizze triemmen kin net fuortsmiten wurde, dizzen moatte jo se hanthavenje';
$_['error_mcrypt']            			= 'PHP-funksje "mcrypt_encrypt" is net ynskeakele. Kontakt jo hosting provider.';
$_['error_mbstring']               		= 'PHP-bibleteek "mb strings" is net ynskeakele. Kontakt jo hosting provider.';
$_['error_oc_version']             		= 'Jo ferzje fan RealEstate wurdt net hifke om te wurkjen mei dit module. Jo kinne problemen hawwe.';
$_['error_fopen']             			= 'PHP-funksje "fopen" is útskeakele troch jo host - jo kinne net bylden ymportearje by it ymportearjen fan produkten';
$_['lang_error_vqmod']             		= 'Jo map fan vqmod befettet âldere OpenBay Pro-bestannen - dy moatte fuorthelle wurde!';

// Help
$_['help_clear_faq']					= 'Lit alle helptechniken werjaan';
$_['help_empty_data']					= 'Dit kin grutte skea liede, brûke it net as jo net witte wat it dogge!';
$_['help_easy_update']					= 'Klik op fernijing om de lêste ferzje fan OpenBay Pro automatysk te ynstallearjen';
$_['help_patch']						= 'Klik om it patch skripts út te fieren';
$_['help_beta']							= 'Foarsichtigens! De beta ferzje is de lêste ûntwikkeling ferzje. It kin net stabyl wêze en bugs befetsje.';