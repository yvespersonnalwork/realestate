<?php
// Heading
$_['heading_title']    = 'Analytics';

// Text
$_['text_success']     = 'Súkses: Jo hawwe feroare analytics!';
$_['text_list']        = 'Analytics List';

// Column
$_['column_name']      = 'AnalytikName Name';
$_['column_status']    = 'Status';
$_['column_action']    = 'Aksje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om analyten te feroarjen!';