<?php
// Heading
$_['heading_title']    = 'Total Sales';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe feroare fan dashboard feroare!';
$_['text_edit']        = 'Dashboard Sales feroarje';
$_['text_view']        = 'Sjoch mear...';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_width']      = 'Breedte';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming foar it feroarjen fan buroblêdferkeap!';