<?php
$_['heading_title']    = 'Basic Captcha';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']	   = 'Súkses: Jo hawwe Basic Captcha geane!';
$_['text_edit']        = 'Basearre Captcha bewurkje seksje edit source';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming foar it meitsjen fan Basic Captcha!';
