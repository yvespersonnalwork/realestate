<?php
// Heading
$_['heading_title']    = 'Kategoryen';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe oanlieding fan klamboerd!';
$_['text_edit']        = 'Dashboard-klant bewurkje';
$_['text_view']        = 'Sjoch mear...';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_width']      = 'Breedte';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om it kanaal fan dashboard te feroarjen!';