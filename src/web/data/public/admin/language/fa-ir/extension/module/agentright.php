<?php

// Heading
$_['heading_title']    = 'Agent Right side';

// Text

$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe modifikaasjemodule feroare!';
$_['text_edit']        = 'Agent Module bewurkje';

// Lable
$_['lable_name']       = 'Module namme';
$_['lable_limit']      = 'Beheine';
$_['lable_width']      = 'Breedte';
$_['lable_height']     = 'Hichte';
$_['lable_status']     = 'Status';

// Entry
$_['entry_name']       = 'Module namme';
$_['entry_limit']      = 'Beheine';
$_['entry_width']      = 'Breedte';
$_['entry_height']     = 'Hichte';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om modul Agent te feroarjen!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_width']      = 'Breedte nedich!';
$_['error_height']     = 'Hichte ferplichte!';