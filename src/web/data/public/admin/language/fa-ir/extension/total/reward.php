<?php
// Heading
$_['heading_title']    = 'Belangpunten';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Sesje: Jo hawwe allinich belibjende puntenpunten feroare!';
$_['text_edit']        = 'Beloangpunten opnij bewurkje';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming om totale puntenpunten yn totaal te wizigjen!';