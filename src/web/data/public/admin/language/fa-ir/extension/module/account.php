<?php
// Heading
$_['heading_title']    = 'Rekken';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Sesje: Jo hawwe feroare modul!';
$_['text_edit']        = 'Konto-modul bewurkje';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin rjochten om it modul fan akkount te wizigjen!';