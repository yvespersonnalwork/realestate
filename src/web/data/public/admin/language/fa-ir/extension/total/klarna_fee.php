<?php
// Heading
$_['heading_title']    = 'Klarna Fee';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe it totale kliïnne-fergoan feroare!';
$_['text_edit']        = 'Klarna Fee bewurkje Total';
$_['text_sweden']      = 'Sweden';
$_['text_norway']      = 'Noarwegen';
$_['text_finland']     = 'Finlân';
$_['text_denmark']     = 'Denemark';
$_['text_germany']     = 'Dútslân';
$_['text_netherlands'] = 'GermanyThe Netherlands';

// Entry
$_['entry_total']      = 'Bestelling Totaal';
$_['entry_fee']        = 'Invoice Fee';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om totalisearring klant te feroarjen!';