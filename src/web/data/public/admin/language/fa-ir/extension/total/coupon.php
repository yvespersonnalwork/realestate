<?php
// Heading
$_['heading_title']    = 'Coupon';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe in totale kûp oanpast!';
$_['text_edit']        = 'Bewurkingsjild';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om de kûpon total te feroarjen!';