<?php
// Heading
$_['heading_title']    = 'Kado Kaart';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe gegevens foar kado fan kado feroare!';
$_['text_edit']        = 'Gavé Gûle Totaal bewurkje';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om totaal foar kado fan kado te feroarjen!';