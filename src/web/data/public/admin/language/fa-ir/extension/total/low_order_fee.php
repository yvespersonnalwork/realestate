<?php
// Heading
$_['heading_title']    = 'Wetter - Agrarwetter';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe feroare oan nûmer bestelling!';
$_['text_edit']        = 'Tydlike fergoedingsburo bewurkje seksje edit source';

// Entry
$_['entry_total']      = 'Bestelling Totaal';
$_['entry_fee']        = 'Honorarium';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Help
$_['help_total']       = 'De kastiel yn totaal de opdracht moat berikke foardat dizze bestelings totaal deaktivearre binne.';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om totaal nûmere bestelling te feroarjen!';
