<?php
$_['heading_title']    = 'Google Analytics';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']	   = 'Sesje: Jo hawwe Google Analytics feroare!';
$_['text_signup']      = 'Oanmelde jo <a href="http://www.google.com/analytics/" target="_blank"><u>Google Analytics</u></a> akkount en nei it meitsjen fan jo webside profyl kopiearje en paste de analytyske koade yn dit fjild.';
$_['text_default']     = 'Standert';

// Entry
$_['entry_code']       = 'Google Analytics Code';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om Google Analytics te feroarjen!';
$_['error_code']	   = 'Code nedich!';
