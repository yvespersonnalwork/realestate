<?php
// Heading
$_['heading_title']    		= 'TMD Blogfilterprodukt<b>(TMD)</b>';

// Text
$_['text_extension']        = 'Extensions';
$_['text_success']     		= 'Súkses: Jo hawwe it fliterproduktmodul feroare!';
$_['text_edit']        		= 'Fliterprodukt modul bewurkje';
$_['text_tab']         		= 'Tab';
$_['text_vertcal']     		= 'Vertical / Normal';
$_['text_normal']      		= 'Normaal';
$_['text_crousal']     		= 'Vertical + Carousel';
$_['text_tabcrousal']  		= 'Tab + Carousel';

// Entry
$_['entry_name']       		= 'Module namme';
$_['entry_layouttype'] 		= 'Layouttype';
$_['entry_crousaltype']		= 'Carousel Type';
$_['entry_limit']      		= 'Beheine';
$_['entry_width']      		= 'Breedte';
$_['entry_height']     		= 'Hichte';
$_['entry_status']     		= 'Status';
$_['entry_moduletitle']     = 'Module Heading Title';
$_['entry_title']       	= 'Module Title';
$_['entry_product']     	= 'Produkt';

// Help


$_['tab_recentpost']     = 'Lêste postmodul';
$_['tab_popular']     = 'Populêre module';
$_['tab_comment']     = 'Comment';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om modusfliterproduktmodul te feroarjen!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_width']      = 'Breedte nedich!';
$_['error_height']     = 'Hichte ferplichte!';