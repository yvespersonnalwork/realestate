<?php
// Heading
$_['heading_title']    = 'Tema's';

// Text
$_['text_success']     = 'Sesje: Jo hawwe tema 's feroarjen!';
$_['text_list']        = 'Tema list';

// Column
$_['column_name']      = 'Temaamme';
$_['column_status']    = 'Status';
$_['column_action']    = 'Aksje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe gjin tastimming foar tema's te feroarjen!';