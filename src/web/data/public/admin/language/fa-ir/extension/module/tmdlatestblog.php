<?php
// Heading
$_['heading_title']    = 'Tmd Latest Blog';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Sesje: Jo hawwe tmdlatestblogmodul feroare!';
$_['text_edit']        = 'Bewurkje Tmd Latest Blog Module';

// Entry
$_['entry_name']       = 'Module namme';
$_['entry_limit']      = 'Beheine';
$_['entry_width']      = 'Breedte';
$_['entry_height']     = 'Hichte';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om modblêd modblad te feroarjen!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_width']      = 'Breedte nedich!';
$_['error_height']     = 'Hichte ferplichte!';