<?php
// Heading
$_['heading_title']    = 'Anti-fraude';

// Text
$_['text_success']     = 'Súkses: Jo hawwe feroare anti-fraud!';
$_['text_list']        = 'Anti-fraudlist';

// Column
$_['column_name']      = 'Anti-fraudnamme';
$_['column_status']    = 'Status';
$_['column_action']    = 'Aksje';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om te feroarjen fan anty-fraude!';