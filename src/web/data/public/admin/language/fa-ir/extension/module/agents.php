<?php
// Heading
$_['heading_title']    = 'Aginten lofterkant';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe modellen Aginten feroare!';
$_['text_edit']        = 'Bewurkje Agents Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om modules Agents te feroarjen!';
