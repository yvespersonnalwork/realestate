<?php
// Heading
$_['heading_title']     = 'Order Totals';

// Text
$_['text_success']      = 'Sesje: Jo hawwe feroare totalen!';
$_['text_list']         = 'Bestellist bestellen';

// Column
$_['column_name']       = 'Order Totals';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Aksje';

// Error
$_['error_permission']  = 'Warskôging: Jo hawwe gjin tastimming om de totalen te feroarjen!';