<?php
// Heading
$_['heading_title']    = 'Tmd Related Blog';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Súkses: Jo hawwe feroare Tmd Related Blog module!';
$_['text_edit']        = 'Bewurkje Tmd Related Blog Module';

// Entry
$_['entry_name']       = 'Module namme';
$_['entry_product']    = 'Products';
$_['entry_limit']      = 'Beheine';
$_['entry_width']      = 'Breedte';
$_['entry_height']     = 'Hichte';
$_['entry_status']     = 'Status';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om te modifere Tmd Related Blog module!';
$_['error_name']       = 'Modulnamme moat tusken 3 en 64 tekens wêze!';
$_['error_width']      = 'Breedte nedich!';
$_['error_height']     = 'Hichte ferplichte!';