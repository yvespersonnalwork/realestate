<?php
// Heading
$_['heading_title']      = 'Anti-fraude IP';

// Text
$_['text_extension']     = 'Extensions';
$_['text_success']       = 'Súkses: Jo hawwe wizige anti-fraude IP!';
$_['text_edit']          = 'Wizigje Anti-Fraud IP';
$_['text_ip_add']        = 'Add IP Address';
$_['text_ip_list']       = 'Fraud-IP-adreslist';

// Column
$_['column_ip']          = 'IP';
$_['column_total']       = 'Total Accounts';
$_['column_date_added']  = 'Total Accounts';
$_['column_action']      = 'Aksje';

// Entry
$_['entry_ip']           = 'IP';
$_['entry_status']       = 'Status';
$_['entry_order_status'] = 'Bestânstatus';

// Help
$_['help_order_status']  = 'Kliïnters dy't in ferbeide IP hawwe op har akkounts, wurde dizze bestelstatus oanbean en sil de folsleine status automatysk net berikke.';

// Error
$_['error_permission']   = 'Warskôging: Jo hawwe gjin tastimming foar it feroarjen fan Anti-Fraud IP!';
