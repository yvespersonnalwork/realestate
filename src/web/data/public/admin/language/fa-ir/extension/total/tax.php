<?php
// Heading
$_['heading_title']    = 'Wetter - Agrarwetter';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Súkses: Jo hawwe allinich belestingen feroare!';
$_['text_edit']        = 'Taksje Totaal bewurkje';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warskôging: Jo hawwe net tastimming om totaal te feroarjen!';