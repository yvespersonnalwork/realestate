<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-plans" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-plans" class="form-horizontal">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li class=""><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="plans_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($plans_description[$language['language_id']]) ? $plans_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="plans_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($plans_description[$language['language_id']]) ? $plans_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
			  
			  
			      <div class="form-group">
              <label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>
              <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
              </div>
            </div>
			  
			      <div class="form-group">
              <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_product_limit; ?></label>
              <div class="col-sm-10">
                <input type="number" name="property_limit" value="<?php echo $property_limit; ?>" placeholder="<?php echo $entry_product_limit; ?>" id="input-price" class="form-control" />
              </div>
            </div>
			  
			      <div class="form-group">
              <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
              <div class="col-sm-10">
                <input type="number" name="price" value="<?php echo $price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-noofday"><?php echo $entry_noodday; ?></label>
              <div class="col-sm-10">
                <input type="number" name="no_of_day" value="<?php echo $no_of_day; ?>" placeholder="<?php echo $entry_noodday; ?>" id="input-noofday" class="form-control" />
              </div>
            </div>
			  
		        <div class="form-group">
              <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_packagelimit; ?></label>
              <div class="col-sm-10">
        				<select name="type" id="input-status" class="form-control">
        					<option value=""><?php echo $text_select; ?></option>
        					<option value="day" <?php if ($type == 'day') echo 'selected';?>><?php echo $text_day; ?></option>
        					<option value="month" <?php if ($type == 'month') echo 'selected';?>><?php echo $text_month; ?></option>
        					<option value="year" <?php if ($type == 'year') echo 'selected';?>><?php echo $text_years; ?></option>
        				</select>
              </div>
            </div>
        		
            <div class="form-group">
        			<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
        			<div class="col-sm-10">
        			<select name="status" id="input-status" class="form-control">
        			<?php if ($status) { ?>
        			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
        			<option value="0"><?php echo $text_disabled; ?></option>
        			<?php } else { ?>
        			<option value="1"><?php echo $text_enabled; ?></option>
        			<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
        			<?php } ?>
        			</select>
        			</div>
        		</div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-top"><?php echo $entry_top; ?></label>
              <div class="col-sm-10">
                <input type="number" name="top" value="<?php echo $top; ?>" placeholder="<?php echo $entry_top; ?>" id="input-top" class="form-control" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-features"><?php echo $entry_features; ?></label>
              <div class="col-sm-10">
                <input type="number" name="feature" value="<?php echo $feature; ?>" placeholder="<?php echo $entry_features; ?>" id="input-features" class="form-control" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-bottom"><?php echo $entry_bottom; ?></label>
              <div class="col-sm-10">
                <input type="number" name="bottom" value="<?php echo $bottom; ?>" placeholder="<?php echo $entry_bottom; ?>" id="input-bottom" class="form-control" />
              </div>
            </div>


      
        </form>
      </div>
    </div>
  </div>
  
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script> 
 	<script type="text/javascript"><!--
	$('#language a:first').tab('show');
	//--></script>	 