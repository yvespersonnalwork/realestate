<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<base href="<?php echo $base; ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <img src="<?php echo $image ?>" style="margin: 15px 0 15px 0;" />
  <div style="page-break-after: always;">
    <div class="row">
      <div class="col-sm-6"><h1><?php echo $text_invoice; ?> #<?php echo $order_id; ?></h1></div>
      <div class="col-sm-6 text-right"><button class="btn btn-info" onclick="window.print()"><i class="fa fa-print" aria-hidden="true"></i><?php echo $text_print; ?></button></div>
    </div>  
    <table class="table table-bordered">
      <tbody>
        <tr>
          <td><b><?php echo $entry_Agent; ?></b></td>
          <td><?php echo $agentname; ?></td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <td><b><?php echo $entry_email; ?></b></td>
          <td><?php echo $email; ?></td>
        </tr>
      </tbody>

      <tbody>
        <tr>
          <td><b><?php echo $entry_address; ?></b></td>
          <td><?php echo $address; ?></td>
        </tr>
      </tbody>
      
      <tbody>
        <tr>
          <td><b><?php echo $entry_plans; ?></b></td>
          <td><?php echo $planname; ?></td>
        </tr>
      </tbody> 
      <tbody>
        <tr>
          <td><b><?php echo $entry_propertylimit  ; ?></b></td>
          <td><?php echo $property_limit; ?></td>
        </tr>
      </tbody>
	  
	  <?php if(!empty($price)) { ?>
	    <tbody>
        <tr>
          <td><b><?php echo $text_price  ; ?></b></td>
          <td><?php echo $price; ?></td>
        </tr>
      </tbody>
	  <?php } ?>
	  	  <?php if(!empty($nameorder)) { ?>
	    <tbody>
        <tr>
          <td><b><?php echo $text_orderstatus  ; ?></b></td>
          <td><?php echo $nameorder; ?></td>
        </tr>
      </tbody>
	  <?php } ?>
	  
  
      <tbody>
        <tr>
          <td><b><?php echo $entry_planstart  ; ?></b></td>
          <td><?php echo $plandate; ?></td>
        </tr>
      </tbody>

      <tbody>
        <tr>
          <td><b><?php echo $entry_planend  ; ?></b></td>
          <td><?php echo $expirydate; ?></td>
        </tr>
      </tbody>
		<?php if(!empty($top)){ ?>
			<tbody>
				<tr>
				  <td><b><?php echo $entry_top  ; ?></b></td>
				  <td><?php echo $top; ?></td>
				</tr>
			</tbody>
		<?php } ?>
		
		<?php if(!empty($feature)){ ?>
      <tbody>
        <tr>
          <td><b><?php echo $entry_feature  ; ?></b></td>
          <td><?php echo $feature; ?></td>
        </tr>
      </tbody>
	  <?php } ?>
   <?php if(!empty($bottom)){ ?>
      <tbody>
        <tr>
          <td><b><?php echo $entry_bottom  ; ?></b></td>
          <td><?php echo $bottom; ?></td>
        </tr>
      </tbody>
	  <?php } ?>
    </table>
    
  </div>
</div>
</body>
</html>
