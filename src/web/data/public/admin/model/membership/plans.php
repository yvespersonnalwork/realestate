<?php
class ModelMembershipPlans extends Model {
	public function addPlans($data) {
	$this->db->query("INSERT INTO " . DB_PREFIX . "plans SET  top = '" . (int)$data['top']. "',feature = '" . (int)$data['feature']. "',bottom = '" . (int)$data['bottom']. "',no_of_day = '" . (int)$data['no_of_day']. "',price = '" . (float)$data['price']. "', image = '" . $this->db->escape($data['image']) . "',property_limit = '" . (int)$data['property_limit'] . "', status = '" . (int)$data['status'] . "',type = '" .$data['type'] . "', date_modified = NOW(), date_added = NOW()");
	$plans_id = $this->db->getLastId();
	if (isset($data['plans_description'])){
	 foreach ($data['plans_description'] as $language_id => $value) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "plans_description SET plans_id = '" . (int)$plans_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
	 }
	}
	return $plans_id;
		
	}

	public function editPlans($plans_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "plans SET  top = '" . (int)$data['top']. "',feature = '" . (int)$data['feature']. "',bottom = '" . (int)$data['bottom']. "',no_of_day = '" . (float)$data['no_of_day']. "',price = '" . (float)$data['price']. "', property_limit = '" . (int)$data['property_limit'] . "', status = '" . (int)$data['status'] . "',type = '" .$data['type'] . "', date_modified = NOW() WHERE plans_id = '" . (int)$plans_id . "'");
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "plans SET image = '" . $this->db->escape($data['image']) . "' WHERE plans_id = '" . (int)$plans_id . "'");
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "plans_description WHERE plans_id = '" . (int)$plans_id . "'");
		foreach ($data['plans_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "plans_description SET plans_id = '" . (int)$plans_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}	

		
		
		$this->cache->delete('plans');
	}

	public function deletePlans($plans_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "plans WHERE plans_id = '" . (int)$plans_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "plans_description WHERE plans_id = '" . (int)$plans_id . "'");
		$this->cache->delete('plans');
	}

	public function getPlans($plans_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'plans_id=" . (int)$plans_id . "') AS keyword FROM " . DB_PREFIX . "plans WHERE plans_id = '" . (int)$plans_id . "'");

		return $query->row;
	}
	
	public function getPlansiesedit($plans_id) {
				$sql = "SELECT * FROM " . DB_PREFIX . "plans p LEFT JOIN " . DB_PREFIX . "plans_description pd ON (p.plans_id = pd.plans_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.plans_id='".$plans_id."'";
				
		
					$query=$this->db->query($sql);
					return $query->row;



				}

	public function getPlansies($data = array()) {
			$sql = "SELECT * FROM " . DB_PREFIX . "plans p LEFT JOIN " . DB_PREFIX . "plans_description pd ON (p.plans_id = pd.plans_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
			$sort_data = array(
				'pd.name',
				'p.property_limit'
			);
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY pd.name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		
	}
	

	public function getPlansDescriptions($plans_id) {
		$plans_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "plans_description WHERE plans_id = '" . (int)$plans_id . "'");

		foreach ($query->rows as $result) {
			$plans_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description']
			);
		}

		return $plans_description_data;
	}
	
	public function getTotalPlansies() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "plans");

		return $query->row['total'];
	}	
	

	
}
