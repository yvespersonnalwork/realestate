<?php

class ControllerMembershipPlans extends Controller {
private $error = array();
	public function index() {
		$this->load->language('membership/plans');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('membership/plans');
		$this->getList();
	}


	public function add() {
		$this->load->language('membership/plans');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('membership/plans');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_membership_plans->addPlans($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url, true));
		}
	 $this->getForm();
	}
	public function edit() {
		$this->load->language('membership/plans');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('membership/plans');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		$this->model_membership_plans->editPlans($this->request->get['plans_id'], $this->request->post);
		$this->session->data['success'] = $this->language->get('text_success');
		$url = '';
		if (isset($this->request->get['sort'])) {
		$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
		$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
		$url .= '&page=' . $this->request->get['page'];
		}
		$this->response->redirect($this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

 public function delete() {
	$this->load->language('membership/plans');
	$this->document->setTitle($this->language->get('heading_title'));
	$this->load->model('membership/plans');
	if (isset($this->request->post['selected']) && $this->validateDelete()) {
		foreach ($this->request->post['selected'] as $plans_id) {
			$this->model_membership_plans->deletePlans($plans_id);
		}
		$this->session->data['success'] = $this->language->get('text_success');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->response->redirect($this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url, true));
	}
	$this->getList();
  }
  protected function getList() {
	if (isset($this->request->get['sort'])) {
	 $sort = $this->request->get['sort'];
	} else {
	 $sort = 'name';
	}

	if (isset($this->request->get['order'])) {
	 $order = $this->request->get['order'];
	} else {
	 $order = 'ASC';
	}
	if (isset($this->request->get['page'])) {
	 $page = $this->request->get['page'];
	} else {
	 $page = 1;
	}

	$url = '';

	if (isset($this->request->get['sort'])) {
		$url .= '&sort=' . $this->request->get['sort'];
	}
	if (isset($this->request->get['order'])) {
	$url .= '&order=' . $this->request->get['order'];
	}
	if (isset($this->request->get['page'])) {
	 $url .= '&page=' . $this->request->get['page'];
	}

	$data['breadcrumbs'] = array();
	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_home'),
		'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
	);
	$data['breadcrumbs'][] = array(
	'text' => $this->language->get('heading_title'),
	'href' => $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url, true)
	);
	
	$data['add'] = $this->url->link('membership/plans/add', 'token=' . $this->session->data['token'] . $url, true);
	$data['delete'] = $this->url->link('membership/plans/delete', 'token=' . $this->session->data['token'] . $url, true);
	$data['plansies'] = array();
	$filter_data = array(
	'sort'  => $sort,
	'order' => $order,
	'start' => ($page - 1) * $this->config->get('config_limit_admin'),
	'limit' => $this->config->get('config_limit_admin')
	);
	$this->load->model('tool/image');
	$plans_total = $this->model_membership_plans->getTotalPlansies();
	$results = $this->model_membership_plans->getPlansies($filter_data);
      foreach ($results as $result) {
		if (is_file(DIR_IMAGE . $result['image'])) {
			$image = $this->model_tool_image->resize($result['image'], 40, 40);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', 40, 40);
		}

		$data['plansies'][] = array(
		'plans_id' => $result['plans_id'],
		'name'        => $result['name'],
		'image'      => $image,
		'price'       => $this->currency->format($result['price'],$this->config->get('config_currency')),
		'sort_order'  => $result['property_limit'],
		'edit'        => $this->url->link('membership/plans/edit', 'token=' . $this->session->data['token'] . '&plans_id=' . $result['plans_id'] . $url, true),
		'delete'      => $this->url->link('membership/plans/delete', 'token=' . $this->session->data['token'] . '&plans_id=' . $result['plans_id'] . $url, true)
		);
	}

	$data['heading_title'] = $this->language->get('heading_title');
	$data['text_list'] = $this->language->get('text_list');
	$data['text_no_results'] = $this->language->get('text_no_results');
	$data['text_confirm'] = $this->language->get('text_confirm');
	$data['column_name'] = $this->language->get('column_name');
	$data['column_image'] = $this->language->get('column_image');
	$data['column_price'] = $this->language->get('column_price');
	$data['column_sort_order'] = $this->language->get('column_sort_order');
	$data['column_action'] = $this->language->get('column_action');
	$data['button_add'] = $this->language->get('button_add');
	$data['button_edit'] = $this->language->get('button_edit');
	$data['button_delete'] = $this->language->get('button_delete');
	$data['column_property_limit'] = $this->language->get('column_property_limit');

	if (isset($this->error['warning'])) {
	 $data['error_warning'] = $this->error['warning'];
	} else {
	 $data['error_warning'] = '';
	}

	if (isset($this->session->data['success'])) {
		$data['success'] = $this->session->data['success'];
		unset($this->session->data['success']);
	} else {
		$data['success'] = '';
	}
	
	if (isset($this->request->post['selected'])) {
	 $data['selected'] = (array)$this->request->post['selected'];
	} else {
	 $data['selected'] = array();
	}

	$url = '';
	if ($order == 'ASC') {
	 $url .= '&order=DESC';
	} else {
	 $url .= '&order=ASC';
	}
	
	if (isset($this->request->get['page'])) {
	 $url .= '&page=' . $this->request->get['page'];
	}
	$data['sort_name'] = $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
	$data['sort_price'] = $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . '&sort=price' . $url, true);
	$data['sort_property_limit'] = $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . '&sort=property_limit' . $url, true);
	$url = '';
	if (isset($this->request->get['sort'])) {
	 $url .= '&sort=' . $this->request->get['sort'];
	}
	if (isset($this->request->get['order'])) {
	 $url .= '&order=' . $this->request->get['order'];
	}
	$pagination = new Pagination();
	$pagination->total = $plans_total;
	$pagination->page = $page;
	$pagination->limit = $this->config->get('config_limit_admin');
	$pagination->url = $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
	$data['pagination'] = $pagination->render();
	$data['results'] = sprintf($this->language->get('text_pagination'), ($plans_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($plans_total - $this->config->get('config_limit_admin'))) ? $plans_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $plans_total, ceil($plans_total / $this->config->get('config_limit_admin')));
	$data['sort'] = $sort;
	$data['order'] = $order;
	$data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');
	$this->response->setOutput($this->load->view('membership/plans_list', $data));
}

  protected function getForm() {
	$data['heading_title'] = $this->language->get('heading_title');
	$data['text_form'] = !isset($this->request->get['plans_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
	$data['text_none'] = $this->language->get('text_none');
	$data['text_default'] = $this->language->get('text_default');
	$data['text_enabled'] = $this->language->get('text_enabled');
	$data['text_disabled'] = $this->language->get('text_disabled');
	$data['text_day'] = $this->language->get('text_day');
	$data['text_month'] = $this->language->get('text_month');
	$data['text_years'] = $this->language->get('text_years');
	$data['text_select'] = $this->language->get('text_select');
	$data['text_none'] = $this->language->get('text_none');
	$data['entry_name'] = $this->language->get('entry_name');
	$data['entry_description'] = $this->language->get('entry_description');
	$data['entry_price'] = $this->language->get('entry_price');
	$data['entry_image'] = $this->language->get('entry_image');
	$data['entry_status'] = $this->language->get('entry_status');
	$data['entry_text'] = $this->language->get('entry_text');
	$data['button_add'] = $this->language->get('button_add');
	$data['entry_title'] = $this->language->get('entry_title');
	$data['button_save'] = $this->language->get('button_save');
	$data['button_cancel'] = $this->language->get('button_cancel');
	$data['button_remove'] = $this->language->get('button_remove');
	$data['button_variation_add'] = $this->language->get('button_variation_add');
	$data['button_extraoption_add'] = $this->language->get('button_extraoption_add');
	$data['entry_packagelimit'] = $this->language->get('entry_packagelimit');
	$data['entry_noodday'] = $this->language->get('entry_noodday');
	$data['entry_top'] = $this->language->get('entry_top');
	$data['entry_features'] = $this->language->get('entry_features');
	$data['entry_latest'] = $this->language->get('entry_latest');
	$data['entry_bottom'] = $this->language->get('entry_bottom');

	
	$data['entry_product_limit'] = $this->language->get('entry_product_limit');
	
	
	
	$data['token'] = $this->session->data['token'];

	if (isset($this->error['warning'])) {
	$data['error_warning'] = $this->error['warning'];
	} else {
	$data['error_warning'] = '';
	}

	if (isset($this->error['name'])) {
	$data['error_name'] = $this->error['name'];
	} else {
	$data['error_name'] = array();
	}
	
	
	
	
	$url = '';
	if (isset($this->request->get['sort'])) {
		$url .= '&sort=' . $this->request->get['sort'];
	}
	if (isset($this->request->get['order'])) {
		$url .= '&order=' . $this->request->get['order'];
	}
	
	if (isset($this->request->get['page'])) {
	 $url .= '&page=' . $this->request->get['page'];
	}
	
	$data['breadcrumbs'] = array();
	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_home'),
		'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
	);
	
	$data['breadcrumbs'][] = array(
		'text' => $this->language->get('heading_title'),
		'href' => $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url, true)
	);
	
	if (!isset($this->request->get['plans_id'])) {
		$data['action'] = $this->url->link('membership/plans/add', 'token=' . $this->session->data['token'] . $url, true);
	} else {
		$data['action'] = $this->url->link('membership/plans/edit', 'token=' . $this->session->data['token'] . '&plans_id=' . $this->request->get['plans_id'] . $url, true);
	}
	$data['cancel'] = $this->url->link('membership/plans', 'token=' . $this->session->data['token'] . $url, true);
	if (isset($this->request->get['plans_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
		$plans_info = $this->model_membership_plans->getPlans($this->request->get['plans_id']);
	}
	$data['token'] = $this->session->data['token'];
	$this->load->model('localisation/language');
	$data['languages'] = $this->model_localisation_language->getLanguages();
	
	if (isset($this->request->post['plans_description'])) {
		$data['plans_description'] = $this->request->post['plans_description'];
	} elseif (isset($this->request->get['plans_id'])) {
		$data['plans_description'] = $this->model_membership_plans->getPlansDescriptions($this->request->get['plans_id']);
	} else {
		$data['plans_description'] = array();
	}
	if (isset($this->request->post['price'])) {
		$data['price'] = $this->request->post['price'];
	} elseif (!empty($plans_info)) {
		$data['price'] = $plans_info['price'];
	} else {
		$data['price'] = '';
	}

	if (isset($this->request->post['no_of_day'])) {
		$data['no_of_day'] = $this->request->post['no_of_day'];
	} elseif (!empty($plans_info)) {
		$data['no_of_day'] = $plans_info['no_of_day'];
	} else {
		$data['no_of_day'] = '';
	}
	

	if (isset($this->request->post['property_limit'])) {
		$data['property_limit'] = $this->request->post['property_limit'];
	} elseif (!empty($plans_info)) {
	$data['property_limit'] = $plans_info['property_limit'];
	} else {
		$data['property_limit'] = '';
	}

	if (isset($this->request->post['type'])) {
		$data['type'] = $this->request->post['type'];
	} elseif (!empty($plans_info)) {
		$data['type'] = $plans_info['type'];
	} else {
		$data['type'] = '';
	}
	if (isset($this->request->post['image'])) {
		$data['image'] = $this->request->post['image'];
	} elseif (!empty($plans_info)) {
		$data['image'] = $plans_info['image'];
	} else {
		$data['image'] = '';
	}
	$this->load->model('tool/image');
	if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
		$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
	} elseif (!empty($plans_info) && is_file(DIR_IMAGE . $plans_info['image'])) {
		$data['thumb'] = $this->model_tool_image->resize($plans_info['image'], 100, 100);
	} else {
		$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);

	}
	$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

	if (isset($this->request->post['status'])) {
		$data['status'] = $this->request->post['status'];
	} elseif (!empty($plans_info)) {
		$data['status'] = $plans_info['status'];
	} else {
		$data['status'] = true;
	}

	if (isset($this->request->post['top'])) {
		$data['top'] = $this->request->post['top'];
	} elseif (!empty($plans_info)) {
		$data['top'] = $plans_info['top'];
	} else {
		$data['top'] = '';
	}

	if (isset($this->request->post['bottom'])) {
		$data['bottom'] = $this->request->post['bottom'];
	} elseif (!empty($plans_info)) {
		$data['bottom'] = $plans_info['bottom'];
	} else {
		$data['bottom'] = '';
	}

	if (isset($this->request->post['feature'])) {
		$data['feature'] = $this->request->post['feature'];
	} elseif (!empty($plans_info)) {
		$data['feature'] = $plans_info['feature'];
	} else {
		$data['feature'] = '';
	}	


	

	$data['header'] = $this->load->controller('common/header');
	$data['column_left'] = $this->load->controller('common/column_left');
	$data['footer'] = $this->load->controller('common/footer');

	$this->response->setOutput($this->load->view('membership/plans_form', $data));
  }

	protected function validateForm() {
	if (!$this->user->hasPermission('modify', 'membership/plans')) {
		$this->error['warning'] = $this->language->get('error_permission');
	}
	foreach ($this->request->post['plans_description'] as $language_id => $value) {
		if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
			$this->error['name'][$language_id] = $this->language->get('error_name');
		}
	}

	if ($this->error && !isset($this->error['warning'])) {
	$this->error['warning'] = $this->language->get('error_warning');
	}
	return !$this->error;
  }
  
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'membership/plans')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;

	}
}


