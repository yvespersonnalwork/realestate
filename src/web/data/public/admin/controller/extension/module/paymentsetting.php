<?php
class ControllerExtensionModulePaymentSetting extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('extension/module/paymentsetting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('paymentsetting', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_authorization'] = $this->language->get('text_authorization');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_complete'] = $this->language->get('text_complete');
		$data['text_select'] = $this->language->get('text_select');
		$data['tab_orderstatus'] = $this->language->get('tab_orderstatus');

		$data['label_email'] = $this->language->get('label_email');
		$data['label_test'] = $this->language->get('label_test');
		$data['label_transaction'] = $this->language->get('label_transaction');
		$data['label_debug'] = $this->language->get('label_debug');
		$data['label_total'] = $this->language->get('label_total');
		$data['label_canceled_reversal_status'] = $this->language->get('label_canceled_reversal_status');
		$data['label_completed_status'] = $this->language->get('label_completed_status');
		$data['label_denied_status'] = $this->language->get('label_denied_status');
		$data['label_expired_status'] = $this->language->get('label_expired_status');
		$data['label_failed_status'] = $this->language->get('label_failed_status');
		$data['label_pending_status'] = $this->language->get('label_pending_status');
		$data['label_processed_status'] = $this->language->get('label_processed_status');
		$data['label_refunded_status'] = $this->language->get('label_refunded_status');
		$data['label_reversed_status'] = $this->language->get('label_reversed_status');
		$data['label_voided_status'] = $this->language->get('label_voided_status');
		$data['label_geo_zone'] = $this->language->get('label_geo_zone');
		$data['label_status'] = $this->language->get('label_status');
		$data['label_sort_order'] = $this->language->get('label_sort_order');
		$data['label_orderstatus'] = $this->language->get('label_orderstatus');

		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_transaction'] = $this->language->get('entry_transaction');
		$data['entry_debug'] = $this->language->get('entry_debug');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_canceled_reversal_status'] = $this->language->get('entry_canceled_reversal_status');
		$data['entry_completed_status'] = $this->language->get('entry_completed_status');
		$data['entry_denied_status'] = $this->language->get('entry_denied_status');
		$data['entry_expired_status'] = $this->language->get('entry_expired_status');
		$data['entry_failed_status'] = $this->language->get('entry_failed_status');
		$data['entry_pending_status'] = $this->language->get('entry_pending_status');
		$data['entry_processed_status'] = $this->language->get('entry_processed_status');
		$data['entry_refunded_status'] = $this->language->get('entry_refunded_status');
		$data['entry_reversed_status'] = $this->language->get('entry_reversed_status');
		$data['entry_voided_status'] = $this->language->get('entry_voided_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_orderstatus'] = $this->language->get('entry_orderstatus');

		$data['help_test'] = $this->language->get('help_test');
		$data['help_debug'] = $this->language->get('help_debug');
		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_order_status'] = $this->language->get('tab_order_status');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/paymentsetting', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/module/paymentsetting', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true);

		if (isset($this->request->post['paymentsetting_email'])) {
			$data['paymentsetting_email'] = $this->request->post['paymentsetting_email'];
		} else {
			$data['paymentsetting_email'] = $this->config->get('paymentsetting_email');
		}

		if (isset($this->request->post['paymentsetting_test'])) {
			$data['paymentsetting_test'] = $this->request->post['paymentsetting_test'];
		} else {
			$data['paymentsetting_test'] = $this->config->get('paymentsetting_test');
		}

		if (isset($this->request->post['paymentsetting_transaction'])) {
			$data['paymentsetting_transaction'] = $this->request->post['paymentsetting_transaction'];
		} else {
			$data['paymentsetting_transaction'] = $this->config->get('paymentsetting_transaction');
		}

		if (isset($this->request->post['paymentsetting_debug'])) {
			$data['paymentsetting_debug'] = $this->request->post['paymentsetting_debug'];
		} else {
			$data['paymentsetting_debug'] = $this->config->get('paymentsetting_debug');
		}

		if (isset($this->request->post['paymentsetting_total'])) {
			$data['paymentsetting_total'] = $this->request->post['paymentsetting_total'];
		} else {
			$data['paymentsetting_total'] = $this->config->get('paymentsetting_total');
		}

		if (isset($this->request->post['paymentsetting_canceled_reversal_status_id'])) {
			$data['paymentsetting_canceled_reversal_status_id'] = $this->request->post['paymentsetting_canceled_reversal_status_id'];
		} else {
			$data['paymentsetting_canceled_reversal_status_id'] = $this->config->get('paymentsetting_canceled_reversal_status_id');
		}

		if (isset($this->request->post['paymentsetting_completed_status_id'])) {
			$data['paymentsetting_completed_status_id'] = $this->request->post['paymentsetting_completed_status_id'];
		} else {
			$data['paymentsetting_completed_status_id'] = $this->config->get('paymentsetting_completed_status_id');
		}

		if (isset($this->request->post['paymentsetting_denied_status_id'])) {
			$data['paymentsetting_denied_status_id'] = $this->request->post['paymentsetting_denied_status_id'];
		} else {
			$data['paymentsetting_denied_status_id'] = $this->config->get('paymentsetting_denied_status_id');
		}

		if (isset($this->request->post['paymentsetting_expired_status_id'])) {
			$data['paymentsetting_expired_status_id'] = $this->request->post['paymentsetting_expired_status_id'];
		} else {
			$data['paymentsetting_expired_status_id'] = $this->config->get('paymentsetting_expired_status_id');
		}

		if (isset($this->request->post['paymentsetting_failed_status_id'])) {
			$data['paymentsetting_failed_status_id'] = $this->request->post['paymentsetting_failed_status_id'];
		} else {
			$data['paymentsetting_failed_status_id'] = $this->config->get('paymentsetting_failed_status_id');
		}

		if (isset($this->request->post['paymentsetting_pending_status_id'])) {
			$data['paymentsetting_pending_status_id'] = $this->request->post['paymentsetting_pending_status_id'];
		} else {
			$data['paymentsetting_pending_status_id'] = $this->config->get('paymentsetting_pending_status_id');
		}

		if (isset($this->request->post['paymentsetting_processed_status_id'])) {
			$data['paymentsetting_processed_status_id'] = $this->request->post['paymentsetting_processed_status_id'];
		} else {
			$data['paymentsetting_processed_status_id'] = $this->config->get('paymentsetting_processed_status_id');
		}

		if (isset($this->request->post['paymentsetting_refunded_status_id'])) {
			$data['paymentsetting_refunded_status_id'] = $this->request->post['paymentsetting_refunded_status_id'];
		} else {
			$data['paymentsetting_refunded_status_id'] = $this->config->get('paymentsetting_refunded_status_id');
		}

		if (isset($this->request->post['paymentsetting_reversed_status_id'])) {
			$data['paymentsetting_reversed_status_id'] = $this->request->post['paymentsetting_reversed_status_id'];
		} else {
			$data['paymentsetting_reversed_status_id'] = $this->config->get('paymentsetting_reversed_status_id');
		}

		if (isset($this->request->post['paymentsetting_voided_status_id'])) {
			$data['paymentsetting_voided_status_id'] = $this->request->post['paymentsetting_voided_status_id'];
		} else {
			$data['paymentsetting_voided_status_id'] = $this->config->get('paymentsetting_voided_status_id');
		}

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['paymentsetting_geo_zone_id'])) {
			$data['paymentsetting_geo_zone_id'] = $this->request->post['paymentsetting_geo_zone_id'];
		} else {
			$data['paymentsetting_geo_zone_id'] = $this->config->get('paymentsetting_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['paymentsetting_status'])) {
			$data['paymentsetting_status'] = $this->request->post['paymentsetting_status'];
		} else {
			$data['paymentsetting_status'] = $this->config->get('paymentsetting_status');
		}

		if (isset($this->request->post['paymentsetting_sort_order'])) {
			$data['paymentsetting_sort_order'] = $this->request->post['paymentsetting_sort_order'];
		} else {
			$data['paymentsetting_sort_order'] = $this->config->get('paymentsetting_sort_order');
		}

		if (isset($this->request->post['paymentsetting_completed_status_id'])) {
			$data['paymentsetting_completed_status_id'] = $this->request->post['paymentsetting_completed_status_id'];
		} else {
			$data['paymentsetting_completed_status_id'] = $this->config->get('paymentsetting_completed_status_id');
		}
		
		
		if (isset($this->request->post['paymentsetting_denied_status_id'])) {
			$data['paymentsetting_denied_status_id'] = $this->request->post['paymentsetting_denied_status_id'];
		} else {
			$data['paymentsetting_denied_status_id'] = $this->config->get('paymentsetting_denied_status_id');
		}
		
		if (isset($this->request->post['paymentsetting_expired_status_id'])) {
			$data['paymentsetting_expired_status_id'] = $this->request->post['paymentsetting_expired_status_id'];
		} else {
			$data['paymentsetting_expired_status_id'] = $this->config->get('paymentsetting_expired_status_id');
		}
		
		if (isset($this->request->post['paymentsetting_failed_status_id'])) {
			$data['paymentsetting_failed_status_id'] = $this->request->post['paymentsetting_failed_status_id'];
		} else {
			$data['paymentsetting_failed_status_id'] = $this->config->get('paymentsetting_failed_status_id');
		}
		if (isset($this->request->post['paymentsetting_pending_status_id'])) {
			$data['paymentsetting_pending_status_id'] = $this->request->post['paymentsetting_pending_status_id'];
		} else {
			$data['paymentsetting_pending_status_id'] = $this->config->get('paymentsetting_pending_status_id');
		}
		
		if (isset($this->request->post['paymentsetting_processed_status_id'])) {
			$data['paymentsetting_processed_status_id'] = $this->request->post['paymentsetting_processed_status_id'];
		} else {
			$data['paymentsetting_processed_status_id'] = $this->config->get('paymentsetting_processed_status_id');
		}
		
		
		if (isset($this->request->post['paymentsetting_refunded_status_id'])) {
			$data['paymentsetting_refunded_status_id'] = $this->request->post['paymentsetting_refunded_status_id'];
		} else {
			$data['paymentsetting_refunded_status_id'] = $this->config->get('paymentsetting_refunded_status_id');
		}
		
		if (isset($this->request->post['paymentsetting_reversed_status_id'])) {
			$data['paymentsetting_reversed_status_id'] = $this->request->post['paymentsetting_reversed_status_id'];
		} else {
			$data['paymentsetting_reversed_status_id'] = $this->config->get('paymentsetting_reversed_status_id');
		}
		
		
		if (isset($this->request->post['paymentsetting_voided_status_id'])) {
			$data['paymentsetting_voided_status_id'] = $this->request->post['paymentsetting_voided_status_id'];
		} else {
			$data['paymentsetting_voided_status_id'] = $this->config->get('paymentsetting_voided_status_id');
		}
		
		
		
		
		

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/paymentsetting', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/paymentsetting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['paymentsetting_email']) {
			$this->error['email'] = $this->language->get('error_email');
		}

		return !$this->error;
	}


}