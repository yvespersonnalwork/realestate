# Collections


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This repository is a refactoring of the already known collections project (previously called aneris). It will deploy collections modules as independent containers.


# Deployded containers 
  - Database
  - PHPMyAdmin
  - Crawler + webmin
  - Web (laravel)
  
 
![alt text](images/aneris_infra.PNG)


# Setup

- Get aneris repo from bitbucket
You may not be able to clone the repo if you don't have a registered ssh key !

```sh
$ git clone git@bitbucket.org:momentfactory/aneris.git aneris
$ cd aneris
$ chmod +x init.sh setup.sh start_all.sh stop_all.sh clean_all.sh
```
# Dev platform deployment
- Install & configure the container host  
This will download docker and docker-compose if there are not available
Install cifs and map 
```sh
$ ./init.sh

$ cp env-dev.ini .env

$ python setup.py --env dev
```
# Prod platform deployment

```sh
$ cp env-prod.ini .env

$ python setup.py --env prod
```
- Deploy the containers 
```sh
$ ./start_all.sh
```

# Configure Google Service Authentification
By default the plateforme use the same Google Console API key as the old collections.  
If you choose to keep it, you must edit your host file to redirect  
http://collections/ to your docker host  
```sh
$ YOUR_DOCKER_HOST_IP    collections
```
However you can create and use your own google console api key.  
1.  Go to https://console.developers.google.com/apis to create your key  

2. Edit app\extensions\google-api-php-client\src\config.php  
'oauth2_client_id'  
'oauth2_client_secret'  
'oauth2_redirect_uri'  

3. Edit app\controllers\LoginController.php  
CLIENT_ID  

4. Edit app\controllers\BaseController.php  
'CLIENT_ID'  
'APPLICATION_NAME'

- Deploy the containers 
```sh
$ ./start_all.sh
```
- How to connect to laravel ?  
As ,  
you must edit your host file to redirect http://collections/ to your docker host  
All the ports used by the aneris module are listed in env.ini file.


Want to contribute? Great!
.........